/**
 * tasks/reset-index.js
 */
const StudentModel = require('../db/students');

async function resetIndex() {
  try {
    // Eliminar el índice si existe

    console.log('Índice eliminado (si existía)');
    
    // Crear el mapping nuevo
    await new Promise((resolve, reject) => {
      StudentModel.createMapping((err, mapping) => {
        if (err) {
          reject(err);
        } else {
          resolve(mapping);
        }
      });
    });

    console.log('Nuevo mapping creado');
    
  } catch (error) {
    console.error('Error resetting index:', error);
    process.exit(1);
  }
}

resetIndex()
  .then(() => {
    console.log('Index reset completed');
    process.exit(0);
  })
  .catch(err => {
    console.error('Fatal error:', err);
    process.exit(1);
  });