/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')
const GroupsModel = require('../db/groups')
const Notification = require('../models/notifications')
const Notifications = new Notification()


// TEST NOTI 5feeb54839950d5aef054987
const init = async () => {
    const groupResult = await GroupsModel.find({
        $and: [{
                course: "5c02310a5d6f712db49d3103"
            },
            {
                startDate: {
                    $gte: new moment("2021-01-01", "YYYY-MM-DD"),
                    $lte: new moment("2022-12-12", "YYYY-MM-DD")
                }
            }
        ]
    })

    let lista = []

    for (let i = 0; i < groupResult.length; i++) {
        lista.push(groupResult[i]._id)
    }

    console.log(lista)

    const normalize = {}
    const body = {
        // sms:{
        //     message: "En Preparación IPN queremos desearte Feliz Navidad con este video sorpresa: https://youtu.be/5WQysEsAIeQ ¡Te acompañaremos hasta que cumplas tus metas!"
        // },
        email: {
            template: "d-a934c1e29ac94e4eb91061956d620114",
            templateData: {
                "subject": "¡Importante!",
                "message": "<p style='text-align: center;'><strong>&iexcl;Ya sali&oacute; la gu&iacute;a oficial del IPN! </strong></p> <p style='text-align: center;'>Queremos que seas de los primeros en estudiarla. Para m&aacute;s informaci&oacute;n, ve este video:</p> <p style='text-align: center;'><strong><a href='https://youtu.be/UcSqWAueGHE'> https://youtu.be/UcSqWAueGHE</a></strong></p><p style='text-align: center;'></p>"
            }
        },
        groups: lista
    }
    const result = await Notifications.sendGenericNotification(
        body,
        normalize
    )


}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)



        setTimeout(() => {
            process.exit(1)
        }, 500)
    })