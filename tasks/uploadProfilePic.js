/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/getFullDB.js
 *              ::
 */

 const axios = require('axios')


const fs = require('fs')
const path = require('path')
const ImagesModel = require('../db/images')
const mongoose = require('mongoose')

//const Images = require('../../../../models/images')
const s3lib = require('../lib/s3')
const moment = require('moment')

const { result } = require('underscore')


const init = async () => {
  // leer el archivo json

      // mandar a llamar la funcion que carga la imagen y guardar directamente en la base de datos la informacion
      
      let nombre = 'name.jpg'
      let path_image = 'images/' + nombre
      let extension = nombre.substring(nombre.length -3, nombre.length)
      nombre = nombre.substring(0, nombre.length -4)
      const stream = fs.readFileSync(path_image)

      
      const uploadImage = await s3lib.uploadImageTemp(nombre, stream, extension)

      console.log(uploadImage)
      const imageId = new mongoose.Types.ObjectId()
      llave = uploadImage.response.key.substring(4, uploadImage.response.key.length)
      const imageN = new ImagesModel({
          _id: imageId,
          url: uploadImage.response.Location,
          key: llave,
          isTempImage: true,
          type: 5
      })
      await imageN.save()
    
console.log(imageN)


// let valores = "https://ipn-images-dev.s3.amazonaws.com/tmp/6gq1270kz4w71uy.nothing?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAQUPC2WJLNYPE57W7%2F20220202%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220202T015005Z&X-Amz-Expires=3600&X-Amz-Signature=45ff61e6716b51180c4eb2db70c2e340b66e209975a1b9f4a4654d228d9bf4a7&X-Amz-SignedHeaders=host%3Bx-amz-acl&x-amz-acl=bucket-owner-full-control"

//       axios
//       .put(valores)
//       .then(res => {
//         console.log(`statusCode: ${res.statusCode}`)
//       })
//       .catch(error => {
//         console.error(error)
//       })



}


init()
  .then(() => {
    console.log('Finished !!')
    setTimeout(function () {
      process.exit(0)
    }, 500)
  })
  .catch(err => {
    console.error(err)
    setTimeout(() => {
      process.exit(1)
    }, 500)
  })