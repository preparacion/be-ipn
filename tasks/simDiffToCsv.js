/**
 * tasks/simDiffToCsv.js
 *
 * @description :: 
 * @docs        :: TODO
 *              ::
 */
const fs = require('fs')

const StudentModel = require('../db/students')
const StudentCounter = require('../lib/studentCounter')

const studentFields = [
  'name', 'lastName', 'secondLastName',
  'phoneNumber', 'secondPhoneNumber', 'email'
]

const dataFile = fs.createWriteStream('./tasks/diff.csv', { autoClose: true })

function write (data, cb) {
  if (!dataFile.write(data)) {
    dataFile.once('drain', cb)
  } else {
    process.nextTick(cb)
  }
}

const init = async () => {
  dataFile.write(studentFields.join(',') + '\n')
  const stIds = await StudentCounter
    .getDiffSimulacro()
  console.log(stIds.length)
  await Promise
    .all(stIds.map(id => {
      return writeStudent(id)
    }))
    .then(() => {
      dataFile.end()
    })
}

const writeStudent = async studentId => {
  const student = await StudentModel
    .findOne({ _id: studentId })
    .select(studentFields.join(' '))
  if (!student) {
    console.log('no se encontró ' + studentId)
    return false
  }

  const data = student.name + ',' +
    student.lastName + ',' +
    student.secondLastName + ',' +
    student.phoneNumber + ',' +
    student.secondPhoneNumber + ',' +
    student.email + '\n'

  write(data, () => {
    // console.log('se escribió ' + student.email)
  })
}

Promise.resolve(init())
  .then(result => {
    // process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
