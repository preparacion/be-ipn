/**
 * tasks/fixGroups.js
 *
 * @description :: Adds schedules to groups
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')

const GroupsModel = require('../db/groups')
const CoursesModel = require('../db/courses')
const SchedulesModel = require('../db/schedules')

const init = async () => {
  const cursor = GroupsModel
    .find({})
    .cursor()
  
  let doc = await cursor.next()

  do {
    if (doc.schedule) {
      const schedule = await SchedulesModel
        .findOne({ _id: doc.schedule })
        .select('schedulesList')

      const list = schedule.schedulesList.map(item => {
        return {
          day: item.day,
          startHour: item.startHour,
          endHour: item.endHour
        }
      })
      doc.schedules = list

      // if (!doc.endDate) {
        const course = await CoursesModel
          .findOne({ _id: doc.course })
          .select('duration')
        if (course.duration) {
          const endDate = moment
            .utc(doc.startDate)
            .add(parseInt(course.duration), 'days')
          doc.endDate = endDate.format()
        }
      // }
      await doc.save()
    }
    doc = await cursor.next()
  } while (doc !== null)
}

init()
  .then(() => {
    console.log('Finished !!')
    setTimeout(function () {
      process.exit(0)
    }, 500)
  })
  .catch(err => {
    console.error(err)
    setTimeout(()=> {
      process.exit(1)
    }, 500)
  })