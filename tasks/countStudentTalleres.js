const moment = require('moment')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const PaymentsModel = require('../db/payments')
const fs = require('fs');
const _ = require('lodash');

const dataFile = fs.createWriteStream('./tasks/tallerBiologia.csv', { autoClose: true })

const write = (data, cb) => {
    if (!dataFile.write(data)) {
        dataFile.once('drain', cb)
    } else {
        process.nextTick(cb)
    }
}

const writeStudent = stData => {
    const data = stData.name + ',' +
        stData.lastName + ',' +
        stData.secondLastName + ',' +
        stData.phoneNumber + ',' +
        stData.secondPhoneNumber + ',' +
        stData.email + '\n'

    write(data, () => {
        // console.log('se escribió ' + stData.email)
    })
}

const init = async () => {
    const courseId = "5c02310a5d6f712db49d3103";
    // const tallerId = "5e683d6a8cf4aa58c1ed454b"; // Quimica
    // const tallerId = "5e3cc76c597884573afad7eb"; // Español
    // const tallerId = "5e683d3f8cf4aa58c1ed4548"; // Fisica
    const tallerId = "5e3f02b6d7c876575a35b498"; // Biología
    const groups = await GroupsModel
        .find({
            course: courseId,
            startDate: { $gte: new Date("12-12-2020") }
        })
        .populate({
            path: "studentList",
            populate: {
                path: "list"
            }
        })
        .lean();
    const tallerGroups = await GroupsModel
        .find({
            course: tallerId,
            startDate: { $gte: new Date("12-12-2020") }
        })
        .lean();
    const tallerIds = []
    const studentsArray = []
    groups.forEach(item => {
        studentsArray.push(...item.studentList.list)
    });
    tallerGroups.forEach(item => {
        tallerIds.push(item._id);
    });
    let contadorSi = 0;
    let contadorNo = 0;
    const studentNoTaller = []
    studentsArray.forEach(student => {
        if (student.groups && student.groups.length > 0) {
            const result = _.intersectionWith(student.groups, tallerIds, _.isEqual);
            if (result.length > 0) {
                contadorSi++
            } else {
                contadorNo++;
                studentNoTaller.push(student);
            }
        }
    })
    console.log("si:", contadorSi);
    console.log("no:", contadorNo);
    studentNoTaller.forEach(st => {
        writeStudent(st)
    })
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })