/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const VideosModel = require('../db/videos')
const fetch = require('node-fetch');
const init = async () => {
    const videos = await VideosModel
        .find({
            statusTranscode: "PROCESSING"
        })
    console.log("GROUPS:", videos.length)
    for (let i = 0; i < videos.length; i++) {
        console.log(videos[i])
        if (videos[i].url !== undefined) {
            await VideosModel.findOneAndUpdate({
                _id: videos[i]._id
            },
                {
                    $set: { statusTranscode: "COMPLETE" }
                })
        } else {
            console.log(i)
            let body = {
                s3: {
                    bucket: {
                        name: "ipn-videos",
                        arn: "arn:aws:s3:::ipn-videos"
                    },
                    object: {
                        key: videos[i].transcodeId
                    }
                }
            }
            await fetch('https://32ydrbedii.execute-api.us-east-1.amazonaws.com/default/transcoding', {
                method: 'POST',
                body: JSON.stringify(body),
                headers: { "Content-Type": "application/json" }
            })
        }
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })