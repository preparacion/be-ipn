/**
 *
 * @description :: Generate the progress indicator of playlist
 * @docs        :: How to run: node tasks/generateProgressPlaylist.js
 *              ::
 */
const VideosModel = require('../db/videos')
const PlaylistModel = require('../db/playlists')
const PlaylistProgress = require('../db/studentProgress')
const studentId = "5c02317161a2ca2decfd3fee"
const init = async () => {
    console.log("INIT")
    //insert the courseID
    await calculateTotalItems("5dc65abeddb6e2178da83448")
}

const calculateProgress = async (playlist, finishItems) => {
    console.log("calculateProgress")
    let result = await PlaylistModel.findOne({ _id: playlist })
    result = await result.toPopulate()
    let itemsTotal = result.contentPlaylist.length
    let finishedCount = finishItems.length
    let percentage = (finishedCount * 100) / itemsTotal
    if (percentage > 100)
        percentage = 100
    return {
        progress: percentage,
        items: itemsTotal
    }
}

calculateTotalItems = async (courseId) => {
    let mainParents = await PlaylistModel.find({ course: courseId })
    let contador = 0
    for (let i = 0; i < mainParents.length; i++) {
        let cuenta = await recursiveCounting(mainParents[i]._id)
        console.log("CUENTA PARENT:",cuenta)
        mainParents[i].totalItems = cuenta
        mainParents[i].save()
        contador = contador + cuenta
    }
    console.log("CUENTA DEL CURSO: ", contador)
}

recursiveCounting = async (playlistId) => {
    let playlist = await PlaylistModel.findOne({ _id: playlistId })
    playlist = await playlist.toPopulate()
    let insideItems = playlist.contentPlaylist
    let contador = 0
    for (let i = 0; i < insideItems.length; i++) {
        if (insideItems[i].type == 1) {
            let contenido = await recursiveCounting(insideItems[i]._id)
            contador = contador + contenido
        } else {
            contador += 1
        }
    }
    console.log("CUENTA DE LA PLAYLIST: ", contador)
    playlist.totalItems = contador
    playlist.save()
    return contador
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })