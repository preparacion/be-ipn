/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const PaymentsModel = require('../db/payments')


const init = async () => {

//     var date = new Date('January 18, 2023 02:30:00');
//     var date2 = new Date('December 18, 2021 02:30:00');
    
//     var fecha = new Date('June 01, 2022 02:30:00');


//     const courseId = "60c7f3f6cd8f1c5b61908190"
//     const comparator = 500
//     const groups = await GroupsModel
//         .find({ $and: [ { course: courseId },  { startDate: {$gte: fecha }  } ] })
//         .populate({
//             path : 'studentList',
//             populate : {
//               path : 'list'
//             }})
//         .populate('course')



//     let suma=0
//     let alumnos = 0
//     for (let i = 0; i < groups.length; i++) {
//         suma = suma + groups[i].studentList.list.length
//         for (let j = 0; j <  groups[i].studentList.list.length; j++) {
//             if((groups[i].studentList.list[j].registerDate < date) ){
//             if( (groups[i].studentList.list[j].registerDate > date2 )){
               
//                 alumnos = alumnos+1
//             }
//             }
//         }
        

//     }

// console.log("Curso: ", groups[0].course.name)
// console.log("Total Grupos:", groups.length)
// console.log("TOTAL EN EL CURSO: ", suma)
// console.log("Alumnos inscritos hasta hace un año: ", alumnos)
    console.log("ENTRA CORRECTAMENTE");
    const comparator = 500
    const courseId = "5f640dec2d282e5a8b27d0dd"
    const groups = ['60c7f3f6cd8f1c5b61908190'];
    const students = await StudentModel
        .find({
            registerDate:
                { $gte: new moment("2021-10-01", "YYYY-MM-DD"), $lte: new moment("2023-04-30", "YYYY-MM-DD") },
            groups : {$in:groups}
        })
    console.log("StudentLenght:", students.length)

    let contador = 0
    for (let i = 0; i < students.length; i++) {

        console.log(students[i].email)

        //students.email
        // console.log("STUDENT:", i)
        // let actual = students[i]
        // let payments = await PaymentsModel
        //     .find({
        //         course: courseId,
        //         student: actual._id
        //     })
        // let cantidad = 0
        // if (payments !== undefined) {
        //     for (let j = 0; j < payments.length; j++) {
        //         cantidad += (payments[j].amount + payments[j].discount)
        //     }
        // }
        // if (cantidad < comparator)
        //     contador++
    }
    // console.log("Conteo final:", contador)
    // const studentMatch = await students.map((item) => {
    //     groups.map((itemgroup) => {
    //         if (item.groups && item.groups.indexOf(itemgroup._id)>-1) {
    //             contador++
    //             return item
    //         }
    //     })
    // })
    // console.log(contador)
    // console.log(studentMatch.length)
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })