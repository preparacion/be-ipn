/**
 * tasks/fixGroups.js
 *
 * @description :: Adds schedules to groups
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')

const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const CoursesModel = require('../db/courses')
const SchedulesModel = require('../db/schedules')

const init = async () => {
    const student = await StudentModel
        .find({ registerDate: { $gte: new Date("2019-01-01T00:00:00.000Z") } })
        .select('groups')
        .populate({
            path: "groups",
            populate: {
                path: "course"
            }
        })
    console.log(student)

    for (let i = 0; i < student.length; i++) {
        const current = student[i]
        let actual = []
        let newArray = []
        let hasNull = false;
        for (let j = 0; j < current.groups.length; j++) {
            actual.push(current.groups[j]._id)
            const course = current.groups[j].course
            if (course == null || course == undefined) {
                console.log(course);
                hasNull = true;
            } else {
                newArray.push(current.groups[j]._id)
            }
        }
        if (hasNull) {
            //then create new array update it
            const resultUodate = await StudentModel.updateOne({
                _id: current._id,
            }, {
                $set: {
                    groups: newArray
                }
            }, {
                upsert: false // just in case
            })
        }
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })