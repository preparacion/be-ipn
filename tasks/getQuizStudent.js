/**
 * tasks/studentsCount.js
 *
 * @description :: Get studeents by Attempt id evaluation
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment');
const fs = require('fs');
const StudentModel = require('../db/students')
const QuizAttemptModel = require('../db/quizAttempt')
const PaymentModelModel = require('../models/payments')

const studentFields = [
    'Nombre', 'Apellido', 'Segundo Apellido',
    'Telefono', 'Segundo telefono', 'email', "Fecha de realización", 'Calificación', "Pagos y conceptos",
]

// const courseId = [
//     "5f1220ca92a1af5a6285f8c7",//paquete 1
//     "5f1220dbe14d795a78b2f28f",//paquete 2
//     "5f1220ecf66c6a5a77c8f743",//paquete 3
//     "5f0b455332dae25a5a1a3b84",//paquete premium
//     "5dc65abeddb6e2178da83448"//paquete presencial
// ]
const courseId = [
    "5c02310a5d6f712db49d3103",//Superior
    "5f12209f517c645a924ff525",//paquete 1
    "5f12209f517c645a924ff525",//paquete 2
    "5c02310a5d6f712db49d3103",//paquete 3
    "5f0b3dbf351fdb5a70f04b2a",//paquete premium
]

function write(data, cb, dataFile) {
    if (!dataFile.write(data)) {
        dataFile.once('drain', cb)
    } else {
        process.nextTick(cb)
    }
}

const init = async () => {
    const quizId = "5f36d2de0fa8985a82123f27"
    const comparator = 101
    const dataFile = fs.createWriteStream('./tasks/' + quizId + '.csv', { autoClose: true })
    const attempt = await QuizAttemptModel
        .find({
            quizParent: quizId,
            percentageResult: {
                $lte: comparator
            }
        })
        .limit(7000)
        .sort({ percentageResult: 1 })
        .populate({
            path: "student",
            populate: {
                path: "groups"
            }
        })
    dataFile.write(studentFields.join(',') + '\n')
    for (let i = 0; i < attempt.length; i++) {
        const student = attempt[i].student
        if (student !== null && student !== undefined) {
            const payments = await PaymentModelModel.getByStudentId(student._id)
            let provi = payments.payments.filter((value, index) => {
                if (courseId.findIndex(item => String(item) == String(value.course)) >= 0)
                    return value
            })
            provi = groupBy(provi)
            let cousesString
            let paymentsString
            Object.keys(provi).map(function (key, index) {
                let currentItem = provi[key]
                let TotalPayment = 0
                for (let x = 0; x < currentItem.length; x++) {
                    TotalPayment += (currentItem[x].amount + currentItem[x].discount)
                }
                if (paymentsString !== undefined)
                    paymentsString = paymentsString + currentItem[0].concept + " pago:$" + TotalPayment + " "
                else
                    paymentsString = currentItem[0].concept + " pago:$" + TotalPayment + " "
            });
            const data = student.name + ',' +
                student.lastName + ',' +
                student.secondLastName + ',' +
                student.phoneNumber + ',' +
                student.secondPhoneNumber + ',' +
                student.email + ',' +
                moment(attempt[i].date).format("DD/MM/YYYY HH:mm A") + ',' +
                attempt[i].percentageResult + ',' +
                paymentsString + '\n'
            write(data, () => { }, dataFile)
        }
    }
}

//this will grup the payments by course Id 
const groupBy = array => {
    return array.reduce((result, currentValue) => {
        (result[currentValue["course"]] = result[currentValue["course"]] || []).push(
            currentValue
        );
        return result;
    }, {});
}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })