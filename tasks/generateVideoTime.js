/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const fs = require("fs").promises;
const VideosModel = require('../db/videos')

const buff = Buffer.alloc(100);
const header = Buffer.from("mvhd");

//FAIL FUNCTION NOT WORKING 
const init = async () => {
    const videos = await VideosModel
        .find({
            statusTranscode: "COMPLETE"
        })
    console.log("GROUPS:", videos.length)
    for (let i = 0; i < videos.length; i++) {
        const file = await fs.open(videos[i].url, "r");
        const { buffer } = await file.read(buff, 0, 100, 0);
        await file.close();
        const start = buffer.indexOf(header) + 17;
        const timeScale = buffer.readUInt32BE(start);
        const duration = buffer.readUInt32BE(start + 4);

        const audioLength = Math.floor((duration / timeScale) * 1000) / 1000;

        console.log(buffer, header, start, timeScale, duration, audioLength);
    }
}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })