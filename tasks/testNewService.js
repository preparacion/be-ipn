/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')

const GroupsModel = require('../models/groups')

const init = async () => {
    const id = "5f46b3c4f0e56a5a70c97b49"
    const result = await GroupsModel.getSidebarInfoById(id)
    console.log(result)
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })