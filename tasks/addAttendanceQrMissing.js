
const LogsModel = require('../db/logs')
const StudentModel = require('../db/students')
const AttendanceStudentModel = require('../db/attendanceStudent')
const moment = require('moment')


const days = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo']

const init = async () => {
    const requestLogs = await LogsModel.find({
        $text: { $search: "qr" }
    }).lean()
    for (let i = 0; i < requestLogs.length; i++) {
        const value = requestLogs[i];
        if (value.method === "PUT") {
            const studentId = value.url.replace("/api/attendance/qr/", "")
            if (studentId.length === 24) {
                const date = value.date;
                await studentFunction(date, studentId)
            }
        }
    }
}

const studentFunction = async (dateTaked, id) => {
    const student = await StudentModel
        .findOne({ _id: id })
        .populate({
            path: 'groups',
            populate: {
                path: "course"
            }
        })
        .populate({
            path: 'groups',
            populate: {
                path: "classRoom",
                populate: {
                    path: "location"
                }
            }
        })
        .lean()
    if (!student) {
        return;
    }

    const groups = student.groups
    if (groups && groups.length >= 0) {
        for (let i = 0; i < groups.length; i++) {
            moment.locale('es');
            let now = moment(dateTaked)
            let startDate = moment(groups[i].startDate)
            startDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
            let endDate = moment(groups[i].endDate)
            if (startDate <= now && now <= endDate) {
                let dayToday = now.format('dddd');
                if (groups[i].schedules) {
                    let result
                    let resultGroup
                    for (let j = 0; j < groups[i].schedules.length; j++) {
                        if (dayToday == days[groups[i].schedules[j].day]) {
                            result = groups[i].schedules[j]
                            resultGroup = groups[i]
                            break
                        }
                    }
                    if (result) {
                        const dateString = moment(dateTaked).format("YYYY-MM-DD").toString()
                        let startHour = moment(dateString + " " + result.startHour, 'YYYY-MM-DD hhmm').subtract(60, 'minutes');
                        let endHour = moment(dateString + " " + result.endHour, 'YYYY-MM-DD hhmm').add(60, 'minutes');
                        if (now >= startHour && now <= endHour) {
                            console.log("ya es hora de tu clase")
                            const dataAttendance = {
                                groupId: resultGroup._id,
                                studentId: student._id,
                                attendance: true,
                                date: dateTaked
                            }
                            await attendanceFunction(dataAttendance)
                            return {
                                success: true,
                                student,
                                group: resultGroup
                            }
                        } else {
                            return {
                                success: false,
                                error: "No puedes pasar asistencia para este grupo, es a partir de las: " + moment(startHour).format("HH:mm A") + " y hasta las: " + moment(endHour).format("HH:mm A"),
                                code: 406
                            }
                        }
                    }
                }
            }
        }
    }
}

const attendanceFunction = async (data) => {
    const groupId = data.groupId;
    const studentId = data.studentId;
    const attendanceValue = data.attendance;
    const dateAttendance = data.date;

    const nowDate = moment(dateAttendance).toDate()
    let nowDateCero = moment(nowDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    let attendanceStudent = await AttendanceStudentModel
        .findOne({
            student: studentId,
            attendance: {
                $elemMatch: {
                    group: groupId,
                    date: { $gte: nowDateCero }
                }
            }
        })
    if (attendanceStudent !== undefined && attendanceStudent !== null) {
        const indexAttendance = attendanceStudent.attendance.findIndex(element => moment(element.date).isSameOrAfter(nowDateCero) && String(element.group) === String(groupId))
        if (indexAttendance >= 0) {
            attendanceStudent.attendance[indexAttendance].attendance = attendanceValue
            await attendanceStudent.save()
        }
    } else {
        await AttendanceStudentModel.findOneAndUpdate({
            student: studentId
        }, {
            $addToSet: {
                attendance: {
                    group: groupId,
                    date: dateAttendance,
                    attendance: attendanceValue
                }
            }
        }, {
            upsert: true,
            new: true
        })
    }
    return {
        success: true,
        studentId: studentId,
        attendance: attendanceValue
    }
}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })