/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */


const fs = require('fs')



const moment = require('moment')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const PaymentsModel = require('../db/payments')


const studentFields = [
    'Nombre', 'Apellido Paterno', 'Apellido materno',
    'Telefono 1', 'Telefono 2', 'email'
]


const dataFile = fs.createWriteStream('./students.csv', { autoClose: true })

function write (data, cb) {
    if (!dataFile.write(data)) {
      dataFile.once('drain', cb)
    } else {
      process.nextTick(cb)
    }
  }


const init = async () => {
    const courseId = "5f9057ee978cf95a916af2ca"
    const arrayCourses = ["5fa7740c70b27e5ab3e912d4", "5f0b3dbf351fdb5a70f04b2a", "5c02310a5d6f712db49d3103", "5dc65abeddb6e2178da83448", "5f0b455332dae25a5a1a3b84", "5fa773f04b83895aafe0c1f8"]

    dataFile.write(studentFields.join(',') + '\n')

    const groups = await GroupsModel
        .find({
            course: courseId
        })
        .populate({
            path: "studentList",
            populate: {
                path: "list",
                populate: "groups",
                select: "name lastName secondLastName email phoneNumber secondPhoneNumber groups"
            }
        })
    const group = groups[1];
    const students = group.studentList.list;

    for (let i = 0; i < students.length; i++) {
        console.log("STUDENT:", i)
        const student = students[i];
        let flagExit = false;
        student.groups.forEach(element => {
            let index = arrayCourses.findIndex(item => item === String(element.course));
            if (!flagExit && index >= 0) {
                if (moment(element.startDate).isBefore(moment("2020-12-31"))) {
                    console.log("is before")
                } else {
                    flagExit = true;
                }
            }
        });
        if (!flagExit) {
            writeStudent(student)
        }
    }
}


const writeStudent = async student => {
    const data = student.name + ',' +
        student.lastName + ',' +
        student.secondLastName + ',' +
        student.phoneNumber + ',' +
        student.secondPhoneNumber + ',' +
        student.email + '\n'
    write(data, () => {
        // console.log('se escribió ' + student.email)
    })
}



init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })