/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const StudentsModel = require('../db/students')
const QuizModel = require('../db/quiz')
const QuizAttemptModel = require('../db/quizAttempt')
const moment = require('moment')
const fetch = require('node-fetch');
const init = async () => {
    const attemps = await QuizAttemptModel.find({
        student: "5e5450dce4612f58cdfb6549"
    })
    .populate("quizParent")
    console.log(attemps)

    for (let i = 0; i < attemps.length; i++) {
        const current = attemps[i]
        console.log(`Fecha ${moment(current.date).format("DD/MM/YYYY HH:mm A")} Calificación:${current.percentageResult?current.percentageResult:"Sin terminar"} Nombre del quiz:${current.quizParent?current.quizParent.name:""}`)
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })