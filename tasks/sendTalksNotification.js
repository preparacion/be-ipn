/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')
const TalksModel = require('../db/talks')
const Notification = require('../models/notifications')
const Notifications = new Notification()

const init = async () => {
    const talksResult = await TalksModel.find({
        date: { $gte: new moment("2021-08-01", "YYYY-MM-DD") }
    })

    const normalize = {}
    for (let i = 0; i < talksResult.length; i++) {
        const body = {
            id: talksResult[i]._id,
            email: {
                message: "Información sobre tu admisión",
                template: "d-a934c1e29ac94e4eb91061956d620114",
                templateData: {
                    subject: "Información proceso de admisión",
                    message: `<h2 style='color: #5e9ca0; text-align: center;'><span style='color: #000000;'>&iexcl;Hola! </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>Tenemos una noticia muy importante para ti que presentar&aacute;s examen de admisi&oacute;n este 2022. </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>Ve este video para enterarte de todos los detalles: </span></h2>
                    <h2 style='color: #5e9ca0; text-align: center;'><span style='color: #000000;' role='gridcell'><a class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h f1sip0of lzcic4wl gpro0wi8 oo483o9r gmql0nx0' style='color: #000000;' tabindex='-1' role='link' href='https://youtu.be/Gwrc4vpxH-4' target='_blank' rel='nofollow noopener'>https://youtu.be/Gwrc4vpxH-4</a> </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>Recuerda que ya falta muy poco tiempo para tu examen, es crucial practicar y repasar todos los temas en estos d&iacute;as. </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>&iexcl;Aprovecha que con nuestros ex&aacute;menes simulacro podr&aacute;s hacerlo! </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>Te ense&ntilde;aremos tips y truquitos que podr&aacute;s aplicar en las preguntas de tu examen real. <span class='pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu'><img src='https://static.xx.fbcdn.net/images/emoji.php/v9/tf6/2/16/1f609.png' alt='😉' width='16' height='16' /></span> </span></h2>
                    <h2 style='color: #5e9ca0;'><span style='color: #000000;'>&iexcl;Te deseamos mucho &eacute;xito futuro polit&eacute;cnico!</span></h2>`
                }
            }
        }
        const result = await Notifications.sendToTalkGroup(
            body,
            normalize
        )
        console.log("Enviado el :", i)
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })