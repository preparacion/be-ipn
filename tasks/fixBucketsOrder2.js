/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */


const fs = require('fs')



const moment = require('moment')
const Utils = require('../lib/Utils')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const StudentListModel = require('../db/studentLists')
const Groups = require('../models/groups')


const studentFields = [
    'Nombre', 'Apellido Paterno', 'Apellido materno',
    'Telefono 1', 'Telefono 2', 'email'
]


const dataFile = fs.createWriteStream('./students.csv', { autoClose: true })

function write(data, cb) {
    if (!dataFile.write(data)) {
        dataFile.once('drain', cb)
    } else {
        process.nextTick(cb)
    }
}


const init = async () => {
    const courseId = "5dc65abeddb6e2178da83448"
    dataFile.write(studentFields.join(',') + '\n')
    const groups = await GroupsModel
        .find({
            course: courseId,
            startDate: { $gte: moment("2020-12-31") }
        })
        .populate({
            path: "studentList",
            populate: {
                path: "list",
                populate: "groups",
                select: "name lastName secondLastName email phoneNumber secondPhoneNumber groups"
            }
        })
        .populate({
            path: "buckets",
            populate: {
                path: "studentList",
                populate: {
                    path: "list",
                    populate: "groups",
                    select: "name lastName secondLastName email phoneNumber secondPhoneNumber groups"
                }
            }
        })
        .lean();



    const studentsArray = []
    groups.forEach(element => {
        if (element.studentList && element.studentList.list.length > 0) {
            const actualStudentList = element.studentList.list;
            actualStudentList.forEach(student => {
                studentsArray.push(student);
            });
        }
    });

    const duplicateResult = studentsArray.filter((item) => {
        let index = studentsArray.indexOf(iternalItem => String(iternalItem._id) === String(item._id));
        return index >= 0;
    })


    const studentsObject = {}

    groups.forEach(element => {
        if (element.buckets && element.buckets.length > 0) {
            element.buckets.forEach(iternalBucket => {
                if (iternalBucket.studentList && iternalBucket.studentList.list.length > 0) {
                    const studentListBucket = iternalBucket.studentList.list;
                    studentListBucket.forEach(student => {
                        if (studentsObject[student._id] !== undefined) {
                            studentsObject[student._id].push(iternalBucket);
                        } else {
                            studentsObject[student._id] = [iternalBucket];
                        }
                    })
                }
            })
        }
    });

    const minus = [];
    const equal = [];
    const more = [];

    const objectKey = Object.entries(studentsObject)
    objectKey.forEach(iterateElement => {
        if (iterateElement[1].length > 4) {
            more.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        } else if (iterateElement[1].length < 4) {
            console.log(iterateElement[1])
            console.log("SSSSSSSSSSSSDDDDDDDDDDD")
            minus.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        } else {
            equal.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        }
    })

    console.log("Duplicados en parent", duplicateResult)
    console.log("Numero de estudiantes totales", studentsArray.length)

    console.log("Alumnos con 4 buckets: ", equal.length);
    console.log("Alumnos con menos de 4 buckets: ", minus.length);
    console.log("Alumnos con mas de 4 buckets: ", more.length);

    for (let i = 0; equal.length > i; i++) {
        const student = await StudentModel.findById({
            _id: equal[i]._id
        }).populate({
            path: "groups",
            populate: {
                path: "course"
            }
        })

        let buckets = equal[i].buckets;
        buckets = buckets.sort((a, b) => {
            return a.comipemsPhase - b.comipemsPhase;
        })

        let newOrder = [];
        buckets.forEach(item => {
            newOrder.push(item._id)
        })
        console.log(newOrder)
        student.comipemsOrder = newOrder;
        await student.save();
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })