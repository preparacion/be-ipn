/**
 * tasks/sendDiffNotification.js
 *
 * @description :: 
 * @docs        :: TODO
 *              ::
 */
const rp = require('request-promise')

const StudentCounter = require('../lib/studentCounter')
const SendGrid = require('../lib/sendGrid')
const SMS = require('../lib/sms')

const SmsModel = require('../db/sms')
const StudentModel = require('../db/students')

const { sendGridUri, sendGridToken } = require('../config')

const templateId = 'd-a934c1e29ac94e4eb91061956d620114'

const testStudents = [
  '5cd3985bf58edb060db0679d',
  '5cd39e99f58edb060db067d5',
  '5cd3a023f58edb060db067fe',
  '5cd3a520f58edb060db0680f'
]

const message = 'El día de ayer ocurrió un error en uno de nuestros servicios y se enviaron notificaciones a alumnos del curso COMIPEMS de manera errónea , si eres uno de ellos te pedimos que hagas caso omiso a este ya que es destinado a alumnos de nivel superior , lamentamos la molestia. Te mantendremos al tanto por este mismo medio.'

const studentFields = [
  'name', 'lastName', 'secondLastName',
  'phoneNumber', 'secondPhoneNumber', 'email'
]

const init = async () => {
  const students = await await StudentCounter
    .getDiffSimulacro()

  const result = await Promise
    .all(testStudents.map(id => {
      return StudentModel
        .findOne({ _id: id })
        .select(studentFields.join(' '))
    }))
  console.log(result.length)
  // console.log(result.slice(0, 3))
  console.log(sendGridUri, sendGridToken)
  await Promise
    .all(result.map(stData => {
      return sendNotification(stData)
    }))
}

const sendNotification = async (studentData) => {
  let phoneNumber
  const sms = new SMS()

  if (studentData && studentData['phoneNumber'] && studentData['phoneNumber'].length === 10) {
    phoneNumber = studentData['phoneNumber']
  } else if (studentData && studentData['secondPhoneNumber'] && studentData['secondPhoneNumber'].length === 10) {
    phoneNumber = studentData['secondPhoneNumber']
  }

  if (phoneNumber) {
    phoneNumber = `+521${phoneNumber}`
    await sms.sendMessage(
      'prepIPN',
      phoneNumber,
      'custom message',
      message
    )

    const smsToStudent = new SmsModel({
      message: message,
      date: new Date(),
      student: studentData._id
    })
    await smsToStudent.save()
  }

  if (studentData && studentData.email) {
    await sendMail(studentData.email, message)
  }
}

const sendMail = async (to, message) => {
  const options = {
    method: 'POST',
    uri: sendGridUri,
    body: {
      from: {
        email: 'no-reply@email.preparacionipn.mx',
        name: 'Preparación IPN'
      },
      content: [{
        type: 'text/html',
        value: 'Invoice'
      }],
      personalizations: [{
        to: [{
          email: to
        }],
        dynamic_template_data: {
          message: message
        }
      }],
      template_id: templateId
    },
    headers: {
      Authorization: `Bearer ${sendGridToken}`
    },
    json: true
  }

  const result = await rp(options)
    .then(response => {
      console.log(`Mensaje enviado a ${to}`)
      return { success: true }
    })
    .catch(err => {
      console.error(err)
      return {
        success: false,
        code: 500,
        error: JSON.stringify(err, null, 2)
      }
    })
  return result
}

Promise.resolve(init())
  .then(result => {
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
