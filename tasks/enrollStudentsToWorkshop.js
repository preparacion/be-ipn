const moment = require('moment')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const PaymentsModel = require('../db/payments')
const StudentLists = require('../db/studentLists');
const fs = require('fs');
const _ = require('lodash');


const init = async () => {

    const courseArray = ["5f1e267775ce5d5a64c3dcf1", "5f1e2657b35e7b5aad7f4dff"];
    const groupTallerId = "6321f4f0d6456d11ebb4a659"; // GRUPO DE SOCIALES

    const courseTaller = "6321f40a7700fe5c61c516c8"; // Curso de sociales

    const groups = await GroupsModel
        .find({
            course: { $in: courseArray },
            startDate: { $gte: new Date("01-01-2022") }
        })
        .populate({
            path: "studentList",
        })
        .lean();

    const studentListGlobal = [];

    groups.forEach(element => {
        const list = element.studentList.list;
        // console.log(list);
        list.forEach(student => {
            //console.log(student);
            studentListGlobal.push(student);
        })
    });



    const groupTaller = await GroupsModel
        .findOne({
            _id: groupTallerId
        })
        .populate({
            path: "studentList",
            select: "_id"
        })


    //new students to the studentList , the "each" iterate the array to add the items stead add the whole array 

    const updateStudentList = await StudentLists
        .findOneAndUpdate({
            _id: groupTaller.studentList._id
        }, {
            $addToSet: { list: { $each: studentListGlobal } }
        }, {
            upsert: false,
        })

    console.log("UpdateStudentList", updateStudentList);


    for (let i = 0; i < studentListGlobal.length; i++) {
        const actualStudent = studentListGlobal[i];
        const updateStudent = await StudentModel.findOneAndUpdate({
            _id: actualStudent
        }, {
            $addToSet: { groups: groupTallerId }
        })
    }


    // This will add payments 
    for (let i = 0; i < studentListGlobal.length; i++) {
        const student = studentListGlobal[i];
        const index = i;

        const bodyPayment = {
            amount: 0,
            discount: 500,
            concept: "Curso de Biología (En línea)",
            course: courseTaller,
            paymentType: "cash",
            student: student,
        }
        const resultInternal = await PaymentsModel.create(bodyPayment)
        console.log("Index:" + index + " result:", resultInternal);
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })