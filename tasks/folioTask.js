/**
 * tasks/folioTask.js
 *
 * @description :: change folio in students schema to schema folio
 * @docs        :: How to run: node tasks/folioTask.js
 *              ::
 */
const moment = require('moment')
const StudentModel = require('../db/students')

const FoliosModel = require('../db/folio')



const init = async () => {

  

    const students = await StudentModel
        .find({"materialFolios.folio": {$gt: 0} })


     //console.log(students)

//    for (let i = 0; i < students.length; i++) {
    for (let i = 0; i < 100; i++) {
        console.log("manual: ", i)

    for (let j = 0; j < students[i].materialFolios.length; j++) {

        let folio = students[i].materialFolios[j].folio
        let title = students[i].materialFolios[j].title
        let idStudent = students[i]._id

        var data = {
            folio: folio,
            title: title,
            idStudent: idStudent
        }

        const nuevoFolio = new FoliosModel(data)
        const result = await nuevoFolio.save()
          .then(ff => {
             if(ff.folio > 0){
                console.log("Folio salvado: ", ff.folio)
             }
          })
          .catch(err => {
            console.error('- Error trying to create a folio', JSON.stringify(err, null, 2))
        console.log("error")
          })


          const studentRemove = await StudentModel
          .findOneAndUpdate(    { _id: idStudent },
            { $pull: { materialFolios: { folio: folio } } },
            { new: true })
          .then(sr => {
            console.log(sr)

         })
         .catch(err => {
           console.error('- Error trying to create a classroom', JSON.stringify(err, null, 2))
       console.log("error")
         })


    }

    }
//     let suma=0
//     let alumnos = 0
//     for (let i = 0; i < groups.length; i++) {
//         suma = suma + groups[i].studentList.list.length
//         for (let j = 0; j <  groups[i].studentList.list.length; j++) {
//             if((groups[i].studentList.list[j].registerDate < date) ){
//             if( (groups[i].studentList.list[j].registerDate > date2 )){
               
//                 alumnos = alumnos+1
//             }
//             }
//         }
        

//     }

// console.log("Curso: ", groups[0].course.name)
// console.log("Total Grupos:", groups.length)
// console.log("TOTAL EN EL CURSO: ", suma)
// console.log("Alumnos inscritos hasta hace un año: ", alumnos)

   
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })