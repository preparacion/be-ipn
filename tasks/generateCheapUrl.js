/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const VideosModel = require('../db/videos')
const fetch = require('node-fetch');
const init = async () => {
    const opts = ["COMPLETE", "COMPLETE_IMAGE"]
    const videos = await VideosModel
        .find({
            statusTranscode: { $in: opts }
        })
    console.log("Videos:", videos.length)
    for (let i = 0; i < videos.length; i++) {
        console.log(i)
        await new Promise(resolve => setTimeout(resolve, 25));
        const current = videos[i]
        if(current.url!==null && current.url!==undefined){
            let newUrl = current.url.replace("https://d20num3pjgo17t.cloudfront.net/","https://nqokz91200lxlqn.belugacdn.link/")
            if(newUrl!==undefined){
                current.cheapUrl=newUrl
                current.save()
            }
        }
    }
}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })