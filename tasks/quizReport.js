/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const StudentsModel = require('../db/students')
const QuizModel = require('../db/quiz')
const QuizAttemptModel = require('../db/quizAttempt')
const QuizAttemptAnswerModel = require('../db/quizAttemptAnswer')
const moment = require('moment')
const fetch = require('node-fetch');

const EXAM_ID = "607cfabca87d005b4daaec77"

const init = async () => {

    const quiz = await QuizModel.findOne({
        _id: EXAM_ID
    })
        .populate("questionCollection")
        .lean()

    const rightAnswers = [];
    quiz.questionCollection.forEach(item => {
        const right = []
        item.answerCollection.forEach(answer => {
            if (answer.validAnswer) {
                right.push(answer.idAnswer)
            }
        })
        rightAnswers.push(right);
    })

    console.log(rightAnswers);
    const number = rightAnswers.length;
    console.log(number)
    const item = new Array(number);
    for (let i = 0; i < number; i++) {
        item[i] = 0;
    }
    console.log(item);

    const attemps = await QuizAttemptModel.find({
        quizParent: EXAM_ID,
        status: "CLOSE"
    })
        .populate("answerCollection")
        .lean()
    attemps.forEach(items => {
        items.answerCollection.forEach((answers,index) => {
            if(answers.isCorrect){
                item[index]++
            }
        })
    })
    console.log(item)


}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })