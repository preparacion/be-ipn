const PlaylistModelDb = require('../../db/playlists');
const CourseModelDb = require('../../db/courses');

const init = async function () {

    const courses = await CourseModelDb.find({})
    for (let i = 0; i < courses.length; i++) {
        const itemCourse = courses[i];
        const playlist = await PlaylistModelDb.find({
            course: itemCourse._id,
            parent: { $exists: false }
        }).lean();
        const childrens = []
        const contentPlaylistData = []
        playlist.sort((a, b) => a.order - b.order);
        playlist.forEach(element => {
            childrens.push(element._id)
            contentPlaylistData.push({
                _id: element._id,
                type: 1,
                active: element.active || true,
                order: element.order || 1,
            })
        })
        // console.log(childrens)
        const updateResult = await CourseModelDb.updateOne({
            _id: itemCourse._id
        },
            {
                childrenPlaylist: childrens,
                videos: [],
                quiz: [],
                contentPlaylist: contentPlaylistData,
            })
        console.log(updateResult)
    }
}

Promise.resolve(init())
    .then(result => {
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })