/**
 * tasks/createPlaylistOrder.js
 *
 * @description :: Create the permissions / role matrix
 * @docs        :: TODO
 *              ::
 */

/**
 * This will only generate first level of playlists content.
 */

const PlaylistModel = require('../../models/playlists');
const PlaylistModelDb = require('../../db/playlists');
const VideosModelDb = require('../../db/videos');
const QuizModelDb = require('../../db/quiz');

const init = async function () {
    const playlist = await PlaylistModelDb.find({
        parent: { $exists: false }
    });

    for (let i = 0; i < playlist.length; i++) {
        const item = playlist[i];
        const videos = await VideosModelDb.find({
            playlist: item._id
        })
        const quiz = await QuizModelDb.find({
            parent: item._id
        })
        const childrens = await PlaylistModelDb.find({
            parent: item._id
        })

        const arrayItems = []
        childrens.forEach(item => {
            arrayItems.push({
                _id: item._id,
                type: 1,
                active: item.active || true,
                order: item.order || 1,
            })
        })
        videos.forEach(item => {
            arrayItems.push({
                _id: item._id,
                type: 2,
                active: item.active || true,
                order: item.order || 1,
            })
        })
        quiz.forEach(item => {
            arrayItems.push({
                _id: item._id,
                type: 3,
                active: item.active || true,
                order: item.order || 1,
            })
        })
        arrayItems.sort((a, b) => {
            return a.order - b.order;
        });
        console.log("------------- ", i, " --------------")
        console.log("Numero de Videos:", videos.length)
        console.log("Numero de Quiz:", quiz.length)
        console.log("Numero de Children:", childrens.length)
        arrayItems.forEach(item => {
            delete item.order
        })
        const updateResult = await PlaylistModelDb.updateOne({
            _id: item._id
        },
            {
                children: childrens,
                videos: videos,
                quiz: quiz,
                contentPlaylist: arrayItems,
            })
        console.log(item._id)
        console.log(updateResult)
    }
}
Promise.resolve(init())
    .then(result => {
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })