/**
 * tasks/createPermissions.js
 *
 * @description :: Create the permissions / role matrix
 * @docs        :: TODO
 *              ::
 */
const bcrypt = require('bcryptjs')

const UserModel = require('../db/users')
const RoleModel = require('../db/roles')

const jsonData = require('./roles.json')

const {
  rootUserPass,
  rootUserMail,
  fooUserPass,
  fooUserMail
} = require('../config')

const init = async function () {
  await removeDocs()
  await createRoles()


  await createUsers()
}

const createRoles = async function () {
  await Promise
    .all(jsonData.map(data => {
      const role = new RoleModel(data)
      return role.save()
    }))
}

const createUsers = async function () {
  const devRole = await RoleModel
    .findOne({
      name: 'dev'
    })
  const devPass = await bcrypt
    .hash(rootUserPass, 10)
  const devUser = new UserModel({
    name: 'dev',
    description: 'dev user',
    lastName: 'devian',
    email: rootUserMail,
    hashed_password: devPass,
    root: true,
    roles: [devRole._id]
  })
  await devUser.save()

  const fooRole = await RoleModel
    .findOne({
      name: 'foo'
    })

    
    const fooPass = await bcrypt
    .hash(fooUserPass, 10)
    const fooUser = new UserModel({
      name: 'foo',
      description: 'foo user',
      lastName: 'bar',
    email: fooUserMail,
    hashed_password: fooPass,
    roles: [fooRole._id]
  })
  await fooUser.save()
  console.log('-----------')
  console.log(fooRole, devRole)
  console.log(devUser, fooUser)
  console.log('-----------')
}

const removeDocs = async function () {
  await UserModel.remove({
    email: {
      $in: [rootUserMail, fooUserMail]
    }
  })
  await RoleModel.remove({
    name: {
      $in: ['dev', 'foo']
    }
  })
}

Promise.resolve(init())
  .then(result => {
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })