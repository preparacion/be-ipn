/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const moment = require('moment')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const PDFModel = require ('../lib/generatePDF')
const fs = require ('fs')
const init = async () => {

    //Todo deprecated ALL
    const generador = new PDFModel() 
    // const student = await StudentModel.findById("5ea368148e6ffc590aa08d72")
    const data = {
        "studentData": {
            "email": "samzero1@hotmail.com",
            "lastName": "more test",
            "name": "sam zero",
            "phoneNumber": "5566112233",
            "secondLastName": "rod test",
            "secondPhoneNumber": "5544112233",
            "_id": "5caad68dbcc33372bec02ce4"
        },
        "groupsArray": [{
            "classRoom": {
                "capacity": 15,
                "location": {
                    "closing": "21:00",
                    "district": "San Bartolo Atepehuacan",
                    "latitude": "19.498521338412544",
                    "longitude": "-99.14771061458588",
                    "name": "Eje Central",
                    "number": "749",
                    "opening": "08:00",
                    "postalCode": "7730",
                    "street": "Lázaro Cárdenas",
                    "_id": "5ca8e6713b14904fc0696295"
                },
                "name": "LA03",
                "_id": "5caad68dbcc33372bec02ce4"
            },
            "endDate": "2021-05-15T00:28:42.000Z",
            "name": "LA03-Examen Simulacro 1-NV",
            "percentage": 3.3333333333333335,
            "quota": 30,
            "schedules": [{
                "day": 4,
                "endHour": "2100",
                "startHour": "1700",
                "_id": "6088bdec94c40b5b5a0a5b12"
            },
        
        ],
            "startDate": "2021-05-15T00:28:42.000Z",
    
            "studentListLength": 1,
            "_id": "6088bb5693fc4d5b43b26d8c"
        },
        {
            "classRoom": {
                "capacity": 15,
                "location": {
                    "closing": "21:00",
                    "district": "San Bartolo Atepehuacan",
                    "latitude": "19.498521338412544",
                    "longitude": "-99.14771061458588",
                    "name": "Eje Central",
                    "number": "749",
                    "opening": "08:00",
                    "postalCode": "7730",
                    "street": "Lázaro Cárdenas",
                    "_id": "5ca8e6713b14904fc0696295"
                },
                "name": "LA03",
                "_id": "5caad68dbcc33372bec02ce4"
            },
            "endDate": "2021-05-15T00:28:42.000Z",
            "name": "LA03-Examen Simulacro 1-NV",
            "percentage": 3.3333333333333335,
            "quota": 30,
            "schedules": [{
                "day": 4,
                "endHour": "2100",
                "startHour": "1700",
                "_id": "6088bdec94c40b5b5a0a5b12"
            },
        
        ],
            "startDate": "2021-05-15T00:28:42.000Z",
    
            "studentListLength": 1,
            "_id": "6088bb5693fc4d5b43b26d8c"
        },
        {
            "classRoom": {
                "capacity": 15,
                "location": {
                    "closing": "21:00",
                    "district": "San Bartolo Atepehuacan",
                    "latitude": "19.498521338412544",
                    "longitude": "-99.14771061458588",
                    "name": "Eje Central",
                    "number": "749",
                    "opening": "08:00",
                    "postalCode": "7730",
                    "street": "Lázaro Cárdenas",
                    "_id": "5ca8e6713b14904fc0696295"
                },
                "name": "LA03",
                "_id": "5caad68dbcc33372bec02ce4"
            },
            "endDate": "2021-05-15T00:28:42.000Z",
            "name": "LA03-Examen Simulacro 1-NV",
            "percentage": 3.3333333333333335,
            "quota": 30,
            "schedules": [{
                "day": 4,
                "endHour": "2100",
                "startHour": "1700",
                "_id": "6088bdec94c40b5b5a0a5b12"
            },
        
        ],
            "startDate": "2021-05-15T00:28:42.000Z",
    
            "studentListLength": 1,
            "_id": "6088bb5693fc4d5b43b26d8c"
        }]
    }
    const prueba = await generador.generateExamen(data)
    const dataFile = fs.createWriteStream('testExamen'+ '.pdf', { autoClose: true })
    write(prueba, () => { }, dataFile)
}

function write(data, cb, dataFile) {
    if (!dataFile.write(data)) {
        dataFile.once('drain', cb)
    } else {
        process.nextTick(cb)
    }
}

init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })