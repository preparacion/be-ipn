const _ = require('underscore')
const mongoose = require('mongoose')

const PhaseModel = require('../db/phase')
const GroupsModel = require('../db/groups')
const CoursesModel = require('../db/courses')
const StudentModel = require('../db/students')
const StudentListsModel = require('../db/studentLists')

const schedule = '5c02310b4c14992dbf735387'

const init = async function () {
    console.log('init')
    // await removeDocs()
    await createPhases()
    const sets = await getGroupSets()
  
    // sets.forEach(set => {
    //   console.log(set)
    // })
  
    await createMirrors(sets)
  }
  
  const createMirrors = async function (groupsByCourses = []) {
    let totalList = [] // the set totalList
    let totalLength = 0
    for (let i = 0; i < groupsByCourses.length; i++) {
      groupsByCourses[i].newStudents = []
      groupsByCourses[i].newGroups = []
  
      let totalQuota = 0
      groupsByCourses[i].groups.forEach(group => {
        totalQuota+= group.slList.length
        totalLength+= group.slList.length
        groupsByCourses[i].newGroups.push({
          name: group.name + '-1',
          quota: group.slList.length,
          students: [],
          active: true,
          etapa: 2,
          phase: groupsByCourses[i].phase,
          course: groupsByCourses[i].course,
          schedule: groupsByCourses[i].schedule,
          classRoom: group.classRoom
        })
      })
      groupsByCourses[i].totalQuota = totalQuota
      groupsByCourses[i].students.forEach(student => {
        totalList.push(student)
      })
    }
  
    console.log('-----------------')
    totalList = totalList.reverse()
    let j = 0
    let arr = []
    do {
      const array = _.shuffle(_.difference(totalList, groupsByCourses[j].students))
  
      if (groupsByCourses[j].newStudents.length < groupsByCourses[j].totalQuota) {
        const element = array.shift()
        groupsByCourses[j].newStudents.push(element)
        totalList.splice(totalList.indexOf(element), 1)
      }
      j++
      if (j > 3) {
        j = 0
      }
    } while (totalList.length > 0)
  
    for (let i = 0; i < groupsByCourses.length; i++) {
      for (let j = 0; j < groupsByCourses[i].newGroups.length; j++) {
        for (let k = 0; k < groupsByCourses[i].newGroups[j].quota; k++) {
          const element = groupsByCourses[i].newStudents.shift()
          if (element) {
            groupsByCourses[i].newGroups[j].students.push(element)
          }
        }
      }
    }
  
    // fixing new groups quotas
    for (let i = 0; i < groupsByCourses.length; i++) {
      for (let j = 0; j < groupsByCourses[i].newGroups.length; j++) {
        const element = groupsByCourses[i].newGroups[j]
        groupsByCourses[i].newGroups[j].quota = groupsByCourses[i].newGroups[j].students.length
      }
    }
    
    const pCreateGroups = []
    for (let i = 0; i < groupsByCourses.length; i++) {
      groupsByCourses[i].newGroups.forEach(nGroup => {
        pCreateGroups.push(createNewGroup(nGroup))
      })
    }
  
    await Promise.all(pCreateGroups.map(p => p))
  }
  
  const createNewGroup = async function (data) {
    // console.log(data)
    const list = data.students || []
    const name = data.name
    const material = data.students.map(student => {
      return {
        material: false,
        student
      }
    })
  
    const studentList = new StudentListsModel({
      list,
      name,
      material
    })
    await studentList.save()
  
    const group = new GroupsModel({
      name: data.name,
      startDate: new Date(),
      active: true,
      quota: data.quota,
      course: data.course,
      schedule: data.schedule,
      phase: data.phase,
      classRoom: data.classRoom,
      studentList: studentList._id,
      etapa: 2
    })
    await group.save()
  
    console.log(studentList._id)
  
    await Promise.all(list.map(student => {
      return addGroupToToStudent(student, group._id)
    }))
  }
  
  const addGroupToToStudent = async function (studentId, groupId) {
    const student = await StudentModel.findOne({
      _id: studentId
    })
  
    if (!student) {
      console.log(`Estudiante no encontrado: ${studentId}`)
      console.log(`Grupo: ${groupId}`)
      process.exit(1)
    }
  
    if (student.groups.indexOf(groupId) === -1) {
      student.groups.push(groupId)
      await student.save()
    }
  }
  
  
  const getGroupSets = async function () {
    const sets = await GroupsModel.aggregate([
      {
        $match: {
          phase: { $exists: true },
          name: /^MA/,
          schedule: new mongoose.Types.ObjectId(schedule)
        }
      },
      {
        $lookup: {
          from: 'Phase',
          let: { ph: '$phase' },
          pipeline: [
            { $match: { $expr: { $eq: ['$$ph', '$_id'] } } },
            { $project: { name: 1, number: 1, _id: 0 } }
          ],
          as: 'phaseInfo'
        }
      },
      { $unwind: '$phaseInfo' },
      {
        $lookup: {
          from: 'Schedules',
          let: { sc: '$schedule' },
          pipeline: [
            { $match: { $expr: { $eq: ['$$sc', '$_id'] } } },
            { $project: { name: 1, day: 1, _id: 0, startHour: 1 } }
          ],
          as: 'scheduleInfo'
        }
      },
      { $unwind: '$scheduleInfo' },
      {
        $lookup: {
          from: 'Courses',
          let: { co: '$course' },
          pipeline: [
            { $match: { $expr: { $eq: ['$$co', '$_id'] } } },
            { $project: { name: 1, depdendency: 1, _id: 0 } }
          ],
          as: 'courseInfo'
        }
      },
      { $unwind: '$courseInfo' },
      {
        $lookup: {
          from: 'StudentLists',
          let: { stList: '$studentList' },
          pipeline: [
            { $match: { $expr: { $eq: ['$$stList', '$_id'] } } },
            { $project: { name: 1, _id: 0, list: 1 } }
          ],
          as: 'studentListInfo'
        }
      },
      { $unwind: '$studentListInfo' },
      {
        $lookup: {
          from: 'ClassRooms',
          let: { clRoom: '$classRoom' },
          pipeline: [
            { $match: { $expr: { $eq: ['$$clRoom', '$_id'] } } }
          ],
          as: 'classRoomInfo'
        }
      },
      { $unwind: '$classRoomInfo' },
      {
        $project: {
          _id: 1, name: 1, active: 1, quota: 1, course: 1, schedule: 1, classRoom: 1,
          studentList: 1, phase: 1, phName: '$phaseInfo.name', phNumber: '$phaseInfo.number',
          scName: '$scheduleInfo.name', scDay: '$scheduleInfo.day',
          scStartHour: '$scheduleInfo.startHour', coName: '$courseInfo.name',
          slList: '$studentListInfo.list', crName: '$classRoomInfo.name',
        }
      },
      {
        $project: {
          _id: 1, name: 1, active: 1, quota: 1, course: 1, classRoom: 1,
          studentList: 1, classRoom: 1, studentList: 1, phase: 1, schedule: 1,
          phName: 1, phNumber: 1, scName: 1, scNumber: 1, scDay: 1,
          scStartHour: 1, slList: 1, crName: 1, crLocation: 1, coName: 1
        }
      },
      {
        $group: {
          _id: {
            course: '$course',
            phase: '$phase',
            schedule: '$schedule'
          },
          groups: { $push: '$$ROOT' }
        }
      }
    ])
  
    let index = 0
  
    return sets.map(set => {
      const students = []
      set.groups.forEach(group => {
        group.slList.forEach(student => {
          students.push(student)
        })
      })
      const result = {
        course: set._id.course,
        phase: set._id.phase,
        schedule: set._id.schedule,
        groups: set.groups,
        students,
        index
      }
      index++
      return result
    })
  }
  
  
  const createPhases = async function () {
    const phase = await PhaseModel.findOne({
      number: 2
    })
  
    const courseIds = await CoursesModel
      .find({ dependency: '5' })
      .select('_id')
      .then(courses => {
        return courses.map(course => course._id)
      })
    
    const groupIds = await GroupsModel
      .find({
        course: { $in: courseIds },
        name: /^MA/,
        schedule
      })
      .select('_id')
      .then(groups => {
        return groups.map(group => group._id)
      })
    
    await Promise.all(
      groupIds.map(groupId => {
        return addPhaseToGroups(groupId, phase._id)
      })
    )
  }
  
  const addPhaseToGroups = async function (groupId, phaseId) {
    const group = await GroupsModel
      .findOne({ _id: groupId })
    group.phase = phaseId
    group.etapa = 1
    await group.save()  
  }
  
  const removeDocs = async function () {
    await GroupsModel.remove({ 
      name: { $regex: '-1$' },
      schedule
    })
  }
  
  Promise.resolve(init())
    .then(() => { process.exit(0) })
    .catch(err => {
      console.error(err)
      process.exit(1)
    })
  