/**
 * tasks/sendSegNotification.js
 */
const fs = require('fs')
const _ = require('underscore')

const GroupModel = require('../db/groups')
const StudentModel = require('../db/students')
const StudentCounter = require('../lib/studentCounter')

const { sendGridUri, sendGridToken } = require('../config')

const templateId = 'd-292238df25ff4cbb84bf548458971fea'

const superiorCourse = '5c02310a5d6f712db49d3103'

const testStudents = [
  '5cd3985bf58edb060db0679d',
  '5cd39e99f58edb060db067d5',
  '5cd3a023f58edb060db067fe',
  '5cd3a520f58edb060db0680f'
]



const dataFile = fs.createWriteStream('./tasks/balance.csv', { autoClose: true })

function write (data, cb) {
  if (!dataFile.write(data)) {
    dataFile.once('drain', cb)
  } else {
    process.nextTick(cb)
  }
}

const init = async () => {
  const stListIds = await GroupModel
    .find({ course: superiorCourse })
    .select('studentList')
    .then(result => {
      return result.map(group => group.studentList)
    })

  const studentIds = await Promise
    .all(stListIds.map(id => StudentCounter.getStudentsByStList(id)))
    .then(result => {
      const stArr = []
      result.forEach(arr => {
        arr.forEach(id => {
          stArr.push(id)
        })
      })
      return _.uniq(stArr.filter(stId => stId))
    })

  console.log('-----------')
  console.log(studentIds.length)
  console.log('-----------')

  const students = await Promise
    .all(studentIds.map(stId => {
      return StudentModel
        .findOne({
          _id: stId,
          balance: { $exists: true }
        })
        .select('name lastName secondLastName email phoneNumber secondPhoneNumber balance')
    }))
    .then(students => {
      const sts = students
        .filter(st => {
          return st && st.balance && st.balance.payments.total >= 3000
        })

      console.log('-----------')
      console.log(sts.length)
      console.log('-----------')

      return sts
    })
    .catch(err => {
      console.error(err)
    })

  const header = 'nombre, ap_pat, ap_mat, tel, tel_2, email \n'

  write(header, () => { console.log('escribiendo archivo') })

  students.forEach(st => {
    writeStudent(st)
  })
}

const writeStudent = stData => {
  const data = stData.name + ',' +
  stData.lastName + ',' +
  stData.secondLastName + ',' +
  stData.phoneNumber + ',' +
  stData.secondPhoneNumber + ',' +
  stData.email + '\n'

  write(data, () => {
    // console.log('se escribió ' + stData.email)
  })
}

Promise
  .resolve(init())
  .then(() => {
    console.log('done')
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
