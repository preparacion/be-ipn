/**
 * tasks/fixSchedules.js
 *
 * @description :: Fix Schedules Collection structure
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const ScheduleModel = require('../db/schedules')

const days = [
  'lunes',
  'martes',
  'miercoles',
  'jueves',
  'viernes',
  'sabado',
  'domingo'
]
const aliasDays = [
  'l',
  'm',
  'w',
  'j',
  'v',
  's',
  'd'
]
/* eslint-disable */
const normField = function (str) {
  if (!str || str === '') {
    return ''
  }
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}
/* eslint-disable */

const init = async () => {
  const cursor = ScheduleModel
    .find({})
    .cursor()

  let doc = await cursor.next()
  do {
    if (!doc.aliasName || !doc.aliasName.length || !doc.schedule) {
      doc.schedulesList = [{
        day: days.indexOf(normField(doc.day.toLowerCase())),
        startHour: doc.startHour,
        endHour: doc.endHour
      }]
      await doc.save()
    } else if (doc.aliasName.length === 1 || doc.schedule.length === 1) {
      doc.schedulesList = [{
        day: aliasDays.indexOf(doc.aliasName[0]),
        startHour: doc.startHour,
        endHour: doc.endHour
      }]
      await doc.save()
    } else {
      const list = await Promise
        .all(doc.schedule.map(scheduleId => {
          return ScheduleModel.findOne({ _id: scheduleId })
        }))
        .then(schedules => {
          const list = []
          schedules.forEach(schedule => {
            list.push({
              day: aliasDays.indexOf(schedule.aliasName[0]),
              startHour: schedule.startHour,
              endHour: schedule.endHour
            })
          })
          return list
        })

      doc.schedulesList = list
      await doc.save()
    }
    doc = await cursor.next()
  } while (doc !== null)
}
  
init()
  .then(() => {
    console.log('Finished !!')
    setTimeout(function () {
      process.exit(0)
    }, 500)
  })
  .catch(err => {
    console.error(err)
    setTimeout(()=> {
      process.exit(1)
    }, 500)
  })
