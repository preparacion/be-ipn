const moment = require('moment-timezone')
const PDFDocument = require('pdfkit')
const fs = require('fs')
const mongoose = require('mongoose')
const LogModel = require('../db/logs')
const StudentModel = require('../db/students')
const PaymentModel = require('../db/payments')
const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')

// Configura moment para usar la zona horaria de México Central
moment.tz.setDefault("America/Mexico_City")

const formatearFecha = (fecha) => {
    return moment(fecha).format('DD/MM/YYYY HH:mm:ss')
}

const registrarActividadDetallada = async (log, stats) => {
    try {
        const data = JSON.parse(log.data)
        const fechaFormateada = formatearFecha(log.date)
        let actividadDetalle = {}

        if (log.action === "register a student") {
            // Obtener detalles del grupo
            let grupoInfo = "Grupo no encontrado"
            if (data.group && data.group.id) {
                const grupo = await GroupModel.findById(data.group.id).populate('course')
                if (grupo) {
                    grupoInfo = {
                        nombre: grupo.name,
                        curso: grupo.course ? grupo.course.name : 'No especificado'
                    }
                }
            }

            actividadDetalle = {
                tipo: "Registro de Estudiante",
                fecha: fechaFormateada,
                estudiante: {
                    nombre: `${data.student.name} ${data.student.lastName} ${data.student.secondLastName}`.trim(),
                    email: data.student.email,
                    telefonos: [
                        data.student.phoneNumber,
                        data.student.secondPhoneNumber
                    ].filter(Boolean).join(' / ')
                },
                grupo: grupoInfo,
                pagoInicial: data.payment ? {
                    monto: parseFloat(data.payment.amount),
                    concepto: data.payment.concept,
                    tipo: data.payment.paymentType
                } : null
            }
        } else if (log.action === "create a payment") {
            // Obtener información del estudiante y curso
            let studentInfo = "Estudiante no encontrado"
            let courseInfo = "Curso no encontrado"

            if (data.student) {
                const student = await StudentModel.findById(data.student)
                if (student) {
                    studentInfo = {
                        nombre: `${student.name} ${student.lastName} ${student.secondLastName}`.trim(),
                        email: student.email
                    }
                }
            }

            if (data.course) {
                const course = await CourseModel.findById(data.course)
                if (course) {
                    courseInfo = {
                        nombre: course.name,
                        tipo: course.courseType || 'No especificado'
                    }
                }
            }

            actividadDetalle = {
                tipo: "Pago",
                fecha: fechaFormateada,
                estudiante: studentInfo,
                curso: courseInfo,
                pago: {
                    monto: parseFloat(data.amount),
                    concepto: data.concept,
                    tipo: data.paymentType,
                    descuento: data.discount ? parseFloat(data.discount) : 0
                }
            }
        }

        // Agregar a las estadísticas
        if (!stats.actividadesDetalladas) {
            stats.actividadesDetalladas = []
        }
        stats.actividadesDetalladas.push(actividadDetalle)
    } catch (error) {
        console.error(`Error procesando actividad del log ${log._id}:`, error)
    }
}

const generarPDF = async (stats) => {
    console.log("Generando PDF...")
    const doc = new PDFDocument()
    doc.pipe(fs.createWriteStream('reporte_actividad_detallado.pdf'))

    // Título
    doc.fontSize(20).text('Reporte Detallado de Actividad del Sistema', {
        align: 'center'
    })
    doc.fontSize(12).text(`Generado el ${formatearFecha(new Date())}`, {
        align: 'center'
    })
    doc.moveDown(2)

    // Resumen General
    doc.fontSize(16).text('Resumen General')
    doc.fontSize(12)
    doc.text(`Total de registros: ${stats.totalRegisters}`)
    doc.text(`Total de pagos: ${stats.totalPayments}`)
    doc.text(`Monto total recaudado: $${stats.totalAmount.toFixed(2)}`)
    doc.text(`Total de descuentos aplicados: $${stats.totalDiscounts.toFixed(2)}`)
    doc.text(`Estudiantes únicos registrados: ${stats.estudiantesUnicos.size}`)
    doc.moveDown(2)

    // Actividades Detalladas
    doc.fontSize(16).text('Registro Detallado de Actividades')
    doc.moveDown()

    // Ordenar actividades por fecha
    stats.actividadesDetalladas.sort((a, b) => 
        moment(a.fecha, 'DD/MM/YYYY HH:mm:ss').diff(moment(b.fecha, 'DD/MM/YYYY HH:mm:ss'))
    )

    stats.actividadesDetalladas.forEach(actividad => {
        doc.fontSize(14).text(`${actividad.tipo}`, {continued: true})
        doc.fontSize(12).text(` - ${actividad.fecha}`)
        
        if (actividad.tipo === "Registro de Estudiante") {
            doc.text('Información del Estudiante:')
            doc.text(`  Nombre: ${actividad.estudiante.nombre}`)
            doc.text(`  Email: ${actividad.estudiante.email}`)
            doc.text(`  Teléfonos: ${actividad.estudiante.telefonos}`)
            
            if (typeof actividad.grupo === 'object') {
                doc.text('Información del Grupo:')
                doc.text(`  Nombre: ${actividad.grupo.nombre}`)
                doc.text(`  Curso: ${actividad.grupo.curso}`)
            }

            if (actividad.pagoInicial) {
                doc.text('Pago Inicial:')
                doc.text(`  Monto: $${actividad.pagoInicial.monto.toFixed(2)}`)
                doc.text(`  Concepto: ${actividad.pagoInicial.concepto}`)
                doc.text(`  Tipo de pago: ${actividad.pagoInicial.tipo}`)
            }
        } else if (actividad.tipo === "Pago") {
            if (typeof actividad.estudiante === 'object') {
                doc.text('Estudiante:')
                doc.text(`  Nombre: ${actividad.estudiante.nombre}`)
                doc.text(`  Email: ${actividad.estudiante.email}`)
            }

            if (typeof actividad.curso === 'object') {
                doc.text('Curso:')
                doc.text(`  Nombre: ${actividad.curso.nombre}`)
                doc.text(`  Tipo: ${actividad.curso.tipo}`)
            }

            doc.text('Detalles del Pago:')
            doc.text(`  Monto: $${actividad.pago.monto.toFixed(2)}`)
            doc.text(`  Concepto: ${actividad.pago.concepto}`)
            doc.text(`  Tipo de pago: ${actividad.pago.tipo}`)
            if (actividad.pago.descuento > 0) {
                doc.text(`  Descuento aplicado: $${actividad.pago.descuento.toFixed(2)}`)
            }
        }
        
        doc.moveDown()
    })

    doc.end()
    console.log('Reporte detallado generado en reporte_actividad_detallado.pdf')
}

const generateReport = async () => {
    try {
        console.log("Iniciando la generación del reporte...")
        
        const logs = await LogModel.find({
            method: "POST",
            user: "64d25b657f84bf156358a030"
        }).sort({date: 1})
        
        console.log(`Logs encontrados: ${logs.length}`)

        const stats = {
            totalRegisters: 0,
            totalPayments: 0,
            totalAmount: 0,
            totalDiscounts: 0,
            estudiantesUnicos: new Set(),
            actividadesDetalladas: []
        }

        // Procesar cada log
        console.log("Procesando logs...")
        for (const log of logs) {
            const data = JSON.parse(log.data)

            if (log.action === "register a student") {
                stats.totalRegisters++
                stats.estudiantesUnicos.add(data.student.email)
                if (data.payment) {
                    stats.totalPayments++
                    stats.totalAmount += parseFloat(data.payment.amount) || 0
                }
            } else if (log.action === "create a payment") {
                stats.totalPayments++
                stats.totalAmount += parseFloat(data.amount) || 0
                if (data.discount) {
                    stats.totalDiscounts += parseFloat(data.discount) || 0
                }
            }

            // Registrar actividad detallada
            await registrarActividadDetallada(log, stats)
        }

        // Generar PDF
        await generarPDF(stats)

    } catch (error) {
        console.error("Error al generar el reporte:", error)
        throw error
    }
}

// Ejecutar el reporte
generateReport()
    .then(() => {
        console.log('Tarea completada')
        setTimeout(() => process.exit(0), 1000)
    })
    .catch(err => {
        console.error('Error:', err)
        setTimeout(() => process.exit(1), 1000)
    })