/**
 * tasks//index-groups.js
 *
 * @description :: Create Index of Groups database
 * @docs        :: TODO
 *              :: 
 */
 const GroupsModel = require('../db/groups')

 GroupsModel.count(function (err, total) {
   if (err) { return console.error(err.message) }
   console.log('Total items to index:', total)
 
 
   GroupsModel.on('es-bulk-sent', function () {
     console.log('buffer sent');
   });
 
   GroupsModel.on('es-bulk-data', function (doc) {
     console.log('Adding ' + doc.name);
   });
 
   GroupsModel.on('es-bulk-error', function (err) {
     console.error(err);
   });
 
   GroupsModel
     .esSynchronize()
     .then(function () {
       console.log('END INDEX.');
     });
 })
 