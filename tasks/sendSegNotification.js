/**
 * tasks/sendSegNotification.js
 */
const rp = require('request-promise')

const StudentCounter = require('../lib/studentCounter')
const SendGrid = require('../lib/sendGrid')
const SMS = require('../lib/sms')

const SmsModel = require('../db/sms')
const StudentModel = require('../db/students')

const { sendGridUri, sendGridToken } = require('../config')

const templateId = 'd-a934c1e29ac94e4eb91061956d620114'

const testStudents = [
  '5cd3985bf58edb060db0679d',
  '5cd39e99f58edb060db067d5',
  '5cd3a023f58edb060db067fe',
  '5cd3a520f58edb060db0680f'
]

const studentFields = [
  'name', 'lastName', 'secondLastName',
  'phoneNumber', 'secondPhoneNumber', 'email'
]

const comMessage = 'Falta 1 mes para el examen de admisión a Nivel Medio Superior. Por ese motivo, NO habrá suspensión de clases. Estamos en los últimos días de estudio, hay que aprovecharlos al máximo.'
const supMessage = 'Falta 1 semana para el examen de admisión a nivel Superior. Por ese motivo, NO habrá suspensión de clases. Estamos en los últimos días de estudio, hay que aprovecharlos al máximo.'

const init = async () => {
  let comStudents = await StudentCounter
    .getComipemsStIds()
  let supStudents = await StudentCounter
    .getSuperiorStudentIds()
  comStudents = comStudents.filter(id => id && id !== '' && id !== null)
  supStudents = supStudents.filter(id => id && id !== '' && id !== null)
  console.log('-----------')
  console.log(comStudents.length)
  console.log(supStudents.length)
  console.log('-----------')

  let comSts = await Promise
    .all(comStudents.map(id => {
      return StudentModel
        .findOne({ _id: id })
        .select(studentFields.join(' '))
    }))
  let supSts = await Promise
    .all(supStudents.map(id => {
      return StudentModel
        .findOne({ _id: id })
        .select(studentFields.join(' '))
    }))
  comSts = comSts.filter(st => st && st !== null)
  supSts = supSts.filter(st => st && st !== null)
  console.log('-----------')
  console.log(comSts[0])
  console.log(supSts[0])
  console.log(comSts.length)
  console.log(supSts.length)
  console.log('-----------')

  await Promise.all(comSts.map(stData => {
    return sendNotification(stData, comMessage)
  }))
  await Promise.all(supSts.map(stData => {
    return sendNotification(stData, supMessage)
  }))

  // const testSts = await Promise
  //   .all(testStudents.map(id => {
  //     return StudentModel
  //       .findOne({ _id: id })
  //       .select(studentFields.join(' '))
  //   }))
  // await Promise.all(testSts.map(stData => {
  //   return sendNotification(stData, comMessage)
  // }))
  // await Promise.all(testSts.map(stData => {
  //   return sendNotification(stData, supMessage)
  // }))
}

const sendNotification = async (studentData, message) => {
  let phoneNumber
  const sms = new SMS()

  if (studentData && studentData['phoneNumber'] && studentData['phoneNumber'].length === 10) {
    phoneNumber = studentData['phoneNumber']
  } else if (studentData && studentData['secondPhoneNumber'] && studentData['secondPhoneNumber'].length === 10) {
    phoneNumber = studentData['secondPhoneNumber']
  }

  if (phoneNumber) {
    phoneNumber = `+521${phoneNumber}`
    await sms.sendMessage(
      'prepIPN',
      phoneNumber,
      'custom message',
      message
    )

    const smsToStudent = new SmsModel({
      message: message,
      date: new Date(),
      student: studentData._id
    })
    await smsToStudent.save()
  }

  if (studentData && studentData.email) {
    await sendMail(studentData.email, message)
  }
}

const sendMail = async (to, message) => {
  const options = {
    method: 'POST',
    uri: sendGridUri,
    body: {
      from: {
        email: 'no-reply@email.preparacionipn.mx',
        name: 'Preparación IPN'
      },
      content: [{
        type: 'text/html',
        value: 'Invoice'
      }],
      personalizations: [{
        to: [{
          email: to
        }],
        dynamic_template_data: {
          message: message
        }
      }],
      template_id: templateId
    },
    headers: {
      Authorization: `Bearer ${sendGridToken}`
    },
    json: true
  }

  const result = await rp(options)
    .then(response => {
      console.log(`Mensaje enviado a ${to}`)
      return { success: true }
    })
    .catch(err => {
      console.error(err)
      return {
        success: false,
        code: 500,
        error: JSON.stringify(err, null, 2)
      }
    })
  return result
}

Promise.resolve(init())
  .then(result => {
    console.log('Mensajes enviados!!!')
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
