/**
 * tasks/implementStageOne.js
 *
 * @description :: Create the permissions / role matrix
 * @docs        :: TODO
 *              ::
 */
const StageModel = require('../db/stage')
const CourseModel = require('../db/courses')

const stageOneData = {
  name: 'Etapa 1',
  description: 'Etapa uno de cursos',
  startDate: new Date()
}

const init = async () => {
  const stageOne = new StageModel(stageOneData)
  await stageOne.save()

  const courses = await CourseModel.find({})

  await Promise.all(courses.map(course => {
    course.stage = stageOne._id
    return course.save()
  }))
}

Promise.resolve(init())
  .then(result => {
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
