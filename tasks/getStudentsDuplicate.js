/**
 * tasks/studentsCount.js
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */


const fs = require('fs')



const moment = require('moment')
const Utils = require('../lib/Utils')
const StudentModel = require('../db/students')
const GroupsModel = require('../db/groups')
const StudentListModel = require('../db/studentLists')
const Groups = require('../models/groups')


const studentFields = [
    'Nombre', 'Apellido Paterno', 'Apellido materno',
    'Telefono 1', 'Telefono 2', 'email'
]


const dataFile = fs.createWriteStream('./students.csv', { autoClose: true })

function write(data, cb) {
    if (!dataFile.write(data)) {
        dataFile.once('drain', cb)
    } else {
        process.nextTick(cb)
    }
}


const init = async () => {
    const courseId = "5dc65abeddb6e2178da83448"
    dataFile.write(studentFields.join(',') + '\n')
    const groups = await GroupsModel
        .find({
            course: courseId,
            startDate: { $gte: moment("2020-12-31") }
        })
        .populate({
            path: "studentList",
            populate: {
                path: "list",
                populate: "groups",
                select: "name lastName secondLastName email phoneNumber secondPhoneNumber groups"
            }
        })
        .populate({
            path: "buckets",
            populate: {
                path: "studentList",
                populate: {
                    path: "list",
                    populate: "groups",
                    select: "name lastName secondLastName email phoneNumber secondPhoneNumber groups"
                }
            }
        })
        .lean();



    const studentsArray = []
    groups.forEach(element => {
        if (element.studentList && element.studentList.list.length > 0) {
            const actualStudentList = element.studentList.list;
            actualStudentList.forEach(student => {
                studentsArray.push(student);
            });
        }
    });

    const duplicateResult = studentsArray.filter((item) => {
        let index = studentsArray.indexOf(iternalItem => String(iternalItem._id) === String(item._id));
        return index >= 0;
    })


    const studentsObject = {}

    groups.forEach(element => {
        if (element.buckets && element.buckets.length > 0) {
            element.buckets.forEach(iternalBucket => {
                if (iternalBucket.studentList && iternalBucket.studentList.list.length > 0) {
                    const studentListBucket = iternalBucket.studentList.list;
                    studentListBucket.forEach(student => {
                        if (studentsObject[student._id] !== undefined) {
                            studentsObject[student._id].push(iternalBucket);
                        } else {
                            studentsObject[student._id] = [iternalBucket];
                        }
                    })
                }
            })
        }
    });

    const minus = [];
    const equal = [];
    const more = [];

    const objectKey = Object.entries(studentsObject)
    objectKey.forEach(iterateElement => {
        if (iterateElement[1].length > 4) {
            more.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        } else if (iterateElement[1].length < 4) {
            minus.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        } else {
            equal.push({
                _id: iterateElement[0],
                buckets: iterateElement[1]
            });
        }
    })

    console.log("Duplicados en parent", duplicateResult)
    console.log("Numero de estudiantes totales", studentsArray.length)

    console.log("Alumnos con 4 buckets: ", equal.length);
    console.log("Alumnos con menos de 4 buckets: ", minus.length);
    console.log("Alumnos con mas de 4 buckets: ", more.length);

    for (let i = 0; more.length > i; i++) {

        //This will remove all the students from the buckets 

        for (j = 0; more[i].buckets.length > j; j++) {
            const studentList = await StudentListModel.findById({
                _id: more[i].buckets[j].studentList._id
            })
            const indexStudent = studentList.list.findIndex(item => String(item) === String(more[i]._id))
            if (indexStudent >= 0) {
                let newStudent = [...studentList.list]
                newStudent.splice(indexStudent, 1);
                studentList.list = newStudent;
                await studentList.save();
            }
        }


        const student = await StudentModel.findById({
            _id: more[i]._id
        }).populate({
            path: "groups",
            populate: {
                path: "course"
            }
        })
        console.log("Estudiante: ",student.email)
        if (student.groups && student.groups.length > 0) {
            for (let j = 0; student.groups.length > j; j++) {
                const groupInside = student.groups[j];
                if (String(groupInside.course._id) === "5dc65abeddb6e2178da83448") {
                    console.log("HAS COMIPEMS");
                    const phaseOrder = await Utils.shuffleArray(groupInside.course.children.length)
                    let buckets = await Promise
                        .all(groupInside.course.children.map((courseId, index) => {
                            return Groups.enrollBucketGroup(courseId, groupInside, student._id, phaseOrder[index])
                        }))
                    buckets = buckets.sort((a, b) => {
                        return a.phase - b.phase;
                    })
                    let orderPhase = []
                    buckets.forEach(bucket => {
                        orderPhase.push(bucket._id);
                    });
                    student.comipemsOrder = orderPhase;
                    student.save()
                }
            }
        }
    }
}


const writeStudent = async student => {
    const data = student.name + ',' +
        student.lastName + ',' +
        student.secondLastName + ',' +
        student.phoneNumber + ',' +
        student.secondPhoneNumber + ',' +
        student.email + '\n'
    write(data, () => {
        // console.log('se escribió ' + student.email)
    })
}



init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })