const MercadoaPago = require('../lib/paymentsProcessor')

const init = async () => {
  let result = await MercadoaPago.getPaymentMethodAvailable()
  console.log(result)
}

init()
  .then(() => {
    console.log('Finished !!')
    setTimeout(function () {
      process.exit(0)
    }, 500)
  })
  .catch(err => {
    console.error(err)
    setTimeout(()=> {
      process.exit(1)
    }, 500)
  })