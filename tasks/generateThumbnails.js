/**
 *
 * @description :: Count students by course in some range
 * @docs        :: How to run: node tasks/fixSchedules.js
 *              ::
 */
const VideosModel = require('../db/videos')
const fetch = require('node-fetch');
const init = async () => {
    const videos = await VideosModel
        .find({
            statusTranscode: "COMPLETE"
        })
    console.log("GROUPS:", videos.length)
    for (let i = 0; i < videos.length ;i++) {
        console.log(i)
        await new Promise(resolve => setTimeout(resolve, 300));
        let keyFix = videos[i].transcodeId
        let body = {
            s3: {
                bucket: {
                    name: "ipn-videos-processed",
                    arn: "arn:aws:s3:::ipn-videos-processed"
                },
                object: {
                    key: keyFix
                }
            }
        }
          fetch(' https://vily0djggk.execute-api.us-east-1.amazonaws.com/default/thumbnails', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: { "Content-Type": "application/json" }
        })

    }
}


init()
    .then(() => {
        console.log('Finished !!')
        setTimeout(function () {
            process.exit(0)
        }, 500)
    })
    .catch(err => {
        console.error(err)
        setTimeout(() => {
            process.exit(1)
        }, 500)
    })