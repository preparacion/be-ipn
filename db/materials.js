/**
 * db/material.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const MaterialsSchema = new Schema({
  name: { type: String, required: true },
  type: { type: Number },
  thumbnailUrl: { type: String },
  videoUrl: { type: String },
  duration: { type: Number },
  order: { type: Number },
  rate: { type: Number },
  videoCount: { type: Number },
  quizCount: { type: Number },
  uploadBy: { type: Schema.Types.ObjectId, ref: 'Users' },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  active: { type: Boolean, default: true }
}, { versionKey: false, timestamps: true })

const Material = db.model('Material', MaterialsSchema, 'Material')

module.exports = Material
