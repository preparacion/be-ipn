/**
 * db/notifications.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const NotificationsSchema = new Schema({
  date: { type: Date, default: new Date() },
  description: { type: String },
  to: [
    { type: Schema.Types.ObjectId }
  ],
  email: {
    message: { type: String },
    template: { type: String },
    templateVersion: { type: String },
    templateData: { type: Schema.Types.Mixed }
  },
  sms: {
    message: { type: String }
  }
}, { versionKey: false })

const Notifications = db.model(
  'Notifications',
  NotificationsSchema,
  'Notifications'
)

module.exports = Notifications
