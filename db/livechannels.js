/**
 * db/material.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const LiveChannelsSchema = new Schema({
    name: { type: String, required: true },
    idInput: { type: Number, required: true },
    idChannel: { type: Number, required: true },

    rtmpUrl: { type: String, required: true },
    cdnUrl: { type: String, required: true },
    awsCdnUrl: { type: String, required: true },
    password: { type: String },
    active: { type: Boolean, default: false }

}, { versionKey: false, timestamps: true })

const LiveChannels = db.model('LiveChannels', LiveChannelsSchema, 'LiveChannels')

module.exports = LiveChannels
