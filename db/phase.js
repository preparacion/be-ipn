/**
 * db/phase.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PhaseSchema = new Schema({
  name: { type: String },
  startDate: { type: Date, default: new Date() },
  number: { type: Number },
  note: { type: String }
}, { versionKey: false })

const Phase = db.model('Phase', PhaseSchema, 'Phase')

module.exports = Phase
