/**
 * db/folio.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const FolioSchema = new Schema({
  folio: { type: Number, required: true },
  title: { type: String },
  idStudent: { type: Schema.Types.ObjectId, ref: 'Students' }
}, { versionKey: false })

const Folio = db.model('Folio', FolioSchema, 'Folio')

module.exports = Folio
