/**
 * db/permissions.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PermissionSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  allows: [
    {
      resource: { type: String },
      methods: [{ type: String }]
    }
  ]
}, { versionKey: false })

const Permissions = db.model('Permissions', PermissionSchema, 'Permissions')

module.exports = Permissions
