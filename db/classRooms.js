/**
 * db/classRooms.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ClassRoomSchema = new Schema({
  name: { type: String, required: true },
  capacity: { type: Number },
  oldId: { type: String },
  oldLocationId: { type: String },
  location: { type: Schema.Types.ObjectId, ref: 'Locations' }
}, { versionKey: false })

const ClassRooms = db.model('ClassRooms', ClassRoomSchema, 'ClassRooms')

module.exports = ClassRooms
