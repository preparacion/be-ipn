/**
 * db/courseLevels.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CourseLevelSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String }
}, { versionKey: false })

const CourseLevels = db.model('CourseLevels', CourseLevelSchema, 'CourseLevels')

module.exports = CourseLevels
