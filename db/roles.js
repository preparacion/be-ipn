/**
 * db/roles.js
 *
 * @description ::
 * @docs        :: None
 */
const _ = require('lodash')
const db = require('./db')
const Schema = db.Schema

const UsersModel = require('./users')

const RoleSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  visible: { type: Boolean, default: true },
  allows: [
    {
      resource: { type: String }, // URL
      methods: [{ type: String }] // get, post
    }
  ]
}, { versionKey: false })

RoleSchema.methods.getUsersCount = async function () {
  const totalUsers = await UsersModel
    .find({ roles: this._id }).countDocuments()
  return _.extend({ totalUsers }, this.toObject())
}

const Roles = db.model('Roles', RoleSchema, 'Roles')

module.exports = Roles
