/**
 * db/appoiments.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const AppoimentsSchema = new Schema({
    date: { type: Date },
    student: { type: Schema.Types.ObjectId, ref: 'Students' },
    group: { type: Schema.Types.ObjectId, ref: 'Groups' },
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comments' }],
    parent: { type: Schema.Types.ObjectId, ref: 'AppoimentsList' },
    courseTopics: { type: Schema.Types.ObjectId, ref: 'CourseTopics' },
    create_at: { type: Date, default: new Date() }
}, { versionKey: false })

const AppoimentsList = db.model('Appoiments', AppoimentsSchema, 'Appoiments')

module.exports = AppoimentsList