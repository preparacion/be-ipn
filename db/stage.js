/**
 * db/stage.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const StageSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  startDate: { type: Date }
}, { versionKey: false })

const Stage = db.model('Stages', StageSchema, 'Stages')

module.exports = Stage
