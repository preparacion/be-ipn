/**
 * db/studentProgress.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PlaylistProgress = new Schema({
    playlist: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    parent: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    date: { type: Date, default: Date.now() },
    progress: { type: Schema.Types.Number, default: 0 },
    student: { type: Schema.Types.ObjectId, ref: 'Students' },
    finishItems: [{ type: Schema.Types.Mixed }],
    finishItemsCount: { type: Schema.Types.Number, default: 0 },
    isLastItemHere: { type: Schema.Types.Boolean },
    lastItem: { type: Schema.Types.ObjectId },
}, { versionKey: false })

const VideoComments = db.model('PlaylistProgress', PlaylistProgress, 'PlaylistProgress')

module.exports = VideoComments
