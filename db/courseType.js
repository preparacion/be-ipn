/**
 * db/videos.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CourseTypesSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String }
}, { versionKey: false })

const CourseTypes = db.model('CourseTypes', CourseTypesSchema, 'CourseTypes')

module.exports = CourseTypes
