/**
 * db/videos.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const VideoSchema = new Schema({
  //Item Props
  title: { type: String },
  description: { type: String },
  duration: { type: String },
  url: { type: String },
  cheapUrl: { type: String },
  urlThumbnail: { type: String },
  active: { type: Boolean, default: true },
  order: { type: Number },
  poster: { type: String },
  playlist: { type: Schema.Types.ObjectId, ref: 'Playlist' },
  quizParent: { type: Schema.Types.ObjectId, ref: 'Quiz' },
  rate: { type: Number, default: 0 },
  rates: [{
    student: { type: Schema.Types.ObjectId, ref: 'Students' },
    rate: { type: Number }
  }],
  views: { type: Number, default: 0 },
  videoComments: { type: Schema.Types.ObjectId, ref: 'VideoComments' },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  type: { type: Number },

  
  //Meta props 
  eTag: { type: String }, // s3 data
  bucket: { type: String }, // s3 data
  version: { type: String }, // s3 data
  transcodeId: { type: String }, // MediaTranscode data
  statusTranscode: { type: String }, // MediaTranscode data
  percentageTranscode: { type: Number, default: 0 },
}, { versionKey: false })

VideoSchema.methods.toPopulate = async function () {
  const data = {
    _id: this._id,
    title: this.title,
    duration: this.duration,
    cheapUrl: this.cheapUrl,
    url: this.url,
    description: this.description,
    rate: this.rate,
    views: this.views,
    eTag: this.eTag,
    bucket: this.bucket,
    version: this.version,
    videoComments: this.videoComments
  }

  const comments = await this.model('VideoComments')
    .find({ video: this._id })
    .populate('student', 'name lastName secondLastName')
    .populate({
      path: 'responses',
      populate: {
        path: 'student',
        select: 'name lastName secondLastName'
      }
    })

  data.videoComments = comments

  if (this.course) {
    const course = await this.model('Courses').findOne({ _id: this.course })
    if (course) {
      data['course'] = course
    }
  }
  return data
}

VideoSchema.methods.calculateRate = async function (data) {
  let rateSum = 0
  let indexStudent = undefined
  for (let index = 0; index < this.rates.length; index++) {
    const rate = this.rates[index]
    if (String(rate.student) === data.student) {
      indexStudent = index
      rateSum += data.rate
    } else {
      rateSum += rate.rate
    }
  }
  let sizeRates = this.rates.length
  if (indexStudent >= 0) {
    let ratesCopy = [...this.rates]
    ratesCopy[indexStudent].rate = data.rate
    this.rates = ratesCopy
  } else {
    this.rates.push({
      student: data.student,
      rate: data.rate
    })
    rateSum += data.rate
    sizeRates++
  }
  const newRate = rateSum / sizeRates
  this.rate = newRate
  this.save()
  let response = {
    rateUser: data.rate,
    rateTotal: newRate
  }
  return response
}

const Videos = db.model('Videos', VideoSchema, 'Videos')

module.exports = Videos
