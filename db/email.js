/**
 * db/email.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const EmailSchema = new Schema({
  message: { type: String, required: true },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  group: { type: Schema.Types.ObjectId, ref: 'Groups' },
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  date: { type: Date },
  data: { type: Schema.Types.Mixed },
  template: { type: Schema.Types.Mixed }
}, { versionKey: false })

const Email = db.model('Email', EmailSchema, 'Email')

module.exports = Email
