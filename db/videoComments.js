/**
 * db/videoComments.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const VideoCommentSchema = new Schema({
  comment: { type: String },
  date: { type: Date, default: Date.now() },
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  video: { type: Schema.Types.ObjectId, ref: 'Videos' },
  parentComment:{ type: Schema.Types.ObjectId, ref: 'VideoComments' },
  responses: [{ type: Schema.Types.ObjectId, ref: 'VideoComments' }]
}, { versionKey: false })

const VideoComments = db
  .model('VideoComments', VideoCommentSchema, 'VideoComments')

module.exports = VideoComments
