/**
 * db/students.js
 *
 * @description ::
 * @docs        :: None
 */
const _ = require('underscore')
const mongoosastic = require('mongoosastic');
const moment = require('moment');
const uniqid = require('uniqid');

const db = require('./db')
const Schema = db.Schema
const Images = require('./images')
const S3 = require('../lib/s3')

const esClient = require('../lib/es-client')()

const UserModel = require('./users')
const GroupModel = require('./groups')
const CourseModel = require('./courses')
const PaymentModel = require('./payments')
const StudentCommentsModel = require('./studentComments')

const normStudent = function (field) {
  if (!field || field === '') {
    return ''
  }
  return field.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

const StudentSchema = new Schema({
  name: { type: String, required: true, es_indexed: true },
  description: { type: String },
  lastName: { type: String, es_indexed: true },
  secondLastName: { type: String, es_indexed: true },
  email: { type: String, es_indexed: true },
  hashed_password: { type: String },
  birthDate: { type: Date },
  address: { type: String },
  postalCode: { type: String },
  name_lower: { type: String, es_indexed: true },
  email_lower: { type: String, es_indexed: true },
  lastName_lower: { type: String, es_indexed: true },
  lastNames_lower: { type: String, es_indexed: true },
  fullName_lower: { type: String, es_indexed: true },
  firstLastName_lower: { type: String, es_indexed: true },
  secondLastName_lower: { type: String, es_indexed: true },
  phoneNumber: { type: String, es_indexed: true },
  secondPhoneNumber: { type: String, es_indexed: true },
  deleted: { type: Boolean, default: false, es_indexed: true },
  balance: {
    payments: {
      discounts: { type: Number, es_indexed: true },
      total: { type: Number, es_indexed: true }
    },
    costs: {
      total: { type: Number, es_indexed: true }
    }
  },
  debt: { type: Boolean },
  materialFolios: [{ type: Schema.Types.ObjectId, ref: 'Folio' }],
  fbUser: { type: String },
  fbToken: { type: String },
  hasTempPass: { type: Boolean, default: false },
  registeredBy: { type: Schema.Types.ObjectId, ref: 'Users' },
  registerDate: { type: Date, default: new Date(), es_indexed: true },
  groups: [{ type: Schema.Types.ObjectId, ref: 'Groups' }],
  freeLessonGroups: [{ type: Schema.Types.ObjectId, ref: 'Groups' }],
  paymentsArranges: [{
    groupId: { type: Schema.Types.ObjectId, ref: 'Groups' },
    courseId: { type: Schema.Types.ObjectId, ref: 'Courses' },
    startDate: { type: Date },
    endDate: { type: Date },
    typeArrange: { type: Number },
    registeredBy: { type: Schema.Types.ObjectId, ref: 'Users' },
    registerDate: { type: Date, default: new Date() }
  }],
  courses: [{ type: Schema.Types.ObjectId, ref: 'Courses' }],
  comipemsOrder: [{ type: Schema.Types.ObjectId, ref: 'Groups' }],
  comipemsGroups: [{
    groupId: { type: Schema.Types.ObjectId, ref: 'Groups' },
    name: { type: String },
    phase: { type: Number }
  }],
  comipemsEnrolledBy: { type: Schema.Types.ObjectId, ref: 'Users' },
  role: { type: Schema.Types.ObjectId, ref: 'Roles' },
  // Parameters to talks users TODO remove using lean in mongo get 
  calledBy: {
    date: { type: Date },
    user: { type: Schema.Types.ObjectId, ref: 'Users' }
  },
  allowed: { type: Boolean},
  picture: { type: Schema.Types.ObjectId, ref: 'Images' }
}, { versionKey: false })

/**
 * middleware pre save
 * builds and assign student's fields for elastics
 */
StudentSchema.pre('save', async function () {

  const nameLower = (this.name) ? normStudent(this.name) : ''
  const emailLower = (this.email) ? normStudent(this.email) : ''
  const lNameLower = (this.lastName) ? normStudent(this.lastName) : ''
  const sLNLower = (this.secondLastName)
    ? normStudent(this.secondLastName) : ''
  const fullName = `${nameLower} ${lNameLower} ${sLNLower}`
  const fLNLower = `${nameLower} ${lNameLower}`
  const lNLower = `${lNameLower} ${sLNLower}`

  const nameSplit = this.name.split(" ");
  let initialsName = "";
  if (nameSplit.length > 1) {
    const first = nameSplit[0];
    const secound = nameSplit[1];
    initialsName = (first.length > 0 ? first[0] : "") + (secound.length > 0 ? secound[0] : "");
  } else {
    initialsName = nameSplit[0][0];
  }

  this.name_lower = nameLower
  this.email_lower = emailLower
  this.lastName_lower = lNameLower
  this.secondLastName_lower = sLNLower
  this.fullName_lower = fullName
  this.firstLastName_lower = fLNLower
  this.lastNames_lower = lNLower

  if (this.picture) {
    let imagen = this.picture
      const ccImage = await Images.findOne({
        _id: imagen
      })
      if (ccImage && ccImage.isTempImage) {

        const url = await S3.moveImageTemporal(ccImage.key, 5)
        if(url != ""){
          ccImage.url = url
          ccImage.isTempImage = false
          ccImage.type = 5
          ccImage.save()
      }else{
        return err
      }


      }
    
  }


})


StudentSchema.pre('findOneAndUpdate', async function (next) {


  if (this._update.$set && this._update.$set.picture) {
    let imagen = data.picture
      const ccImage = await Images.findOne({
        _id: imagen
      })
      if (ccImage && ccImage.isTempImage) {

        const url = await S3.moveImageTemporal(ccImage.key, 5)
        if(url != ""){
          ccImage.url = url
          ccImage.isTempImage = false
          ccImage.type = 5
          ccImage.save()
      }else{
        return err
      }


      }
    
  }
  next()
});



StudentSchema.methods.getCourses = async function () {
  return Promise
    .resolve(
      this.model('StudentLists')
        .find({ list: this._id })
        .select('_id')
        .then(stLists => stLists.map(stList => stList._id))
    )
    .then(stLists => {
      return Promise
        .all(stLists.map(stList => {
          return this.model('Groups')
            .findOne({ studentList: stList })
            .select('course')
            .then(group => group ? group.course : null)
        }))
    })
    .then(courses => courses.filter(course => course))
}

StudentSchema.methods.toTalks = function () {
  const data = {
    registerDate: this.registerDate,
    name: `${this.name} ${this.lastName} ${this.secondLastName}`,
    email: this.email,
    phoneNumbers: `${this.phoneNumber} | ${this.secondPhoneNumber}`,
    calledBy: this.calledBy
  }

  return data
}

StudentSchema.methods.toSimple = function () {
  const data = {
    _id: this._id,
    name: this.name,
    lastName: this.lastName,
    secondLastName: this.secondLastName,
    email: this.email,
    birthDate: this.birthDate,
    materialFolios: this.materialFolios,
    address: this.address,
    postalCode: this.postalCode,
    phoneNumber: this.phoneNumber,
    secondPhoneNumber: this.secondPhoneNumber,
    registeredBy: this.registeredBy,
    registerDate: this.registerDate,
    picture: this.picture,
    groups: this.groups,
    allowed: this.allowed,
    comipemsCourses: this.comipemsCourses || [],
    comipemsGroups: this.comipemsGroups || [],
    comipemsOrder: this.comipemsOrder || [],
  }

  return data
}

StudentSchema.methods.toBalance = async function () {
  const payments = await PaymentModel.find({
    student: this._id
  })
  const data = this.toSimple()

  data.payments = payments

  const balance = { payments: {}, costs: {} }
  let discounts, total, costs

  discounts = 0.0
  total = 0.0
  costs = 0.0

  const groups = await GroupModel.find({
    _id: { $in: this.groups }
  })

  const courseIds = groups.map(group => group.course)

  const courses = await CourseModel.find({
    _id: { $in: courseIds }
  })

  courses.forEach(course => {
    costs += course.price
  })

  payments.forEach(payment => {
    discounts += payment.discount
    total += payment.amount
  })

  balance.payments.discounts = discounts
  balance.payments.total = total
  balance.costs.total = costs

  const student = _.extend(data, { balance })
  return student
}

StudentSchema.methods.toProfile = function () {
  const student = {
    _id: this._id,
    registerDate: this.registerDate,
    name: this.name,
    lastName: this.lastName,
    secondLastName: this.secondLastName,
    email: this.email,
    address: this.address,
    birthDate: this.birthDate,
    phoneNumber: this.phoneNumber,
    secondPhoneNumber: this.secondPhoneNumber,
    freeLessonGroups: this.freeLessonGroups,
    paymentsArranges: this.paymentsArranges,
    postalCode: this.postalCode,
    fbToken: this.fbToken,
    fbUser: this.fbUser,
    financial: this.balance,
    picture: this.picture
  }

  return student
}

StudentSchema.methods.saveBalance = async function () {
  const stBalance = await this.toBalance()
  this.balance = stBalance.balance
  this.debt = (stBalance.balance.payments.total + stBalance.balance.payments.total) <
    stBalance.balance.costs.total

  await this.save()
}

StudentSchema.statics.findOneToTalk = async function (id) {
  const student = await this.findOne({ _id: id })
  if (!student) {
    return null
  }

  const comments = await StudentCommentsModel
    .findOne({ student: id })
    .then(stComments => {
      return (stComments && stComments.comments.length)
        ? stComments.comments.sort((a, b) => b.date - a.date)
        : []
    })

  let calledBy = {}
  if (student.calledBy && student.calledBy.user) {
    const user = await UserModel.findOne({ _id: student.calledBy.user })
    calledBy.date = student.calledBy.date
    calledBy.user = `${user.name} ${user.lastName} ${user.secondLastName}`
  }

  const data = {
    _id: student._id,
    registerDate: student.registerDate,
    name: `${student.name} ${student.lastName} ${student.secondLastName}`,
    email: student.email,
    phoneNumbers: `${student.phoneNumber} | ${student.secondPhoneNumber}`,
    calledBy,
    comments
  }

  return data
}

// Configuración de mongoosastic
StudentSchema.plugin(mongoosastic, {
  esClient: esClient,
  index: 'students',
});

const Students = db.model('Students', StudentSchema, 'Students');

module.exports = Students
