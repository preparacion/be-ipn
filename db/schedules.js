/**
 * db/schedules.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ScheduleSchema = new Schema({
  name: { type: String, required: true },
  day: { type: String },
  oldId: { type: String },
  startHour: { type: String },
  endHour: { type: String },
  aliasName: [{ type: String }],
  schedule: [{ type: Schema.Types.ObjectId, ref: 'Schedules' }],
  schedulesList: [
    {
      day: Number,
      startHour: String,
      endHour: String
    }
  ]
}, { versionKey: false })

ScheduleSchema.methods.getChilds = async function () {
  if (this.schedule.length === 1) {
    return [this.toCalendar()]
  }
}

ScheduleSchema.methods.toCalendar = function () {
  return {
    _id: this._id,
    name: this.name,
    alias: this.aliasName[0],
    startHour: formatHour(this.startHour),
    endHour: formatHour(this.endHour),
    dayName: this.day
  }
}

const Schedules = db.model('Schedules', ScheduleSchema, 'Schedules')

function formatHour (hour) {
  let arrHour = hour.split('')
  arrHour.splice(-2, 0, ':')

  const str = arrHour.join('')
  const arrStr = str.split(':')

  return {
    string: str,
    minutes: arrStr[1],
    hours: arrStr[0]
  }
}

module.exports = Schedules
