/**
 * db/shuffle.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ShuffleSchema = new Schema({
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  group: { type: Schema.Types.ObjectId, ref: 'Groups' },
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  phase: { type: Schema.Types.ObjectId, ref: 'Phase' },
  schedule: { type: Schema.Types.ObjectId, ref: 'Schedules' },
  taken: { type: Boolean, default: false },
  paid: { type: Boolean, default: false }
}, { versionKey: false })

const Shuffle = db.model('Shuffle', ShuffleSchema, 'Shuffle')

module.exports = Shuffle
