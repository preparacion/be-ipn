/**
 * db/comBucketLists.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ComBucketListSchema = new Schema({
  name: { type: String },
  list: [{ type: Schema.Types.ObjectId, ref: 'Students' }],
  deleted: { type: Boolean, default: false },
  delDate: { type: Date }
}, { versionKey: false })

const ComBucketList = db.model('ComBucketList', ComBucketListSchema, 'ComBucketList')

module.exports = ComBucketList
