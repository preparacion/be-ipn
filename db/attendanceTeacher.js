/**
 * db/attendanceTeacher.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const AttendanceTeacherSchema = new Schema({
  teacher: { type: Schema.Types.ObjectId, ref: 'Users' },
  attendance: [
    {
      group: { type: Schema.Types.ObjectId, ref: 'Groups' },
      date: { type: Date, default: new Date() },
      attendance: { type: Boolean }
    }
  ]
}, { versionKey: false })

const AttendanceTeacher = db.model('AttendanceTeacher', AttendanceTeacherSchema, 'AttendanceTeacher')

module.exports = AttendanceTeacher
