/**
 * db/talks.js
 *
 * @description ::
 * @docs        :: None
 */
const _ = require('lodash')
const db = require('./db')
const Schema = db.Schema

const ClassRoomsModel = require('./classRooms')
const LocationModel = require('./locations')
const TalkStudentListModel = require('./talkStList')

const TalksSchema = new Schema({
  name: { type: String, required: true },
  date: { type: Date },
  active: { type: Boolean, default: true },
  quota: { type: Number },
  creationDate: { type: Date, default: Date.now() },
  courseLevel: { type: Schema.Types.ObjectId, ref: 'CourseLevels' },
  classRoom: { type: Schema.Types.ObjectId, ref: 'ClassRooms' },
  teacher: { type: Schema.Types.ObjectId, ref: 'Users' },
  talkStudentList: { type: Schema.Types.ObjectId, ref: 'TalkStudentLists' },
  isFeria: {type: Boolean}, 
  filter: {type: String},
  schedules: [
    {
      day: Number,
      startHour: String,
      endHour: String
    }
  ]
}, { versionKey: false })

TalksSchema.methods.toPopulate = async function () {
  const talk = {
    _id: this._id,
    name: this.name,
    date: this.date,
    active: this.active,
    quota: this.quota,
    creationDate: this.creationDate,
    courseLevel: this.courseLevel,
    classRoom: this.classRoom,
    teacher: this.teacher,
    talkStudentList: this.talkStudentList,
    schedules: this.schedules,
    isFeria: this.isFeria
  }
  const talkStudentList = await TalkStudentListModel.findOne({
    _id: this.talkStudentList
  })
  if (talkStudentList) {
    talk.studentListLength = talkStudentList.list.length
    talk.percentage = (talkStudentList.list.length / this.quota) * 100
  }

  if (this.classRoom) {
    const classRoom = await ClassRoomsModel.findOne({ _id: this.classRoom })
    if (classRoom) {
      talk['classRoomInfo'] = classRoom
      if (classRoom.location) {
        const location = await LocationModel.findOne({ _id: classRoom.location })
        if (location) {
          talk['locationInfo'] = location
        }
      }
    }
  }
  return talk;
}

const Talks = db.model('Talks', TalksSchema, 'Talks')
module.exports = Talks
