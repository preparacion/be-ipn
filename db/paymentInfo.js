/**
 * db/paymentInfo.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const PaymentInfoSchema = new Schema({
  cardNumber: { type: String },
  expirationDate: { type: String },
  cardType: { type: String },
  cvv: { type: String },
  student: { type: Schema.Types.ObjectId, ref: 'Students' }
}, { versionKey: false })

PaymentInfoSchema.virtual('id').get(() => {
  return this._id
})

const PaymentInfo = db.model('PaymentInfo', PaymentInfoSchema, 'PaymentInfo')

module.exports = PaymentInfo
