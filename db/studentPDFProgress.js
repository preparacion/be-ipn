const db = require('./db')
const Schema = db.Schema

const StudentPDFProgressSchema = new Schema({
    student: { type: Schema.Types.ObjectId, ref: 'Student', required: true },
    pdf: { type: Schema.Types.ObjectId, ref: 'PDFs', required: true },
    playlist: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    lastPage: { type: Number, default: 0 },
    viewedPages: [{ type: Number }],
    progress: { type: Number, default: 0 },
    viewCount: { type: Number, default: 0 },
    lastViewedAt: { type: Date }
}, { versionKey: false })

const StudentPDFProgress = db.model('StudentPDFProgress', StudentPDFProgressSchema, 'StudentPDFProgress')