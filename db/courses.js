/**
 * db/courses.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Images = require('./images')
const S3 = require('../lib/s3')
const Schema = db.Schema

const CourseSchema = new Schema({
  name: { type: String, required: true },
  dependency: { type: String },
  duration: { type: String },
  price: { type: Number },
  priceValidationOnlineMaterial: { type: Number },
  priceByLesson: { type: Number },
  active: { type: Boolean, default: true },
  activeExternal: { type: Boolean, default: false },
  isActiveAppoiments: { type: Boolean, default: false },
  creationDate: { type: Date, default: Date.now() },
  image: { type: Schema.Types.ObjectId, ref: 'Images' },
  backgroundColor: { type: String },
  type: { type: String },
  discounts: [{
    course: { type: Schema.Types.ObjectId, ref: 'Courses' },
    discount: { type: Number }
  }],
  children: [{ type: Schema.Types.ObjectId }],
  //Comipems Identifier 
  isParent: { type: Boolean }, // for Comipems
  isChildren: { type: Boolean }, // for Comipems
  parent: { type: Schema.Types.ObjectId }, // for Comipems

  courseType: { type: Schema.Types.ObjectId, ref: 'CourseTypes' },
  courseLevel: { type: Schema.Types.ObjectId, ref: 'CourseLevels' },
  stage: { type: Schema.Types.ObjectId, ref: 'Stages' },
  schedulesAppoimentList: [{ type: Schema.Types.Mixed }],
  topicByDayList: [{ type: Schema.Types.Mixed }],
  topicList: [{ type: Schema.Types.ObjectId, ref: 'CourseTopics' }],

  //Material
  onlineAllowedMaterial: [{ type: Schema.Types.ObjectId, ref: 'Courses' }],
  specificAllowedPlaylist: [{ type: Schema.Types.ObjectId, ref: 'Playlist' }],

  //New Material
  childrenPlaylist: [{ type: Schema.Types.ObjectId, ref: 'Playlist' }],
  videos: [{ type: Schema.Types.ObjectId, ref: 'Videos' }],
  quiz: [{ type: Schema.Types.ObjectId, ref: 'Quiz' }],
  contentPlaylist: [{ type: Schema.Types.Mixed }],

  //Livestream
  liveStreamChannel: { type: Schema.Types.ObjectId, ref: 'LiveChannels' },
  liveStreamUrl: { type: String },
  activeLiveStream: { type: Boolean, default: false },

}, { versionKey: false })


/***
 * begin of Functions of course model database
 */
CourseSchema.pre('findOneAndUpdate', async function (next) {
  let id = this.getQuery()._id
  const actualInfo = await Courses.findOne({ _id: id })
  let updateData = this._update.$set
  if (updateData.image !== null && updateData.image !== undefined) {
    if (actualInfo.image && updateData.image || actualInfo.image && !updateData.image) {
      Images.deleteOne({ _id: updateData.image })
    }
    if (updateData.image) {
      const courseImage = await Images.findOne({ _id: updateData.image })
      if (courseImage && courseImage.isTempImage) {
        const url = await S3.moveImageTemporal(courseImage.key, 4)
        courseImage.url = url
        courseImage.isTempImage = false
        courseImage.type = 1
        courseImage.save()
      }
    }
  }
})

CourseSchema.methods.getAvailableGroups = async function () {
  const groupsCount = await this.model('Groups').find({
    course: this._id,
    $or: [
      {
        deleted: { $exists: false }
      },
      {
        deleted: { $eq: false }
      }
    ]
  }).countDocuments()

  const data = {
    _id: this._id,
    name: this.name,
    image: this.image,
    dependency: this.dependency,
    duration: this.duration,
    price: this.price,
    priceValidationOnlineMaterial: this.priceValidationOnlineMaterial,
    priceByLesson: this.priceByLesson,
    active: this.active,
    activeExternal: this.activeExternal,
    creationDate: this.creationDate,
    type: this.type,
    groups: groupsCount,
    isParent: this.isParent,
    isChildren: this.isChildren,
    schedules: this.schedules,
    courseLevel: this.courseLevel || null,
    courseType: this.courseType || null,
    stage: this.stage,
    discounts: this.discounts,
    isActiveAppoiments: this.isActiveAppoiments,
    schedulesAppoimentList: this.schedulesAppoimentList,
    topicByDayList: this.topicByDayList,
    topicList: this.topicList,
    liveStreamChannel: this.liveStreamChannel,
    liveStreamUrl: this.liveStreamUrl,
    activeLiveStream: this.activeLiveStream
  }
  return data
}

CourseSchema.methods.getChildren = async function () {
  if (this.children && this.children.length) {
    const children = await Promise
      .all(this.children.map(id => {
        return this.model('Courses')
          .findOne({ _id: id })
      }))
      .then(courses => {
        return Promise.all(courses.map(course => {
          return course.getAvailableGroups()
        }))
      })
    const cObject = await this.getAvailableGroups()
    cObject.children = children
    return cObject
  }
  const courseWGroups = await this.getAvailableGroups()
  return courseWGroups
}

CourseSchema.methods.getPrice = async function () {
  return this.price || 0
}

CourseSchema.methods.updatePrice = async function () {
  return this
}

const Courses = db.model('Courses', CourseSchema, 'Courses')

module.exports = Courses
