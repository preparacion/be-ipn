/**
 * db/groups.js
 *
 * @description ::
 * @docs        :: None
 */


const db = require('./db')
const Schema = db.Schema

const mongoosastic = require('mongoosastic');
const esClient = require('../lib/es-client')()

const CourseModel = require('./courses')
const ScheduleModel = require('./schedules')
const LocationModel = require('./locations')
const ClassRoomsModel = require('./classRooms')
const StudentListModel = require('./studentLists')

const GroupSchema = new Schema({
  name: { type: String, required: true, es_indexed: true },
  startDate: { type: Date, es_indexed: true },
  endDate: { type: Date, es_indexed: true },
  active: { type: Boolean, es_indexed: true },
  activeLanding: { type: Boolean, es_indexed: true },
  isFreeLesson: { type: Boolean, default: false },
  groupLinkFreeLesson: { type: Schema.Types.ObjectId, ref: 'Groups' },
  quota: { type: Number, es_indexed: true },
  available: { type: Number },
  special: { type: Boolean },
  etapa: { type: Number }, // only 1, 2, 3, 4
  mate: { type: String },
  fisica: { type: String },
  quimica: { type: String },
  calculo: { type: String },
  comment: { type: String },
  deleted: { type: Boolean, es_indexed: true },
  delDate: { type: Date, es_indexed: true },
  comipemsPhase: { type: String },
  isBucket: { type: Boolean, es_indexed: true },
  schedules: [
    {
      day: Number,
      startHour: String,
      endHour: String
    }
  ],
  buckets: [{ type: Schema.Types.ObjectId, ref: 'Groups' }],
  teacher: { type: Schema.Types.ObjectId, ref: 'Users', es_indexed: true },
  phase: { type: Schema.Types.ObjectId, ref: 'Phase', es_indexed: true },
  course: { type: Schema.Types.ObjectId, ref: 'Courses', es_indexed: true },
  classRoom: { type: Schema.Types.ObjectId, ref: 'ClassRooms', es_indexed: true },
  studentList: { type: Schema.Types.ObjectId, ref: 'StudentLists' }
}, { versionKey: false })

GroupSchema.methods.getAvailable = async function () {
  let available = 0
  if (this.quota && this.studentList) {
    const studentList = await StudentListModel.findOne({ _id: this.studentList })
    if (studentList && studentList.list) {
      return (this.quota > studentList.list.length) ? this.quota - studentList.list.length : 0
    }
  }
  return available
}

GroupSchema.methods.toAvailable = async function () {
  let available = await this.getAvailable()

  const group = {
    _id: this._id,
    active: this.active,
    quota: this.quota,
    name: this.name,
    schedule: this.schedule,
    available
  }

  return group
}

GroupSchema.methods.toPopulate = async function () {
  const group = {
    _id: this._id,
    startDate: this.startDate,
    endDate: this.endDate,
    active: this.active,
    activeLanding: this.activeLanding,
    quota: this.quota,
    name: this.name,
    comipemsPhase: this.comipemsPhase,
    course: this.course,
    teacher: this.teacher,
    studentList: this.studentList,
    classRoom: this.classRoom,
    schedule: this.schedule,
    schedules: this.schedules
  }

  const studentList = await StudentListModel.findOne({
    _id: this.studentList
  })

  if (studentList) {
    group.studentListLength = studentList.list.length
    group.percentage = (studentList.list.length / this.quota) * 100
  }

  if (this.buckets && this.buckets.length) {
    const buckets = await Promise
      .all(this.buckets.map(groupId => {
        return this.model('Groups')
          .findOne({ _id: groupId })
      }))
      .then(groups => groups.filter(group => group))
      .then(groups => {
        return Promise
          .all(groups.map(group => group.toPopulate()))
      })
    group['buckets'] = buckets
  }

  if (this.course) {
    const course = await CourseModel.findOne({ _id: this.course })
    if (course) {
      group['courseInfo'] = course
    }
  }

  if (this.schedule) {
    const schedule = await ScheduleModel.findOne({ _id: this.schedule })
    if (schedule) {
      group['scheduleInfo'] = schedule
    }
  }

  if (this.classRoom) {
    const classRoom = await ClassRoomsModel.findOne({ _id: this.classRoom })
    if (classRoom) {
      group['classRoomInfo'] = classRoom
      if (classRoom.location) {
        const location = await LocationModel.findOne({ _id: classRoom.location })
        if (location) {
          group['locationInfo'] = location
        }
      }
    }
  }

  group['available'] = await this.getAvailable()

  return group
}

GroupSchema.methods.toCalendar = async function () {
  const schedule = await ScheduleModel.findOne({
    _id: this.schedule
  })
  return {
    _id: this._id,
    name: this.name,
    startDate: this.startDate,
    endDate: this.endDate,
    day: schedule.day,
    startHour: schedule.startHour,
    endHour: schedule.endHour
  }
}

GroupSchema.plugin(mongoosastic, {
  index: 'groups',
  client: esClient
})

const Groups = db.model('Groups', GroupSchema, 'Groups')

module.exports = Groups
