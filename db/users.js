/**
 * db/users.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema
const Images = require('./images')
const S3 = require('../lib/s3')


const UserSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  lastName: { type: String },
  secondLastName: { type: String },
  email: { type: String },
  hashed_password: { type: String },
  birthDate: { type: Date },
  address: { type: String },
  postalCode: { type: String },
  phoneNumber: { type: String },
  secondPhoneNumber: { type: String },
  isProfessor: { type: Boolean, default: false },
  registerDate: { type: Date, default: new Date() },
  oldId: { type: String },
  root: { type: Boolean, default: false },
  roles: [{ type: Schema.Types.ObjectId, ref: 'Roles' }],
  picture: { type: Schema.Types.ObjectId, ref: 'Images' }
}, { versionKey: false })


UserSchema.pre('findOneAndUpdate', async function (next) {
  let data = this._update.$set
  if (data.picture) {
    let imagen = data.picture
      const ccImage = await Images.findOne({
        _id: imagen
      })
      if (ccImage && ccImage.isTempImage) {

        const url = await S3.moveImageTemporal(ccImage.key, 5)
        if(url != ""){
          ccImage.url = url
          ccImage.isTempImage = false
          ccImage.type = 5
          ccImage.save()
      }else{
        return err
      }
      }
  }
  next()
});



UserSchema.methods.toSimple = function () {
  return {
    _id: this._id,
    name: this.name,
    lastName: this.lastName,
    secondLastName: this.secondLastName,
    email: this.email,
    roles: this.roles
  }
}

const Users = db.model('Users', UserSchema, 'Users')

module.exports = Users
