/**
 * db/courseTopics.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CourseTopicsSchema = new Schema({
    name: { type: String, required: true },
    parent: { type: Schema.Types.ObjectId, ref: 'Courses' },
    create_at: { type: Date, default: new Date() }
}, { versionKey: false })

const CourseTopics = db.model('CourseTopics', CourseTopicsSchema, 'CourseTopics')

module.exports = CourseTopics
