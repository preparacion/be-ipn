/**
 * db/db.js
 *
 * @description :: Describes the db mongoose.
 * @docs        :: https://mongoosejs.com/
 */
const mongoose = require('mongoose');

const { mongodbURI, mongodbUser, mongodbPass } = require('../config');

const options = (mongodbUser && mongodbPass)
  ? { user: mongodbUser, pass: mongodbPass } : {};

mongoose.connect(mongodbURI, options)
  .then(() => console.log('db connected'))
  .catch(err => console.error('connection error:', err));

module.exports = mongoose;
