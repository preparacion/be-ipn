/**
 * db/attendanceList.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const AttendanceListSchema = new Schema({
  date: { type: Date, default: new Date() },
  students: [{ type: Schema.Types.ObjectId, ref: 'Students' }],
  teacher: { type: Schema.Types.ObjectId, ref: 'Users' },
  studentList: { type: Schema.Types.ObjectId, ref: 'StudentLists' },
  group: { type: Schema.Types.ObjectId, ref: 'Groups' },
  studentInfo: [{ type: Schema.Types.Mixed }]
}, { versionKey: false })

const AttendanceList = db.model('AttendanceList', AttendanceListSchema, 'AttendanceList')

module.exports = AttendanceList
