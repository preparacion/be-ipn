/**
 * db/reports.js
 *
 * @description :: Reports created when a video is broken or has wrong info
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ReportSchema = new Schema({
  date: { type: Date, default: new Date() },
  reason: { type: String },
  comments: [{ type: String }],
  seen: [
    {
      by: { type: Schema.Types.ObjectId, ref: 'Users' },
      date: { type: Date, default: new Date() }
    }
  ],
  status: { type: String },
  active: { type: Boolean, default: true },
  video: { type: Schema.Types.ObjectId, ref: 'Videos' },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  playlist: { type: Schema.Types.ObjectId, ref: 'Playlist' },
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  student: { type: Schema.Types.ObjectId, ref: 'Student' }
}, { versionKey: false })

const Reports = db.model('Reports', ReportSchema, 'Reports')

module.exports = Reports
