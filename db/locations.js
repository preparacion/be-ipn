/**
 * db/locations.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const LocationsSchema = new Schema({
  name: { type: String, required: true },
  city: { type: String },
  latitude: { type: String },
  longitude: { type: String },
  oldId: { type: String },
  opening: { type: String },
  closing: { type: String },
  colonia: { type: String },
  number: { type: String },
  street: { type: String },
  postalCode: { type: String },
  district: { type: String }
}, { versionKey: false })

LocationsSchema.virtual('id').get(() => {
  return this._id
})

const Locations = db.model('Locations', LocationsSchema, 'Locations')

module.exports = Locations
