// db/pdfs.js
const db = require('./db')
const Schema = db.Schema
const S3 = require('../lib/s3')

const PDFSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    key: { type: String }, // S3 key
    url: { type: String },
    thumbnail: { 
        key: { type: String },
        url: { type: String }
    },
    totalPages: { type: Number, default: 0 },
    playlist: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    order: { type: Number },
    active: { type: Boolean, default: true },
    type: { type: Number }
}, { versionKey: false })

// Pre-save hook para manejar thumbnails y URLs
PDFSchema.pre('save', async function() {
    if (this.key && !this.url) {
        this.url = `${awsCloudfromEndpointPDF}${this.key}`
    }
})

// Pre-delete hook para limpiar archivos de S3
PDFSchema.pre('deleteOne', async function() {
    const pdf = await PDFs.findOne(this.getQuery())
    if (pdf && pdf.key) {
        await S3.deletePDF(pdf.key)
    }
    if (pdf && pdf.thumbnail && pdf.thumbnail.key) {
        await S3.deleteImage(pdf.thumbnail.key, 6) // 6 será el nuevo tipo para thumbnails de PDF
    }
})

const PDFs = db.model('PDFs', PDFSchema, 'PDFs')

module.exports = PDFs
