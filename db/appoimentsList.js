/**
 * db/appoimentsList.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const appoimentsListSchema = new Schema({
    date: { type: Date, default: new Date() },
    maxQuota: { type: Schema.Types.Number, default: 10 },
    appoiments: [{ type: Schema.Types.ObjectId, ref: 'Appoiments' }],
    course: { type: Schema.Types.ObjectId, ref: 'Courses' }
}, { versionKey: false })

const AppoimentsList = db.model('AppoimentsList', appoimentsListSchema, 'AppoimentsList')

module.exports = AppoimentsList