/**
 * db/sms.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const SmsSchema = new Schema({
  message: { type: String, required: true },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  group: { type: Schema.Types.ObjectId, ref: 'Groups' },
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  date: { type: Date }
}, { versionKey: false })

const Sms = db.model('Sms', SmsSchema, 'Sms')

module.exports = Sms
