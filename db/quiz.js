/**
 * db/quiz.js
 *
 * @description :: Describe the database model for quiz 
 * @docs        :: 
 */
const db = require('./db')
const Videos = require('./videos')
const QuizQuestions = require('./quizQuestion')
const Schema = db.Schema

const QuizSchema = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    order: { type: Number },
    questionsPerQuiz: { type: Number },
    studentAttempts: { type: Number },
    limitTime: { type: Schema.Types.Number },
    order: { type: Number },
    parent: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    questionCollection: [{ type: Schema.Types.ObjectId, ref: 'QuizQuestion' }],
    videoBucket: [{ type: Schema.Types.ObjectId, ref: 'Videos' }],
    active: { type: Boolean, default: true },
    random: { type: Boolean, default: false },
    course: { type: Schema.Types.ObjectId, ref: 'Courses' },
    createdBy: { type: Schema.Types.ObjectId, ref: 'Users' },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'Users' },
    registerDate: { type: Date, default: new Date() },
    quizType: { type: Number, default: 1 },
    type: { type: Number },
}, { versionKey: false })


QuizSchema.pre('remove', async function (next) {
    await QuizQuestions.deleteMany({
        _id: this.questionCollection
    });

    await Videos.deleteMany({
        _id: this.videoBucket
    });

    next()
});

const Quiz = db.model('Quiz', QuizSchema, 'Quiz')

module.exports = Quiz
