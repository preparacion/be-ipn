/**
 * db/attendanceStudent.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const AttendanceStudentSchema = new Schema({
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  attendance: [
    {
      group: { type: Schema.Types.ObjectId, ref: 'Groups' },
      date: { type: Date, default: new Date() },
      attendance: { type: Boolean }
    }
  ]
}, { versionKey: false })

const AttendanceStudent = db.model('AttendanceStudent', AttendanceStudentSchema, 'AttendanceStudent')

module.exports = AttendanceStudent
