/**
 * db/socketConnection.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const SocketConnectionsSchema = new Schema({
    idSocket: { type: Schema.Types.String },
    ip: { type: Schema.Types.String },
    student: { type: Schema.Types.ObjectId, ref: 'Students' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    date: { type: Date, default: new Date() },
}, { versionKey: false })

const SocketConnection = db.model('SocketConnection', SocketConnectionsSchema, 'SocketConnection')

module.exports = SocketConnection