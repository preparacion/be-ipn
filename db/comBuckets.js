/**
 * db/comBuckets.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const ComBucketsSchema = new Schema({
  name: { type: String, required: true },
  startDate: { type: Date },
  endDate: { type: Date },
  active: { type: Boolean },
  comment: { type: String },
  teacher: { type: Schema.Types.ObjectId, ref: 'Users' },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  comBucketList: { type: Schema.Types.ObjectId, ref: 'ComBucketList' },
  schedules: [
    {
      day: Number,
      startHour: String,
      endHour: String
    }
  ]
}, { versionKey: false })

const ComBuckets = db.model('ComBuckets', ComBucketsSchema, 'ComBuckets')

module.exports = ComBuckets
