/**
 * db/quiz.js
 *
 * @description :: Describe the database model for quiz attempt for student
 * @docs        :: 
 */
const db = require('./db')
const Schema = db.Schema

const QuizAttemptSchema = new Schema({
    quizParent: { type: Schema.Types.ObjectId, ref: 'Quiz' },
    student: { type: Schema.Types.ObjectId, ref: 'Students' },
    date: { type: Date },
    status: { type: String },
    questionCollection: [{ type: Schema.Types.ObjectId, ref: 'QuizQuestion' }],
    answerCollection: [{ type: Schema.Types.ObjectId, ref: 'QuizAttemptAnswer' }],
    lastAnswerIndex: { type: Number },
    percentageAnswer: { type: Number },
    percentageResult: { type: Number },
    totalQuestions: { type: Number },
    rightQuestions: { type: Number },
    type: { type: Number },
}, { versionKey: false })


QuizAttemptSchema.pre('save', async function () {
})


const QuizAttempt = db.model('QuizAttempt', QuizAttemptSchema, 'QuizAttempt')

module.exports = QuizAttempt
