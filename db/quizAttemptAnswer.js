/**
 * db/quizAttemptAnswer.js
 *
 * @description :: Describe the database model for quiz attempt for student
 * @docs        :: 
 */
const db = require('./db')
const Schema = db.Schema

const QuizAttemptAnswerSchema = new Schema({
    quizAttemptParent: { type: Schema.Types.ObjectId, ref: 'QuizAttempt' },
    student: { type: Schema.Types.ObjectId, ref: 'Student' },
    question: { type: Schema.Types.ObjectId, ref: 'QuizQuestion' },
    date: { type: Date },
    answers: [{ type: Schema.Types.String }],
    answerCorrects: [{ type: Schema.Types.String }],
    isCorrect: { type: Schema.Types.Boolean },
    type: { type: Number },
}, { versionKey: false })


QuizAttemptAnswerSchema.pre('save', async function () {
    // if (!this.videos) {
    //     this.videos = []
    // }
})


const QuizAttemptAnswer = db.model('QuizAttemptAnswer', QuizAttemptAnswerSchema, 'QuizAttemptAnswer')

module.exports = QuizAttemptAnswer
