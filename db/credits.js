/**
 * db/credits.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const CreditSchema = new Schema({
  amount: { type: Number },
  date: { type: Date, default: Date.now },
  student: { type: Schema.Types.ObjectId, ref: 'Students' }
}, { versionKey: false })

CreditSchema.virtual('id').get(() => {
  return this._id
})

const Credits = db.model('Credits', CreditSchema, 'Credits')

module.exports = Credits
