/**
 * db/playlists.js
 *
 * @description ::
 * @docs        :: None
 */
 const db = require('./db')
 const S3 = require('../lib/s3')
 const Schema = db.Schema
 
 const VideoModel = require('./videos')
 const QuizModel = require('./quiz')
 const CourseModel = require('./courses')
 const Images = require('./images')
 
 const PlaylistSchema = new Schema({
   name: { type: String, required: true },
   description: { type: String },
   order: { type: Number },
   image: { type: Schema.Types.ObjectId, ref: 'Images' },
   backgroundColor: { type: String },
   parent: { type: Schema.Types.ObjectId, ref: 'Playlist' },
    childs: [{
      type: { type: String, enum: ['video', 'playlist'] },
      id: { type: Schema.Types.Mixed },
      order: { type: Number }
    }],
   videosCollection: [{ type: Schema.Types.Mixed }],
   contentPlaylist: [{ type: Schema.Types.Mixed }],
   courseInfo: { type: Schema.Types.Mixed },
   course: { type: Schema.Types.ObjectId, ref: 'Courses' },
   active: { type: Boolean, default: true },
   totalItems: { type: Number },
   //JUST TO RETURN PARAMETERS
   type: { type: Number },
   progress: { type: Number },
   isLastItemHere: { type: Boolean },
 }, { versionKey: false })
 
 /**
  * METHODS
  */
 PlaylistSchema.methods.toPopulate = async function (studentId = undefined) {
 
 
   if (this.course) {
     const course = await CourseModel.findOne({ _id: this.course })
     if (course) {
       this['courseInfo'] = course
     }
   }
   //Get Video Section 
   let contentArray = []
   const videos = await this.model('Videos')
     .find({ playlist: this._id })
   if (videos.length) {
     await videos.forEach(function (element) {
       element["type"] = 2;
       contentArray.push(element)
     });
   }
   let quizSearch = {
     parent: this._id,
   }
   if (studentId !== undefined) {
     quizSearch.active = true
   }
   const quiz = await this.model('Quiz')
     .find(quizSearch)
   if (quiz.length) {
     await quiz.forEach(function (element) {
       element["type"] = 3;
       contentArray.push(element)
     });
   }

    let pdfSearch = {
    playlist: this._id
  }
  if (studentId !== undefined) {
    pdfSearch.active = true
  }
  const pdfs = await this.model('PDFs')
    .find(pdfSearch)
  if (pdfs.length) {
    const pdfProgress = studentId ? 
      await this.model('StudentPDFProgress').find({
        student: studentId,
        pdf: { $in: pdfs.map(pdf => pdf._id) }
      }) : [];

    await pdfs.forEach(function (element) {
      element.type = 4; // Tipo 4 para PDFs
      if (studentId) {
        const progress = pdfProgress.find(p => String(p.pdf) === String(element._id));
        if (progress) {
          element["progress"] = progress.progress;
          element["lastPage"] = progress.lastPage;
          element["viewCount"] = progress.viewCount;
          element["lastViewedAt"] = progress.lastViewedAt;
        }
      }
      contentArray.push(element);
    });
  }

 
   //Get Playlist section return all playlist with parent id.
   let playlistSearch = {
     parent: this._id,
   }
   if (studentId !== undefined)
     playlistSearch.active = true
   const children = await this.model('Playlist')
     .find(playlistSearch)
     .populate('image')
   if (children && children.length) {
     if (studentId !== undefined) {
       const progress = await this.model('PlaylistProgress').find({
         parent: this._id,
         student: studentId
       })
       await children.forEach(function (element) {
         let index = progress.findIndex(item => String(item.playlist) == String(element._id))
         if (index >= 0) {
           let percentage = (progress[index].finishItemsCount * 100) / element.totalItems
           if (progress[index].progress !== percentage) {
             progress[index].progress = isNaN(percentage) ? 0 : percentage
             progress[index].save()
           }
           element["progress"] = progress[index].progress
           element["isLastItemHere"] = progress[index].isLastItemHere
         }
         element["type"] = 1;
         contentArray.push(element)
       });
     } else {
       await children.forEach(function (element) {
         element["type"] = 1;
         contentArray.push(element)
       });
     }
   }
   contentArray.sort(function (a, b) {
     if (a.order) {
       if (b.order) {
         if (a.order < b.order) return -1;
         if (a.order > b.order) return 1;
       } else {
         return 1;
       }
     } else {
       return -1;
     }
     return 0;
   });
   this['contentPlaylist'] = contentArray
   return this
 }
 
 // Will deprecated
 PlaylistSchema.methods.getChildren = async function () {
   const children = await this.model('Playlist').find({
     parent: this._id
   }).sort({ order: 1 })
 
   if (!children || !children.length) {
     const populated = await this.toPopulate()
     return populated
   }
 
   const childs = await Promise.all(
     children.map(c => c.getChildren())
   ).then(cs => cs)
 
   const populated = await this.toPopulate().then(pl => pl.toObject())
   populated.childs = childs
 
   return populated
 }
 
 PlaylistSchema.methods.getDataPopulate = async function () {
 
 }
 
 /**
  * END METHODS
  */
 
 PlaylistSchema.pre('save', async function () {
   if (!this.videos) {
     this.videos = []
   }
 })
 
 PlaylistSchema.pre('findOneAndUpdate', async function (next) {
   let id = this.getFilter()._id
   const actualInfo = await Playlist.findOne({ _id: id })
   let updateData = this._update.$set
   if (updateData.image !== null && updateData.image !== undefined) {
     if (actualInfo.image && updateData.image || actualInfo.image && !updateData.image) {
       Images.deleteOne({ _id: updateData.image })
     }
     if (updateData.image) {
       const courseImage = await Images.findOne({ _id: updateData.image })
       if (courseImage && courseImage.isTempImage) {
         const url = await S3.moveImageTemporal(courseImage.key, 3)
         courseImage.url = url
         courseImage.isTempImage = false
         courseImage.type = 1
         courseImage.save()
       }
     }
   } else {
     if (actualInfo.image) {
       Images.deleteOne({ _id: updateData.image })
     }
   }
   next()
 })
 
 PlaylistSchema.pre('deleteOne', async function () {
   //TODO remove all items in views 
 })
 
 const Playlist = db.model('Playlist', PlaylistSchema, 'Playlist')
 
 module.exports = Playlist