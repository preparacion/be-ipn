/**
 * db/quiz.js
 *
 * @description :: Describe the database model for quiz 
 * @docs        :: 
 */
const _ = require('lodash')
const db = require('./db')
const S3 = require('../lib/s3')
const Images = require('./images')
const Schema = db.Schema

const QuizQuestionSchema = new Schema({
    order: { type: Number },
    parent: { type: Schema.Types.ObjectId, ref: 'Quiz' },
    question: { type: String },
    questionFormula: { type: String },
    questionImage: { type: Schema.Types.ObjectId, ref: 'Images' },
    questionVideo: { type: Schema.Types.ObjectId, ref: 'Videos' },
    questionType: { type: Number },
    answerCollection: [{ type: Schema.Types.Mixed }],
    explanationVideo: [{ type: Schema.Types.ObjectId, ref: 'Videos' }],
    active: { type: Boolean, default: true },
    type: { type: Number },
}, { versionKey: false })

QuizQuestionSchema.pre('save', async function (next) {
    let data = this
    if (data.questionImage) {
        const questionImage = await Images.findOne({ _id: data.questionImage })
        if (questionImage && questionImage.isTempImage) {
            const url = await S3.moveImageTemporal(questionImage.key, 1)
            questionImage.url = url
            questionImage.isTempImage = false
            questionImage.type = 1
            questionImage.save()
        }
    }
    for (let i = 0; i < data.answerCollection.length; i++) {
        let answer = data.answerCollection[i]
        //IS image 
        if (answer.answerType == 2) {
            let answerImage = await Images.findOne({ _id: answer.answerImage })
            if (answerImage && answerImage.isTempImage) {
                const url = await S3.moveImageTemporal(answerImage.key, 2)
                answerImage.url = url
                answerImage.isTempImage = false
                answerImage.type = 2
                answerImage.save()
            }
        }
    }
    next()
});


QuizQuestionSchema.pre('findOneAndUpdate', async function (next) {
    const docToUpdate = await this.model.findOne({ _id: this.getQuery()._id });
    let data = await this.getUpdate().$set
    if (data.questionImage) {
        if (docToUpdate.questionImage!==undefined && data.questionImage !== String(docToUpdate.questionImage._id)) {
            await Images.deleteOne({ _id: docToUpdate.questionImage._id })
        }
        const questionImage = await Images.findOne({ _id: data.questionImage })
        if (questionImage && questionImage.isTempImage) {
            const url = await S3.moveImageTemporal(questionImage.key, 1)
            questionImage.url = url
            questionImage.isTempImage = false
            questionImage.type = 1
            questionImage.save()
        }
    }


    //ANSWER CODE

    let oldAnswerCollection = JSON.parse(JSON.stringify(docToUpdate.answerCollection))
    let actualAnswerCollection = JSON.parse(JSON.stringify(data.answerCollection))

    const removeElements = _.differenceBy(oldAnswerCollection, actualAnswerCollection, 'idAnswer')
    for (let i = 0; i < removeElements.length; i++) {
        let answer = removeElements[i]
        if (answer.answerType == 2) {
            await Images.deleteOne({ _id: answer.answerImage })
        }
    }

    const addElements = _.differenceBy(actualAnswerCollection, oldAnswerCollection, 'idAnswer')
    for (let i = 0; i < addElements.length; i++) {
        let answer = addElements[i]
        //IS image 
        if (answer.answerType == 2) {
            let answerImage = await Images.findOne({ _id: answer.answerImage })
            if (answerImage && answerImage.isTempImage) {
                const url = await S3.moveImageTemporal(answerImage.key, 2)
                answerImage.url = url
                answerImage.isTempImage = false
                answerImage.type = 2
                answerImage.save()
            }
        }
    }
    const editElements = _.differenceBy(actualAnswerCollection, removeElements.concat(addElements), 'idAnswer')
    for (let i = 0; i < editElements.length; i++) {
        let answer = editElements[i]
        //IS image 
        if (answer.answerType == 2) {
            let index = oldAnswerCollection.findIndex(x => x.idAnswer == answer.idAnswer)
            if (index !== -1) {

                if (oldAnswerCollection[index].answerImage && oldAnswerCollection[index].answerImage._id) {
                    if (oldAnswerCollection[index].answerImage._id !== answer.answerImage) {
                        await Images.deleteOne({ _id: oldAnswerCollection[index].answerImage })
                    }
                } else {
                    if (oldAnswerCollection[index].answerImage !== answer.answerImage) {
                        await Images.deleteOne({ _id: oldAnswerCollection[index].answerImage })
                    }
                }
                let answerImage = await Images.findOne({ _id: answer.answerImage })
                if (answerImage && answerImage.isTempImage) {
                    const url = await S3.moveImageTemporal(answerImage.key, 2)
                    answerImage.url = url
                    answerImage.isTempImage = false
                    answerImage.type = 2
                    answerImage.save()
                }

            }
        }
    }

    next()
});

//This will delete all image in database and s3 resource
QuizQuestionSchema.pre('deleteOne', async function (next) {
    const docToUpdate = await this.model.findOne(this.getQuery());
    if (docToUpdate) {
        if (docToUpdate.questionImage) {
            await Images.deleteOne({ _id: docToUpdate.questionImage })
        }
        if (docToUpdate.answerCollection) {
            for (let i = 0; i < docToUpdate.answerCollection.length; i++) {
                let answer = docToUpdate.answerCollection[i]
                if (answer.answerType == 2) {
                    await Images.deleteOne({ _id: answer.answerImage })
                }
            }
        }
    }
    next()
});

QuizQuestionSchema.pre('remove', async function (next) {
    console.log("QuizQuestionRemove")
    next()
});

QuizQuestionSchema.pre('deleteMany', async function (doc, next) {
    let query = this.getQuery()
    if (query) {
        let array = query._id
        if (array && array.length > 0) {
            for (let x = 0; x < array.length; x++) {
                //Here recibe an objectId element , change to string for use in the query 
                const docToUpdate = await this.model.findOne({ _id: array[x].toString() })
                if (docToUpdate) {
                    if (docToUpdate.questionImage) {
                        await Images.deleteOne({ _id: docToUpdate.questionImage })
                    }
                    if (docToUpdate.answerCollection) {
                        for (let i = 0; i < docToUpdate.answerCollection.length; i++) {
                            let answer = docToUpdate.answerCollection[i]
                            if (answer.answerType == 2) {
                                await Images.deleteOne({ _id: answer.answerImage })
                            }
                        }
                    }
                }
            }
        }
    }
    next()
});

const QuizQuestion = db.model('QuizQuestion', QuizQuestionSchema, 'QuizQuestion')

module.exports = QuizQuestion
