/**
 * db/deleteTables.js
 *
 * @description :: Deletes collections
 * @docs        :: TODO
 */
const ClassRoomModel = require('./classRooms')
const CourseModel = require('./courses')
const CreditModel = require('./credits')
const GroupModel = require('./groups')
const LocationModel = require('./locations')
const LogModel = require('./logs')
const PaymentInfoModel = require('./paymentInfo')
const PaymentModel = require('./payments')
const ScheduleModel = require('./schedules')
const StudentsListModel = require('./studentLists')
const PermissionModel = require('./permissions')
const RoleModel = require('./roles')
const StudentModel = require('./students')
const UserModel = require('./users')

const deleteCollections = async () => {
  await ClassRoomModel.remove({})
  await CourseModel.remove({})
  await CreditModel.remove({})
  await GroupModel.remove({})
  await LocationModel.remove({})
  await LogModel.remove({})
  await PaymentInfoModel.remove({})
  await LogModel.remove({})
  await PaymentModel.remove({})
  await ScheduleModel.remove({})
  await StudentsListModel.remove({})
  await PermissionModel.remove({})
  await RoleModel.remove({})
  await StudentModel.remove({})
  await UserModel.remove({})
}

Promise.resolve(deleteCollections())
  .then(() => {
    console.log('Collections removed')
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
