/**
 * db/comments.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const UserModel = require('./users')
const GroupModel = require('./groups')
const TalksModel = require('./talks')
const StudentModel = require('./students')
const UserTalksModel = require('./usersTalks')

const CommentSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'Users' }, // Whom write the comment
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  userTalks: { type: Schema.Types.ObjectId, ref: 'UserTalks' },
  group: { type: Schema.Types.ObjectId, ref: 'Groups' },
  talk: { type: Schema.Types.ObjectId, ref: 'Talks' },
  appoiment: { type: Schema.Types.ObjectId, ref: 'Appoiment' },
  date: { type: Date, default: new Date() },
  comment: { type: String },
  edited: { type: Boolean, default: false },
  responses: [{ type: Schema.Types.Mixed }],
  editions: [
    {
      date: { type: Date, default: new Date() },
      user: { type: Schema.Types.ObjectId, ref: 'Users' },
      comment: { type: String }
    }
  ]
}, { versionKey: false })

CommentSchema.methods.toPopulate = async function (extend = false) {
  const data = {
    _id: this._id,
    edited: false
  }

  if (this.editions && this.editions.length) {
    data.edited = true
    this.edited = true
    await this.save()
  }

  const us = await UserModel.findOne({ _id: this.user })
  if (us) {
    const user = {
      _id: us._id,
      email: us.email,
      name: us.name
    }
    data.user = user
  }

  if (this.edited) {
    const editions = this.editions.sort((a, b) => {
      if (a.date < b.date) return 1
      if (a.date > b.date) return -1
      return 0
    })

    const us = await UserModel.findOne({ _id: editions[0].user })

    if (us) {
      const user = {
        _id: us._id,
        email: us.email,
        name: us.name
      }
      data.user = user
    }
    data.comment = editions[0].comment
    data.edited = true
    data.date = editions[0].date
  } else {
    data._id = this._id
    data.comment = this.comment
    data.date = this.date
  }

  if (this.group) {
    const group = await GroupModel.findOne({ _id: this.group })
    if (group) {
      data.group = group
    }
  }

  if (this.talk) {
    const talk = await TalksModel.findOne({ _id: this.talk })
    if (talk) {
      data.talk = talk
    }
  }

  if (this.student) {
    const student = await StudentModel.findOne({ _id: this.student })
    if (student) {
      data.student = student
    }
  }
  if (this.userTalks) {
    const userTalks = await UserTalksModel.findOne({ _id: this.userTalks })
    if (userTalks) {
      data.userTalks = userTalks
    }
  }
  return data
}

CommentSchema.methods.getResponses = function () {
  
}

const Comments = db.model('Comments', CommentSchema, 'Comments')

module.exports = Comments
