/**
 * db/payemnts.js
 *
 * Status Dic
 * 
 * 1 Approved
 * 2 In Progress
 * 3 Rejected
 * 
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const Utils = require('../lib/Utils')

const PaymentSchema = new Schema({
  date: { type: Date },
  paymentType: { type: String }, // card or cash
  amount: { type: Number, required: true },
  concept: { type: String },
  discount: { type: Number, default: 0 },
  emailSent: { type: Boolean, default: false },
  student: { type: Schema.Types.ObjectId, ref: 'Students' },
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },
  onlinePaymentId: { type: String },
  status: { type: Number, default: 0 },
}, { versionKey: false })

PaymentSchema.methods.updateStudentBalance = async function () {
  if (this.student) {
    const payments = await Payment.find({
      student: this.student
    })
    await Utils.updateStudentBalance(this.student, payments)
  }
}

const Payment = db.model('Payment', PaymentSchema, 'Payment')

module.exports = Payment
