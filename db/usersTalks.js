/**
 * db/usersTalks.js
 *
 * @description ::
 * @docs        :: None
 */
const _ = require('underscore')
// const mongoosastic = require('mongoosastic')

const db = require('./db')
const Schema = db.Schema

// const esClient = require('../lib/es-client')()

// Check if we need it 
const UserModel = require('./users')
const GroupModel = require('./groups')

//isModeltoComments
const StudentCommentsModel = require('./studentComments')

const normStudent = function (field) {
  if (!field || field === '') {
    return ''
  }
  return field.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}


const UserTalksSchema = new Schema({
  name: { type: String, required: true, es_indexed: true },
  description: { type: String },
  lastName: { type: String, es_indexed: true },
  secondLastName: { type: String, es_indexed: true },
  email: { type: String, es_indexed: true },
  name_lower: { type: String, es_indexed: true },
  email_lower: { type: String, es_indexed: true },
  lastName_lower: { type: String, es_indexed: true },
  lastNames_lower: { type: String, es_indexed: true },
  fullName_lower: { type: String, es_indexed: true },
  firstLastName_lower: { type: String, es_indexed: true },
  secondLastName_lower: { type: String, es_indexed: true },
  phoneNumber: { type: String, es_indexed: true },
  secondPhoneNumber: { type: String, es_indexed: true },
  deleted: { type: Boolean, default: false, es_indexed: true },
  calledBy: { type: Object },
  status: { type: Object },
  attendance: { type: Boolean, default: false },
  course: { type: Schema.Types.ObjectId, ref: 'Courses' },

  consumerId: { type: String },
  fbUser: { type: String },
  fbToken: { type: String },
  registerDate: { type: Date, default: new Date(), es_indexed: true },

  freeTestResult: { type: Number },

}, { versionKey: false })

/**
 * middleware pre save
 * builds and assign student's fields for elastics
 */
UserTalksSchema.pre('save', async function () {
  const nameLower = (this.name) ? normStudent(this.name) : ''
  const emailLower = (this.email) ? normStudent(this.email) : ''
  const lNameLower = (this.lastName) ? normStudent(this.lastName) : ''
  const sLNLower = (this.secondLastName)
    ? normStudent(this.secondLastName) : ''
  const fullName = `${nameLower} ${lNameLower} ${sLNLower}`
  const fLNLower = `${nameLower} ${lNameLower}`
  const lNLower = `${lNameLower} ${sLNLower}`

  this.name_lower = nameLower
  this.email_lower = emailLower
  this.lastName_lower = lNameLower
  this.secondLastName_lower = sLNLower
  this.fullName_lower = fullName
  this.firstLastName_lower = fLNLower
  this.lastNames_lower = lNLower
})

UserTalksSchema.methods.getCourses = async function () {
  return Promise
    .resolve(
      this.model('StudentLists')
        .find({ list: this._id })
        .select('_id')
        .then(stLists => stLists.map(stList => stList._id))
    )
    .then(stLists => {
      return Promise
        .all(stLists.map(stList => {
          return this.model('Groups')
            .findOne({ studentList: stList })
            .select('course')
            .then(group => group ? group.course : null)
        }))
    })
    .then(courses => courses.filter(course => course))
}

UserTalksSchema.methods.toTalks = function () {
  const data = {
    registerDate: this.registerDate,
    name: `${this.name} ${this.lastName} ${this.secondLastName}`,
    email: this.email,
    phoneNumbers: `${this.phoneNumber} | ${this.secondPhoneNumber}`,
    calledBy: this.calledBy
  }
  return data
}

UserTalksSchema.methods.toSimple = function () {
  const data = {
    _id: this._id,
    name: this.name,
    lastName: this.lastName,
    secondLastName: this.secondLastName,
    email: this.email,
    birthDate: this.birthDate,
    address: this.address,
    postalCode: this.postalCode,
    phoneNumber: this.phoneNumber,
    secondPhoneNumber: this.secondPhoneNumber,
    registeredBy: this.registeredBy,
    registerDate: this.registerDate,
    groups: this.groups,
    calledBy: this.calledBy,
    status: this.status,
    course: this.course,
    attendance: this.attendance,
    comipemsCourses: this.comipemsCourses || [],
  }
  return data
}


UserTalksSchema.methods.toProfile = function () {
  const student = {
    _id: this._id,
    registerDate: this.registerDate,
    name: this.name,
    lastName: this.lastName,
    secondLastName: this.secondLastName,
    email: this.email,
    address: this.address,
    birthDate: this.birthDate,
    phoneNumber: this.phoneNumber,
    secondPhoneNumber: this.secondPhoneNumber,
    fbToken: this.fbToken,
    fbUser: this.fbUser,
    financial: this.balance
  }

  return student
}

UserTalksSchema.methods.addGroupsInfo = async function () {
  if (this.groups.length) {
    const groupsInfo = await Promise
      .all(this.groups.map(groupId => {
        return GroupModel.findOne({ _id: groupId })
          .select('name')
      }))
    this.groupsInfo = groupsInfo
  }
  return this
}

UserTalksSchema.statics.findOneToTalk = async function (id) {
  const student = await this.findOne({ _id: id })
    .populate({
      path: "course"
    })
  if (!student) {
    return null
  }
  const comments = await StudentCommentsModel
    .findOne({ student: id })
    .then(stComments => {
      return (stComments && stComments.comments.length)
        ? stComments.comments.sort((a, b) => b.date - a.date)
        : []
    })
  let calledBy = {}
  if (student.calledBy && student.calledBy.user) {
    const user = await UserModel.findOne({ _id: student.calledBy.user })
    if (user) {
      calledBy.user = `${user.name} ${user.lastName} ${user.secondLastName}`
      calledBy.userId = user._id
    }
    calledBy.date = student.calledBy.date
  }

  const data = {
    _id: student._id,
    registerDate: student.registerDate,
    fullName: `${student.name} ${student.lastName} ${student.secondLastName}`,
    name: student.name,
    lastName: student.lastName,
    secondLastName: student.secondLastName,
    email: student.email,
    phoneNumber: student.phoneNumber,
    secondPhoneNumber: student.secondPhoneNumber,
    calledBy,
    status: student.status,
    attendance: student.attendance,
    course: student.course,
    comments
  }
  return data
}

// UserTalksSchema.plugin(mongoosastic, {
//   index: 'userstalks',
//   esClient
// })

const UsersTalks = db.model('UsersTalks', UserTalksSchema, 'UsersTalks')

module.exports = UsersTalks
