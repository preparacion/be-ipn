/**
 * db/studentComments.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const StudentCommentsSchema = new Schema({
  student: { type: Schema.Types.ObjectId, ref: 'Student' },
  comments: [{
    date: { type: Date, default: new Date() },
    createdBy: { type: Schema.Types.ObjectId, ref: 'Users' },
    comment: { type: String }
  }]
}, { versionKey: false })

const StudentComments = db.model(
  'StudentComments',
  StudentCommentsSchema,
  'StudentComments'
)

module.exports = StudentComments
