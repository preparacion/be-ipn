/**
 * db/talkStList.js
 *
 * @description ::
 * @docs        :: None
 */
const db = require('./db')
const Schema = db.Schema

const TalkStListSchema = new Schema({
  name: { type: String, required: true },
  list: [{ type: Schema.Types.ObjectId, ref: 'UsersTalks' }],
  deleted: { type: Boolean, default: false },
  delDate: { type: Date }
}, { versionKey: false })

const TalkStudentLists = db.model(
  'TalkStudentLists',
  TalkStListSchema,
  'TalkStudentLists'
)

module.exports = TalkStudentLists
