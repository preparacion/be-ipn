# be-courses

## Deployment

### 1. Install packages
```
$ sudo apt install linux-headers-$(uname -r) build-essential curl wget git
```

### 2. Install NVM
```
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

$ nvm install 10.16.3
```

### 3. Install Nginx
```
$ sudo apt install nginx ufw

$ sudo ufw allow 'Nginx HTTP' 

$ cd /etc/nginx/sites-enabled

$ sudo nano default
```

### 3.1 Config Nginx

```
$ sudo nano default
```

Copy and paste the next lines in the "location" sections

```
location / {
        proxy_pass http://localhost:3100;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
}
```

### 4. Clone this repo

```
$ npm i pm2 -g

$ cd

$ git clone https://driverInside@bitbucket.org/preparacion/be-ipn.git

$ cd be-ipn

$ pm2 install pm2-logrotate

$ npm i

$ npm i socket.io --save

mkdir .envs && cd .envs

touch production

(copiar y pegar las variables de entorno)

```

### 5 Install https Certs

```
$ sudo apt-get update

$ sudo apt-get install software-properties-common

$ sudo add-apt-repository ppa:certbot/certbot

$ sudo apt-get update

$ sudo apt-get install certbot python-certbot-nginx

$ sudo certbot --nginx

Yes Terms and conditions 

enter the next email when ask moisescorreo@outlook.com

then enter the domain names (Check if the dns redirects are already pointing to the current ip address)

www.api.preparacionipn.mx,api.preparacionipn.mx

chose option 2 for redirecta all request to https (make sure you already accept https income request in server / 443 port )

then enable autorenew if success last steps:

$ set autorenew cert 

$ sudo crontab -e

choose nano (option 1)

copy and paste next in end of file 

15 3 * * * /usr/bin/certbot renew --quiet
        


```


## IMPORTANT (Nov, 16. 2018)
In order to change the database client, you must install mongodb.
Please see point 5 (Install mongo)

## 1. Create .envs directory and add the configuration files

For example: **development**

```
NODE_ENV=development
APP_PORT=3100
```

## 5. Install mongo (docker container)

After install docker in your computer, in a terminal:

```
docker pull mongo
```

then:

```
docker run --name mongoprep --restart=always -d -p 27017:27017 mongo
```

---

## 2. Run a local dynamoDB instance (deprecated)

Download a .zip version from

```
https://docs.aws.amazon.com/es_es/amazondynamodb/latest/developerguide/DynamoDBLocal.html
```

Unzip and run:

```
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
```

Probably, you must install _aws cli_:

```
https://aws.amazon.com/es/cli/
```

In a new terminal:

```
aws configure
```

Type:

```
AWS Access Key ID [None]: foo
AWS Secret Access Key [None]: bar
Default region name [None]: local
Default output format [None]: json

```
## 3. Create Tables (deprecated)

Run

```
npm run createDB
```

## 4. Delete Tables (deprecated)

Run

```
npm run deleteDB
```

## Notes:

Config es un archivo con todas las variables de entorno que las obtiene de .envs

En db están todas las conexiónes y operaciones a base de datos 

Docs son los yml de autentificación 

Los http son los archivos rest para la extensión de vs 

Lib son clientes y librerias para conexión 3d party

En models están todas las funcones y clases 

Routes es el API

Scchemas , validaciones de entradas 

Seeds (Deprecaded)

Task son archivos de una sola ejecución , el unico importante es index-student se ocupa para realizar el indexado de la base de datos a elastic search.
