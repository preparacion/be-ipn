/**
 * crons/setStudentCourses.js
 *
 * @description :: set the names data to student
 * @docs        :: TODO
 */
const cron = require('node-cron')

const GroupModel = require('../db/groups')
const StudentModel = require('../db/students')
const StudentListModel = require('../db/studentLists')

const limit = 100
const time = '* * * * *'

console.log(process.env.MONGODB_URI)

const init = async function () {
  const students = await StudentModel
    .find({
      courses: { $exists: false },
      deleted: false
    })
    .limit(limit)

  Promise.all(students.map(student => {
    return getStudentCourses(student._id)
  })).then(() => { console.log('done') })
}

const getStudentCourses = async function (studentId) {
  const student = await StudentModel.findOne({ _id: studentId })
  const studentLists = await StudentListModel
    .find({
      list: studentId
    })
    .select('_id')
    .then(studentLists => {
      return studentLists.map(st => st._id)
    })
  if (!studentLists.length) {
    student.courses = []
    await student.save()
  }

  const courses = await Promise
    .all(studentLists.map(stListId => {
      return GroupModel.findOne({ studentList: stListId })
        .select('course')
    }))
    .then(groups => {
      return groups.map(group => group.course)
    })

  student.courses = courses
  await student.save()
  console.log(`Se han agregado cursos a ${studentId}`)
}

// Promise.resolve(init())
//   .then(() => {
//     console.log('done')
//     // process.exit(0)
//   })
//   .catch(err => {
//     console.error(err)
//     process.exit(1)
//   })

cron.schedule(time, init())
