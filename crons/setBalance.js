/**
 * crons/setBalance.js
 *
 * @description :: set the balance data to student
 * @docs        :: TODO
 */
const cron = require('node-cron')

const StudentModel = require('../db/students')

const limit = 100
const time = '* * * * *'

const setBalance = async function (st) {
  const student = await StudentModel.findOne({ _id: st._id })
  const stBalance = await student.toBalance()
  student.balance = stBalance.balance

  student.debt = (stBalance.balance.payments.total + stBalance.balance.payments.discounts) <
  stBalance.balance.costs.total
  await student.save()
}

const init = async function () {
  const students = await StudentModel
    .aggregate([
      {
        $match: {
          $or: [
            { balance: { $exists: false } },
            { 'balance.payments.total': { $eq: 0 } }
          ]
        }
      },
      {
        $sample: { size: limit }
      }
    ])
  if (students.length > 0) {
    console.log('se seteará el balance en ' + students.length)
  } else {
    console.log('- no hay estudiantes')
  }

  await Promise
    .all(students.map(student => setBalance(student)))
}

// Promise.resolve(init()).then(() => console.log('done'))

cron.schedule(time, init)
