/**
 * crons/sendPaymentEmails.js
 *
 * @description :: Sends emails
 * @docs        :: TODO
 */
const cron = require('node-cron')

const PaymentModel = require('../db/payments')
const StudentModel = require('../db/students')

const SendGrid = require('../lib/sendGrid')

const limit = 10
const time = '*/10 * * * *'

const sendMail = async function (payment) {
  const student = await StudentModel
    .findOne({ _id: payment.student })

  if (!student) {
    console.log(`Student not found: ${payment.student}`)
    console.log(`Payment id ${payment._id}`)
    return false
  }

  const email = student.email

  const data = {
    id: payment._id,
    to: email,
    name: student.name,
    lastName: student.lastName,
    email: email,
    phone: student.phoneNumber,
    grantotal: payment.amount,
    items: [
      {
        description: payment.concept || 'anticipo',
        date: new Date(payment.date).toLocaleDateString() || new Date().toLocaleDateString(),
        total: payment.amount
      }
    ]
  }

  await SendGrid.sendPayment(data)
  payment.emailSent = true
  await payment.save()

  console.log('email sent: ' + payment._id)
}

cron.schedule(time, async () => {
  const payments = await PaymentModel
    .find({
      student: { $exists: true },
      $or: [
        {
          emailSent: { $exists: false }
        }, {
          emailSent: false
        }
      ]
    })
    .sort({ date: -1 })
    .limit(limit)

  await Promise
    .all(payments.map(payment => sendMail(payment)))
})
