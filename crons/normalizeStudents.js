/**
 * crons/normalizeStudents.js
 *
 * @description :: set the names data to student
 * @docs        :: TODO
 */
const cron = require('node-cron')

const StudentModel = require('../db/students')

const limit = 100
const time = '* * * * *'

const normStudent = function (field) {
  if (!field || field === '') {
    return ''
  }
  return field.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

const init = async function () {
  const students = await StudentModel
    .find({ secondLastName_lower: { $exists: false } })
    .limit(limit)

  Promise.all(students.map(student => {
    const nameLower = (student.name) ? normStudent(student.name) : ''
    const emailLower = (student.email) ? normStudent(student.email) : ''
    const lNameLower = (student.lastName) ? normStudent(student.lastName) : ''
    const sLNLower = (student.secondLastName)
      ? normStudent(student.secondLastName) : ''
    const fullName = `${nameLower} ${lNameLower} ${sLNLower}`
    const fLNLower = `${nameLower} ${lNameLower}`

    student.name_lower = nameLower
    student.email_lower = emailLower
    student.lastName_lower = lNameLower
    student.secondLastName_lower = sLNLower
    student.fullName_lower = fullName
    student.firstLastName_lower = fLNLower

    return student.save()
  }))
}

cron.schedule(time, init)
