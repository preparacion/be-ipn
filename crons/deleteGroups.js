/**
 * crons/deleteGroups.js
 *
 * @description :: Deletes groups with attr "deleted"
 * @docs        :: TODO
 */
const moment = require('moment')
const cron = require('node-cron')

const GroupModel = require('../db/groups')
const StudentModel = require('../db/students')
const StudentListModel = require('../db/studentLists')

const time = '0 1 * * *' // Everyday at 1 am.

const init = async function () {
  const fifteenDaysAgo = moment(new Date()).subtract(15, 'days')

  await GroupModel
    .find({
      deleted: {
        $exists: true,
        $eq: true
      },
      delDate: { $lte: fifteenDaysAgo.format() }
    })
    .then(groups => {
      return Promise.all(groups.map(
        group => removeGroupData(group))
      )
    })
}

const removeGroupData = async group => {
  const groupId = group._id
  const stList = await StudentListModel
    .findOne({ _id: group.studentList })

  await Promise.all(stList.list.map(stId => {
    return removeGroupFromSt(stId, groupId)
  }))

  await GroupModel.deleteOne({ _id: groupId })
  await StudentListModel.deleteOne({ _id: stList._id })

  return false
}

const removeGroupFromSt = async (studentId, groupId) => {
  const student = await StudentModel
    .findOne({ _id: studentId })

  const index = student.groups.indexOf(groupId)
  if (index > -1) {
    student.groups.splice(index, 1)
    await student.save()
  }
}

// Promise.resolve(init())
//   .then(() => {
//     console.log('done')
//     process.exit(0)
//   })
//   .catch(err => {
//     console.error(err)
//     process.exit(1)
//   })

cron.schedule(time, init)
