/**
 * schemas/groups.js
 *
 * @description :: Defines groups validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const createGroupSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  startDate: Joi.date().required(),
  endDate: Joi.date(),
  active: Joi.boolean().default(false),
  activeLanding: Joi.boolean().default(false),
  quota: Joi.number().integer().min(1).default(1),
  comment: Joi.string().min(1).max(500),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  schedule: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  classRoom: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  location: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  studentList: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  schedules: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6),
      startHour: Joi.string().min(3).max(4),
      endHour: Joi.string().min(3).max(4)
    })
  )
})

const updateGroupSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  startDate: Joi.date(),
  endDate: Joi.date(),
  active: Joi.boolean(),
  activeLanding: Joi.boolean(),
  quota: Joi.number().integer().min(1),
  comment: Joi.string().min(1).max(500),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  schedule: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  classRoom: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  location: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  studentList: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  schedules: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6),
      startHour: Joi.string().min(3).max(4),
      endHour: Joi.string().min(3).max(4)
    })
  )
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const filterSchema = paginationSchema.keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer(),
  stRngStart: Joi.date(),
  stRngEnd: Joi.date(),
  endRngStart: Joi.date(),
  endRngEnd: Joi.date(),
  schedule: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  location: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  showAll: Joi.boolean()
})

module.exports = {
  idSchema,
  createGroupSchema,
  updateGroupSchema,
  paginationSchema,
  filterSchema
}
