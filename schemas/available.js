/**
 * schemas/available.js
 *
 * @description :: Defines available validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(10),
  skip: Joi.number().integer().default(0)
})

const filterSchema = paginationSchema.keys({
  startDate: Joi.date(),
  endDate: Joi.date(),
  schedule: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  group: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  paginationSchema,
  filterSchema
}
