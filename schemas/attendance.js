/**
 * schemas/attendance.js
 *
 * @description :: Defines attendance validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const attendanceSchema = Joi.object().keys({
  studentList: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  students: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ).required(),
  date: Joi.date().iso()
})

const attendanceIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const attendanceQuery = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  startDate: Joi.date(),
  endDate: Joi.date(),
  group: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const attendanceUpdateSingle = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendanceId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  groupId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  date: Joi.date(),
  attendance: Joi.boolean()
})

const attendanceUpdateUsers = Joi.object().keys({
  userId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendanceId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  date: Joi.date(),
  attendance: Joi.boolean(),

})

const attendanceDelete = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendanceId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const attendanceTeacherDelete = Joi.object().keys({
  userId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendanceId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})


const attendancePostSingle = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  date: Joi.date(),
  group: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendance: Joi.boolean()
})

const attendanceTeacher = Joi.object().keys({
  userId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  date: Joi.date(),
  group: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendance: Joi.boolean()
})


const attendanceUpdateSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  studentList: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  students: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ).required(),
  date: Joi.date().iso()
})

module.exports = {
  attendanceSchema,
  attendanceIdSchema,
  attendanceQuery,
  attendanceUpdateSchema,
  attendancePostSingle,
  attendanceTeacher,
  attendanceUpdateSingle,
  attendanceUpdateUsers,
  attendanceDelete,
  attendanceTeacherDelete
}
