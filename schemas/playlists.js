/**
 * schemas/playlists.js
 *
 * @description :: Defines playlists validation schemas
 * @docs        :: TODO
 */
 const Joi = require('joi')

 const playlistSchema = Joi.object().keys({
   _id: Joi.string()
     .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
   name: Joi.string().min(1).max(100).required(),
   description: Joi.string().min(1).max(200),
   backgroundColor: Joi.string().allow('').optional(),
   image: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
   order: Joi.number().integer(),
   active: Joi.boolean(),
   course: Joi.string()
     .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
   parent: Joi.string()
     .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
   videos: Joi.array().items(
     Joi.object().keys({
       video: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
       order: Joi.number().integer()
     })
   )
 })
 
 
 const updatePlaylistSchema = Joi.array().items(
   Joi.object().keys({
   _id: Joi.string()
     .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
   order: Joi.number().integer(),
   type: Joi.number().integer(),
   })
 )
 
 const playlistIdSchema = Joi.object().keys({
   id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
 })
 
 module.exports = {
   playlistSchema,
   playlistIdSchema,
   updatePlaylistSchema
 }
 