/**
 * schemas/talks.js
 *
 * @description :: Defines talks validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})



const createTalkSchema = Joi.object().keys({
  name: Joi.string().required(),
  creationDate: Joi.date().iso().default(new Date()),
  date: Joi.date().iso().required(),
  quota: Joi.number().integer().default(50),
  active: Joi.boolean().default(true),
  courseLevel: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  classRoom: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    isFeria: Joi.boolean(),
    filter: Joi.string().allow(''),
  schedules: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6),
      startHour: Joi.string().min(3).max(4),
      endHour: Joi.string().min(3).max(4)
    })
  ).default([])
})

const updateTalkSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  name: Joi.string(),
  date: Joi.date().iso(),
  quota: Joi.number().integer(),
  active: Joi.boolean(),
  courseLevel: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  classRoom: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  teacher: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    isFeria: Joi.boolean(),
    filter: Joi.string(),
  schedules: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6),
      startHour: Joi.string().min(3).max(4),
      endHour: Joi.string().min(3).max(4)
    })
  )
})

const callTalkSchema = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
})

const statusTalkSchema = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  status: Joi.number().integer().min(0).required()
})

const attendanceTalkSchema = Joi.object().keys({
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  attendance: Joi.boolean().required()
})



module.exports = {
  paginationSchema,
  idSchema,
  createTalkSchema,
  updateTalkSchema,
  callTalkSchema,
  statusTalkSchema,
  attendanceTalkSchema
}