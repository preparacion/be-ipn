/**
 * schemas/quiz.js
 *
 * @description :: Defines quiz validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const quizSchema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().min(1).max(1000),
    random: Joi.boolean().required(),
    active: Joi.boolean().required(),
    quizType: Joi.number().required(),
    questionsPerQuiz: Joi.number(),
    limitTime: Joi.number(),
    studentAttempts: Joi.number(),
    quiz: Joi.array().items(
        Joi.object().keys({
            question: Joi.string(),
            questionFormula: Joi.string(),
            questionImage: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
            questionType: Joi.number(),
            answerCollection: Joi.array().items(
                Joi.object().keys({
                    validAnswer: Joi.boolean(),
                    content: Joi.string(),
                    answerImage: Joi.string(),
                    answerFormula: Joi.string(),
                    idAnswer: Joi.string(),
                    answerType: Joi.number(),
                })
            ),
            explanationVideo: Joi.array().items(
                Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
            )
        })
    )
})

const quizSchemaUpdate = Joi.object().keys({
    name: Joi.string(),
    description: Joi.string().min(1).max(1000),
    random: Joi.boolean(),
    active: Joi.boolean(),
    quizType: Joi.number(),
    questionsPerQuiz: Joi.number(),
    studentAttempts: Joi.number(),
    limitTime: Joi.number(),
    quiz: Joi.array().items(
        Joi.object().keys({
            question: Joi.string(),
            questionFormula: Joi.string(),
            questionImage: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
            questionType: Joi.number(),
            active: Joi.boolean(),
            order: Joi.number(),
            _id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
            parent: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
            answerCollection: Joi.array().items(
                Joi.object().keys({
                    validAnswer: Joi.boolean(),
                    content: Joi.string(),
                    answerImage: Joi.string(),
                    answerFormula: Joi.string(),
                    idAnswer: Joi.string(),
                    answerType: Joi.number(),
                })
            ),
            explanationVideo: Joi.array().items(
                Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
            )
        })
    )
})

const quizIdSchema = Joi.object().keys({
    id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const paginationSchema = Joi.object().keys({
    limit: Joi.number().integer().max(1000).default(0),
    skip: Joi.number().integer()
})

module.exports = {
    quizSchema,
    quizSchemaUpdate,
    quizIdSchema,
    paginationSchema
}
