/**
 * schemas/liveChannels.js
 *
 * @description :: Defines liveChannels validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const idSchema = Joi.object().keys({
  id: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
})

const createChannelSchema = Joi.object().keys({
  name: Joi.string().required(),
  idInput: Joi.number().integer().required(),
  idChannel: Joi.number().integer().required(),

  rtmpUrl: Joi.string().required(),
  cdnUrl: Joi.string().required(),
  awsCdnUrl: Joi.string().required(),
  password: Joi.string(),
  active: Joi.boolean().default(false),
})



module.exports = {
  paginationSchema,
  idSchema,
  createChannelSchema
}
