/**
 * schemas/register.js
 *
 * @description :: Defines register validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const registerSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
  password: Joi.string().min(4).max(100).required(),
  lastName: Joi.string().min(1).max(100),
  secondLastName: Joi.string().min(1).max(100),
  phoneNumber: Joi.string(),
  birthDate: Joi.date(),
  address: Joi.string()
})

module.exports = { registerSchema }
