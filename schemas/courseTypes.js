/**
 * schemas/courseTypes.js
 *
 * @description :: Defines courseTypes validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const ctId = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const ctUpdateSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  name: Joi.string(),
  description: Joi.string()
})

module.exports = { ctId, ctUpdateSchema }
