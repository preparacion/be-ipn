/**
 * schemas/roles.js
 *
 * @description :: Defines roles validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const createRoleSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    description: Joi.string().min(1).max(250),
    allows: Joi.array().items(
      Joi.object().keys({
        resource: Joi.string().min(1),
        methods: Joi.array().items(
          Joi.string().valid('get', 'post', 'put', 'delete')
        )
      })
    ).required()
  })

const updateRoleSchema = Joi.object()
  .keys({
    id: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    name: Joi.string().min(1).max(100),
    description: Joi.string().min(1).max(250),
    allows: Joi.array().items(
      Joi.object().keys({
        resource: Joi.string().min(1),
        methods: Joi.array().items(
          Joi.string().valid('get', 'post', 'put', 'delete')
        )
      })
    )
  })

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer()
})


module.exports = {
  createRoleSchema,
  updateRoleSchema,
  idSchema,
  paginationSchema
}
