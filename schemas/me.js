/**
 * schemas/me.js
 *
 * @description :: Defines me validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const updateSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  lastName: Joi.string().min(1).max(100),
  email: Joi.string().email({ minDomainAtoms: 2 }),
  password: Joi.string().min(4).max(100),
  secondLastName: Joi.string().min(1).max(100),
  birthDate: Joi.date(),
  phoneNumber: Joi.string(),
  secondPhoneNumber: Joi.string(),
  address: Joi.string()
})


module.exports = {
  updateSchema
}
