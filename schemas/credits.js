/**
 * schemas/credits.js
 *
 * @description :: Defines credits validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const creditSchema = Joi.object()
  .keys({
    amount: Joi.number(),
    date: Joi.date(),
    student: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  })

module.exports = {
  creditSchema
}
