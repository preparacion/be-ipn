/**
 * schemas/classRooms.js
 *
 * @description :: Defines classRooms validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const classRoomSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  capacity: Joi.number().integer().required(),
  location: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const updateClassRoomSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  capacity: Joi.number().integer(),
  location: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  classRoomSchema,
  updateClassRoomSchema
}
