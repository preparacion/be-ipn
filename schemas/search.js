/**
 * schemas/search.js
 *
 * @description :: Defines search validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const sortValidFields = [
  'lastName',
  'secondLastName',
  'fullName_lower',
  'email',
  'phoneNumber',
  'secondPhoneNumber',
  'payments',
  'costs',
  'registerDate'
]

const searchFilter = Joi.object().keys({
  q: Joi.string().max(250).default('').example('test'),
  sort: Joi.string().valid(sortValidFields),
  asc: Joi.boolean().default(true),
  skip: Joi.number().integer().default(0),
  limit: Joi.number().integer().default(10)
})


module.exports = {
  searchFilter
}
