/**
 * schemas/users.js
 *
 * @description :: Defines users validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const teachers = Joi.extend((joi) => {
  return {
    name: 'teacher',
    base: Joi.string(),
    language: {
      'notTrue': '"{{#label}}" only accepts "true" as valid value'
    },
    rules: [
      {
        name: 'isTrue',
        validate (params, value, state, options) {
          if (value !== 'true') {
            return this.createError('teacher.notTrue', {}, state, options)
          }
          return value
        }
      }
    ]
  }
})


const userSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerDate: Joi.string(),
    isProfessor: Joi.boolean().default(false),
    type: Joi.number().integer(),
    titularTarjeta: Joi.string().min(1).max(100),
    numeroDeCuenta: Joi.string().min(1).max(20),
    clabe: Joi.string().min(1).max(20),
    numeroTarjeta: Joi.string().min(1).max(16),
    banco: Joi.string().min(1).max(100),
    roles: Joi.array().items(
      Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    )
  })

const updateUserSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    isProfessor: Joi.boolean(),
    registerDate: Joi.string(),
    type: Joi.number().integer(),
    titularTarjeta: Joi.string().min(1).max(100),
    numeroDeCuenta: Joi.string().min(1).max(20),
    clabe: Joi.string().min(1).max(20),
    numeroTarjeta: Joi.string().min(1).max(16),
    banco: Joi.string().min(1).max(100),
    roles: Joi.array().items(
      Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    ),
    picture: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    
  })

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const userFilterSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer(),
  teachers: teachers.teacher(),
  rolesIn: Joi.string(),
  noRolesIn: Joi.string()
})

const rolesUpdateSchema = Joi.object()
  .keys({
    role: Joi.string().default('')
  })



module.exports = {
  teachers,
  userSchema,
  updateUserSchema,
  idSchema,
  userFilterSchema,
  rolesUpdateSchema
}
