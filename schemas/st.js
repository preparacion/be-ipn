/**
 * schemas/st.js
 *
 * @description :: Defines st validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const stIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const updateStSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string().required(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    fbTok: Joi.string(),
    picture: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  })

  const imageSchema = Joi.object().keys({
    filename: Joi.string().required(),
  })
  

module.exports = {
  stIdSchema,
  updateStSchema,
  imageSchema
  
}
