
/**
 * schemas/videoComments.js
 *
 * @description :: Defines videoComments validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const getVideoComments = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer(),
  idStudent: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  words: Joi.string().max(100),
  idVideo: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  idSchema,
  getVideoComments
}
