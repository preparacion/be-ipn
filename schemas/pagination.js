/**
 * schemas/pagination.js
 *
 * @description :: Defines pagination validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer()
})

module.exports = { paginationSchema }
