/**
 * schemas/unprotected/exams.js
 *
 * @description :: Defines exams validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const registerSchema = Joi.object().keys({
  courseId: Joi.string(),
  studentData: Joi.object().keys({
      name: Joi.string().min(1).max(250).required(),
      lastName: Joi.string().min(1).max(250).required(),
      secondLastName: Joi.string(),
      email: Joi.string().email({ minDomainAtoms: 2 }).required(),
      phoneNumber: Joi.string().required(),
      secondPhoneNumber: Joi.string(),
  }),
  groupsArray: Joi.array()
})

module.exports = {
  registerSchema
}
