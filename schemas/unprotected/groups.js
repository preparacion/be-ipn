/**
 * schemas/unprotected/groups.js
 *
 * @description :: Defines groups validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')


const paymentSchema = Joi.object().keys({
  paymentType: Joi.string().valid(['cash', 'card']).required(),
  concept: Joi.string().min(1).max(200),
  amount: Joi.number().required(),
  discount: Joi.number(),
  date: Joi.date(),
  cardNumber: Joi.string().creditCard(),
  expirationDate: Joi.string().length(4),
  cardType: Joi.string().valid(['credit', 'debit']),
  cvv: Joi.string().length(3)
})

const studentSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date().iso(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string(),
    fbToken: Joi.string()
  })


const registerSchema = Joi.object().keys({
  payment_method_id: Joi.string(),
  payment_intent_id: Joi.string(),
  amount: Joi.number(),
  token: Joi.string(),
  paymentMethodId: Joi.string(),
  transaction_amount: Joi.number(),
  courseId: Joi.string(),
  studentData: Joi.object().keys({
    name: Joi.string().min(1).max(250).required(),
    lastName: Joi.string().min(1).max(250).required(),
    secondLastName: Joi.string(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    phoneNumber: Joi.string().required(),
    secondPhoneNumber: Joi.string(),
  })
})

const groupIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})


const registerStudentSchema = Joi.object().keys({
  student: studentSchema.required(),
  groups: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ).required(),
  payment: paymentSchema
})

const idsSchemaForExa = Joi.object().keys({
  examen1:  Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  examen2:  Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  examen3:  Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  registerSchema,
  registerStudentSchema,
  idsSchemaForExa
}
