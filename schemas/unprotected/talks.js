/**
 * schemas/unprotected/talks.js
 *
 * @description :: Defines talks validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const registerSchema = Joi.object().keys({
  name: Joi.string().min(1).max(250).required(),
  lastName: Joi.string().min(1).max(250).required(),
  secondLastName: Joi.string(),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
  birthDate: Joi.date().iso(),
  phoneNumber: Joi.string().required(),
  secondPhoneNumber: Joi.string(),
  course: Joi.string(),
})

module.exports = {
  registerSchema
}
