/**
 * schemas/external.js
 *
 * @description :: Defines external validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const externalSchema = Joi.object().keys({
  name: Joi.string().min(1).max(200).required(),
  lastName: Joi.string().min(1).max(200),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
  password: Joi.string().min(4).max(100),
  secondLastName: Joi.string().min(1).max(200),
  birthDate: Joi.date(),
  phoneNumber: Joi.string(),
  secondPhoneNumber: Joi.string(),
  address: Joi.string(),
  postalCode: Joi.number().integer()
})

module.exports = {
  externalSchema
}
