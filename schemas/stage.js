/**
 * schemas/stage.js
 *
 * @description :: Defines stage validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const stageIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const stageSchema = Joi.object().keys({
  name: Joi.string().min(1).max(250).required().example('Etapa 2'),
  description: Joi.string().min(1).max(250).example('Periodo de verano'),
  startDate: Joi.date().iso().default(new Date())
})

const updateStageSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  name: Joi.string().min(1).max(250).example('Etapa 2'),
  description: Joi.string().min(1).max(250).example('Periodo de verano'),
  startDate: Joi.date().iso()
})

module.exports = { stageIdSchema, stageSchema, updateStageSchema }
