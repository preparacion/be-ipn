/**
 * schemas/courses.js
 *
 * @description :: Defines courses validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const createCourseSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    image: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    duration: Joi.string().required(),
    creationDate: Joi.date().default(new Date().toISOString()),
    active: Joi.bool().default(false),
    activeExternal: Joi.bool().default(false),
    price: Joi.number().positive(),
    kind: Joi.string().min(1).max(100),
    type: Joi.string().min(1).max(100),
    courseType: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    courseLevel: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    stage: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
    children: Joi.array().items([
      Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    ]),
    discounts: Joi.array().items(
      Joi.object().keys({
        course: Joi.string()
          .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
          .required(),
        discount: Joi.number().positive().required()
      })
    ),
    schedulesAppoimentList: Joi.array(),
    topicByDayList: Joi.array(),
    isActiveAppoiments: Joi.boolean()
  })


const datesQuery = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  startDate: Joi.date(),
  endDate: Joi.date()
})

const updateCourseSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  image: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  duration: Joi.string(),
  active: Joi.boolean(),
  activeExternal: Joi.boolean(),
  price: Joi.number().positive(),
  kind: Joi.string().min(1).max(100),
  type: Joi.string().min(1).max(100),
  parent: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  courseType: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  courseLevel: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  stage: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  children: Joi.array().items([
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ]),
  discounts: Joi.array().items(
    Joi.object().keys({
      course: Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
      discount: Joi.number().positive().required()
    })
  ),
  schedulesAppoimentList: Joi.array(),
  topicByDayList: Joi.array(),
  isActiveAppoiments: Joi.boolean()
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer().max(1000)
})

const discountsSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})


module.exports = {
  createCourseSchema,
  datesQuery,
  updateCourseSchema,
  paginationSchema,
  discountsSchema
}
