/**
 * schemas/materials.js
 *
 * @description :: Defines materials validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')


const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const createMaterialSchema = Joi.object().keys({
  name: Joi.string().required(),
  type: Joi.number().integer().min(1),
  thumbnailUrl: Joi.string(),
  videoUrl: Joi.string(),
  duration: Joi.number().integer().min(1),
  order: Joi.number().integer().min(1),
  rate: Joi.number().integer().min(1).max(5),
  videoCount: Joi.number().integer().min(1),
  quizCount: Joi.number().integer().min(1),
  active: Joi.boolean().default(true),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})


module.exports = {
  paginationSchema,
  createMaterialSchema
}
