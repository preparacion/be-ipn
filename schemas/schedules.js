/**
 * schemas/schedules.js
 *
 * @description :: Defines schedules validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')


const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const creationSchema = Joi.object().keys({
  name: Joi.string().min(1).max(250).required(),
  day: Joi.string().max(250),
  startHour: Joi.string().max(4),
  endHour: Joi.string().max(4),
  aliasName: Joi.array().items(
    Joi.string().max(1)
  ),
  schedule: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ),
  schedulesList: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6).required(),
      startHour: Joi.string().max(4).required(),
      endHour: Joi.string().max(4).required()
    })
  ).required()
})

const updateSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  name: Joi.string().min(1).max(250),
  day: Joi.string().max(250),
  startHour: Joi.string().max(4),
  endHour: Joi.string().max(4),
  aliasName: Joi.array().items(
    Joi.string().max(1)
  ),
  schedule: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ),
  schedulesList: Joi.array().items(
    Joi.object().keys({
      day: Joi.number().integer().min(0).max(6),
      startHour: Joi.string().max(4),
      endHour: Joi.string().max(4)
    })
  )
})


const isAvailableSchema = Joi.object().keys({
  classRoomId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  startDate: Joi.date().iso().required(),
  endDate: Joi.date().iso().required(),
  scheduleArray: Joi.array().items(
    Joi.object().keys({
      day: Joi.string()
        .min(1).max(1).valid('l', 'm', 'w', 'j', 'v', 's', 'd').required(),
      startHour: Joi.string().max(4).required(),
      endHour: Joi.string().max(4).required()
    })
  ).required()
})


module.exports = {
  paginationSchema,
  idSchema,
  creationSchema,
  updateSchema,
  isAvailableSchema
}
