/**
 * schemas/folio.js
 *
 * @description :: Defines classRooms validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const FolioSchema = Joi.object().keys({
  title: Joi.string().min(1).max(100),
  folio: Joi.number().integer().required(),
  idStudent: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const updateFolioSchema = Joi.object().keys({
  idFolio: Joi.string()
  .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  title: Joi.string().min(1).max(100),
  folio: Joi.number().integer(),
  idStudent: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  FolioSchema,
  updateFolioSchema
}
