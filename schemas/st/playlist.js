/**
 * schemas/st/playlist.js
 *
 * @description :: Defines playlist validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const playlistIdSchema = Joi.object().keys({
  id: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})

const pdfSchema = Joi.object().keys({
  idPdf: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  user: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})

module.exports = {
  playlistIdSchema,
  pdfSchema
}
