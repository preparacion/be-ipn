/**
 * schemas/st/payments.js
 *
 * @description :: Defines payments validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const courseIdSchema = Joi.object().keys({
  id: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})

const paymentSchema = Joi.object().keys({
  token: Joi.string().required(),
  paymentMethodId: Joi.string(),
  installments: Joi.number(), //Numero de cuotas P.E Meses sin intereses o con intereses
  transaction_amount: Joi.number(),
  courseId: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  rememberCard: Joi.boolean()
})

const paymentSchemaCards = Joi.object().keys({
  token: Joi.string().required(),
  installments: Joi.number(), //Numero de cuotas P.E Meses sin intereses o con intereses
  transaction_amount: Joi.number().required(),
  courseId: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  groupId: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const schemaCards = Joi.object().keys({
  token: Joi.string().required(),
})

module.exports = {
  courseIdSchema,
  paymentSchema,
  paymentSchemaCards,
  schemaCards
}
