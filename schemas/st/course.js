/**
 * schemas/st/course.js
 *
 * @description :: Defines course validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const courseIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})


module.exports = {
  courseIdSchema
}
