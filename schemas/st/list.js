/**
 * schemas/st/list.js
 *
 * @description :: Defines list validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

module.exports = {
  idSchema
}
