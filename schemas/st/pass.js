/**
 * schemas/st/pass.js
 *
 * @description :: Defines pass validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const emailSchema = Joi.object().keys({
  email: Joi.string().email({ minDomainAtoms: 2 }).required()
})

module.exports = {
  emailSchema
}
