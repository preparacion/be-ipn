/**
 * schemas/quizAttempt.js
 *
 * @description :: Defines quizAttempt validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')


const quizAttemptSchema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().min(1).max(300),
    random: Joi.boolean().required(),
    active: Joi.boolean().required(),
    quizType: Joi.number().required(),
    quiz: Joi.array().items(
        Joi.object().keys({
            question: Joi.string(),
            questionFormula: Joi.string(),
            questionImage: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
            questionType: Joi.number(),
            answerCollection: Joi.array().items(
                Joi.object().keys({
                    validAnswer: Joi.boolean(),
                    content: Joi.string(),
                    answerImage: Joi.string(),
                    answerFormula: Joi.string(),
                    idAnswer: Joi.string(),
                    answerType: Joi.number(),
                })
            ),
            explanationVideo: Joi.array().items(
                Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
            )
        })
    )
})

const quizAttemptAnswerSchema = Joi.object().keys({
    idQuizQuestion: Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
    questionAnswers: Joi.string().required()
})

const attemptIdSchema = Joi.object().keys({
    id: Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
        .required()
})

module.exports = {
    quizAttemptAnswerSchema,
    attemptIdSchema
}
