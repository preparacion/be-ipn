/**
 * schemas/payment.js
 *
 * @description :: Defines payment validation schemas
 * @docs        :: TODO //
 */
const Joi = require('joi')

const paymentSchema = Joi.object().keys({
  paymentType: Joi.string().valid(['cash', 'card']).required(),
  concept: Joi.string().min(1).max(200),
  amount: Joi.number().required(),
  discount: Joi.number(),
  date: Joi.date(),
  cardNumber: Joi.string().creditCard(),
  expirationDate: Joi.string().length(4),
  cardType: Joi.string().valid(['credit', 'debit']),
  cvv: Joi.string().length(3)
})

const paymentIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})




const paymentUpdateSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  date: Joi.date(),
  paymentType: Joi.string().valid(['card', 'cash']),
  amount: Joi.number().example('333.44'),
  concept: Joi.string().example('pago parial'),
  discount: Joi.number().example('444.33'),
  user: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const createSchema = Joi.object().keys({
  date: Joi.date(),
  paymentType: Joi.string().valid(['card', 'cash']),
  amount: Joi.number().example('333.44'),
  concept: Joi.string().example('pago parial'),
  discount: Joi.number().example('444.33'),
  user: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer()
})

module.exports = {
  paymentSchema,
  paymentIdSchema,
  paymentUpdateSchema,
  createSchema,
  paginationSchema
}
