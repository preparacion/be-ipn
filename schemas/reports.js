/**
 * schemas/reports.js
 *
 * @description :: Defines reports validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const createReportSchema = Joi.object().keys({
  reason: Joi.string().min(5).required(),
  date: Joi.date().iso().default(new Date()),
  comment: Joi.string().min(5),
  status: Joi.string(),
  active: Joi.boolean().default(true),
  video: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})

const filterSchema = Joi.object().keys({
  limit: Joi.number().integer().default(10),
  skip: Joi.number().integer(0)
})

module.exports = {
  idSchema,
  createReportSchema,
  filterSchema
}
