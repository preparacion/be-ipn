/**
 * schemas/notifications.js
 *
 * @description :: Defines notifications validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const notificationSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  description: Joi.string().min(1).max(1000),
  sms: Joi.object().keys({
    message: Joi.string().min(1).max(1000)
      .example('Este es un mensaje de ejemplo para sms.')
  }),
  email: Joi.object().keys({
    message: Joi.string().min(1).max(5000)
      .example('Este es un mensaje de ejemplo para correo.'),
    template: Joi.string()
      .example('d-aaaefbd3698d4fff94ba953142cd5c8a'),
    templateData: Joi.any()
  }),
  to: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  )
})

/// New schema to the new notifications
/// Parameters executionDateTime in order to program some notifications. 

const genericNotificationSchema = Joi.object().keys({
  executionDateTime: Joi.string(),
  description: Joi.string().min(1).max(1000),
  sms: Joi.object().keys({
    message: Joi.string().min(1).max(1000).required()
      .example('Este es un mensaje de ejemplo para sms.')
  }),
  email: Joi.object().keys({
    template: Joi.string().required()
      .example('d-aaaefbd3698d4fff94ba953142cd5c8a'),
    templateData: Joi.object().keys({
      message: Joi.string().min(1).max(90000).required()
        .example('Este es un mensaje de ejemplo para correo.'),
      subject: Joi.string()
    })
  }),
  students: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ),
  groups: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  )
})


const sendToSchema = Joi.object().keys({
  description: Joi.string().min(1).max(1000),
  sms: Joi.object().keys({
    message: Joi.string().min(1).max(1000)
      .example('Este es un mensaje de ejemplo para sms.')
  }),
  email: Joi.object().keys({
    message: Joi.string().min(1).max(5000)
      .example('Este es un mensaje de ejemplo para correo.'),
    template: Joi.string()
      .example('d-aaaefbd3698d4fff94ba953142cd5c8a'),
    templateData: Joi.any()
  }),
  to: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  )
})


const byCourseSchema = Joi.object().keys({
  id: Joi.string()
  .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  .required(),
  sms: Joi.object().keys({
    message: Joi.string().min(1).max(1000)
      .example('Este es un mensaje de ejemplo para sms.')
  }),
  email: Joi.object().keys({
    message: Joi.string().min(1).max(5000)
      .example('Este es un mensaje de ejemplo para correo.'),
    subject: Joi.string().min(1).max(200)
      .example('Este es el título del correo.'),
  }),
  startGroupDate: Joi.date(),
  endGroupDate: Joi.date()
})


const paginationSchema = Joi.object().keys({
  limit: Joi.number().integer().default(0),
  skip: Joi.number().integer()
})

const searchFilter = Joi.object().keys({
  q: Joi.string().max(250).default('').example('test'),
  startDate: Joi.string(),
  endDate: Joi.string(),
  active: Joi.string(),
  sort: Joi.string(),
  asc: Joi.boolean().default(true),
  skip: Joi.number().integer().default(0),
  limit: Joi.number().integer().default(10)
})


module.exports = {
  notificationSchema,
  genericNotificationSchema,
  byCourseSchema,
  sendToSchema,
  paginationSchema,
  searchFilter
}
