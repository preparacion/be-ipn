// N O T   U S E D 

/**
 * schemas/studentComments.js
 *
 * @description :: Defines studentComments validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const createStudentCommentSchema = Joi.object().keys({
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  comment: Joi.object().keys({
    date: Joi.date().iso().default(new Date()),
    comment: Joi.string().min(1),
    createdBy: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  }).required()
})

module.exports = {
  idSchema,
  createStudentCommentSchema
}
