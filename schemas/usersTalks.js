/**
 * schemas/usersTalks.js
 *
 * @description :: Defines usersTalks validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const updateUserTalksSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerDate: Joi.string(),
  })


  const resendUserTalkSchema = Joi.object().keys({
    idTalk: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  
    usersTalk: Joi.array().items(
      Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    )
  })


module.exports = {
  updateUserTalksSchema,
  resendUserTalkSchema
}
