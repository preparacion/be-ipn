/**
 * schemas/appoiments.js
 *
 * @description :: Defines appoiments validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const createAppoiment = Joi.object().keys({
  idCourse: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  questionAnswers: Joi.string().required()
})

module.exports = {
  createAppoiment,
}
