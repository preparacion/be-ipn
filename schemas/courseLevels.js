/**
 * schemas/courseLevels.js
 *
 * @description :: Defines courseLevels validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const clId = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const clUpdateSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  name: Joi.string(),
  description: Joi.string()
})

module.exports = { clId, clUpdateSchema }
