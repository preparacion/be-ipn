/**
 * schemas/calendar.js
 *
 * @description :: Defines calendar validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const calendarSchema = Joi.object().keys({
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  group: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

module.exports = {
  calendarSchema
}
