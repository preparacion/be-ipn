/**
 * schemas/student.js
 *
 * @description :: Defines student validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const { paymentSchema } = require('./payment')

const groupIdSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required()
})

const studentSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100).required(),
    lastName: Joi.string().min(1).max(100).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date().iso(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string(),
    fbToken: Joi.string()
  })

const updateStudentSchema = Joi.object()
  .keys({
    name: Joi.string().min(1).max(100),
    lastName: Joi.string().min(1).max(100),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().min(4).max(100),
    secondLastName: Joi.string().min(1).max(100),
    birthDate: Joi.date(),
    phoneNumber: Joi.string(),
    secondPhoneNumber: Joi.string(),
    address: Joi.string(),
    postalCode: Joi.number().integer(),
    registerType: Joi.string(),
    fbToken: Joi.string(),
    picture: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)

  })

const studentIdSchema = Joi.object().keys({
  id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const deleteGroupSchema = Joi.object().keys({
  id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  groupId: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const studentFilterSchema = Joi.object().keys({
  limit: Joi.number().integer().max(1000).default(0),
  skip: Joi.number().integer(),
  sort: Joi.string().valid([
    'lastName',
    'secondLastName',
    'name'
  ]),
  balanceMin: Joi.number().integer(),
  balanceMax: Joi.number().integer(),
  registerStart: Joi.date().iso(),
  registerEnd: Joi.date().iso()
})

const idSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const notificationsFilter = Joi.object().keys({
  courses: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ),
  groups: Joi.array().items(
    Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  ),
  minCost: Joi.number().integer(),
  maxCost: Joi.number().integer()
})

const registerStudentSchema = Joi.object().keys({
  student: studentSchema.required(),
  group: groupIdSchema,
  payment: paymentSchema
})

module.exports = {
  studentSchema,
  updateStudentSchema,
  studentIdSchema,
  deleteGroupSchema,
  studentFilterSchema,
  idSchema,
  notificationsFilter,
  registerStudentSchema
}
