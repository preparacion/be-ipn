/**
 * schemas/sms.js
 *
 * @description :: Defines sms validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const smsIdSchema = Joi.object()
  .keys({
    id: Joi.string()
      .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
  })

const smsSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
    .required(),
  message: Joi.string().min(3).max(1000).required(),
  sendMail: Joi.boolean().default(false)
})

module.exports = {
  smsIdSchema,
  smsSchema
}
