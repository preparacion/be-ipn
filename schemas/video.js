/**
 * schemas/video.js
 *
 * @description :: Defines video validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const videoSchema = Joi.object().keys({
  title: Joi.string().required(),
  description: Joi.string().min(1).max(300),
  active: Joi.boolean(),
  filename: Joi.string(),
  duration: Joi.string(),
  playlist: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  quizParent: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const videoIdSchema = Joi.object().keys({
  id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
})

const rateVideoSchema = Joi.object().keys({
  id: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  student: Joi.string().regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  rate: Joi.number().integer().min(1).max(5)
})

const reportSchema = Joi.object().keys({
  reason: Joi.string().required(),
  date: Joi.date().iso().default(new Date()),
  user: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  course: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  playlist: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i),
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i) // for video id
})

const commentSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  comment: Joi.string().min(1).required(),
  date: Joi.date().iso().default(new Date())
})


const updateCommentSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  student: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  commentId: Joi.string().min(1).required(),
  comment: Joi.string().min(1).required(),
  date: Joi.date().iso().default(new Date())
})



const deleteCommentSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  commentId: Joi.string().min(1).required()
})


const watchVideoSchema = Joi.object().keys({
  id: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
  studentId: Joi.string()
    .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
})



module.exports = {
  videoSchema,
  videoIdSchema,
  rateVideoSchema,
  reportSchema,
  commentSchema,
  watchVideoSchema,
  updateCommentSchema,
  deleteCommentSchema
}
