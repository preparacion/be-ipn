/**
 * schemas/location.js
 *
 * @description :: Defines location validation schemas
 * @docs        :: TODO
 */
const Joi = require('joi')

const createLocationSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100).required(),
  opening: Joi.string().min(1).max(50).required(),
  closing: Joi.string().min(1).max(50).required(),
  street: Joi.string().min(1).max(100),
  postalCode: Joi.number().integer(),
  number: Joi.number().integer(),
  district: Joi.string().min(1).max(100),
  latitude: Joi.string(),
  longitude: Joi.string()
})

const updateLocationSchema = Joi.object().keys({
  name: Joi.string().min(1).max(100),
  opening: Joi.string().min(1).max(50),
  closing: Joi.string().min(1).max(50),
  street: Joi.string().min(1).max(100),
  postalCode: Joi.number().integer(),
  number: Joi.number().integer(),
  district: Joi.string().min(1).max(100),
  latitude: Joi.string(),
  longitude: Joi.string()
})


module.exports = {
  createLocationSchema,
  updateLocationSchema
}
