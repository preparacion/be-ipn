/**
 * middleware/logs.js
 *
 * @description :: Middleware for create logs in db
 * @docs        :: TODO
 */
const LogModel = require('../db/logs')

module.exports = async (ctx, next) => {
  const data = {
    url: ctx.url || null,
    method: ctx.method || null
  }

  if (ctx.state && ctx.state.user) {
    data['date'] = new Date().toISOString()
    data['user'] = ctx.state.user._id
    data['action'] = ctx.state.action || 'none'
    data['data'] = JSON.stringify(ctx.state.data, null, 2) || JSON.stringify('{}', null, 2)
    
    LogModel.create(data)
      .then()
      .catch(err => {
        console.error('- Error trying to create a new log.', JSON.stringify(err, null, 2))
      })
  }
}
