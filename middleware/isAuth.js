/**
 * middleware/isAuth.js
 *
 * @description :: Middleware for authentication
 * @docs        :: TODO
 */
const jwt = require('jsonwebtoken')

const { jwtKey } = require('../config')

module.exports = async (ctx, next) => {
  const authorization = ctx.request.header.authorization || ''

  const token = authorization.split(' ')[1] || ''
  try {
    if (!token) {
      ctx.status = 401
      ctx.body = {
        success: false,
        error: 'No auth token'
      }
      return false
    }

    const decoded = jwt.verify(token, jwtKey)

    if (decoded.user) {
      const user = JSON.parse(decoded.user)
      ctx.state.user = user
      ctx.state.roles = decoded.roles || ''
    } else if (decoded.student) {
      const student = JSON.parse(decoded.student)
      ctx.state.student = student
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        error: 'No person in jwt.'
      }
    }

    return next()
  } catch (err) {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: err.message
    }
    return false
  }
}
