/**
 * middleware/whoami.js
 *
 * @description :: Middleware for permissions
 * @docs        :: TODO
 */

/**
 * @param {object} middleware - Middleware function that executes after next() call
 */
const ErrMsg = require('../lib/errMsg')

module.exports = function (middleware) {
  const errMsg = new ErrMsg();
  return async function (ctx, next) {
    if (ctx.state && ctx.state.student) {
      const url = ctx.request.url
      const prefix = '/api/st'
      if (url.includes(prefix)) {
        return next()
      } else {
        const errorExt = errMsg.getByIndex('userNoPermissions')
        ctx.status = 401
        ctx.body = {
          success: false,
          error: errorExt.error,
          errorExt
        }
        return false
      }
    } else {
      // call 'hasPermission' middleware
      // see router/index.js whoami(hasPermission)
      await middleware.call(this, ctx, next)
    }
  }
}
