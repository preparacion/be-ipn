/**
 * middleware/hasPermissions.js
 *
 * @description :: Middleware for permissions
 * @docs        :: TODO
 */
const RoleModel = require('../db/roles')

const ErrMsg = require('../lib/errMsg')

/**
 * stripQueryStrings
 * @param {string} url url in request (ctx.request.url)
 * @returns {string} url without query params
 */
const stripQueryStrings = url => url.split(/[?#]/)[0]

/**
 * normalizeResourceUrl
 * @param {string} url source url (in role allows resources)
 * @param {string} prefix in this api, '/api'
 * @returns {string} the 'normalized' url
 */
const normalizeResourceUrl = function (url, prefix = null) {
  if (!url.startsWith('/')) {
    url = '/' + url
  }

  if (prefix && url.indexOf(prefix) === -1) {
    url = '/api' + url
  }
  url = `${url}`.replace(/\/+/g, '/')
  return url
}

/**
 * urlIsAllowed
 * the source urls uses two types of wildcards: * and ?
 * @param {string} url url in the request
 * @param {string} source url in the role allow resource
 * @returns {boolean}
 */
const urlIsAllowed = function (url, source) {
  const urlArr = url.split('/').filter(item => item)
  const sourceArr = source.split('/').filter(item => item)

  for (let i = 0; i < sourceArr.length; i++) {
    if (!sourceArr[i]) {
      return false
    }
    if (urlArr[i] !== sourceArr[i]) {
      if (sourceArr[i] === '*') {
        return true
      }
      if (sourceArr[i] !== '?') {
        return false
      }
    }
  }

  return true
}

/**
 * urlIsAllowed
 * @param {string} url url in the request
 * @param {string} source url in the role allows resource
 * @param {string} urlMethod http method in the request
 * @param {array} sourceMethods http methods in the role allow methods
 */
const hasPermissions = function (url, source, urlMethod, sourceMethods) {
  const isMethodAllowed = sourceMethods.indexOf(urlMethod.toLowerCase()) !== -1
  const isUrlAllowed = urlIsAllowed(url, source)

  return isMethodAllowed && isUrlAllowed
}

module.exports = async (ctx, next) => {
  const errMsg = new ErrMsg()
  if (!ctx.state || !ctx.state.user) {
    const errorExt = errMsg.getByIndex('noStateInUser')
    ctx.status = 401
    ctx.body = {
      success: false,
      error: errorExt.error,
      errorExt
    }
    return false
  }

  if (!ctx.state.roles) {
    const errorExt = errMsg.getByIndex('noRoleInToken')
    ctx.status = 401
    ctx.body = {
      success: false,
      error: errorExt.error,
      errorExt
    }
    return false
  }

  const url = ctx.request.url
  const method = ctx.request.method
  const prefix = '/api/'

  let isThisRouteAllowed = false

  const roles = await Promise
    .all(ctx.state.roles.map(id => RoleModel.findOne({ _id: id })))

  if (!roles) {
    const errorExt = errMsg.getByIndex('noRoleInState')
    ctx.status = 401
    ctx.body = {
      success: false,
      error: errorExt.error,
      errorExt
    }
    return false
  }

  roles.forEach(role => {
      role.allows.forEach(element => {
      const sourceUrl = normalizeResourceUrl(element.resource, prefix)
      const isAllowed = hasPermissions(
        stripQueryStrings(url),
        sourceUrl,
        method,
        element.methods
      )
      if (isAllowed) {
        isThisRouteAllowed = true
      }
    })
  })

  ctx.state.isAllowed = isThisRouteAllowed

  if (!isThisRouteAllowed) {
    const errorExt = errMsg.getByIndex('userNoPermissions')
    ctx.status = 401
    ctx.body = {
      success: false,
      error: errorExt.error,
      errorExt
    }
    return false
  }

  return next()
}
