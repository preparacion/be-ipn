const chai = require("chai");
const chaiHttp = require("chai-http");
const  app  = require("./index");
const expect = chai.expect;

chai.use(chaiHttp);

describe("Basic routes", () => {

  it("get Home", done => {
    chai
      .request(app)
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  after(()=>{
    app.close();
  })

});