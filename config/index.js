/**
 * config/index.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
'use strict'

const dotenv = require('dotenv')
const path = require('path')
const env = process.env.NODE_ENV || 'development'

const envPath = path.resolve(__dirname, `../.envs/${env}`)
dotenv.config({ path: envPath })

const configs = {
  base: {
    env,
    name: process.env.APP_NAME || 'prep-IPN-be',
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 3100,
    frontUrlAdmin: process.env.FRONT_URL_ADMIN || 'https://www.dashboard.preparacionpolitecnico.mx',
    frontUrl: process.env.FRONT_URL || 'https://www.alumnos.preparacionpolitecnico.mx',
    key: process.env.APP_KEY || 'ThisISnotAsafeK3y.',
    jwtKey: process.env.JWT_KEY || 'An0th3rUnS4f3tyKey..',
    sendGridUri: process.env.SEND_GRID_URI || 'https://api.sendgrid.com/v3/mail/send',
    sendGridApi: process.env.SEND_GRID_URI || 'https://api.sendgrid.com/v3/',
    sendGridToken: process.env.SEND_GRID_TOKEN || 'fooBar',
    awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID || 'foo',
    awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || 'bar',
    awsBucketName: process.env.AWS_BUCKET_NAME || 'ipn-videos',
    awsBucketImagesName: process.env.AWS_BUCKET_IMAGES_NAME || 'ipn-images',
    awsBucketPdfsName: process.env.AWS_BUCKET_PDF_NAME || 'ipn-pdfs',
    awsBucketProcessedName: process.env.AWS_BUCKET_PROCESSED_NAME || 'ipn-videos-processed',
    awsMediaConverterPoint: process.env.AWS_MEDIACONVERTER_POINT || '',
    awsBucketRegion: process.env.AWS_BUCKET_REGION || 'us-east-1',
    esHost: process.env.ES_HOST || 'localhost',
    esPort: process.env.ES_PORT || 9200,
    esUser: process.env.ES_USER || '',
    esPassword: process.env.ES_PSW || '',
    stripePk: process.env.STRIPE_SECRET_KEY || '',
    mercadopagoPk: process.env.MERCADOPAGO_PK || '',
    mercadopagoAt: process.env.MERCADOPAGO_AT || '',
    mercadopagoWebhook: process.env.MERCADOPAGO_WEBHOOK || '',
    socketPort: process.env.SOCKET_PORT || 8080
  },
  production: {
    mongodbURI: process.env.MONGODB_URI || 'mongodb://localhost:27017/prep',
    mongodbUser: process.env.MONGODB_USER || 'user',
    mongodbPass: process.env.MONGODB_PASS || 'pass',
    esLog: process.env.ES_LOG || '',
    awsSMSAccessKeyId: process.env.AWS_SMS_ACCESS_KEY_ID || 'aws sms access key',
    awsSMSSecretAccessKey: process.env.AWS_SMS_SECRET_ACCESS_KEY || 'awssmssecret',
    awsSMSRegion: process.env.AWS_SMS_REGION || 'us-east-1',
    awsCloudfromEndpoint: process.env.AWS_CLOUDFROM_ENDPOINT || 'https://d20num3pjgo17t.cloudfront.net/',
    cdnCheapEndpoint: process.env.CDN_CHEAP_ENDPOINT || 'https://nqokz91200lxlqn.belugacdn.link/',
    awsCloudfromEndpointImage: process.env.AWS_CLOUDFROM_ENDPOINT_IMAGE || 'https://d3a1pfnxy2709m.cloudfront.net/',
    awsCloudfromEndpointPdfs: process.env.AWS_CLOUDFROM_ENDPOINT_PDF || 'https://d2misdlakpcc4e.cloudfront.net/',
    appId: process.env.FB_APP_ID || '',
    appSecret: process.env.FB_APP_SECRET || '',
    rootUserPass: process.env.ROOT_USER_PASS || '',
    rootUserMail: process.env.ROOT_USER_MAIL || '',
    fooUserPass: process.env.FOO_USER_PASS || '',
    fooUserMail: process.env.FOO_USER_MAIL || ''
  },
  development: {
    mongodbURI: process.env.MONGODB_URI || 'mongodb://localhost:27017/prep',
    mongodbUser: process.env.MONGODB_USER || 'user',
    mongodbPass: process.env.MONGODB_PASS || 'pass',
    esLog: process.env.ES_LOG || 'trace',
    awsSMSAccessKeyId: process.env.AWS_SMS_ACCESS_KEY_ID || 'aws sms access key',
    awsSMSSecretAccessKey: process.env.AWS_SMS_SECRET_ACCESS_KEY || 'awssmssecret',
    awsSMSRegion: process.env.AWS_SMS_REGION || 'us-east-1',
    awsCloudfromEndpoint: process.env.AWS_CLOUDFROM_ENDPOINT || 'https://d3sg2pnrcsl4y.cloudfront.net/',
    cdnCheapEndpoint: process.env.CDN_CHEAP_ENDPOINT || 'https://nqokz91200lxlqn.belugacdn.link/',
    awsCloudfromEndpointImage: process.env.AWS_CLOUDFROM_ENDPOINT_IMAGE || 'https://d2pnck9pbo5fgt.cloudfront.net/',
    awsCloudfromEndpointPdfs: process.env.AWS_CLOUDFROM_ENDPOINT_PDF || 'https://d2misdlakpcc4e.cloudfront.net/',
    appId: process.env.FB_APP_ID || '',
    appSecret: process.env.FB_APP_SECRET || '',
    rootUserPass: process.env.ROOT_USER_PASS || '',
    rootUserMail: process.env.ROOT_USER_MAIL || '',
    fooUserPass: process.env.FOO_USER_PASS || '',
    fooUserMail: process.env.FOO_USER_MAIL || ''
  },
  test: {
    port: process.env.APP_PORT || 3110,
    host: process.env.APP_HOST || '127.0.0.1'
  }
}

const config = Object.assign(configs.base, configs[env])
module.exports = config
