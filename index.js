/**
 * index.js
 *
 * @description :: Loads server and inits the app
 * @docs        :: README
 */
const { port } = require('./config')
const server = require('./server')

const app = server.listen(port, () => {
  console.log(`Webapp running on port ${port}`)
})

module.exports = app