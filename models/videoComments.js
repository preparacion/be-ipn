
/**
 * models/videoComments.js
 *
 * @description :: Describes the studentComments functions
 * @docs        :: TODO
 */
const VideoCommentsModel = require('../db/videoComments')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()


const VideoComments = {

  get: async ({
    skip,
    limit,
    idStudent,
    idVideo,
    words
  } = {}) => {

    try {

      const filter = {  }

      if (idStudent) {
        filter['student'] = idStudent
      } 

      if (idVideo) {
        filter['video'] = idVideo
      } 

      if (words) {
        filter['comment'] = {$regex: words}
      } 


    const total = await VideoCommentsModel
    .find(filter)
    .countDocuments({})

    const result = await VideoCommentsModel
      .find(filter)
      .skip(skip)
      .limit(limit)
      .sort({date:-1})
      .populate({
        path: 'student',
        select:'-balance -groups -hashed_password'})
      .populate({
        path: 'video',
        select:'-rates' })
      .populate({
        path: 'responses',
        populate: {
            path: 'student',
            select: ['-balance', '-groups', '-hashed_password']
        }})
      .then(videoComments => {
        return {
          success: true,
          total: total,
          idVideo: idVideo,
          videoComments: videoComments
        }
      })

      return result

    } catch (err) {
      const errorExt = errMsg
        .generalGet('videoComments')
      return {
        success: false,
        error: errorExt
      }
    }
    
  }
  ,


  delete: async id => {

    try {

      const result = await VideoCommentsModel.deleteOne({
        _id: id
      })
      if (result.n == 0) {
        const errorExt = errMsg
          .getNotEntityFound('videocomment', id)
        return {
          success: false,
          error: errorExt
        }

      } else {

        return {
          success: true,
          deleted: true
        }
      }
    } catch (err) {
      const errorExt = errMsg
        .generalDelete('videocomment')
      return {
        success: false,
        error: errorExt
      }

    }
  },


}





module.exports = VideoComments
