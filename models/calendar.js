/**
 * models/calendar.js
 *
 * @description :: Describes the calendar functions
 * @docs        :: TODO
 */
const moment = require('moment')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const ScheduleModel = require('../db/schedules')
const AttendanceStudentModel = require('../db/attendanceStudent')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const priority = 'high'

const Calendar = {
  /**
   * get
   * @description Get calendar info by student and group
   * @param {object} query Query
   */
  get: async (studentId, groupId) => {

    try{
    const student = await StudentModel
      .findOne({ _id: studentId })
    if (!student) {

      const errorExt = errMsg.getNotEntityFound('student', studentId)
      return {
          success: false,
          error: errorExt
      }

    }

    const group = await GroupModel
      .findOne({ _id: groupId })
    if (!group) {
      const errorExt = errMsg.getNotEntityFound('group', groupId)
      return {
          success: false,
          error: errorExt
      }
    }

    const attendance = await AttendanceStudentModel
      .findOne({ student: studentId })
    if (!attendance) {
      const errorExt = errMsg.getNotEntityFound('attendance by studentId', studentId)
      return {
          success: false,
          error: errorExt
      }
    }

    const course = await CourseModel
      .findOne({ _id: group.course })
      if (!course) {
        const errorExt = errMsg.getNotEntityFound('Course', group.course)
        return {
            success: false,
            error: errorExt
        }
      }

    const schedule = await ScheduleModel
      .findOne({ _id: group.schedule })
      if (!schedule) {
        const errorExt = errMsg.getNotEntityFound('schedule', group.schedule)
        return {
            success: false,
            error: errorExt
        }
      }


    const schedules = await schedule.getChilds()

    const data = []
    const duration = parseInt(course.duration)
    const startDate = moment.utc(group.startDate)

    schedules.forEach(element => {
      let currentDays = 0
      do {
        currentDays += 7

        const start = moment.utc(startDate).add(currentDays, 'd')
          .add(element.startHour.hours, 'h')
          .add(element.startHour.minutes, 'm')
        const end = moment.utc(startDate).add(currentDays, 'd')
          .add(element.endHour.hours, 'h')
          .add(element.endHour.minutes, 'm')

        const isAttendance = Calendar.isAttendance(
          attendance.attendance,
          start
        )

        const item = {
          title: '',
          allDay: true,
          stObj: {
            year: start.year(),
            month: start.month(),
            day: start.date(),
            hour: start.hours(),
            minutes: start.minutes()
          },
          endObj: {
            year: end.year(),
            month: end.month(),
            day: end.date(),
            hour: end.hours(),
            minutes: end.minutes()
          },
          end: end.toDate(),
          start: start.toDate(),
          attendance: isAttendance,
          priority
        }

        data.push(item)
      } while (currentDays <= duration)
    })

    return {
      success: true,
      data
    }


  }
  catch (err) {
      const errorExt = errMsg.generalGet('Calendar')
      return {
          success: false,
          error: errorExt
      }
  
}

  },

  /**
   * isAttendance
   * Compares a date in a list of student attendance
   * @param {array} arr List of student attendance
   * @param {object} date Moment object. Date to compare
   * @returns {boolean} is attendance
   */
  isAttendance: (arr, date) => {
    for (let i = 0; i < arr.length; i++) {
      const element = arr[i]
      const elDate = moment.utc(element.date)
      if (
        parseInt(date.year()) === parseInt(elDate.year()) &&
        parseInt(date.month()) === parseInt(elDate.month()) &&
        parseInt(date.date()) === parseInt(elDate.date())
      ) {
        return true
      }
    }
    return false
  }
}

module.exports = Calendar
