/**
 * models/playlist.js
 *
 * @description :: Describes the playlist functions
 * @docs        :: TODO
 */

 const PlaylistStudentObject = require('./st/playlist')
 const PlaylistModel = require('../db/playlists')
 const VideoModel = require('../db/videos')
 const QuizModel = require('../db/quiz')
 const PdfsModel = require('../db/pdfs')
 const StudentProgressModel = require('../db/studentProgress')
 
 const Playlist = {
   get: async ({ skip, limit } = {}) => {
     const playlists = await PlaylistModel
       .find({
         active: true,
         parent: { $exists: false }
       })
       .limit(limit)
       .skip(skip)
       .then(playlists => {
         return {
           success: true,
           playlists
         }
       })
       .catch(err => {
         console.error('- Error trying to get all playlists.', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     if ('success' in playlists && !playlists.success) {
       return playlists // return error
     }
     // playlist populated
     const plPopulated = await Promise.all(
       playlists.playlists.map(pl => pl.getChildren())
     )
 
     return {
       success: true,
       playlists: plPopulated
     }
   },
 
   getById: async id => {
     const result = await PlaylistModel
       .findOne({ _id: id })
       .populate('image')
       .then(playlist => {
         if (!playlist) {
           return {
             success: false,
             code: 404,
             error: `Playlist not found: ${id}`
           }
         }
         return {
           success: true,
           playlist: playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to get a playlist by id', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     if ('success' in result && !result.success) {
       // Return error
       return result
     }
 
     const playlist = await result.playlist.toPopulate()
 
     return {
       success: true,
       playlist: playlist
     }
   },
 
   create: async data => {
     const playlist = new PlaylistModel(data)
     const result = await playlist.save()
       .then(playlist => {
         playlist.type = 1
         return {
           success: true,
           playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to create a playlist', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     return result
   },
 
   getByCourseId: async id => {
     const playlists = await PlaylistModel
       .find({
         course: id,
         parent: { $exists: false }
       }).populate('image')
       .sort({
         'order': 1
       })
       .then(playlists => {
         return {
           success: true,
           playlists
         }
       })
       .catch(err => {
         console.error('- Error trying to create a playlist', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     if ('success' in playlists && !playlists.success) {
       return playlists // return error
     }
     // playlist populated
     const resPlaylist = await playlists.playlists.map(value => {
       value.type = 1
       return value
     })
 
 
     return {
       success: true,
       playlists: resPlaylist
     }
   },
 
   delete: async id => {
     const playlist = await PlaylistModel
       .findOne({ _id: id })
     if (!playlist) {
       return {
         success: false,
         code: 404,
         error: `Playlist not found ${id}`
       }
     }
 
     await PlaylistModel
       .deleteOne({ _id: id })
       .catch(err => {
         console.error('- Error trying to create a playlist', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
     return {
       success: true,
       deleted: true
     }
   },
   updateOrder: async (id, data) => {
     //This will get playlist info to update
     const result = await PlaylistModel
       .findOne({ _id: id })
       .then(playlist => {
         if (!playlist) {
           return {
             success: false,
             code: 404,
             error: `Playlist not found: ${id}`
           }
         }
         return {
           success: true,
           playlist: playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to get a playlist by id', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
     if ('success' in result && !result.success) {
       // Return error
       return result
     }
 
 
     const playlist = await result.playlist
     const videos = await VideoModel
       .find({ playlist: playlist._id })
     if (videos.length) {
       //console.log("HAS VIDEOS")
       await videos.forEach(function (element) {
         let index = data.findIndex(i => i != null && i._id == element._id);
         if (index >= 0) {
           console.log(index)
           if (data[index].order) {
             VideoModel.findOneAndUpdate({
               _id: element._id
             }, {
               $set: { order: data[index].order }
             })
               .then(video => {
                 //console.log(video)
               })
               .catch(err => {
                 //console.error('- Error trying to update a Video', JSON.stringify(err, null, 2))
               })
           }
         }
       })
     }
 
     const quiz = await QuizModel
       .find({ parent: playlist._id })
     if (quiz.length) {
       //console.log("HAS VIDEOS")
       await quiz.forEach(function (element) {
         let index = data.findIndex(i => i != null && i._id == element._id);
         if (index >= 0) {
           console.log(index)
           if (data[index].order) {
             QuizModel.findOneAndUpdate({
               _id: element._id
             }, {
               $set: { order: data[index].order }
             })
               .then(quiz => {
                 //console.log(video)
               })
               .catch(err => {
                 //console.error('- Error trying to update a Video', JSON.stringify(err, null, 2))
               })
           }
         }
       })
     }
     //Get Playlist section return all playlist with parent id.
     const children = await PlaylistModel.find({
       parent: playlist._id
     })
     if (children && children.length) {
       //console.log("HAS CHILDREN")
       await children.forEach(function (element) {
         let index = data.findIndex(i => i != null && i._id == element._id);
         if (index >= 0) {
           if (data[index].order) {
             PlaylistModel.findOneAndUpdate({
               _id: element._id
             }, {
               $set: { order: data[index].order }
             })
               .then(video => {
                 //console.log(video)
               })
               .catch(err => {
                 //console.error('- Error trying to update a Playlist', JSON.stringify(err, null, 2))
               })
           }
         }
       })
     }
    const pdfs = await PdfsModel
      .find({ playlist: playlist._id })
    if (pdfs.length) {
      await pdfs.forEach(function (element) {
        let index = data.findIndex(i => i != null && i._id == element._id);
        if (index >= 0) {
          if (data[index].order) {
            PdfsModel.findOneAndUpdate({
              _id: element._id
            }, {
              $set: { order: data[index].order }
            })
              .then(pdf => {
                //console.log(pdf)
              })
              .catch(err => {
                //console.error('- Error trying to update a PDF', JSON.stringify(err, null, 2))
              })
          }
        }
      })
    }
     return {
       success: true,
       playlist: playlist
     }
   },
 
   updateOrderCourse: async (id, data) => {
     const children = await PlaylistModel.find({
       course: id
     })
     if (children && children.length) {
       await children.forEach(function (element) {
         let index = data.findIndex(i => i != null && i._id == element._id);
         if (index >= 0) {
           if (data[index].order) {
             PlaylistModel.findOneAndUpdate({
               _id: element._id
             }, {
               $set: { order: data[index].order }
             })
               .then(video => {
                 //console.log(video)
               })
               .catch(err => {
                 //console.error('- Error trying to update a Playlist', JSON.stringify(err, null, 2))
               })
           }
         }
       })
     }
     return {
       success: true,
       children
     }
   },
 
   changeMaterialList: async (idOrigin, idDestination, data) => {
     const origin = await PlaylistModel
       .findOne({ _id: idOrigin })
       .then(playlist => {
         if (!playlist) {
           return {
             success: false,
             code: 404,
             error: `Playlist Origin not found: ${idOrigin}`
           }
         }
         return {
           success: true,
           playlist: playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to get a playlist by id', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
     if ('success' in origin && !origin.success) {
       // Return error
       return origin
     }
 
     const destination = await PlaylistModel
       .findOne({ _id: idDestination })
       .then(playlist => {
         if (!playlist) {
           return {
             success: false,
             code: 404,
             error: `Playlist Destination not found: ${idDestination}`
           }
         }
         return {
           success: true,
           playlist: playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to get a playlist by id', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
     if ('success' in origin && !origin.success) {
       // Return error
       return origin
     }
     if ('success' in destination && !destination.success) {
       // Return error
       return destination
     }
     /**
      * END OF VALIDATIONS
      */
 
     switch (data.type) {
       case 1:
         // console.log("Playlist")
         let thisPlaylist = await PlaylistModel.findOneAndUpdate({
           _id: data._id
         }, {
           $set: { parent: idDestination }
         })
         Playlist.updateProgressStudentRemove(idOrigin, idDestination, thisPlaylist).then(
           value => console.log(value)
         )
         break
       case 2:
         // console.log("VIDEO")
         await VideoModel.findOneAndUpdate({
           _id: data._id
         }, {
           $set: { playlist: idDestination }
         })
         break
       case 3:
         // console.log("Quiz")
         await QuizModel.findOneAndUpdate({
           _id: data._id
         }, {
           $set: { parent: idDestination }
         })
         break
       case 4:
        // console.log("PDF")
          await PdfsModel.findOneAndUpdate({
            _id: data._id
          }, {
            $set: { playlist: idDestination }
          })
        break
     }
 
     if (data.type !== 1) {
       Playlist.getAllPlaylistId(idOrigin).then(
         async (value) => {
           let result = await PlaylistModel.updateMany(
             { _id: { $in: value } },
             { $inc: { totalItems: -1 } })
         }
       )
       Playlist.getAllPlaylistId(idDestination).then(
         async (value) => {
           let result2 = await PlaylistModel.updateMany(
             { _id: { $in: value } },
             { $inc: { totalItems: 1 } })
         }
       )
       Playlist.updateProgressItem(idOrigin, idDestination, data._id).then(
         value => {
           console.log("UPDATES", value)
         }
       )
     }
     return {
       success: true
     }
   },
 
   update: async (id, data) => {
     let unset = {}
     if (data.image == undefined) {
       unset.image = "";
     }
     if (data.backgroundColor == undefined) {
       unset.backgroundColor = ""
     }
     return PlaylistModel.findOneAndUpdate({
       _id: id
     }, {
       $set: data,
       $unset: unset
     }, {
       upsert: false,
       new: true
     })
       .then(playlist => {
         if (!playlist) {
           const errorExt = errMsg
             .getNotEntityFound('Playlist', id)
           return {
             success: false,
             code: 404,
             error: errorExt.error,
             errorExt
           }
         }
         playlist.type = 1
         return {
           success: true,
           playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to update a Playlist', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
   },
   /**
   * WARNING
   * 
   * *
   * 
  */
 
 
   // This will recive The id of the playlist 
   // the student Id and will return a promisse
   getAllPlaylistId: (playlistId) => {
     return new Promise(async (resolve, reject) => {
       let arrayId = []
       let currentId = playlistId
       let current
       do {
         arrayId.push(currentId)
         current = await PlaylistModel.findOne({ _id: currentId }).select("_id parent")
         if (current && current.parent !== undefined) {
           currentId = current.parent
         }
       } while (current && current.parent !== undefined)
       resolve(arrayId)
     })
   },
 
 
   updateProgressItem: async (originId, destinationId, itemId) => {
     return new Promise(async (resolve, reject) => {
       const studentsProgressData = await StudentProgressModel.find({ playlist: originId })
       let conteo = 0
       if (studentsProgressData !== null && studentsProgressData !== undefined) {
         for (let i = 0; i < studentsProgressData.length; i++) {
           let index = studentsProgressData[i].finishItems.findIndex(value => String(value._id) == String(itemId))
           if (index >= 0) {
             conteo++
             let finishItem = { ...studentsProgressData[i].finishItems[index] }
             studentsProgressData[i].finishItems.splice(index, 1)
             await studentsProgressData[i].save()
             Playlist.getAllProgressPlaylistId(studentsProgressData[i].playlist, studentsProgressData[i].student)
             const studentProgressDestination = await StudentProgressModel.findOne({ playlist: destinationId, student: studentsProgressData[i].student })
             if (studentProgressDestination !== null && studentProgressDestination !== undefined) {
               studentProgressDestination.finishItems.push(finishItem)
               await studentProgressDestination.save()
               Playlist.getAllProgressPlaylistAdd(studentProgressDestination.playlist, studentProgressDestination.student)
             } else {
               PlaylistStudentObject.updateProgress(destinationId, studentsProgressData[i].student, finishItem)
             }
           }
         }
       }
       resolve(conteo)
     })
   },
 
   updateDeleteItem: async (playlistId, itemId) => {
     return new Promise(async (resolve, reject) => {
       const studentsProgressData = await StudentProgressModel.find({ playlist: playlistId })
       let conteo = 0
       if (studentsProgressData !== undefined) {
         for (let i = 0; i < studentsProgressData.length; i++) {
           let index = studentsProgressData[i].finishItems.findIndex(value => String(value._id) == String(itemId))
           if (index >= 0) {
             conteo++
             studentsProgressData[i].finishItems.splice(index, 1)
             Playlist.getAllProgressPlaylistId(studentsProgressData[i].playlist, studentsProgressData[i].student)
             studentsProgressData[i].save()
           }
         }
       }
       resolve(conteo)
     })
   },
 
   getAllProgressPlaylistId: async (playlistId, studentId) => {
     let currentId = playlistId
     let current
     do {
       current = await StudentProgressModel.findOne({ playlist: currentId, student: studentId }).select("_id parent finishItemsCount")
       if (current.parent !== undefined) {
         currentId = current.parent
       }
       current.finishItemsCount = current.finishItemsCount - 1
       current.save()
     } while (current.parent !== undefined)
   },
 
   getAllProgressPlaylistAdd: async (playlistId, studentId) => {
     let currentId = playlistId
     let current
     do {
       current = await StudentProgressModel.findOne({ playlist: currentId, student: studentId }).select("_id parent finishItemsCount")
       if (current.parent !== undefined) {
         currentId = current.parent
       }
       current.finishItemsCount = current.finishItemsCount + 1
       current.save()
     } while (current.parent !== undefined)
   },
 
 
   updateProgressStudentRemove: async (idOrigin, idDestination, thisPlaylist) => {
     /**
      * This will get tree of playlist and change the totalItems counts
      */
     Playlist.getAllPlaylistId(idOrigin).then(
       async (value) => {
         let valueMinus = value
         await PlaylistModel.updateMany(
           { _id: { $in: valueMinus } },
           { $inc: { totalItems: -thisPlaylist.totalItems } })
       }
     )
     Playlist.getAllPlaylistId(idDestination).then(
       async (value) => {
         let valueMinus = value
         await PlaylistModel.updateMany(
           { _id: { $in: valueMinus } },
           { $inc: { totalItems: thisPlaylist.totalItems } })
       }
     )
     /**
      * This will update all the studentPlaylist , it will collect all the studentsPlaylist related to 
      * the actual playlist, and update all tree buttom to top 
      * P.E StudentProgressLength 10 , take 1 element ,this element has parent ->parent ->parent ->parent
      */
     return new Promise(async (resolve, reject) => {
       //get all progress of playlist
       let arrayModify = []
       let lists = await StudentProgressModel.find({ playlist: thisPlaylist._id }).select("_id parent finishItemsCount student")
       for (i = 0; i < lists.length; i++) {
         const currentItem = lists[i]
         let progressItemsCount = currentItem.finishItemsCount
         let current = currentItem
         let currentId = current.parent
         currentItem.parent = idDestination
         currentItem.save()
         //Decrement old playlist tree
         do {
           current = await StudentProgressModel.findOne({ playlist: currentId, student: currentItem.student }).select("_id parent finishItemsCount")
           if (current !== null && current !== undefined) {
             if (current.parent !== undefined) {
               currentId = current.parent
             }
             current.finishItemsCount = current.finishItemsCount - progressItemsCount
             await current.save()
           }
         } while (current !== null && current.parent !== undefined)
         //Increment new playlist tree
         currentId = idDestination
         do {
           current = await StudentProgressModel.findOne({ playlist: currentId, student: currentItem.student }).select("_id parent finishItemsCount")
           if (current !== null && current !== undefined) {
             if (current.parent !== undefined) {
               currentId = current.parent
             }
             current.finishItemsCount = current.finishItemsCount + progressItemsCount
             await current.save()
           }
         } while (current !== null && current.parent !== undefined)
 
 
         arrayModify.push(progressItemsCount)
       }
       resolve(arrayModify)
     })
   },
 
 }
 
 module.exports = Playlist
 