/**
 * models/talks.js
 *
 * @description :: Describes the talks functions
 * @docs        :: TODO
 */
const _ = require('lodash')
const TalksModel = require('../db/talks')
const UsersTalksModel = require('../db/usersTalks')
const UsersModel = require('../db/users')
const TalkStudentListModel = require('../db/talkStList')
const CommentModel = require('../db/comments')

const ErrMsg = require('../lib/errMsg')

const errMsg = new ErrMsg()

const selectFields = [
  'name', 'date', 'active',
  'quota', 'course', 'schedule', 'classRoom',
  'talkStudentList', 'endDate', 'schedules',]

const Talks = {
  get: async ({ skip, limit } = {}) => {
    try {

      let filter = {
        deleted: { $ne: true }
      }
      const counter = await TalksModel.find().countDocuments();
      let talks = await TalksModel
        .find(filter)
        .skip(skip)
        .limit(limit)
        .sort({ date: -1 })
        .select(selectFields.join(' '))
        .populate({
          path: "talkStudentList",
          select: 'list'
        })
        .populate({
          path: "classRoom",
          populate: {
            path: 'location'
          }
        })
        .lean()
        .then((talks) => {
          return Promise.all(talks.map(talk => {
            return Talks.toLite(talk)
          }))
        })
        .catch(err => {
          console.error('- Error trying to get talk', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to get talks'
            }
          }
        })
      return {
        success: true,
        totalTalks: counter,
        talks
      }

    } catch (err) {
      const errorExt = errMsg
        .generalGet('talks')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  toLite: (group) => {
    const data = group
    data['percentage'] = 0
    data['studentListLength'] = 0

    if (group.talkStudentList) {
      const studentList = group.talkStudentList
      const percentage = studentList.list !== undefined ? (studentList.list.length / group.quota) : 0
      data['percentage'] = percentage * 100
      data['studentListLength'] = studentList.list !== undefined ? studentList.list.length : 0
    }
    if (group.classRoom) {
      data['locationInfo'] = group.classRoom.location
    }
    return data
  },
  getById: async (id) => {
    let talk = await TalksModel
      .findOne({ _id: id })
      .catch(err => {
        console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    talk = await talk.toPopulate()
    return {
      success: true,
      talk
    }
  },

  getSidebarInfoById: async (id) => {
    const talk = await TalksModel.findOne({
      _id: id
    }).populate({
      path: "talkStudentList",
      populate: {
        path: 'list',
        populate: {
          path: "calledBy.user",
          model: "Users",
          select: "name lastName secondLastName "
        },
        select: "name lastName secondLastName email phoneNumber secondPhoneNumber status calledBy attendance registerDate freeTestResult"
      },
      select: 'list',
    })
      .populate("teacher")
      .populate({
        path: "classRoom",
        populate: {
          path: "location",
          select: "name"
        },
        select: "name location"
      })
      .lean()
    //lean will return a simple POJO instead mongodb Document class
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const talksComments = await CommentModel.find({
      talk: id
    })
      .populate({
        path: "user",
        select: "name lastName"
      })
      .populate({
        path: "editions",
        populate: {
          path: "user",
          select: "name lastName"
        }
      })
      .sort({ userTalk: 1 })
    let students = [...talk.talkStudentList.list]
    if (students.length > 0) {
      students = _.uniq(students);
      for (let i = 0; i < students.length; i++) {
        const comments = talksComments.filter(item => String(item.userTalks) === String(students[i]._id))
        for (j = 0; j < comments.length; j++) {
          const indexDelete = talksComments.findIndex(value => value._id === comments[j]._id);
          if (indexDelete >= 0) {
            talksComments.splice(indexDelete, 1)
          }
        }
        students[i].comments = comments
        // console.log(talksComments.length)
      }
      talk.studentList = students;
    } else {
      talk.studentList = [];
    }
    delete talk.talkStudentList
    return {
      success: true,
      talk
    }
  },

  create: async (data) => {

    try {
      const talkStList = new TalkStudentListModel({
        name: data.name,
        list: []
      })
      await talkStList.save()
      data.talkStudentList = talkStList._id

      const talk = new TalksModel(data)
      await talk.save()
      const standartResult = await Talks.getSidebarInfoById(talk._id);
      if (standartResult.success) {
        const talkLite = Talks.toLite(standartResult.talk);
        return {
          success: true,
          talk: talkLite
        }
      } else {
        return {
          success: true,
          talk
        }
      }

    } catch (err) {
      const errorExt = errMsg
        .generalCreate('talks')
      return {
        success: false,
        error: errorExt
      }
    }
  },

  update: async (id, data) => {

    try {

      const talk = await TalksModel
        .findOneAndUpdate({
          _id: id
        }, {
          $set: data
        }, {
          upsert: false,
          returnNewDocument: true
        })
        .catch(err => {
          console.error('- Error trying to update a talk by id', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to update a talk by id'
            }
          }
        })
      if (!talk) {
        const errorExt = errMsg
          .getNotEntityFound('Talks', id)
        return {
          success: false,
          error: errorExt
        }
      }
      const standartResult = await Talks.getSidebarInfoById(talk._id);
      if (standartResult.success) {
        const talkLite = Talks.toLite(standartResult.talk);
        return {
          success: true,
          talk: talkLite
        }
      } else {
        return {
          success: true,
          talk
        }
      }

    } catch (err) {
      const errorExt = errMsg
        .generalUpdate('talks')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  toggle: async (id) => {
    const talk = await TalksModel
      .findOne({ _id: id })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    talk.active = !talk.active
    await talk.save()
    return {
      success: true,
      active: talk.active
    }
  },

  delete: async (id) => {
    try {

      tlks = await TalksModel
        .findOneAndDelete({ _id: id })
      if (!tlks) {
        const errorExt = errMsg
          .getNotEntityFound('talks', id)
        return {
          success: false,
          error: errorExt
        }
      }



      return {
        success: true,
        deleted: true
      }

    } catch (err) {
      const errorExt = errMsg
        .generalDelete('talks')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  getStudents: async (id) => {
    const talk = await TalksModel
      .findOne({ _id: id })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const students = _.uniq(await TalkStudentListModel
      .findOne({ _id: talk.talkStudentList })
      .then(talkStList => {
        return Promise.all(talkStList.list.map(stId => {
          return UsersTalksModel.findOneToTalk(stId)
        }))
      }))

    return {
      success: true,
      students
    }
  },

  callStudent: async (studentId, user, socket = undefined) => {
    const userTalk = await UsersTalksModel
      .findOne({ _id: studentId })
    if (!userTalk) {
      const errorExt = errMsg
        .getNotEntityFound('UserTalk', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    let validation = userTalk.calledBy == undefined
    let resCalledBy = {}
    if (validation) {
      userTalk.calledBy = {
        date: new Date(),
        user: user._id
      }
      resCalledBy.date = userTalk.calledBy.date
      resCalledBy.user = { _id: user._id, name: user.name, lastName: user.lastName };
      resCalledBy.userId = user._id
    } else {
      userTalk.calledBy = undefined
    }
    await userTalk.save()
    if (socket) {
      socket.to('update_table_event').emit("update_table_event", {
        event: "update_calledBy",
        calledBy: resCalledBy,
        studentId: studentId
      }
      );
    }

    return {
      success: true,
      called: validation,
      calledBy: resCalledBy
    }
  },

  statusStudent: async (studentId, statusIN, user, socket = undefined) => {
    const userTalk = await UsersTalksModel
      .findOne({ _id: studentId })
    if (!userTalk) {
      const errorExt = errMsg
        .getNotEntityFound('UserTalk', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    userTalk.status = {
      status: statusIN,
      user: user._id
    }
    if (socket) {
      socket.to('update_table_event').emit("update_table_event", {
        event: "update_status",
        status: userTalk.status,
        studentId: studentId
      })
    }
    await userTalk.save()
    return {
      success: true,
      status: userTalk.status,
    }
  },
  attendanceStudent: async (studentId, attendance, socket = undefined) => {
    const userTalk = await UsersTalksModel
      .findOne({ _id: studentId })
    if (!userTalk) {
      const errorExt = errMsg
        .getNotEntityFound('UserTalk', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    userTalk.attendance = attendance
    await userTalk.save()
    if (socket) {
      socket.to('update_table_event').emit("update_table_event", {
        event: "update_attendance",
        attendance: attendance,
        studentId: studentId
      })
    }
    return {
      success: true,
      attendance: userTalk.attendance,
    }
  },


  reports: async (id, socket = undefined) => {
    const result = await Talks.getSidebarInfoById(id);
    if (!result.success) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const talkResult = result.talk;
    const initObject = {
      buzon: 0,
      confirmados: 0,
      finishCall: 0,
      sinComentarios: 0,
      returnCall: 0,
      noExistPhone: 0,
      reprogramDate: 0,
      wrongNumber: 0,
    }
    let studentsCount = talkResult.studentList.length
    let buzon = 0, confirmados = 0, finishCall = 0, sinComentarios = 0, sinInteraccion = 0, returnCall = 0, noExistPhone = 0, reprogramDate = 0, wrongNumber = 0;
    let employees = {}
    for (let i = 0; i < studentsCount; i++) {
      let student = talkResult.studentList[i];
      let actualUser = "no_definido";
      if (student.calledBy && student.calledBy.date) {
        if (student.calledBy.user !== null && student.calledBy.user._id !== undefined) {
          actualUser = student.calledBy.user._id
        }
        if (employees[actualUser] === undefined) {
          employees[actualUser] = { ...initObject }
        }
        confirmados++;
        employees[actualUser].confirmados++;
      } else {
        if (student.status) {
          if (student.status.user !== undefined && student.status.user !== null) {
            actualUser = student.status.user
          }
          if (employees[actualUser] === undefined) {
            employees[actualUser] = { ...initObject }
          }
          if (student.status.status <= 5) {
            buzon++;
            employees[actualUser].buzon++;
          } else if (student.status.status == 6) {
            finishCall++;
            employees[actualUser].finishCall++;
          } else if (student.status.status == 7) {
            reprogramDate++;
            employees[actualUser].reprogramDate++;
          } else if (student.status.status == 8) {
            returnCall++;
            employees[actualUser].returnCall++;
          } else if (student.status.status == 9) {
            noExistPhone++;
            employees[actualUser].noExistPhone++;
          }
          else if (student.status.status == 10) {
            wrongNumber++;
            employees[actualUser].wrongNumber++;
          }
        } else {
          sinInteraccion++;
        }
      }
      if (!student.comments || student.comments < 1) {
        sinComentarios++;
      }
    }
    /**
     * This will take the object ande return the an array of employes
     * then folter to remove the null
     * then iterate this array and convert into a array of objects with data 
     */
    const arrayUsers = Object.keys(employees).filter(v => v !== "no_definido")
    let employeesData = []
    const employeesDatabase = await UsersModel.find({
      _id: { $in: arrayUsers }
    })
      .select("_id name lastName secondLastName")
      .lean();

    for (let i = 0; i < arrayUsers.length; i++) {
      const indexEmployee = employeesDatabase.findIndex(item => String(item._id) === String(arrayUsers[i]));
      if (indexEmployee >= 0) {
        employeesData.push({ employee: employeesDatabase[i], data: employees[arrayUsers[i]] })
      }

    }
    if (employees["no_definido"] !== undefined) {
      const noAvailable = {
        _id: "77",
        name: "Empleado no disponible",
        lastName: "-",
        secondLastName: "-"
      }
      employeesData.push({ employee: noAvailable, data: employees["no_definido"] })
    }

    const report = {
      studentsCount,
      confirmados,
      buzon,
      finishCall,
      reprogramDate,
      returnCall,
      noExistPhone,
      wrongNumber,
      sinComentarios,
      sinInteraccion,
      employeesData
    }
    return {
      success: true,
      report
    }
  },



  getFeria: async () => {
    let talk = await TalksModel
      .find({  $and: [{isFeria: true},{active: true}] })
      .populate({
        path: "classRoom",
        populate: {
          path: 'location'
        }
      })
      .catch(err => {
        console.error('- Error trying to get Feria talks', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    return {
      success: true,
      talk
    }
  },

  // UNPROTECTED SERVICE 

  getTalkGroups: async (courseLevel) => {
    const filter = {
      active: true
    }
    if (courseLevel) {
      filter['courseLevel'] = courseLevel
    }

    const talks = await TalksModel
      .find(filter)
      .populate({
        path: "talkStudentList"
      })
      .populate({
        path:"classRoom",
        select:"location",
        populate:{
          path:"location"
        }
      })
      .lean();
    const filterTalks = talks.filter(item => item.talkStudentList.list.length < item.quota)
    //this will remove the sensitive information
    filterTalks.forEach(item => {
      delete item.talkStudentList;
    })
    return {
      success: true,
      talks: filterTalks
    }
  },

  refresh: async id => {
    return {
      success: true,
      error: 'refresh'
    }
  },


}

module.exports = Talks
