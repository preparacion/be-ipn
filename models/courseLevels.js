/**
 * models/courseLevels.js
 *
 * @description :: Describes the courseLevels functions
 * @docs        :: TODO
 */
const CourseModel = require('../db/courses')
const CourseLevelModel = require('../db/courseLevels')
const ErrMsg = require('../lib/errMsg')

const errMsg = new ErrMsg()

const CourseLevels = {
  /**
   * get
   * @description Return all the course levels
   */
  get: async () => {

    try{
      
    const courseLevels = await CourseLevelModel
      .find({})
    return {
      success: true,
      courseLevels
    }


  } catch (err) {
    const errorExt = errMsg
      .generalGet('courseLevels')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * getById
   * @param {string} id CourseLevel object id
   * @returns {object} The course level
   */
  getById: async id => {
    const courseLevel = await CourseLevelModel
      .findOne({ _id: id })
    if (!courseLevel) {
      const errorExt = errMsg
        .getNotEntityFound('CourseLevel', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    return {
      success: true,
      courseLevel
    }
  },

  /**
   * update by id
   */
  update: async (id, data) => {

    try{

    const courseLevel = await CourseLevelModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        upsert: false,
        returnNewDocument: true
      })
    if (!courseLevel) {
    const errorExt = errMsg
      .getNotEntityFound('courseLevel', id)
    return {
      success: false,
      error: errorExt
    }
    }
    return {
      success: true,
      courseLevel
    }

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('courseLevels')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * getCoursesById
   */
  getCoursesById: async id => {
    const courses = await CourseModel
      .find({ courseLevel: id })
      .then(courses => {
        return Promise.all(courses.map(course => course.getChildren()))
      })
    return {
      success: true,
      courses
    }
  }
}

module.exports = CourseLevels
