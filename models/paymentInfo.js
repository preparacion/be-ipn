/**
 * models/paymentInfo.js
 *
 * @description :: Describes the paymentInfo functions
 * @docs        :: TODO
 */
const PaymentInfoModel = require('../db/paymentInfo')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()


const PaymentInfo = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {

    try{

    const paymentInfos = await PaymentInfoModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all paymentInfos.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all paymentInfos.'
          }
        }
      })
    return {
      success: true,
      paymentInfos
    }
  } catch (err) {
    const errorExt = errMsg
      .generalGet('paymentInfo')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const paymentInfo = await PaymentInfoModel
      .findOne({ _id: id })
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      paymentInfo
    }
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {
    try{
    const paymentInfo = new PaymentInfoModel(data)
    const result = await paymentInfo
      .save()
      .then(paymentInfo => {
        return paymentInfo
      })
      .catch(err => {
        console.error('- Error trying to create paymentInfo.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create paymentInfo.'
          }
        }
      })

    return {
      success: true,
      result
    }

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('paymentInfo')
    return {
      success: false,
      error: errorExt
    }
  }
  }
}

module.exports = PaymentInfo
