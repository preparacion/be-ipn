/**
 * models/reports.js
 *
 * @description :: Describes the reports functions
 * @docs        :: TODO
 */
const ReportsModel = require('../db/reports')

const ErrMsg = require('../lib/errMsg')

const errMsg = new ErrMsg()

const Reports = {
  /* / Get all */
  get: async ({
    skip,
    limit
  } = {}) => {

    try{

    const reports = await ReportsModel
      .find({})
      .sort({ active: 1 })
      .skip(skip)
      .limit(limit)

    return {
      success: true,
      reports
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('reports')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /* /:id Get by id */
  getById: async (id, user = null) => {
    const report = await ReportsModel
      .findOne({ _id: id })
    if (!report) {
      const errorExt = errMsg
        .getNotEntityFound('Report', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    report.seen.push({
      by: user,
      date: new Date()
    })

    await report.save()

    return {
      success: true,
      report
    }
  },

  /* /courses/:courseId Get by course id */
  getByCourse: async (courseId) => {
    const reports = await ReportsModel
      .find({
        course: courseId
      })
      .sort({ active: 1 })
    return {
      success: true,
      reports
    }
  },

  /* / Create */
  create: async (data) => {
    try{
    const report = new ReportsModel(data)
    await report.save()
    return {
      success: true,
      data
    }
  } catch (err) {
    const errorExt = errMsg
      .generalCreate('reports')
    return {
      success: false,
      error: errorExt
    }
  }

  },

  comment: async (id, data) => {
    return {
      success: true,
      comment: []
    }
  },

  update: async (id, data) => {
    return {
      success: true,
      message: 'update'
    }
  }
}

module.exports = Reports
