/**
 * models/materials.js
 *
 * @description :: Describes the materials functions
 * @docs        :: TODO
 */
const LiveChannelsModel = require('../db/livechannels');

const MediaLive = require('../lib/mediaLive');


const LiveChannels = {
    /** get all the materials */

    get: async ({ skip, limit } = {}) => {
        const resultGet = await LiveChannelsModel.find()
        if (!resultGet) {
            return {
                success: false,
                error: "error al listar canales de livestream"
            }
        }

        return {
            success: true,
            channels: resultGet
        }
    },

    getAwsInfo: async () => {
        const resultGet = await MediaLive.listChannels()
        return resultGet
    },


    create: async (data) => {

        const channel = new LiveChannelsModel(data)
        await channel.save()
        return {
            success: true,
            channel
        }
    },

    /** get a single material by id */
    getById: async (id) => {

    }
}

module.exports = LiveChannels
