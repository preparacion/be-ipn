/**
 * models/st/quiz.js
 *
 * @description :: Describes the Quiz functions to student
 * @docs        :: TODO
 */
const ErrMsg = require('../../lib/errMsg')

const QuizModel = require('../../db/quiz')


const errMsg = new ErrMsg()

// TODO: DOCUMENTATION
const Quiz = {
    getById: async (idQuiz, idStudent) => {
        const quiz = await QuizModel.findOne({
            _id: idQuiz
        })
            .select("name description viewed questionCollection parent active")
        if (!quiz) {
            const errorExt = errMsg
                .getNotEntityFound('Quiz', id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        return {
            success: true,
            quiz
        }

    },
}
module.exports = Quiz