/**
 * models/quizAttempt.js
 *
 * @description :: Describes the Quiz functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const mongoose = require('mongoose')
const moment = require('moment')
const ErrMsg = require('../../lib/errMsg')

const PlaylistMethod = require('./playlist')

const QuizModel = require('../../db/quiz')
const QuizQuestionModel = require('../../db/quizQuestion')
const QuizAttemptModel = require('../../db/quizAttempt')
const QuizAttemptAnswerModel = require('../../db/quizAttemptAnswer')
const ImageModel = require('../../db/images')


const errMsg = new ErrMsg()


const QuizAttempt = {
    create: async (idQuiz, student) => {
        const quizAttempt = await QuizModel.findOne({
            _id: idQuiz
        })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'explanationVideo'
                }
            })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'questionImage'
                }
            })
        if (!quizAttempt) {
            return {
                success: false,
                code: 404,
                error: `Quiz not found: ${idQuiz}`
            }
        }
        let questionCollection = []
        if (quizAttempt.random) {
            questionCollection = _.shuffle(quizAttempt.questionCollection)
        } else {
            questionCollection = quizAttempt.questionCollection.sort((a, b) => a.order - b.order)
        }
        for (let i = 0; i < quizAttempt.questionCollection.length; i++) {
            for (let j = 0; j < quizAttempt.questionCollection[i].answerCollection.length; j++) {
                let answer = quizAttempt.questionCollection[i].answerCollection[j]
                if (answer.answerImage) {
                    let image = await ImageModel.findOne({
                        _id: answer.answerImage
                    })
                    quizAttempt.questionCollection[i].answerCollection[j].answerImage = image
                }
            }
        }
        const attemptId = new mongoose.Types.ObjectId()
        const quizAttemptNew = new QuizAttemptModel({
            _id: attemptId,
            quizParent: idQuiz,
            student: student._id,
            date: new Date(),
            status: "OPEN",
            questionCollection,
        })
        await quizAttemptNew.save()
        let quizAttemptcopy = { ...quizAttemptNew._doc }
        quizAttemptcopy.quizParent = {
            _id: quizAttempt._id,
            limitTime: quizAttempt.limitTime,
            active: quizAttempt.active,
            parent: quizAttempt.parent,
            quizType: quizAttempt.quizType,
            name: quizAttempt.name
        }
        if (quizAttemptcopy.quizParent.limitTime !== undefined) {
            let start = moment(quizAttemptcopy.date)
            let end = start.add(quizAttemptcopy.quizParent.limitTime, 's')
            let diff = end.diff(moment(), 's')
            quizAttemptcopy.availableTime = diff
            quizAttemptcopy.limitTime = quizAttemptcopy.quizParent.limitTime
        }
        return {
            success: true,
            quizAttempt: quizAttemptcopy
        }
    },

    getById: async (idQuizAttempt, student) => {
        const quizAttempt = await QuizAttemptModel.findOne({
            _id: idQuizAttempt
        })
            .populate({
                path: 'quizParent',
                select: 'limitTime active parent quizType name'
            })
            .populate({
                path: 'answerCollection'
            })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'explanationVideo'
                }
            })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'questionImage'
                }
            })
        if (!quizAttempt) {
            return {
                success: false,
                code: 404,
                error: `Quiz Attempt not found: ${idQuizAttempt}`
            }
        }

        for (let i = 0; i < quizAttempt.questionCollection.length; i++) {
            for (let j = 0; j < quizAttempt.questionCollection[i].answerCollection.length; j++) {
                let answer = quizAttempt.questionCollection[i].answerCollection[j]
                if (answer.answerImage) {
                    let image = await ImageModel.findOne({
                        _id: answer.answerImage
                    })
                    quizAttempt.questionCollection[i].answerCollection[j].answerImage = image
                }
            }
        }

        let quizAttemptcopy = { ...quizAttempt._doc }
        if (quizAttemptcopy.quizParent.limitTime !== undefined) {
            let start = moment(quizAttemptcopy.date)
            let end = start.add(quizAttempt.quizParent.limitTime, 's')
            let diff = end.diff(moment(), 's')
            quizAttemptcopy.availableTime = diff
            quizAttemptcopy.limitTime = quizAttemptcopy.quizParent.limitTime
        }
        return {
            success: true,
            quizAttempt: quizAttemptcopy
        }
    },

    getByQuizId: async (idQuiz, student) => {
        const quiz = await QuizModel.findOne({
            _id: idQuiz
        })
        if (!quiz) {
            return {
                success: false,
                code: 404,
                error: `Quiz not found: ${idQuiz}`
            }
        }
        const quizAttempts = await QuizAttemptModel.find({
            quizParent: idQuiz,
            student: student._id
        })
            .sort({ status: -1 })
            .limit(10)
        return {
            success: true,
            quizAttempts
        }
    },

    answerQuestion: async (idQuizAttempt, data, student) => {
        const idQuizQuestion = data.idQuizQuestion
        const questionAnswers = data.questionAnswers
        const quizAttempt = await QuizAttemptModel.findOne({
            _id: idQuizAttempt
        })
            .populate({
                path: 'answerCollection'
            })
            .populate({
                path: 'questionCollection'
            })
            .populate({
                path: "quizParent"
            })
        if (!quizAttempt) {
            return {
                success: false,
                code: 404,
                error: `Quiz Attempt not found: ${idQuizAttempt}`
            }
        }
        if (quizAttempt.status !== "OPEN") {
            return {
                success: false,
                code: 401,
                error: `The attempt you are trying to send is Already Close`
            }
        }
        if (!quizAttempt.quizParent) {
            quizAttempt.status = "CLOSE"
            quizAttempt.percentageAnswer = 100
            quizAttempt.percentageResult = 0
            quizAttempt.save()
            return {
                success: false,
                code: 404,
                error: `The quiz you are trying to answer is not longer available`
            }
        }
        const quizQuestion = await QuizQuestionModel.findOne({
            _id: idQuizQuestion
        })
        if (!quizQuestion) {
            return {
                success: false,
                code: 404,
                error: `Quiz Question not found: ${idQuizQuestion}`
            }
        }
        const rightAnswer = quizQuestion.answerCollection.filter(answer => answer.validAnswer)
        let rightAnswerIds = []
        let isValid = false
        for (let i = 0; i < rightAnswer.length; i++) {
            rightAnswerIds.push(rightAnswer[i].idAnswer)
        }
        if (rightAnswerIds.sort().join(',') === questionAnswers.sort().join(',')) {
            isValid = true
        }
        let answersAttempt = quizAttempt.answerCollection.filter(answer => {
            return String(answer.question) == String(idQuizQuestion)
        })
        let quizAnswer
        if (answersAttempt.length > 0) {
            quizAnswer = await QuizAttemptAnswerModel.findOneAndUpdate({
                _id: answersAttempt[0]._id
            }, {
                $set: {
                    answers: questionAnswers,
                    answerCorrects: rightAnswerIds,
                    isCorrect: isValid
                }
            }, {
                upsert: false
            })
        } else {
            const idQuestionAnswer = new mongoose.Types.ObjectId()
            quizAnswer = new QuizAttemptAnswerModel({
                _id: idQuestionAnswer,
                quizAttemptParent: idQuizAttempt,
                student: student._id,
                question: idQuizQuestion,
                date: new Date(),
                answers: questionAnswers,
                answerCorrects: rightAnswerIds,
                isCorrect: isValid,
            })
            quizAnswer.save()
            quizAttempt.answerCollection.push(idQuestionAnswer)
        }
        quizAttempt.lastAnswerIndex = quizAttempt.answerCollection.length
        quizAttempt.save()
        return {
            success: true,
            quizAnswer
        }
    },

    finishQuizAttempt: async (idQuizAttempt, student) => {
        const quizAttempt = await QuizAttemptModel.findOne({
            _id: idQuizAttempt
        })
            .populate({
                path: 'questionCollection',
                path: 'answerCollection'
            })
        if (!quizAttempt) {
            return {
                success: false,
                code: 404,
                error: `Quiz Attempt not found: ${idQuizAttempt}`
            }
        }
        if (quizAttempt.status !== "OPEN") {
            return {
                success: false,
                code: 401,
                error: `The attempt you are trying to send is Already Close`
            }
        }
        const quiz = await QuizModel.findOne({
            _id: quizAttempt.quizParent
        })
        if (!quiz) {
            quizAttempt.status = "CLOSE"
            quizAttempt.percentageAnswer = 100
            quizAttempt.percentageResult = 0
            quizAttempt.totalQuestions = 0
            quizAttempt.rightQuestions = 0
            quizAttempt.save()
            return {
                success: false,
                code: 404,
                error: `The quiz you are trying to answer is not longer available`
            }
        }
        const answers = await QuizAttemptAnswerModel.find({
            quizAttemptParent: idQuizAttempt
        })
        let rightAnswer = 0
        for (let i = 0; i < answers.length; i++) {
            if (answers[i].isCorrect)
                rightAnswer++
        }
        let percentage = (rightAnswer * 100) / quizAttempt.questionCollection.length
        if (percentage > 100)
            percentage = 100
        quizAttempt.status = "CLOSE"
        quizAttempt.percentageAnswer = 100
        quizAttempt.percentageResult = percentage
        quizAttempt.answerCollection = answers
        quizAttempt.totalQuestions = quizAttempt.questionCollection.length
        quizAttempt.rightQuestions = rightAnswer
        quizAttempt.save()
        if (percentage >= 60) {
            let finishItem = {
                type: 3,
                _id: quiz._id
            }
            PlaylistMethod.updateProgress(quiz.parent, student._id, finishItem)
        }
        return {
            success: true,
            quizAttempt
        }

    },
}
module.exports = QuizAttempt;