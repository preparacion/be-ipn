/**
 * models/playlist.js
 *
 * @description :: Describes the playlist functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')

const moment = require('moment')

const AppoimentsModel = require('../../db/appoiments')
const PlaylistModel = require('../../db/playlists')
const StudentModel = require('../../db/students')
const StudentProgress = require('../../db/studentProgress')
const PaymentsModel = require('../../db/payments')
const GroupModel = require('../../db/groups')
const CourseModel = require('../../db/courses')

const StudentModelModel = require('../students')

const St = require('../../models/st')
const stObj = new St()


const Course = {
    /**
      * getCourses
      * @description Get the student courses info
      * @param {string} id Student's id. Mongo Object Id
      * @returns {object} The result
      */
    async getCourses(id) {
        const student = await StudentModel
            .findOne({ _id: id })
            .select('groups paymentsArranges')
            .populate({
                path: "groups",
                populate: {
                    path: "course",
                    populate: {
                        path: "liveStreamChannel"
                    }
                }
            })
        if (!student) {
            return {
                success: false,
                code: 404,
                error: `Student not found: ${id}`
            }
        }
        let coursesInfo = []
        for (let i = 0; i < student.groups.length; i++) {
            const course = student.groups[i].course
            if (course !== null && course !== undefined) {
                const group = student.groups[i]
                const courseInfo = {
                    _id: course._id,
                    name: course.name,
                    backgroundColor: course.backgroundColor,
                    image: course.image,
                    duration: course.duration,
                    activeLiveStream: course.activeLiveStream,
                    liveStreamChannelUrl: course.liveStreamChannel !== undefined ? course.liveStreamChannel.cdnUrl : "",
                    liveStreamUrl: course.liveStreamUrl,
                    active: course.active,
                    isActiveAppoiments: course.isActiveAppoiments,
                    price: course.price,
                    group: group,
                    playlists: []
                }
                //this will return true if fr
                if (group.isFreeLesson) {
                    if (moment(group.startDate).isSame(moment(), 'day')) {
                        courseInfo.isTodayFreeLesson = true
                    }
                    courseInfo.isTodayFreeLesson = false
                    courseInfo.isFreeLesson = true
                }
                if (student.paymentsArranges !== undefined) {
                    const index = student.paymentsArranges.findIndex(value => String(value.courseId) === String(course._id))
                    if (index >= 0) {
                        const arrangementPayment = student.paymentsArranges[index]
                        if (moment(arrangementPayment.endDate).isSameOrAfter(moment(), 'day')) {
                            courseInfo.hasArrangePayment = true
                        }
                    }
                }
                if ((moment(group.endDate).add(1, 'M')).isBefore(moment(), 'day')) {
                    courseInfo.isFinished = true
                }
                if (group.teacher) {
                    courseInfo['teacher'] = group.teacher
                }
                coursesInfo.push(courseInfo)
            }
        }
        return {
            success: true,
            courses: coursesInfo
        }
    },

// SAM
    async getCourseinfo(courseId, studentId) {
        const course = await CourseModel
            .findOne({ _id: courseId })
            .select('_id name isActiveAppoiments onlineAllowedMaterial')
        if (!course) {
            const errorExt = errMsg.getNotEntityFound('Course', courseId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        let courseArray = [courseId]
        if (course.onlineAllowedMaterial !== undefined) {
            const arrayCourseMaterial = course.onlineAllowedMaterial
            for (let i = 0; i < arrayCourseMaterial.length; i++) {
                courseArray.push(arrayCourseMaterial[i])
            }
        }
        let playlists = await PlaylistModel
            .find({ course: { $in: courseArray }, active: true })
        let PlaylistModi = {}
        let contentArray = []

        const finance = await stObj.getFinancial(studentId)
        if(finance){
            validate = finance.financial.payments
            for(let num=0; num<validate.length; num++){
               if(validate[num]._id == courseId)
                        if (validate[num].debt == true){
                         playlists = []
                   }
                }
            
        }

        if (playlists && playlists.length) {
            let arrayPlaylist = []
            await playlists.forEach(function (element) {
                arrayPlaylist.push(element._id)
            });
            const studentProgress = await StudentProgress.find({
                student: studentId,
                playlist: { $in: arrayPlaylist }
            })
            let totalItems = 0
            let totalFinishItems = 0
            for (let i = 0; i < studentProgress.length; i++) {
                let index = playlists.findIndex(item => String(item._id) == String(studentProgress[i].playlist))
                if (index >= 0) {
                    totalItems += playlists[index].totalItems
                    totalFinishItems += studentProgress[i].finishItemsCount
                    if (studentProgress[i].isLastItemHere) {


                    }
                }
            }
            let totalProgress = 0
            if (totalFinishItems > 0 && totalItems > 0) {
                totalProgress = (totalFinishItems * 100) / totalItems
            }
            PlaylistModi['contentPlaylist'] = contentArray
            return {
                success: true,
                course: {
                    _id: course._id,
                    name: course.name,
                    totalProgress: totalProgress,
                    isActiveAppoiments: course.isActiveAppoiments,
                    playlists
                },
                playlist: PlaylistModi,
            }
        } else {
            return {
                success: true,
                course: {
                    _id: course._id,
                    name: course.name,
                    playlists,
                }
            }
        }
    },

    /**
       * getPlayListsByCourse
       * @description Return the playlist associated to a specific course
       * @param {string} courseId Course id
       * @returns {array} List of playlist by course
       */
    async getPlayListsByCourse(courseId, student) {
        const groups = await GroupModel.find({
            _id: { $in: student.groups }
        })
        let index = groups.findIndex((value) => String(courseId) === String(value.course))
        if (index < 0) {
            return {
                success: false,
                code: 401,
                error: "No tienes acceso al material de este curso."
            }
        }
        //
        const course = await CourseModel
            .findOne({ _id: courseId })
            .select('_id name onlineAllowedMaterial specificAllowedPlaylist')
        if (!course) {
            const errorExt = errMsg.getNotEntityFound('Course', courseId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        let courseArray = [courseId]
        if (course.onlineAllowedMaterial !== undefined && course.onlineAllowedMaterial.length) {
            const arrayCourseMaterial = course.onlineAllowedMaterial
            for (let i = 0; i < arrayCourseMaterial.length; i++) {
                courseArray.push(arrayCourseMaterial[i])
            }
        }
        let playlists = await PlaylistModel
            .find({ course: { $in: courseArray }, active: true })
        if (course.specificAllowedPlaylist !== undefined && course.specificAllowedPlaylist.length) {
            for (let i = 0; i < course.specificAllowedPlaylist.length; i++) {
                const specificPlaylist = await PlaylistModel
                    .findOne({ _id: course.specificAllowedPlaylist[i], active: true })
                if (specificPlaylist)
                    playlists.push(specificPlaylist)
            }
        }
        let PlaylistModi = {}
        // PlaylistModi['courseInfo'] = course
        let contentArray = []
        if (playlists && playlists.length) {
            let arrayPlaylist = []
            await playlists.forEach(function (element) {
                element["type"] = 1;
                contentArray.push(element)
                arrayPlaylist.push(element._id)
            });

            const studentProgress = await StudentProgress.find({
                student: student._id,
                playlist: { $in: arrayPlaylist }
            })
            for (let i = 0; i < studentProgress.length; i++) {
                let index = playlists.findIndex(item => String(item._id) == String(studentProgress[i].playlist))
                if (index >= 0) {
                    let percentage = (studentProgress[i].finishItemsCount * 100) / playlists[index].totalItems
                    if (studentProgress[i].progress !== percentage) {
                        studentProgress[i].progress = isNaN(percentage) ? 0 : percentage
                        studentProgress[i].save()
                    }
                    playlists[index].progress = studentProgress[i].progress
                    playlists[index].isLastItemHere = studentProgress[i].isLastItemHere
                }
            }
            contentArray.sort(function (a, b) {
                if (a.order) {
                    if (b.order) {
                        if (a.order < b.order) return -1;
                        if (a.order > b.order) return 1;
                    } else {
                        return 1;
                    }
                } else {
                    return -1;
                }
                return 0;
            });
            PlaylistModi['contentPlaylist'] = contentArray

            return {
                success: true,
                course: {
                    _id: course._id,
                    name: course.name,
                    playlists
                },
                playlist: PlaylistModi,
            }
        }
    },


    /**
      * getGroupsByCourse
      * @description Get the active group from a specific course
      * @param {string} courseId Course id
      * @returns {array} List of groups
      */
    async getGroupsByCourse(courseId) {
        const course = await CourseModel
            .findOne({ _id: courseId })
        if (!course) {
            const errorExt = errMsg.getNotEntityFound('Course', courseId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }

        const groups = await GroupModel
            .aggregate([
                {
                    $match: {
                        course: new mongoose.Types.ObjectId(courseId),
                        startDate: { $gt: new Date() },
                        active: true,
                        deleted: { $ne: true }
                    }
                },
                {
                    $lookup: {
                        from: 'StudentLists',
                        let: { stList: '$studentList' },
                        pipeline: [
                            { $match: { $expr: { $eq: ['$$stList', '$_id'] } } },
                            { $project: { size: { $size: '$list' } } }
                        ],
                        as: 'stList'
                    }
                },
                { $unwind: '$stList' },
                {
                    $lookup: {
                        from: 'ClassRooms',
                        localField: "classRoom",
                        foreignField: "_id",
                        as: "classRoom"
                    }
                },
                { $unwind: '$classRoom' },
                {
                    $lookup: {
                        from: 'Locations',
                        localField: "classRoom.location",
                        foreignField: "_id",
                        as: "classRoom.location"
                    }
                },
                { $unwind: '$classRoom.location' }
            ])

        return {
            success: true,
            course,
            groups: groups.filter(group => {
                return group.stList.size < group.quota
            })
        }
    },

    async hasDebts(courseId, student) {
        const course = await CourseModel
            .findOne({ _id: courseId })
            .select("name price priceValidationOnlineMaterial priceByLesson")
        if (!course) {
            const errorExt = errMsg.getNotEntityFound('Course', courseId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        const groups = await GroupModel.find({
            _id: { $in: student.groups },
            course: courseId
        })
        const payments = await PaymentsModel.find({
            course: courseId,
            student: student._id
        })
        let totalPayments = 0
        for (let i = 0; i < payments.length; i++) {
            totalPayments += (payments[i].amount + payments[i].discount)
        }
        let groupsNumber = groups.length
        const debt = (groupsNumber * course.price) > (totalPayments)
        let debtQuantity = 0
        if (debt)
            debtQuantity = (groupsNumber * course.price) - totalPayments
        return {
            success: true,
            course,
            hasDebts: debt,
            debtQuantity: debtQuantity
        }
    },

    /**
    * getSchoolData
    * @description Get the students School Data
    * @param {string} id Student's id. Mongo Object Id
    * @returns {object} The result
    */
    async getSchoolData(id) {
        const resultCourse = await Course.getCourses(id)
        if (resultCourse.success) {
            const courses = [...resultCourse.courses]
            const appoiments = await AppoimentsModel.find({
                student: id
            })
                .populate({
                    path: 'group',
                    select: '_id name course',
                    populate: {
                        path: 'course',
                        select: '_id name'
                    }
                })
            let payments = []
            for (let i = 0; i < courses.length; i++) {
                const result = await StudentModelModel.calcBalanceByCourse(id, courses[i]._id)
                if (result !== undefined) {
                    payments.push(result)
                }
            }
            let schoolData = {}
            schoolData['activeCourses'] = courses.length
            schoolData['courses'] = courses
            schoolData['appoiments'] = appoiments
            schoolData['payments'] = payments
            return {
                success: true,
                schoolData
            }
        } else {
            return {
                success: false,
                code: 404,
                error: "Ocurrio un error al recuperar schoolData del estudiante:" + id,
            }
        }
    },
    /**
     * getCoursesByLevelId
     */
    getCoursesByLevelId: async id => {
        const courses = await CourseModel
            .find({ courseLevel: id, activeExternal: true })
            .populate("courseType")
            .then(courses => {
                return courses
            })
        return {
            success: true,
            courses
        }
    }
}
module.exports = Course