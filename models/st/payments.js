/**
 * models/st/payments.js
 *
 * @description :: Describes the paymentMethod functions
 * @docs        :: TODO
 */
const MercadoPago = require('../../lib/paymentsProcessor')

const PaymentsModel = require('../../models/payments')

const Payments = {

    createPayment: async (student, data) => {
        const result = await MercadoPago.createSinglePayment(student, data)
        if (result.success) {
            const bodyPayment = {
                amount: data.transaction_amount,
                concept: "Pago con tarjeta",
                course: data.courseId,
                paymentType: "card",
                student: student._id,
                onlinePaymentId: result.idOperation,
                status: result.status
            }
            const resultInternal = await PaymentsModel.create(bodyPayment)
            return result
        } else {
            return result
        }
    },

    doPaymetWithCard: async (student, data) => {
        const result = await MercadoPago.doPaymentToClient(student, data)
        if (result.success) {
            const bodyPayment = {
                amount: data.transaction_amount,
                concept: "Pago con tarjeta",
                course: data.courseId,
                paymentType: "card",
                student: student._id,
                onlinePaymentId: result.idOperation,
                status: result.status
            }
            const resultInternal = await PaymentsModel.create(bodyPayment)
            return result
        } else {
            return result
        }
    },

    doJustCargePaymetWithCard: async (student, data) => {
        let result
        if (data.paymentMethodId !== undefined) {
            result = await MercadoPago.createSinglePayment(student, data)
        } else {
            result = await MercadoPago.doPaymentToClient(student, data)
        }
        if (result.success) {
            return result
        } else {
            return result
        }
    },

    doPaymentGeneral: async (student, data) => {
        let result
        if (data.paymentMethodId !== undefined) {
            result = await Payments.createPayment(student, data)
        } else {
            result = await Payments.doPaymetWithCard(student, data)
        }
        if (result.success) {
            return result
        } else {
            return result
        }
    },



    addCardsToUser: async (student, data) => {
        const result = await MercadoPago.createClientAndAddCard(student.email, data.token)
        if (result.success) {
            return result
        } else {
            return result
        }
    },

    getCardsOfUser: async (student) => {
        const result = await MercadoPago.getCardsOfClientByEmail(student.email)
        if (result.success) {
            return result
        } else {
            return result
        }
    },

    editCardToUser: async (student, data) => {
        const result = await MercadoPago.editCartToClient(student.email, data)
        if (result.success) {
            return result
        } else {
            return result
        }
    },
    removeCardToUser: async (student, idCard) => {
        const result = await MercadoPago.removeCardToClient(student.email, idCard)
        if (result !== undefined && result.response !== undefined) {
            return {
                success: true,
                message: "Tarjeta borrada correctamente."
            }
        } else {
            return {
                success: false,
                message: "Ocurrió un error al eliminar tu tarjeta."
            }
        }
    }

}
module.exports = Payments