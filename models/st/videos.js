
/**
 * models/st/videos.js
 *
 * @description :: Describes the videos functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')

const VideoModel = require('../../db/videos')
const VideoCommentsModel = require('../../db/videoComments')
const StudentProgress = require('../../db/studentProgress')
const StudentModel = require('../../db/students')


const PlaylistMethod = require('./playlist')
const socket = require('../../socket')

const Videos = {

    getById: async (id, student) => {
        const result = await VideoModel
            .findOne({ _id: id })
            .then(video => {
                if (!video) {
                    return {
                        success: false,
                        code: 404,
                        error: `Video not found: ${id}`
                    }
                }
                let index = video.rates.findIndex(value => value.student == student._id)
                let rate = 0
                if (index >= 0) {
                    rate = video.rates[index].rate
                }
                return {
                    success: true,
                    video: video,
                    rate: rate
                }
            })
            .catch(err => {
                console.error('- Error trying to get a video by id', JSON.stringify(err, null, 2))
                return {
                    success: false,
                    code: 500,
                    error: JSON.stringify(err, null, 2)
                }
            })

        if ('success' in result && !result.success) {
            // Return error
            return result
        }

        let video = await result.video.toPopulate()
        video.myRate = result.rate
        return {
            success: true,
            video,
        }
    },

    comment: async (id, data) => {
        if (data._id) {
            delete data._id
        }
        const video = await VideoModel
            .findOne({ _id: id })
            .select('_id')
        if (!video) {
            const errorExt = errMsg
                .getNotEntityFound('Video', id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }

        const comment = new VideoCommentsModel({
            comment: data.comment,
            date: data.date,
            student: data.student,
            video: video._id,
            responses: []
        })
        await comment.save()

        return {
            success: true,
            comment
        }
    },

    //TODO
    editComment: async (id, data) => {

        const videoComment = await VideoCommentsModel
            .find({ _id: data.commentId })
        if (!videoComment) {
            const errorExt = errMsg
                .getNotEntityFound('VideoComment', commentId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        videoComment[0].comment = data.comment
        await videoComment[0].save()
        return {
            success: true,
            videoComment
        }
    },

    //TODO
    deleteComment: async (data) => {
        await VideoCommentsModel.deleteOne({ _id: data.commentId })
        return {
            success: true,
            deleted: true
        }
    },



    respondComment: async (commentId, data) => {
        if (data.id) {
            delete data.id
        }
        const videoComment = await VideoCommentsModel
            .findOne({ _id: commentId })
        if (!videoComment) {
            const errorExt = errMsg
                .getNotEntityFound('VideoComment', commentId)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        let responseCommentId = new mongoose.Types.ObjectId()
        const comment = new VideoCommentsModel({
            _id: responseCommentId,
            comment: data.comment,
            date: new Date(),
            student: data.student,
            parentComment: commentId,
        })
        await comment.save()
        videoComment.responses.push(responseCommentId)
        await videoComment.save()

        return {
            success: true,
            videoComment
        }
    },

    rate: async (data) => {
        const video = await VideoModel.findOne({ _id: data.id })
        if (!video) {
            return {
                success: false,
                code: 404,
                error: `Video not found: ${data.id}`
            }
        }
        const currentRate = await video.calculateRate(data)
        return {
            success: true,
            userRate: currentRate.rateUser,
            currentRate: currentRate.rateTotal
        }
    },

    watchVideo: async (id, student) => {
        /********************************************
        *************** Validation Begin *************
        *********************************************/
        const Student = await StudentModel
            .findOne({ _id: student._id })
        if (!Student) {
            const errorExt = errMsg
                .getNotEntityFound('Student', student._id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        const video = await VideoModel
            .findOneAndUpdate(
                { _id: id },
                { $inc: { 'views': 1 } })

        if (!video) {
            const errorExt = errMsg
                .getNotEntityFound('Video', id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        /********************************************
        *************** Validation END *************
        *********************************************/
        let finishItem = {
            type: 2,
            _id: id
        }
        PlaylistMethod.updateProgress(video.playlist, student._id, finishItem)
        return {
            success: true,
            video,
        }


    },
}
module.exports = Videos