/**
 * models/appoiments.js
 *
 * @description :: Describes the appoiments functions
 * @docs        :: TODO
 */
const _ = require('lodash')
const moment = require('moment')
const mongoose = require('mongoose')
const GroupsModel = require('../../db/groups')
const AppoimentsModel = require('../../db/appoiments')
const AppoimentsListModel = require('../../db/appoimentsList')

const ErrMsg = require('../../lib/errMsg')
const errMsg = new ErrMsg()

const Appoiments = {
    get: async (idGroup, student) => {
        const dicHours = ["8:00", "10:00", "12:00", "14:00", "16:00", "18:00"]
        let currentDate = moment(new Date()).set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 })
        let stopDate = moment(currentDate).add(1, 'w').set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 })
        let dateArray = []

        const group = await GroupsModel.findOne({
            _id: idGroup
        })
            .populate({
                path: 'course',
                select: '_id name isActiveAppoiments schedulesAppoimentList topicByDayList topicList',
                populate: {
                    path: 'topicList'
                }
            })
        if (!group) {
            const errorExt = errMsg
                .getNotEntityFound('Group', dataEvent.key)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        const appoimentList = await AppoimentsListModel.find({
            date: {
                $gte: currentDate.toISOString(),
                $lt: stopDate.toISOString()
            }
        })
        /* This is The date Generator base on the date now*/
        /* Check the hour available */
        const courseAvailableList = group.course.schedulesAppoimentList
        const isActive = group.course.isActiveAppoiments
        let objectHour = []
        if (isActive) {
            for (let i = 0; i < courseAvailableList.length; i++) {
                let day = courseAvailableList[i][0]
                let hour = courseAvailableList[i][2]
                if (hour == undefined) {
                    let obj = {
                        day,
                        arrayHour: [...dicHours]
                    }
                    objectHour.push(obj)
                } else {
                    let index = objectHour.findIndex((item) => item.day == day)
                    if (index !== -1) {
                        objectHour[index].arrayHour.push(dicHours[hour])
                        objectHour[index].arrayHour.sort()
                    } else {
                        let obj = {
                            day,
                            arrayHour: [dicHours[hour]]
                        }
                        objectHour.push(obj)
                    }
                }
            }

            let modulesByDay = _.groupBy(group.course.topicByDayList, (x) => x.day)

            while (currentDate < stopDate) {
                const nowDay = moment(currentDate).weekday()
                const index = objectHour.findIndex((object) => object.day == nowDay)
                if (index !== -1) {
                    const arrayHour = objectHour[index].arrayHour
                    let current = moment(currentDate).format('DD-MM-YYYY')
                    let arrayTime = []
                    if (appoimentList) {
                        let listOfCurrent = appoimentList.filter((value) => {
                            let date = moment(value.date).format('DD-MM-YYYY')
                            return (date == current)
                        })
                        for (let i = 0; i < arrayHour.length; i++) {
                            let index = listOfCurrent.findIndex(x => moment(x.date).format("HH:mm") == moment(arrayHour[i], 'HH:mm').format('HH:mm'))
                            if (index !== -1) {
                                if (listOfCurrent[index].maxQuota > listOfCurrent[index].appoiments.length) {
                                    let date = moment(current + " " + arrayHour[i], ('DD-MM-YYYY HH:mm')).format('HH:mm')
                                    arrayTime.push(date)
                                }
                            } else {
                                let date = moment(current + " " + arrayHour[i], ('DD-MM-YYYY HH:mm')).format('HH:mm')
                                arrayTime.push(date)
                            }
                        }
                    } else {
                        for (let i = 0; i < arrayHour.length; i++) {
                        }
                    }
                    let result = []
                    if (modulesByDay[nowDay]) {
                        let res = modulesByDay[nowDay]
                        for (let i = 0; i < res.length; i++) {
                            let obj = group.course.topicList.find((value) => value._id == res[i].topics)
                            result.push(obj)
                        }
                    }
                    dateArray.push({
                        day: currentDate,
                        hours: arrayTime,
                        modules: result
                    })
                }
                //increment the day counter 
                currentDate = moment(currentDate).add(1, 'd');
            }
        }
        return {
            success: true,
            dateArray,
        }
    },
    create: async (idGroup, data, student) => {
        const dateTime = moment(data.date + ' ' + data.hour, 'DD-MM-YYYY HH:mm')
        const group = await GroupsModel.findOne({
            _id: idGroup
        })
        if (!group) {
            const errorExt = errMsg
                .getNotEntityFound('Group', dataEvent.key)
            return {
                success: false,
                code: 404,
                error: "No group found",

            }
        }
        const appoiments = await AppoimentsModel.findOne({
            student: student._id,
            date: dateTime.toISOString()
        })
        if (appoiments) {
            return {
                success: false,
                code: 401,
                error: "Ya te encuentras inscrito a una asesorìa a esta misma hora",
            }
        }
        const appoimentsList = await AppoimentsListModel.findOne({
            course: group.course,
            date: dateTime.toISOString()
        })
        //IF Exist appoimentList create 
        if (appoimentsList) {
            if (appoimentsList.appoiments.lenght < 11 || appoimentsList.appoiments < appoimentsList.maxQuota) {
                return {
                    success: false,
                    code: 401,
                    error: "StudentList is overquota try other schedule",

                }
            } else {
                let appoimentId = new mongoose.Types.ObjectId()
                const appoiment = new AppoimentsModel({
                    _id: appoimentId,
                    date: dateTime.toISOString(),
                    student: student._id,
                    group: group._id,
                    parent: appoimentsList._id,
                    courseTopics: data.module
                })
                appoimentsList.appoiments.push(appoimentId)
                appoiment.save()
                appoimentsList.save()
                return {
                    success: true,
                    appoiment
                }
            }
        } else {
            let appoimentListId = new mongoose.Types.ObjectId()
            let appoimentId = new mongoose.Types.ObjectId()
            const appoiment = new AppoimentsModel({
                _id: appoimentId,
                date: dateTime.toISOString(),
                student: student._id,
                group: group._id,
                parent: appoimentListId,
                courseTopics: data.module
            })
            const appoimentList = new AppoimentsListModel({
                _id: appoimentListId,
                date: dateTime.toISOString(),
                appoiments: [appoimentId],
                course: group.course
            })
            appoiment.save()
            appoimentList.save()
            return {
                success: true,
                appoiment
            }
        }
    }
}

module.exports = Appoiments