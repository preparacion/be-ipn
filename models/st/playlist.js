/**
 * models/playlist.js
 *
 * @description :: Describes the playlist functions
 * @docs        :: TODO
 */
 const GroupModel = require('../../db/groups')
 const PlaylistModel = require('../../db/playlists')
 const StudentProgressModel = require('../../db/studentProgress')
 const StudentModel = require('../../db/students')

 const jwt = require('jsonwebtoken')
 const { jwtKey } = require('../../config')
 
 const Playlist = {
   getById: async (id, student) => {

      const token = student.authorization.split(' ')[1] || ''
      const decoded = jwt.verify(token, jwtKey)

      const std_jsn = JSON.parse(decoded.student)

       if (std_jsn.allowed == false) {
          return {
            success: false,
            code: 404,
            error: `Student not allowed to Playlist: ${id}`
          }
        }
      

     const result = await PlaylistModel
       .findOne({ _id: id })
       .populate('image')
       .then(playlist => {
         if (!playlist) {
           return {
             success: false,
             code: 404,
             error: `Playlist not found: ${id}`
           }
         }
         return {
           success: true,
           playlist: playlist
         }
       })
       .catch(err => {
         console.error('- Error trying to get a playlist by id', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     if ('success' in result && !result.success) {
       // Return error
       return result
     }
     let playlist = await result.playlist.toPopulate(student._id)
     playlist = playlist.toObject()
     if (playlist.course) {
       const groups = await GroupModel.find({
         _id: { $in: student.groups }
       })
       let index = groups.findIndex((value) => String(playlist.course) === String(value.course))
       if (index < 0) {
         delete playlist.course
         delete playlist.courseInfo
       }
     }
     const studentProgress = await StudentProgressModel.findOne({
       student: student._id,
       playlist: id
     })
     let playlistContent = [...playlist.contentPlaylist]
     if (studentProgress) {
       for (let i = 0; i < playlistContent.length; i++) {
         let current = playlistContent[i]
         if (studentProgress.finishItems.findIndex(item => String(item._id) == String(current._id)) >= 0) {
           playlistContent[i].viewed = true
         }
         if (String(studentProgress.lastItem) == String(current._id) && studentProgress.isLastItemHere && (i + 1) < playlistContent.length) {
           playlistContent[i + 1].lastItem = true
         }
       }
       playlist.progress = studentProgress.progress
     }
     playlist.contentPlaylist = playlistContent
     return {
       success: true,
       playlist: playlist
     }
   },
 
 
   getByCourseId: async (id, student) => {
     const playlists = await PlaylistModel
       .find({
         course: id,
         parent: { $exists: false },
         active: true
       })
       .populate('image')
       .sort({
         'order': 1
       })
       .then(playlists => {
         return {
           success: true,
           playlists
         }
       })
       .catch(err => {
         console.error('- Error trying to create a playlist', JSON.stringify(err, null, 2))
         return {
           success: false,
           code: 500,
           error: JSON.stringify(err, null, 2)
         }
       })
 
     if ('success' in playlists && !playlists.success) {
       return playlists // return error
     }
     // playlist populated
     const resPlaylist = await playlists.playlists.map(value => {
       value.type = 1
       return value
     })
 
     return {
       success: true,
       playlists: resPlaylist
     }
   },
   /**
    * This method is recursive WARNING, create or set 
    * thi studentProgress for a given playlist by buttom to top 
    * p.e child1.1.1->child1.1->child1->parentPlaylistProgress
    * playlistId - type Mongoo id
    * studentId - type Mongoo id
    * finishItems - object {_id:(Mongo id), type :(Number)}
    * *
   */
   updateProgress: async (playlistId, studentId, finishItems = undefined) => {
     let result = await PlaylistModel.findOne({ _id: playlistId })
     let progressObject = await StudentProgressModel.findOne({ playlist: playlistId, student: studentId })
     if (!progressObject) {
       let body = {
         playlist: playlistId,
         parent: result.parent,
         student: studentId,
         isLastItemHere: true,
         progress: (1 * 100) / result.totalItems,
         finishItemsCount: 1
       }
       if (finishItems !== undefined) {
         await StudentProgressModel.updateMany({ student: studentId }, { isLastItemHere: false })
         body.finishItems = [finishItems]
         body.lastItem = finishItems._id
       }
       studentP = new StudentProgressModel(body)
       studentP.save()
     } else {
       if (finishItems !== undefined) {
         if (progressObject.finishItems.findIndex(item => String(item._id) == String(finishItems._id)) >= 0)
           return
         await StudentProgressModel.updateMany({ student: studentId }, { isLastItemHere: false })
         progressObject = await StudentProgressModel.findOne({ playlist: playlistId, student: studentId })
         progressObject.finishItems.push(finishItems)
         progressObject.lastItem = finishItems._id
       }
       progressObject.finishItemsCount = progressObject.finishItemsCount + 1
       let itemsTotal = result.totalItems
       let finishedCount = progressObject.finishItemsCount
       let percentage = (finishedCount * 100) / itemsTotal
       if (percentage > 100)
         percentage = 100
       progressObject.progress = percentage
       progressObject.isLastItemHere = true
       progressObject.save()
     }
     if (result.parent !== undefined) {
       Playlist.updateProgress(result.parent, studentId)
     }
   },
 }
 
 module.exports = Playlist
 