/**
 * models/videos.js
 *
 * @description :: Describes the videos functions
 * @docs        :: TODO
 */
// const mongoose = require('mongoose')

const S3 = require('../lib/s3')
const ErrMsg = require('../lib/errMsg')
const ImageModel = require('../db/images')
const mongoose = require('mongoose')
const {
    awsCloudfromEndpointImage
} = require('../config')
// const VideoModel = require('../db/videos')
// const ReportModel = require('../db/reports')
// const StudentModel = require('../db/students')
// const PlaylistModel = require('../db/playlists')
// const QuizModel = require('../db/quiz')

const errMsg = new ErrMsg()
const TYPE_IMAGE = 2

const Images = {
    /* 
      * This function generate a pre-signed URL to return to server , then server upload content direct to s3
      * that's we manage the server load file problem.
      * Generate tmp image file
      */
    createUrl: async (data) => {
        let responseUrl = await S3.genereteS3SignedUrl(data.filename, TYPE_IMAGE)
        if (!responseUrl.success) {
            return {
                success: false,
                code: 500,
                error: `Error trying to get a url`
            }
        }
        const url = awsCloudfromEndpointImage + 'tmp/' + responseUrl.keyFile
        const imageId = new mongoose.Types.ObjectId()
        const image = new ImageModel({
            _id: imageId,
            course: data.course,
            url,
            key: responseUrl.keyFile,
            quizParent: data.quizParent,
            isTempImage: true
        })
        await image.save()
        return {
            success: true,
            image,
            url: responseUrl.url
        }
    },

    get: async (data) => {

    },

    getById: async (data) => {

    },

    delete: async (data) => {

    },

    /* 
  * This function generate a pre-signed URL to return to server , then server upload content direct to s3
  * that's we manage the server load file problem.
  */
    toProfile: async (data) => {
        let responseUrl = await S3.genereteS3SignedUrl(data.filename, TYPE_IMAGE)
        if (!responseUrl.success) {
            return {
                success: false,
                code: 500,
                error: `Error trying to get a url`
            }
        }
        return {
            success: true,
            url: responseUrl.url
        }
    },
}



module.exports = Images
