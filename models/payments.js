/**
 * models/payments.js
 *
 * @description :: Describes the payments functions
 * @docs        :: TODO
 */
const _ = require('lodash')

const CreditModel = require('../db/credits')
const CoursesModel = require('../db/courses')
const PaymentModel = require('../db/payments')
const StudentModel = require('../db/students')

const MercadoaPago = require('../lib/paymentsProcessor')

const SendGrid = require('../lib/sendGrid')
const SMS = require('../lib/sms')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Payments = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async ({ skip, limit } = {}) => {
    try{
    const payments = await PaymentModel
      .find({})
      .skip(skip)
      .limit(limit)
      .then(result => result)
      .catch(err => {
        console.error('- Error trying saveBalanceto get all payments.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying saveBalanceto get all payments.'
          }
        }
      })
    return {
      success: true,
      payments
    }
    
  } catch (err) {
    const errorExt = errMsg
      .generalGet('payments')
    return {
      success: false,
      error: errorExt
    }
  }
  
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const payment = await PaymentModel
      .findOne({ _id: id })
      .then(payment => {
        if (!payment) {
          return {
            success: false,
            code: 404,
            error: `Payment not found: ${id}`
          }
        }

        return {
          success: true,
          payment
        }
      })
      .catch(err => {
        console.error('- Error trying saveBalanceto get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return payment
  },

  getByStudentId: async studentId => {
    const payments = await PaymentModel
      .find({
        student: studentId
      })
      .populate({
        path: "user",
        select: "name lastName secondLastName"
      })
      .then(result => {
        return Promise
          .all(result.map(payment => Payments.getPaymentCourse(payment)))
      })
      .then(payments => payments)
      .catch(err => {
        console.error('- Error trying to get all payments.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      payments
    }
  },

  getByStudentIdGroupedByCourse: async studentId => {
    //first get the student you want to check.
    const student = await StudentModel
      .findOne({
        _id: studentId
      })
      .populate({
        path: "groups",
        select: "course",
      })
      .lean()
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', email)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const payments = await PaymentModel
      .find({
        student: studentId
      })
      .populate({
        path: "user",
        select: "name lastName secondLastName"
      })
      .lean()
      .catch(err => {
        console.error('- Error trying to get all payments.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    const groupedPayments = {}
    groupedPayments["otros"] = []
    // grouped by course id 
    payments.forEach(element => {
      if (element.course !== undefined) {
        if (groupedPayments[element.course] !== undefined) {
          groupedPayments[element.course].push(element)
        } else {
          groupedPayments[element.course] = [element]
        }
      } else {
        groupedPayments["otros"].push(element);
      }
    });
 // console.log(groupedPayments)
    //this will add the missing items into the array
    student.groups.forEach(element => {
      if (groupedPayments[element.course] !== undefined) {
      } else {
        groupedPayments[element.course] = []
      }
    })
    //Get keys and get course info 
    const keys = Object.keys(groupedPayments)
    const indexOtros = keys.findIndex(item => item === "otros");
    keys.splice(indexOtros, 1);
    /**
     *  NETWORK OPERATION
     */
    const coursesInfo = await CoursesModel.find({
      _id: { $in: keys }
    })
      .select("name price discounts")
      .lean();

    coursesInfo.push({
      _id: "otros",
      name: 'Otros',
      price: 0,
    })
    /**
     * END NETWORK OPERATION
     */
    coursesInfo.forEach((item, index) => {
      let totalPayments = 0;
      let totalDiscounts = 0;
      const paymentsArray = groupedPayments[item._id];
      paymentsArray.forEach((item, index) => {
        totalPayments = totalPayments + item.amount;
        totalDiscounts = totalDiscounts + item.discount;
      })
      delete coursesInfo[index].discounts
      coursesInfo[index].payments = paymentsArray;
      coursesInfo[index].totalPayments = totalPayments;
      coursesInfo[index].totalDiscounts = totalDiscounts;
      coursesInfo[index].debt = (totalPayments + totalDiscounts) < coursesInfo[index].price;

    })
    // console.log(coursesInfo)

    return {
      success: true,
      coursePayments: coursesInfo
    }
  },

  getPaymentCourse: async payment => {
    if (payment.course) {
      const course = await CoursesModel
        .findOne({ _id: payment.course })
        .select('name')

      return course
        ? _.extend({ courseInfo: course }, payment.toObject())
        : payment
    }
    return payment
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data, user) => {

try{

    //Add date if is not in data object 
    if (!data.date) {
      data['date'] = new Date()
    }
    if (user && user._id !== undefined) {
      data['user'] = user._id
    }
    //add credit 
    const credit = new CreditModel({
      amount: data['amount'] || 0,
      date: new Date(),
      student: data['student']
    })
    await credit.save()

    //Create payment model data to save 
    const payment = new PaymentModel(data)
    const result = await payment
      .save()
      .then(async payment => {
        return {
          success: true,
          payment
        }
      })
      .catch(err => {
        console.error('- Error trying to create payment', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create payment'
          }
        }
      })

    // Send email
    const student = await StudentModel.findOne({ _id: data['student'] })
    if (!student) {
      const errorExt = errMsg
      .getNotEntityFound('Student', data['student'])
    return {
      success: false,
      error: errorExt
    }
    }
    //Check if only register discount or payment.
    if (data.amount && data.amount > 0) {
      const emailData = {
        id: result.payment.onlinePaymentId !== undefined ? result.payment.onlinePaymentId : result.payment._id,
        to: student.email,
        name: student.name,
        lastName: student.lastName,
        email: student.email,
        phone: student.phoneNumber,
        date: new Date().toLocaleDateString(),
        items: [{
          description: data.concept || 'Pago a curso',
          date: new Date().toLocaleDateString(),
          total: data.amount
        }],
        totalDiscounts: payment.discount,
        granTotal: payment.amount
      }
      const emailResult = await SendGrid.sendPayment(emailData)
      let phoneNumber
      if (student.phoneNumber && student.phoneNumber.length === 10) {
        phoneNumber = `+521${student.phoneNumber}`
      } else if (student.secondPhoneNumber && student.secondPhoneNumber.length === 10) {
        phoneNumber = `+521${student.secondPhoneNumber}`
      }
      if (phoneNumber) {
        const sms = new SMS()

        const message = `Se ha recibido su pago por $${data.amount}, se ha enviado una copia del comprobante de pago a su correo electrónico.`

        await sms.sendMessage('prepIPN', phoneNumber, 'recepcion pago', message)
          .catch(err => {
            console.error(err)
          })
      }
      if (emailResult && !emailResult.success) {
        return emailResult
      }
    }
    payment.emailSent = true
    await payment.save()
    await student.saveBalance()
    return result

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('payments')
    return {
      success: false,
      error: errorExt
    }
  }


  },

  update: async (id, data) => {

    try{
    const result = await PaymentModel.findOneAndUpdate({
      _id: id
    }, {
      $set: data
    }, {
      upsert: false
    })
      .then(payment => {
        if (!payment) {
          const errorExt = errMsg
          .getNotEntityFound('payment', id)
        return {
          success: false,
          error: errorExt
        }
        }
        return {
          success: true,
          payment
        }
      })
      .catch(err => {
        console.error('- Error trying to update a payment', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB  Error trying to update a payment'
          }
        }
      })

    if ('success' in result && result.success && result.payment.student) {
      const student = await StudentModel
        .findOne({ _id: result.payment.student })
      if (student) {
        await student.saveBalance()
      }
    }

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('payments')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  send: async id => {
    const payment = await PaymentModel.findOne({ _id: id })
    if (!payment) {
      return {
        success: false,
        code: 404,
        error: `Payment not found: ${id}`
      }
    }

    const student = await StudentModel.findOne({
      _id: payment.student
    })

    const emailData = {
      id: payment._id,
      to: student.email,
      name: student.name,
      lastName: student.lastName,
      email: student.email,
      phone: student.phoneNumber,
      date: new Date().toLocaleDateString(),
      items: [{
        description: 'Pago a curso',
        date: payment.date || new Date(),
        total: payment.amount
      }],
      totalDiscounts: payment.discount,
      granTotal: payment.amount
    }

    const emailResult = await SendGrid.sendPayment(emailData)

    if (!emailResult.success) {
      return emailResult
    }

    return emailResult
  },


  // **** ARREGLAR SERVICIO
  delete: async id => {

    try{
    await PaymentModel
      .findOneAndDelete({ _id: id })
      .then(payment => {
        return payment
          ? StudentModel.findOne({ _id: payment.student }) : false
      })
      .then(student => {
        if (student) {
          return student.saveBalance()
        }
      })      
      .then(() => true)
      



    return {
      success: true,
      deleted: true
    }

  } catch (err) {
    const errorExt = errMsg
      .generalDelete('payments')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  webhookPayment: async (type, data) => {
    switch (type) {
      case "mercadopago":
        const pagoData = await MercadoaPago.findPaymentDetails(data)
        if (pagoData.success) {
          const status = pagoData.payment.status == "approved" ? 1 : pagoData.payment.status == "in_process" ? 2 : 3
          PaymentModel
            .findOneAndUpdate({
              onlinePaymentId: pagoData.payment.id
            }, {
              $set: {
                status: status
              }
            })
        }
        break;
      default:
        console.log("Ocurrio un error al verificar el tipo de operación recibida")
        break;
    }
  },
}
module.exports = Payments
