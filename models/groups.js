/**
 * models/groups.js
 *
 * @description :: Describes the groups functions
 * @docs        :: TODO
 */
const fs = require('fs')
const _ = require('underscore')
const moment = require('moment')
const mongoose = require('mongoose')

const GeneratePDF = require('../lib/generatePDF')
const SendGrid = require('../lib/sendGrid')
const Utils = require('../lib/Utils')
const ErrMsg = require('../lib/errMsg')

const StudentList = require('./studentLists')
const Schedules = require('./schedules')


const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const GroupModel = require('../db/groups')
const StudentListModel = require('../db/studentLists')
const PaymentInfoModel = require('../db/paymentInfo')
const PaymentModel = require('../db/payments')
const CreditModel = require('../db/credits')
const ClassRoomModel = require('../db/classRooms')
const LocationModel = require('../db/locations')
const AttendanceStudent = require('../db/attendanceStudent')
const CommentModel = require('../db/comments')


const errMsg = new ErrMsg()

const days = [
  'lunes', 'martes', 'miércoles',
  'jueves', 'viernes', 'sábado',
  'domingo'
]

const daysFL = ['l', 'm', 'w', 'j', 'v', 's', 'd']

const Groups = {

  getLite: async ({
    skip,
    limit,
    stRngStart,
    stRngEnd,
    endRngStart,
    endRngEnd,
    schedule,
    course,
    location,
    teacher,
    showAll
  } = {}) => {

    /*******************************
     ******* FILTER AREA ***********
     ******************************/
    let filter = {
      deleted: {
        $ne: true
      },
      isBucket: { $ne: true }
    }

    if (schedule) {
      filter['schedule'] = schedule
    }

    if (course) {
      filter['course'] = course
    }

    if (teacher) {
      filter['teacher'] = teacher
    }

    if (location) {
      const classRooms = await ClassRoomModel
        .find({ location })
        .select('_id')
        .then(result => {
          return result.map(classRoom => classRoom._id)
        })
      filter['classRoom'] = { $in: classRooms }
    }

    if (stRngStart) {
      const stRngStartDate = moment
        .utc(new Date(stRngStart))
        .hour(0)
        .minute(0)

      filter['startDate'] = {
        $gte: stRngStartDate.format()
      }
    } else if (stRngEnd) {
      const stRngEndDate = moment
        .utc(new Date(stRngEnd))
        .hour(23)
        .minute(59)

      filter['startDate'] = {
        $lte: stRngEndDate.format()
      }
    }

    if (stRngStart && stRngEnd) {
      const stRngStartDate = moment
        .utc(new Date(stRngStart))
        .hour(0)
        .minute(0)
      const stRngEndDate = moment
        .utc(new Date(stRngEnd))
        .hour(23)
        .minute(59)
      filter['startDate'] = {
        $gte: stRngStartDate.format(),
        $lte: stRngEndDate.format()
      }
    }

    if (endRngStart) {
      const endRngStartDate = moment
        .utc(new Date(endRngStart))
        .hour(0)
        .minute(0)

      filter['endDate'] = {
        $gte: endRngStartDate.format()
      }
    } else if (endRngEnd) {
      const endRngEndDate = moment
        .utc(new Date(endRngEnd))
        .hour(23)
        .minute(59)

      filter['endDate'] = {
        $lte: endRngEndDate.format()
      }
    }

    if (endRngStart && endRngEnd) {
      const endRngStartDate = moment
        .utc(new Date(endRngStart))
        .hour(0)
        .minute(0)
      const endRngEndDate = moment
        .utc(new Date(endRngEnd))
        .hour(23)
        .minute(59)

      filter['endDate'] = {
        $gte: endRngStartDate.format(),
        $lte: endRngEndDate.format()
      }
    }

    /* display only active groups by default */
    if (!filter['endDate'] && !filter['startDate']) {
      filter['endDate'] = {
        '$gte': moment
          .utc(new Date())
          .hour(0)
          .minute(0)
          .format()
      }
    }

    // reset all filters!!! Don't use it!!!
    if (showAll) {
      filter = {
        $or: [
          {
            deleted: { $exists: false }
          },
          {
            deleted: { $eq: false }
          }
        ]
      }
    }

    const selectFields = [
      '_id', 'name', 'startDate', 'active',
      'quota', 'course', 'schedule', 'classRoom',
      'studentList', 'etapa', 'phase', 'endDate',
      'schedules'
    ]

    const locations = await LocationModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all locations.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    const groups = await GroupModel
      .find(filter)
      .limit(limit)
      .skip(skip)
      .select(selectFields.join(' '))
      .populate({
        path: "studentList",
        select: 'list'
      })
      .populate({
        path: "classRoom",
        populate: {
          path: 'location'
        }
      })
      .lean()
      .then((groups) => {
        return Promise.all(groups.map(group => {
          return Groups.toLite(group, locations)
        }))
      })
      .then(groups => groups)
      .catch(err => {
        console.error('- Error trying to get all groups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      groups
    }
  },


  getGroupsService: async ({
    skip, //pagination
    limit, //pagination
    stRngStart, //range start a
    stRngEnd, //range start b
    endRngStart,//range end a
    endRngEnd,//range end b
    schedule, // specific schedule 
    course, // specific course
    location, //specific location
    teacher, //specific teacher 
    showAll //Warning this will return all groups
  } = {}) => {
    let filter = {}


    if (showAll) {
      filter = {
        $or: [
          {
            deleted: { $exists: false }
          },
          {
            deleted: { $eq: false }
          }
        ]
      }
    } else {

    }


    const groups = await GroupModel
      .find(filter)
      .limit(limit)
      .skip(skip)
      .select(selectFields.join(' '))
      .populate({
        path: "studentList",
        select: 'list'
      })
      .populate({
        path: "classRoom",
        populate: {
          path: 'location'
        }
      })
      .lean()
    if (groups !== undefined) {
      groups.forEach((groupElement, index) => {
        Groups.toLite(groupElement);
      })
    } else {
      return {
        code: 404,
        error: `Not groups returned with search parameters`
      }
    }
    return {
      success: true,
      groups
    }
  },

  toLite: (group, location) => {
    const data = group
    data['percentage'] = 0
    data['studentListLength'] = 0

    if (group.studentList) {
      const studentList = group.studentList
      const percentage = studentList.list !== undefined ? (studentList.list.length / group.quota) : 0
      data['percentage'] = percentage * 100
      data['studentListLength'] = studentList.list !== undefined ? studentList.list.length : 0
    }
    if (group.classRoom) {
      data['locationInfo'] = group.classRoom.location
    }
    return data
  },
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({
    skip,
    limit,
    stRngStart,
    stRngEnd,
    endRngStart,
    endRngEnd,
    schedule,
    course,
    location,
    teacher,
    showAll
  } = {}) => {

try{


    let filter = {
      deleted: { $ne: true }
    }

    if (schedule) {
      filter['schedule'] = schedule
    }

    if (course) {
      filter['course'] = course
    }

    if (teacher) {
      filter['teacher'] = teacher
    }

    if (location) {
      const classRooms = await ClassRoomModel
        .find({ location })
        .select('_id')
        .then(result => {
          return result.map(classRoom => classRoom._id)
        })
      filter['classRoom'] = { $in: classRooms }
    }

    if (stRngStart) {
      const stRngStartDate = moment
        .utc(new Date(stRngStart))
        .hour(0)
        .minute(0)

      filter['startDate'] = {
        $gte: stRngStartDate.format()
      }
    } else if (stRngEnd) {
      const stRngEndDate = moment
        .utc(new Date(stRngEnd))
        .hour(23)
        .minute(59)

      filter['startDate'] = {
        $lte: stRngEndDate.format()
      }
    }

    if (stRngStart && stRngEnd) {
      const stRngStartDate = moment
        .utc(new Date(stRngStart))
        .hour(0)
        .minute(0)
      const stRngEndDate = moment
        .utc(new Date(stRngEnd))
        .hour(23)
        .minute(59)
      filter['startDate'] = {
        $gte: stRngStartDate.format(),
        $lte: stRngEndDate.format()
      }
    }

    if (endRngStart) {
      const endRngStartDate = moment
        .utc(new Date(endRngStart))
        .hour(0)
        .minute(0)

      filter['endDate'] = {
        $gte: endRngStartDate.format()
      }
    } else if (endRngEnd) {
      const endRngEndDate = moment
        .utc(new Date(endRngEnd))
        .hour(23)
        .minute(59)

      filter['endDate'] = {
        $lte: endRngEndDate.format()
      }
    }

    if (endRngStart && endRngEnd) {
      const endRngStartDate = moment
        .utc(new Date(endRngStart))
        .hour(0)
        .minute(0)
      const endRngEndDate = moment
        .utc(new Date(endRngEnd))
        .hour(23)
        .minute(59)

      filter['endDate'] = {
        $gte: endRngStartDate.format(),
        $lte: endRngEndDate.format()
      }
    }

    /* display only active groups by default */
    if (!filter['endDate']) {
      filter['endDate'] = {
        '$gte': moment
          .utc(new Date())
          .hour(0)
          .minute(0)
          .format()
      }
    }

    const groups = await GroupModel
      .find(filter)
      .limit(limit)
      .skip(skip)
      .then(groups => {
        return {
          success: true,
          groups
        }
      })
      .catch(err => {
        console.error('- Error trying to get all groups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all groups.'
          }
        }
      })

    groups.groups = await Promise.all(groups.groups.map(group => group.toPopulate()))

    return groups

    
  } catch (err) {
    const errorExt = errMsg
      .generalGet('groups')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  getInfoById: async id => {
    const group = await GroupModel
      .findOne({ _id: id })
      .catch(err => {
        console.error('- Error trying to get a group info by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    if (!group) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${id}`
      }
    }

    return {
      success: true,
      group
    }
  },

  /**
   * getById
   *
   * @description: Returns all the items
   * @param id {string} - Group id
   */
  getById: async id => {
    const group = await GroupModel.findOne({ _id: id })
      .then(group => {
        if (!group) {
          const errorExt = errMsg
            .getNotEntityFound('Group', id)
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
        return group.toPopulate()
      })
      .catch(err => {
        console.error('- Error trying to get a group by id')
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      group
    }
  },

  /**
   * This method will return all data for student in a single request to evoid multiple requests in front end
   * Only return the id of the group
   */
  getSidebarInfoById: async id => {
    const group = await GroupModel.findOne({
      _id: id
    }).populate({
      path: "studentList",
      populate: {
        path: 'list',
        select: "name lastName secondLastName phoneNumber secondPhoneNumber email groups balance debt"
      },
      select: 'list',
    })
      .populate({
        path: "buckets",
        select: "name course comipemsPhase quota schedules startDate endDate active classRoom studentList",
        populate: [{
          path: "course",
          select: "name"
        }, {
          path: "studentList",
          select: "list"
        }, {
          path: "classRoom",
          populate: "location"
        }],
      })
      .populate("teacher")
      .populate({
        path: "course",
        select: "name"
      })
      .populate({
        path: "classRoom",
        populate: {
          path: "location",
          select: "name"
        },
        select: "name location"
      })
      .lean()
    //lean will return a simple POJO instead mongodb Document class
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    let students = [...group.studentList.list]
    const d = moment().toDate()
    let sd = moment(d).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    if (students.length > 0) {
      Groups.toLite(group, undefined);
      //this will search all attendance in once step 
      const attendanceArray = await AttendanceStudent
        .find({
          student: { $in: students },
          attendance: {
            $elemMatch: {
              group: group._id,
              date: { $gte: sd }
            }
          }
        })

      const studentsComments = await CommentModel.find({
        group: id
      })
        .populate({
          path: "user",
          select: "name lastName"
        })
        .populate({
          path: "editions",
          populate: {
            path: "user",
            select: "name lastName"
          }
        })
        .sort({ student: 1 })
        .lean()

      for (let i = 0; i < students.length; i++) {
        /**
         * Attendance 
         */
        const objectAttendance = attendanceArray.find(element => String(element.student) === String(students[i]._id))
        //at this point if objectAttendances has something we know that has attendance for today.
        if (objectAttendance) {
          const indexAttendance = objectAttendance.attendance.findIndex(element => moment(element.date).isSameOrAfter(sd) && String(element.group) === String(group._id))
          students[i].todayAttendance = objectAttendance.attendance[indexAttendance].attendance
        } else {
          students[i].todayAttendance = false
        }
        /**
         * Comments
         */
        if (studentsComments !== undefined && studentsComments.length > 0) {
          const comments = studentsComments.filter(item => String(item.student) === String(students[i]._id))
          students[i].comments = comments
        } else {
          students[i].comments = []
        }
      }
      group.studentList = students;
    }

    if (group.buckets && group.buckets.length > 0) {
      group.buckets.forEach((element, index) => {
        const resultado = Groups.toLite(element, undefined);
        group.buckets[index] = resultado;
      });
    }

    return {
      success: true,
      group
    }
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data - List of attributes.
   */

  create: async data => {
    try{
    if (data.startDate && data.endDate && data.schedules) {
      const schedulesArray = data.schedules.map(schedule => {
        return {
          day: daysFL[schedule.day]
        }
      })
      if (
        !Schedules._hasDaysInRange(
          data.startDate,
          data.endDate,
          schedulesArray
        )
      ) {
        const errorExt = errMsg
          .getNoDaysInRange()
          return {
            success: false,
            error: errorExt
          }
      }
    }

    let course = null
    let isHead = false
    if (data.course) {
      course = await CourseModel
        .findOne({ _id: data.course })
      if (!course) {
        const errorExt = errMsg
          .getNotEntityFound('Course', data.course)
          return {
            success: false,
            error: errorExt
          }
      }
      if (course.isParent) {
        if (data.classRoom) {
          delete data.classRoom
        }
        if (data.location) {
          delete data.location
        }
        if (data.teacher) {
          delete data.teacher
        }
        data.quota = 1000000
        isHead = true
      }
    }

    if (!data.studentList) {
      const id = new mongoose.Types.ObjectId()
      const name = data.name || 'studentList'
      const studentList = new StudentListModel({
        list: [],
        _id: id,
        name
      })
      data['studentList'] = id
      await studentList.save()
    }
    const bucketGroups = []
    if (isHead) {
      // createBuckets
      const buckets = await Groups.createBuckets(course, data)
        .then(result => {
          const buckets = []
          result.forEach(phases => {
            phases.forEach(bucket => {
              buckets.push(bucket)
            })
          })
          return buckets
        })
      buckets.forEach(bucket => {
        bucketGroups.push(bucket)
      })
    }

    if (bucketGroups.length) {
      data['buckets'] = bucketGroups.map(group => group._id)
    }

    let groupSaved = await GroupModel.create(data)
    let result = await GroupModel.findById({
      _id: groupSaved._id
    })
      .populate({
        path: "studentList",
        populate: {
          path: 'list',
          select: "name lastName secondLastName phoneNumber secondPhoneNumber email balance debt"
        },
        select: 'list',
      })
      .populate("teacher")
      .populate({
        path: "classRoom",
        populate: {
          path: "location",
          select: "name"
        },
        select: "name location"
      })
      .lean()

    if (bucketGroups.length) {
      result.buckets = bucketGroups
    }
    if (result.classRoom && result.classRoom.location)
      result.locationInfo = result.classRoom.location
    result.percentage = 0
    result.studentListLength = 0;
    return {
      success: true,
      group: result
    }

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('groups')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  createBuckets: async (course, data) => {
    const phasesCount = course.children ? course.children.length : 0
    return Promise
      .all(course.children.map(courseId => {
        return Groups.createBucketGroups(courseId, data, phasesCount)
      }))
  },

  createBucketGroups: async (courseId, data, phasesCount) => {
    const childCourse = await CourseModel
      .findOne({ _id: courseId })

    const moduleName = childCourse.name
    const promises = []
    for (let i = 1; i <= phasesCount; i++) {
      const name = `${moduleName} - ${i}`
      const startDate = moment.utc(data.startDate)
        .add(34 * (i - 1), 'days').format()
      const endDate = moment.utc(data.startDate)
        .add(34 * i, 'days').format()

      const groupData = {
        active: true,
        quota: 1000000,
        schedules: data.schedules || [],
        course: childCourse._id,
        comipemsPhase: i,
        isBucket: true,
        name,
        startDate,
        endDate
      }
      promises.push(Groups.createGroup(groupData))
    }
    return Promise.all(promises.map(p => p))
  },

  createGroup: async data => {
    const studentList = new StudentListModel({
      name: data.name,
      list: []
    })
    await studentList.save()
    data.studentList = studentList._id

    const bucketGroup = new GroupModel(data)
    await bucketGroup.save()
    return bucketGroup
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {

    try{
    if (data.startDate && data.endDate && data.schedules.length) {
      const schedulesArray = data.schedules.map(schedule => {
        return {
          day: daysFL[schedule.day]
        }
      })
      if (
        !Schedules._hasDaysInRange(
          data.startDate,
          data.endDate,
          schedulesArray
        )
      ) {
        const errorExt = errMsg
          .getNoDaysInRange()
        return {
          success: false,
          error: errorExt
        }
      }
    }
    const group = await Groups.getById(id)

    if (!group.success) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
        return {
          success: false,
          error: errorExt
        }
    }

    const result = await GroupModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        new: true
      })
      .populate({
        path: "studentList",
        populate: {
          path: 'list',
          select: "name lastName secondLastName phoneNumber secondPhoneNumber email balance debt"
        },
        select: 'list',
      })
      .populate("teacher")
      .populate({
        path: "course",
        select: "name"
      })
      .populate({
        path: "classRoom",
        populate: {
          path: "location",
          select: "name"
        },
        select: "name location"
      })
      .lean()

    Groups.toLite(result, undefined);
    let students = [...result.studentList.list]
    const d = moment().toDate()
    let sd = moment(d).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    if (students.length > 0) {
      //this will search all attendance in once step 
      const attendanceArray = await AttendanceStudent
        .find({
          student: { $in: students },
          attendance: {
            $elemMatch: {
              group: id,
              date: { $gte: sd }
            }
          }
        })
        .lean()

      const studentsComments = await CommentModel.find({
        group: id
      })
        .populate({
          path: "user",
          select: "name lastName"
        })
        .populate({
          path: "editions",
          populate: {
            path: "user",
            select: "name lastName"
          }
        })
        .sort({ student: 1 })
        .lean()

      for (let i = 0; i < students.length; i++) {
        /**
         * Attendance 
         */
        const objectAttendance = attendanceArray.find(element => String(element.student) === String(students[i]._id))
        //at this point if objectAttendances has something we know that has attendance for today.
        if (objectAttendance) {
          const indexAttendance = objectAttendance.attendance.findIndex(element => moment(element.date).isSameOrAfter(sd) && String(element.group) === String(id))
          students[i].todayAttendance = objectAttendance.attendance[indexAttendance].attendance
        } else {
          students[i].todayAttendance = false
        }
        /**
         * Comments
         */
        if (studentsComments !== undefined && studentsComments.length > 0) {
          const comments = studentsComments.filter(item => String(item.student) === String(students[i]._id))
          students[i].comments = comments
        }
      }
      result.studentList = students;
    }
    return {
      success: true,
      group: result
    }

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('groups')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * delete
   * @description Sets the attribute 'deleted' in true
   */
  delete: async id => {

    try{
    const group = await GroupModel
      .findOne({
        _id: id
      })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
        return {
          success: false,
          error: errorExt
        }
    }
    const studentList = await StudentListModel
      .findOne({
        _id: group.studentList
      })

    if (group) {
      group.deleted = true
      group.delDate = new Date()
      await group.save()
    }

    if (studentList) {
      studentList.deleted = true
      studentList.delDate = new Date()
      await studentList.save()

      await Promise.all(
        studentList.list.map(studentId => {
          return Groups.removeFromStudent(studentId, group._id)
        })
      )
    }

    return {
      success: true,
      message: 'deleted'
    }

  } catch (err) {
    const errorExt = errMsg
      .generalDelete('groups')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * removeFromStudent
   * @param {string} studentId Student object id
   * @param {string} groupId Group object id
   */
  removeFromStudent: async (studentId, groupId) => {
    const student = await StudentModel.findOne({ _id: studentId })
    if (!student) {
      return false
    }
    const index = student.groups.indexOf(groupId)
    if (index > -1) {
      student.groups.splice(index, 1)
      await student.save()
    }
  },

  getAvailable: async group => {
    const quota = group.quota

    const studentListId = group.studentList

    const studentListResult = await StudentList.getById(studentListId)
    const studentList = studentListResult.studentList
    const enrolled = studentList.list.length

    const vailable = quota - enrolled

    const result = Object.assign(group, { available: vailable })

    return result
  },

  /**
   * _isAlreadyEnrolled
   * @param {string} stId - student object id
   * @param {string} groupId - group object id
   * @returns {boolean} If a student is already enrolled to a course
   */
  _isAlreadyEnrolled: async (stId, groupId) => {
    const groupIds = await StudentListModel
      .find({ list: stId })
      .select('_id')
      .then(stLists => stLists.map(stList => stList._id))
      .then(stListIds => {
        return Promise
          .all(stListIds.map(stListId => {
            return GroupModel
              .findOne({ studentList: stListId })
              .select('_id')
          }))
      })
      .then(groups => groups.map(group => String(group._id)))

    return groupIds.indexOf(String(groupId)) !== -1
  },

  enrollBucketGroup: async (courseId, group, studentId, phase) => {
    const bucketGroup = await Promise
      .all(group.buckets.map(bId => GroupModel.findOne({ _id: bId })
        .populate("teacher")
        .populate({
          path: "classRoom",
          populate: "location"
        })
        .lean()
      ))
      .then(buckets => {
        return buckets.filter(bucket => {
          return String(bucket.course) === String(courseId) &&
            String(bucket.comipemsPhase) === String(phase)
        })
      })
      .then(buckets => buckets[0])

    const studentList = await StudentListModel
      .findOne({ _id: bucketGroup.studentList })
    if (!studentList) {
      throw new Error('No studentList found: ' + bucketGroup.studentList)
    }
    studentList.list.push(studentId)
    await studentList.save()
    return {
      ...bucketGroup,
      phase: phase
    };
  },

  enrollBucket: async (group, student, course, data, user) => {
    if (!course.children || !course.children.length) {
      return {
        code: 400,
        error: 'No children courses',
        success: false
      }
    }
    const customPhaseOrder = data.customPhaseOrder ? data.customPhaseOrder : [];
    let phaseOrder
    //if has not customPhaseOrder then generate the random function.
    if (customPhaseOrder.length > 0) {
      if (customPhaseOrder.length < course.children.length) {

      }
    } else {
      phaseOrder = await Utils.shuffleArray(course.children.length)
    }
    let buckets = await Promise
      .all(course.children.map((courseId, index) => {
        return Groups.enrollBucketGroup(courseId, group, student._id, phaseOrder[index])
      }))
    buckets = buckets.sort((a, b) => {
      return a.phase - b.phase;
    })

    let orderPhase = []
    buckets.forEach(bucket => {
      orderPhase.push(bucket._id);
    });

    student.comipemsOrder = orderPhase;

    if (student.groups) {
      student.groups.push(group._id)
    } else {
      student.groups = [group._id]
    }

    const studentListId = group.studentList
    const studentList = await StudentListModel.findOne({ _id: studentListId })
      .then(studentList => studentList)

    studentList.list.push(student._id)
    studentList.material.push({
      student: student._id,
      material: false
    })

    await student.save()
    await studentList.save()

    let paymentInfoId, paymentId, creditId
    if (data.payment) {
      data.payment['course'] = course._id
      data.payment['student'] = student._id
      data.payment['date'] = new Date()
      if (user && user._id !== undefined) {
        data.payment['user'] = user._id;
      }
      if (data.payment['paymentType'] === 'card') {
        paymentInfoId = new mongoose.Types.ObjectId()
        const paymentInfo = new PaymentInfoModel({
          _id: paymentInfoId,
          cardNumber: data.payment['cardNumber'],
          cardType: data.payment['cardType'],
          cvv: data.payment['cvv'],
          expirationDate: data.payment['expirationDate']
        })
        await paymentInfo.save()
      }

      paymentId = new mongoose.Types.ObjectId()
      data.payment = _.extend(data.payment, { _id: paymentId })
      const payment = new PaymentModel(data.payment)
      await payment.save()

      creditId = new mongoose.Types.ObjectId()
      const credit = new CreditModel({
        _id: creditId,
        amount: data.payment.amount,
        date: new Date(),
        student: student._id
      })
      await credit.save()
      await student.saveBalance()
    }
    const studentNew = { ...student._doc }
    const bucketsPopulated = await GroupModel.find({
      _id: { $in: buckets }
    })
      .populate({
        path: "teacher"
      })
      .populate({
        path: "classRoom",
        populate: "location"
      })
      .lean()
    studentNew.comipemsOrder = bucketsPopulated;
    const generator = new GeneratePDF()
    const b64Data = await generator.generateComipems(studentNew)

    const schedulesInfo = group.schedules.map(schedule => {
      const startHour = formatHour(String(schedule.startHour))
      const endHour = formatHour(String(schedule.endHour))
      return {
        day: days[schedule.day],
        startHour: startHour.string,
        endHour: endHour.string
      }
    })
    const startDate = moment(group.startDate).format('DD/MM/YYYY');

    await SendGrid.sendComipems({
      email: student.email,
      name: student.name,
      startDate: startDate,
      schedules: schedulesInfo,
      attachments: [
        {
          content: b64Data,
          filename: 'comprobante.pdf',
          type: 'application/pdf'
        }
      ],
      buckets
    })

    return {
      success: true,
      enrolled: true,
      creditId,
      paymentInfoId,
      paymentId,
      buckets
    }
  },

  /**
   * 
   * @param groupId String 
   * @param studentId String 
   * @param data Object 
   *  @param user Object 
   */

  updateBuckets: async (groupId, data, user) => {

    const group = await GroupModel.findById({
      _id: groupId
    })
      .populate({
        path: "course"
      })
      .populate({
        path: "buckets",
        populate: "studentList"
      })

    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', groupId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const student = await StudentModel.findById({
      _id: data.studentId
    })
      .populate("comipemsOrder");

    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const newBuckets = data.buckets;
    //Check if the parameters is valid entry into the buckets in course 
    newBuckets.forEach(bucket => {
      const index = group.course.buckets.findIndex(item => String(item._id) === String(bucket));
      if (index < 0) {
        const errorExt = errMsg.getTemplateMissingParams()
        return {
          success: false,
          code: errMsg.code,
          error: errorExt,
          errorExt
        }
      }
    })

    //Here start the buckets
    let buckets = oldGroup.buckets
    let groupsBucket = await GroupModel.find({ _id: { $in: buckets } }).populate({ path: "studentList" }).lean()
    //Empty student studentList of buckets
    for (let i = 0; i < groupsBucket.length; i++) {
      let index = groupsBucket[i].studentList ? groupsBucket[i].studentList.list.findIndex(item => JSON.stringify(item) == JSON.stringify(student._id)) : -1
      if (index >= 0) {
        let studentListPre = new StudentListModel(groupsBucket[i].studentList)
        studentListPre.list.splice(index, 1)
        await studentListPre.save()
      }
    }


  },

  /**
   * 
   * @param {*} id 
   * @param {*} studentId 
   * @param {*} data 
   * @param {*} user 
   */

  enroll: async (id, studentId, data, user) => {
    let paymentInfoId, paymentId, creditId
    const student = await StudentModel.findOne({ _id: studentId })
      .then(student => student)
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const group = await GroupModel.findOne({ _id: id })
      .populate("course")
      .populate("studentList")
      .populate({
        path: "classRoom",
        populate: "location"
      })
      .then(group => group)
    if (!group) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${id}`
      }
    }

    const course = group.course;

    if (student.groups && student.groups.indexOf(group._id) > -1) {
      // Already enrolled to this group
      const errorExt = errMsg.getStudentAlreadyEnrolled('group', id)
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }

    // verify that a student only can enroll a course once.
    const alreadyEnrolled = await Groups
      ._isAlreadyEnrolled(student._id, group._id)

    if (alreadyEnrolled) {
      // Already enrolled to this course
      const errorExt = errMsg.getStudentAlreadyEnrolled('course', id)
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }


    const studentList = group.studentList

    if (group.quota <= studentList.list.length) {
      return {
        success: false,
        error: `No se puede inscribir, cupo lleno`,
        code: 400
      }
    }

    if (course.isParent) {
      return Groups.enrollBucket(group, student, course, data, user)
    }

    // TODO Chek why is 2 times in this section the already enrolled 
    if (student.groups) {
      if (student.groups.indexOf(id) !== -1) {
        // Already enrolled to this course
        const errorExt = errMsg
          .getStudentAlreadyEnrolled('course', id)
        return {
          success: false,
          code: 400,
          error: errorExt.error,
          errorExt
        }
      }
      student.groups.push(group._id)
    } else {
      student.groups = [id]
    }

    studentList.list.push(studentId)
    studentList.material.push({
      student: studentId,
      material: false
    })


    if(student.allowed==false){
      student.allowed = undefined
 
    }

    await student.save()
    await studentList.save()

    if (data.payment) {
      data.payment['course'] = course._id
      data.payment['student'] = studentId
      data.payment['date'] = new Date()
      if (user && user._id !== undefined) {
        data.payment['user'] = user._id;
      }

      paymentId = new mongoose.Types.ObjectId()
      data.payment = _.extend(data.payment, { _id: paymentId })
      const payment = new PaymentModel(data.payment)
      await payment.save()

      creditId = new mongoose.Types.ObjectId()
      const credit = new CreditModel({
        _id: creditId,
        amount: data.payment.amount,
        date: new Date(),
        student: studentId
      })
      await credit.save()
      await student.saveBalance()
    }


    const startDate = moment(group.startDate).format("DD-MM-YYYY")

    const classRoom = await ClassRoomModel
      .findOne({ _id: group.classRoom })
    const location = await LocationModel
      .findOne({ _id: classRoom.location })

      const schedulesInfo = group.schedules.map(schedule => {
        const startHour = formatHour(String(schedule.startHour))
        const endHour = formatHour(String(schedule.endHour))
        return {
          day: days[schedule.day],
          startHour: startHour.string,
          endHour: endHour.string
        }
      })

      const courseInfoPdf = {
        group,
        course,
        startDate
      }


      const sendGridData = {
        email: student.email,
        name: student.name,
        courseName: course.name,
        groupName: group.name,
        startDate: startDate
      }



    if(location._id != "5f1221fb566b9f5a6abb6fec"){


    courseInfoPdf.classRoom = classRoom
    courseInfoPdf.location = location
    courseInfoPdf.schedulesInfo = schedulesInfo


    sendGridData.schedules = schedulesInfo
    sendGridData.classRoomName= classRoom.name
    sendGridData.locationName= location.name
    sendGridData.locationDirection= `${location.street} ${location.number}, Col. ${location.district}`

  }
  

    const generator = new GeneratePDF()
    const b64Data = await generator.generateCourse(student, courseInfoPdf)

    sendGridData.attachments = [
      {
        content: b64Data,
        filename: 'comprobante.pdf',
        type: 'application/pdf'
      }
    ]

    await SendGrid.sendRegister(sendGridData)

    return {
      success: true,
      enrolled: true,
      creditId,
      paymentInfoId,
      paymentId
    }
  },

  getByCourseId: async courseId => {
    const groups = await GroupModel
      .find({
        course: courseId
      })
      .then(groups => {
        return {
          success: true,
          groups
        }
      })
      .catch(err => {
        console.error('- Error trying to get groups by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return groups
  },

  getStudentsById: async id => {
    //get group info
    const group = await GroupModel
      .findOne({ _id: id })
      .populate({
        path: "studentList",
        populate: {
          path: "list"
        }
      })
      .populate({
        path: "course",
        select: "name price"
      })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const students = group.studentList.list.filter(item => item)
    const payments = await PaymentModel
      .find({
        student: { $in: students },
        course: group.course
      })
      .select("student amount discount")
    for (let i = 0; i < students.length; i++) {
      let totalDisccount = 0
      let totalPayment = 0
      for (let j = 0; j < payments.length; j++) {
        if (String(payments[j].student) == String(students[i]._id)) {
          totalDisccount += (payments[j].discount !== undefined ? payments[j].discount : 0)
          totalPayment += (payments[j].amount !== undefined ? payments[j].amount : 0)
        }
      }
      students[i].balance.payments.total = totalPayment
      students[i].balance.payments.discounts = totalDisccount
      let grantTotal = totalPayment + totalDisccount
      if (grantTotal < group.course.price) {
        students[i].debt = true
      } else {
        students[i].debt = false
      }
    }
    return {
      success: true,
      students,
      group
    }
  },

  getStudentsByIdLite: async id => {
    const group = await GroupModel.findOne({
      _id: id
    }).populate({
      path: "studentList",
      populate: {
        path: 'studentList',
        select: 'list',
      }
    })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const list = group.studentList.list
    var d = moment().toDate()
    sd = moment(d).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

    const students = await Promise.all(list.map((_id, index) => {
      const estudiante = StudentModel.findOne(
        { _id },
        {
          name: 1,
          lastName: 1,
          secondLastName: 1,
          email: 1,
          phoneNumber: 1,
          registerDate: 1,
          groups: 1
        }
      )
      return estudiante

    }))

    var data = [students.length]
    if (students.length > 0) {
      for (let i = 0; i < students.length; i++) {
        //  CONSULTA en asistencia que sea ese grupo ese estudiante y ese dia
        const attendance = await AttendanceStudent
          .find({
            $and: [
              { "attendance.group": group._id },
              { student: students[i] },
              { "attendance.date": { $gte: sd } }
            ]
          }
          )
        if (attendance.length > 0) {

          for (let j = 0; j < Object.keys(attendance[0].attendance).length; j++) {
            if (attendance[0].attendance[j].date >= sd) {
              data[i] = {
                student: students[i],
                attendance: attendance[0].attendance[j]
              }
            } else {
              data[i] = {
                student: students[i],
                attendance: {}
              }
            }
          }
        } else {
          data[i] = {
            student: students[i],
            attendance: {}
          }
        }
      }
      return {
        success: true,
        data
      }
    }
    else {
      return {
        success: true,
        data: []
      }
    }
  },




  /**
   * 
   * @param {*} studentId 
   * @param {*} groupId 
   * @param {*} data 
   * @param {*} force 
   */
  masiveChangeGroups: async (groupId, data, force = false) => {
    const studentArray = data.students;

    const students = await StudentModel.findOne({
      _id: { $in: studentArray }
    })

    if (!students) {
      return {
        success: false,
        code: 404,
        error: `Students not found: ${studentId}`
      }
    }

    const oldGroup = await GroupModel
      .findOne({ _id: groupId })
      .populate({ path: 'course' })
      .populate("studentList")
    if (!oldGroup) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${groupId}`
      }
    }

    const oldStudentList = oldGroup.studentList;
    if (!oldStudentList) {
      return {
        success: true,
        code: 500,
        error: `Group without studentlist`
      }
    }

    if (!data.group || !data.group.id) {
      return {
        success: false,
        code: 400,
        error: `No new group data`
      }
    }

    const newGroup = await GroupModel
      .findOne({ _id: data.group.id })
      .populate({ path: 'course' })
      .populate("studentList")
    if (!newGroup) {
      return {
        success: false,
        code: 404,
        error: `New group not found: ${data.group.id}`
      }
    }

    const groupIndex = student.groups.indexOf(groupId)
    if (groupIndex === -1) {
      return {
        success: false,
        code: 400,
        error: `Student don't enrolled in this group`
      }
    }
    /*********************************************
     *********************************************
     ********* End of main validations ************
     *********************************************
     *********************************************
     */

    // Before change payments , change student 
    const studentList = newGroup.studentList;
    if ((newGroup.quota <= studentList.list.length) && !(force === 'true')) {
      const errorExt = errMsg
        .getQuotaExceded()
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }
    studentList.list.push(student._id)
    //remove the material for last studentList and set the value to the new;
    if (oldStudentList.material && oldStudentList.material.length) {
      const index = oldStudentList.material.findIndex(item => String(item.student) === String(student._id))
      if (index >= 0) {
        studentList.material.push({
          student: student._id,
          material: oldStudentList.material[index].material
        })
        oldStudentList.material.splice(index, 1)
      } else {
        studentList.material.push({
          student: student._id,
          material: false
        })
      }
    } else {
      studentList.material.push({
        student: student._id,
        material: false
      })
    }

    student.groups.splice(groupIndex, 1)
    student.groups.push(newGroup._id)

    const studentIndex = oldStudentList.list.indexOf(student._id)
    oldStudentList.list.splice(studentIndex, 1)

    //Update the payments to the new group
    if (String(newGroup.course) !== String(oldGroup.course)) {
      await PaymentModel
        .updateMany({
          student: studentId,
          course: oldGroup.course
        }, {
          $set: {
            course: newGroup.course
          }
        })
    }

    //This will remove the students from the buckets if is parent
    if (oldGroup.course.isParent) {
      let buckets = oldGroup.buckets
      let groupsBucket = await GroupModel.find({ _id: { $in: buckets } }).populate({ path: "studentList" })
      //Empty student studentList of buckets
      for (let i = 0; i < groupsBucket.length; i++) {
        let index = groupsBucket[i].studentList ? groupsBucket[i].studentList.list.findIndex(item => String(item) === String(student._id)) : -1
        if (index >= 0) {
          let studentListPre = groupsBucket[i].studentList
          studentListPre.list.splice(index, 1)
          await studentListPre.save()
        }
      }
      student.comipemsOrder = [];
    }
    //This will Add the students from the buckets if is parent
    if (newGroup.course.isParent) {
      const phaseOrder = await Utils.shuffleArray(newGroup.course.children.length)
      let buckets = await Promise
        .all(newGroup.course.children.map((courseId, index) => {
          return Groups.enrollBucketGroup(courseId, newGroup, student._id, phaseOrder[index])
        }))
      buckets = buckets.sort((a, b) => {
        return a.phase - b.phase;
      })
      const newBuckets = []
      buckets.forEach(element => {
        newBuckets.push(element._id)
      });
      student.comipemsOrder = newBuckets;
    }

    //Update the main studentList
    await StudentListModel.updateOne({
      _id: oldGroup.studentList
    },
      {
        $pull: {
          list: student._id,
          material: { student: student._id }
        }
      }
    )

    await StudentListModel.updateOne({
      _id: newGroup.studentList
    },
      {
        $addToSet: {
          list: student._id,
          material: {
            student: student._id,
            material: false
          }
        }
      }
    )
    await student.save();
    student.saveBalance();

    return {
      success: true,
      student
    }

  },


  toggleActive: async id => {
    const group = await GroupModel.findOne({ _id: id })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const isActive = group.active
    group.active = !isActive

    await group.save()

    return {
      success: true,
      active: !isActive
    }
  },

  material: async (id, data) => {
    const group = await GroupModel
      .findOne({ _id: id })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const studentList = await StudentListModel
      .findOne({ _id: group.studentList })

    const materialIds = studentList.material.map(item => String(item.student))

    for (let i = 0; i < data.material.length; i++) {
      const element = data.material[i]
      if (studentList.list.indexOf(element.student) !== -1) {
        if (materialIds.indexOf(element.student) !== -1) {
          for (let j = 0; j < studentList.material.length; j++) {
            const material = studentList.material[j]
            if (String(element.student) === String(material.student)) {
              studentList.material[j].material = element.material
            }
          }
        } else {
          studentList.material.push(element)
        }
      }
    }

    await studentList.save()

    return {
      success: true,
      material: [],
      studentList
    }
  },


  sendReceipt: async (id, groupId) => {
    const student = await StudentModel
      .findOne({ _id: id })
      .populate({
        path: "comipemsOrder",
        populate: [{
          path: "teacher"
        },
        {
          path: "classRoom",
          populate: "location"
        }]
      })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const group = await GroupModel
      .findOne({ _id: groupId })
      .populate({ path: "course" })
      .populate({ path: "classRoom", populate: { path: "location" } })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    // In this point we have a object of group populated with the course info
    if (group.course.isParent) {
      const generator = new GeneratePDF()
      const b64Data = await generator.generateComipems(student)
      const schedulesInfo = group.schedules.map(schedule => {
        const startHour = formatHour(String(schedule.startHour))
        const endHour = formatHour(String(schedule.endHour))
        return {
          day: days[schedule.day],
          startHour: startHour.string,
          endHour: endHour.string
        }
      })
      const startDate = moment(group.startDate).format('DD/MM/YYYY');
      const buckets = student.comipemsOrder
      await SendGrid.sendComipems({
        email: student.email,
        name: student.name,
        startDate: startDate,
        schedules: schedulesInfo,
        attachments: [
          {
            content: b64Data,
            filename: 'Comprobante.pdf',
            type: 'application/pdf'
          }
        ],
        buckets
      })
    } else {
      const schedulesInfo = group.schedules.map(schedule => {
        const startHour = formatHour(String(schedule.startHour))
        const endHour = formatHour(String(schedule.endHour))
        return {
          day: days[schedule.day],
          startHour: startHour.string,
          endHour: endHour.string
        }
      })

      const startDate = moment(group.startDate).format("DD-MM-YYYY")
     




      const courseInfoPdf = {
        group,
        course: group.course,
        startDate
      }


      const sendGridData = {
        email: student.email,
        name: student.name,
        courseName: group.course.name,
        groupName: group.name,
        startDate: startDate
      }


      // if (group.classRoom) {
      //   courseInfoPdf.classRoom = group.classRoom
      //   if (group.classRoom.location)
      //     courseInfoPdf.location = group.classRoom.location
      // }

    if(group.classRoom.location._id != "5f1221fb566b9f5a6abb6fec"){

    courseInfoPdf.classRoom = group.classRoom
    courseInfoPdf.location =  group.classRoom.location
    courseInfoPdf.schedulesInfo = schedulesInfo


    sendGridData.schedules = schedulesInfo
    sendGridData.classRoomName= group.classRoom.name
    sendGridData.locationName= group.classRoom.location.name
    sendGridData.locationDirection= `${group.classRoom.location.street} ${group.classRoom.location.number}, Col. ${group.classRoom.location.district}`

  }
  

  
      const generator = new GeneratePDF()
      const b64Data = await generator.generateCourse(student, courseInfoPdf)
      
      
      // const sendGridData = {
      //   email: student.email,
      //   name: student.name,
      //   courseName: group.course.name,
      //   groupName: group.name,
      //   schedules: schedulesInfo,
      //   startDate: startDate,
      //   classRoomName: group.classRoom.name,
      //   locationName: group.classRoom.location.name,
      //   locationDirection: `${group.classRoom.location.street} ${group.classRoom.location.number}, Col. ${group.classRoom.location.district}`,
      //   attachments: [
      //     {
      //       content: b64Data,
      //       filename: 'comprobante.pdf',
      //       type: 'application/pdf'
      //     }
      //   ],
      // }

      sendGridData.attachments = [
        {
          content: b64Data,
          filename: 'comprobante.pdf',
          type: 'application/pdf'
        }
      ]
  

      await SendGrid.sendRegister(sendGridData)

    }
    return {
      success: true,
      status: "sended"
    }
  },

  getToDownload: async id => {
    const group = await GroupModel
      .findOne({ _id: id })
    if (!group) {
      const errorExt = errMsg
        .getNotEntityFound('Group', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const studentList = await StudentListModel
      .findOne({ _id: group.studentList })

    const classRoom = await ClassRoomModel
      .findOne({ _id: group.classRoom })

    const students = await Promise.all(
      studentList.list.map(studentId => StudentModel
        .findOne({ _id: studentId }))
    )
      .catch(err => {
        console.error('- Error trying to get student to list', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    let stSorted = students.map(st => {
      return {
        'apPaterno': st.lastName,
        'apMaterno': st.secondLastName,
        'nombre': st.name,
        'salon': classRoom.name,
        'lApPaterno': st.lastName.toLowerCase()
      }
    })

    return {
      success: true,
      students: stSorted.sort((a, b) => {
        if (a.lApPaterno < b.lApPaterno) return -1
        if (a.lApPaterno > b.lApPaterno) return 1
        return 0
      })
    }
  },

  getByClassRoomId: async (id, query) => {
    const classRoom = await ClassRoomModel
      .findOne({ _id: id })
    if (!classRoom) {
      const errorExt = errMsg
        .getNotEntityFound('Classroom', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const filter = {
      classRoom: id
    }

    if (query.course) {
      filter.course = query.course
    }
    if (query.startDate) {
      filter.startDate = {
        $gte: query.startDate
      }
    }
    if (query.endDate) {
      filter.endDate = {
        $lte: query.endDate
      }
    }
    const groups = await GroupModel
      .find(filter)

    const groupsToCalendar = await Promise
      .all(groups.map(group => group.toCalendar()))

    return {
      success: true,
      groups: groupsToCalendar
    }
  },

  /**
   * getByTeacher
   * @description Get groups by user id
   */
  getByTeacher: async (userId) => {
    const groups = await GroupModel
      .find({
        teacher: userId
      })
      .then(groups => {
        return Promise.all(
          groups.map(group => group.toPopulate())
        )
      })
      .then(groups => groups)
      .catch(err => {
        console.error('- Error trying to get a group by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      groups
    }
  },

  /**
 * 
 * UNPROTECTED FUNCTIONS WARNING!!
 */
  getGroupsForLanding: async (courseId) => {
    let filter = {
      deleted: {
        $ne: true
      }
    }
    filter['course'] = courseId
    filter['activeLanding'] = true
    let stRngStartDate = moment
      .utc(new Date())
      .hour(0)
      .minute(0)
    filter['startDate'] = {
      $gte: stRngStartDate.format()
    }

    const groups = await GroupModel
      .find(filter)
      .populate("course")
      .populate({
        path: "studentList"
      })
      .populate({
        path:"classRoom",
        populate:"location"
      })
      .lean()
      .then(groups => {
        return Promise.all(groups.map(group => {
          return Groups.toLite(group, undefined)
        }))
      })
      .catch(err => {
        console.error('- Error trying to get all groups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })


    let course = {}

    course = await CourseModel
        .findOne({ _id: courseId })
      if (!course) {
        const errorExt = errMsg
          .getNotEntityFound('Course', courseId)
          return {
            success: false,
            code: 404,
            error: errorExt
          }
      }
    

    const filterGroups = groups.filter(element => element.studentList && element.studentList.list.length < element.quota)
    if (filterGroups && filterGroups.length > 0) {
      course = filterGroups[0].course
    }
    return {
      success: true,
      groups: filterGroups,
      course
    }
  },

  getGroupsByIdForLanding: async (groupId) => {
    const group = await GroupModel
      .findById(groupId)
      .populate("course")
      .lean()
    const groupReturn = Groups.toLite(group, undefined)
    return {
      success: true,
      group: groupReturn
    }
  },

  getCoursesIngMedSoc: async (ids) =>{
    const active = true
    let allGroups = []
    const result1 = await GroupModel.find({course: ids.examen1, activeLanding: active, active: active}).maxTimeMS(30000).catch(err =>{console.log(err)})
      allGroups = allGroups.concat(result1)

      const result2 = await GroupModel.find({course: ids.examen2, activeLanding: active, activeLanding: active}).maxTimeMS(30000).catch(err =>{console.log(err)})
      allGroups = allGroups.concat(result2)

      const result3 = await GroupModel.find({course: ids.examen3, activeLanding: active, activeLanding: active}).maxTimeMS(30000).catch(err =>{console.log(err)})
      allGroups = allGroups.concat(result3)

      if (allGroups[0] != null || allGroups[1] != null || allGroups[2] != null) { 
        return{
          success:true,
          httpCode: 200,
          groups: allGroups
        }
      } else {
        return {
          success: false,
          error: 'not found',
          httpCode: 404,
          message: {
            en: 'Groups not found',
            es: 'No se encontraron grupos'
          }
        }
    }
  }

}

function formatHour(hour) {
  let arrHour = hour.split('')
  arrHour.splice(-2, 0, ':')

  const str = arrHour.join('')
  const arrStr = str.split(':')

  return {
    string: str,
    minutes: arrStr[1],
    hours: arrStr[0]
  }
  
}

module.exports = Groups
