/**
 * models/roles.js
 *
 * @description :: Describes the roles functions
 * @docs        :: TODO
 */
const _ = require('lodash')

const RoleModel = require('../db/roles')
const UsersModel = require('../db/users')

const ErrMsg = require('../lib/errMsg')

const errMsg = new ErrMsg()

const Roles = {
  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const result = await RoleModel.findOne({ _id: id, visible: true })
      .then(role => {
        if (!role) {
          const errorExt = errMsg
            .getNotEntityFound('Role', id)
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to get a role by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in result && result.success) {
      const role = await result.role.getUsersCount()
      return {
        success: true,
        role
      }
    }

    return result
  },

  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({ skip, limit } = {}) => {

    try{
    const roles = await RoleModel
      .find({ visible: true })
      .skip(skip)
      .limit(limit)
      .then(roles => {
        return Promise.all(
          roles.map(role => role.getUsersCount())
        )
      })
      .then(roles => {
        return {
          success: true,
          roles
        }
      })
      .catch(err => {
        console.error('- Error trying to get all roles.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all roles'
          }
        }
      })
    return roles
  
  } catch (err) {
    const errorExt = errMsg
      .generalGet('roles')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * create
   *
   * @description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {

    try{
    const role = new RoleModel(data)

    const result = await role.save()
      .then(role => {
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to create a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a rol'
          }
        }
      })

    if ('success' in result && result.success) {
      const totalUsers = await UsersModel
        .find({ role: result.role._id }).countDocuments()
      return {
        success: true,
        role: _.extend({ totalUsers }, result.role.toObject())
      }
    }

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('roles')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * update
   *
   * @description: Update a single item by id.
   * @param {string} id - Item id.
   * @param {object} data - List of properties.
   */
  update: async (id, data) => {
    try{
    const result = await RoleModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        new: true
      })
      .then(role => {
        if (!role) {
          const errorExt = errMsg
            .getNotEntityFound('Role', id)
            return {
              success: false,
              error: errorExt
            }
        }
        return {
          success: true,
          role
        }
      })
      .catch(err => {
        console.error('- Error trying to update a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a role'
          }
        }
      })

    if ('success' in result && result.success) {
      const totalUsers = await UsersModel
        .find({ role: result.role._id }).countDocuments()
      return {
        success: true,
        role: _.extend({ totalUsers }, result.role.toObject())
      }
    }

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('roles')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Permission id.
   */
  delete: async id => {

    try{

    await UsersModel.updateOne(
      {
        root: { $ne: true },
        roles: id
      },
      {
        $pull: { roles: id }
      }
    )


    const result = await RoleModel.deleteOne({ _id: id })
      .then((ress) => {

        if(ress.n>0){
                  return {
                    success: true,
                    deleted: true
                  }
                }else{
                  const errorExt = errMsg
                  .getNotEntityFound('roles', id)
                return {
                  success: false,
                  error: errorExt
                }

                }
      })
      .catch(err => {
        console.error('- Error trying to delete a role', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to delete a role'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalDelete('roles')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  getUsers: async id => {
    const role = await RoleModel.findOne({ _id: id })
    if (!role) {
      const errorExt = errMsg
        .getNotEntityFound('Role', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const users = await UsersModel
      .find({
        roles: id
      })

    return {
      success: true,
      users: users.map(user => user.toSimple())
    }
  }
}

module.exports = Roles
