/**
 * models/credits.js
 *
 * @description :: Describes the credits functions
 * @docs        :: TODO
 */
const CreditsModel = require('../db/credits')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Credits = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {

    try{
    const credits = await CreditsModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all credits'
          }
        }
      })
    return {
      success: true,
      credits
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('credits')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const credit = await CreditsModel
      .findOne({ _id: id })
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      credit
    }
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {

    try{

    const credit = new CreditsModel(data)
    const result = await credit
      .save()
      .then(credit => {
        return {
          success: true,
          credit
        }
      })
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all credits.'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('credits')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  getByStudentId: async studentId => {
    const credits = await CreditsModel
      .find({
        student: studentId
      })
      .then(credits => {
        return {
          success: true,
          credits
        }
      })
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return credits
  }
}

module.exports = Credits
