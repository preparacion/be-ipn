const { PDFDocument, rgb, degrees } = require('pdf-lib');
const PdfModel = require('../db/pdfs');
const S3 = require('../lib/s3');
const { awsCloudfromEndpointPdfs } = require('../config');

const PDF_STATUS = {
  NOT_FOUND: 1,
  ALREADY_EXISTS: 2,
  SAVED: 3
};

const WATERMARK_CONFIG = {
  SIZE: 8,
  SPACING: { X: 80, Y: 120 },
  STYLE: {
    color: rgb(0.5, 0.5, 0.5),
    opacity: 0.3,
    rotation: 45
  }
};

const PDF_PERMISSIONS = {
  userPassword: '',
  ownerPassword: process.env.PDF_OWNER_PASSWORD || 'm4spr3p4r4c10n_secure_pdf_encryption',
  permissions: {
    printing: 'highResolution',
    modifying: false,
    copying: false,
    annotating: false,
    fillingForms: false,
    contentAccessibility: true,
    documentAssembly: false
  }
};

class SecurePDFService {
  static async getById(id, userId) {
    try {
      const pdf = await PdfModel.findOne({ _id: id });
      if (!pdf) {
        return this._createErrorResponse(404, `PDF not found ${id}`);
      }

      const securePdf = await this._processAndSecurePDF(pdf.key, userId);
      if (!securePdf.success) {
        return securePdf;
      }

      return {
        success: true,
        url: `${awsCloudfromEndpointPdfs}secured_files/${securePdf.key}`,
        pdf: this._sanitizePdfData(pdf)
      };
    } catch (error) {
      return this._createErrorResponse(500, error);
    }
  }

  static async _processAndSecurePDF(pdfKey, userId) {
    try {
      // 1. Verificar si ya existe una versión segura
      const existingPdf = await this._getExistingSecurePdf(pdfKey, userId);
      if (existingPdf.success) return existingPdf;

      // 2. Procesar nuevo PDF
      const originalPdf = await S3.readOriginalPDF(pdfKey);
      const processedPdf = await this._processNewPdf(originalPdf, userId);
      
      // 3. Guardar PDF procesado
      const savedPdf = await S3.saveSignedPdf(pdfKey, userId, processedPdf);
      return savedPdf.success ? {
        success: true,
        key: savedPdf.response.key,
        flag: PDF_STATUS.SAVED
      } : this._createErrorResponse(500, savedPdf.error);
    } catch (error) {
      return this._createErrorResponse(500, error);
    }
  }

  static async _getExistingSecurePdf(pdfKey, userId) {
    const response = await S3.readStudentPDF(pdfKey, userId);
    if (response.success && response.response?.flag !== PDF_STATUS.ALREADY_EXISTS) {
      return {
        success: true,
        key: response.response.key,
        flag: response.response.flag
      };
    }
    return { success: false };
  }

  static async _processNewPdf(originalPdf, userId) {
    const pdfDoc = await PDFDocument.load(Buffer.from(originalPdf));
    await this._addWatermark(pdfDoc, userId);
    return pdfDoc.save(PDF_PERMISSIONS);
  }

  static async _addWatermark(pdfDoc, userId) {
    const watermarkText = `${userId}\nPROHIBIDO COMPARTIR`;
    const pages = pdfDoc.getPages();
    
    pages.forEach(page => {
      const { width, height } = page.getSize();
      
      for (let y = 0; y < height; y += WATERMARK_CONFIG.SPACING.Y) {
        for (let x = 0; x < width; x += WATERMARK_CONFIG.SPACING.X) {
          page.drawText(watermarkText, {
            x: x + WATERMARK_CONFIG.SIZE,
            y: y + WATERMARK_CONFIG.SIZE,
            size: WATERMARK_CONFIG.SIZE,
            color: WATERMARK_CONFIG.STYLE.color,
            rotate: degrees(WATERMARK_CONFIG.STYLE.rotation),
            opacity: WATERMARK_CONFIG.STYLE.opacity
          });
        }
      }
    });
  }

  static _sanitizePdfData(pdf) {
    return {
      _id: pdf._id,
      name: pdf.name,
      totalPages: pdf.totalPages,
      description: pdf.description,
      order: pdf.order,
      playlist: pdf.playlist,
      active: pdf.active
    };
  }

  static _createErrorResponse(code, error) {
    return {
      success: false,
      code,
      error: error?.message || error
    };
  }
}

module.exports = SecurePDFService;