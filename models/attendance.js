/**
 * models/attendance.js
 *
 * @description :: Describes the attendance functions
 * @docs        :: TODO
 */
const GroupModel = require('../db/groups')
const StudentModel = require('../db/students')
const UsersTalksModel = require('../db/usersTalks.js')
const StudentListModel = require('../db/studentLists')
const AttendanceListModel = require('../db/attendanceList')
const AttendanceStudentModel = require('../db/attendanceStudent')
const AttendanceTeacherModel = require('../db/attendanceTeacher')

const moment = require('moment')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Attendance = {
  create: async data => {

    try{

    const group = await GroupModel.findOne({
      studentList: data.studentList
    })
    if (!group) {
      const errorExt = errMsg.getNotEntityFound('Group', data.studentList)

      return {
        success: false,
        error: errorExt
      }
    }
    data['group'] = group._id

    const studentList = await StudentListModel.findOne({
      _id: data.studentList
    })
    if (!studentList) {
      const errorExt = errMsg
        .getNotEntityFound('StudentList', data.studentList)

      return {
        success: false,
        error: errorExt
      }
    }

    let startDate = (data.date) ? new Date(data.date) : new Date()
    startDate.setSeconds(0)
    startDate.setHours(0)
    startDate.setMinutes(0)

    let endDate = new Date(startDate)
    endDate.setHours(23)
    endDate.setMinutes(59)
    endDate.setSeconds(59)

    const currentAttendance = await AttendanceListModel
      .findOne({
        group: group._id,
        date: {
          $gte: startDate,
          $lte: endDate
        }
      })

    if (currentAttendance) {
      // Attendance List already created
      const errorExt = errMsg.getByCode('03')

      return {
        success: false,
        error: errorExt
      }
    }

    const attendanceList = new AttendanceListModel(data)
    await attendanceList.save()

    const attendance = []
    for (let i = 0; i < studentList.list.length; i++) {
      const element = String(studentList.list[i])
      if (data.students.indexOf(element) === -1) {
        attendance.push({
          date: data.date,
          student: element,
          group: group._id,
          attendance: false
        })
      } else {
        attendance.push({
          date: data.date,
          student: element,
          group: group._id,
          attendance: true
        })
      }
    }

    const result = await Promise
      .all(attendance.map(attendance => Attendance.addStudentAttendance(attendance)))
      .catch(err => {
        console.error('- Error trying to create attendance student', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create attendance student'
          }
        }
      })
    if ('success' in result && !result.success) {
      return result // return error
    }

    ///// CREATE TEACHER ATTENDANCE
    if (data.teacher) {
      const teacherAttendance = await Attendance.addTeacherAttendance({
        teacher: data.teacher,
        group: group._id,
        date: data.date || new Date()
      })

      if (
        'success' in teacherAttendance &&
        !teacherAttendance.success
      ) {
        return teacherAttendance // return error 500
      }
    }

    return {
      success: true,
      group,
      studentList,
      attendanceList
    }

  }
  catch (err) {
      const errorExt = errMsg
          .generalCreate('Attendance')
      return {
          success: false,
          error: errorExt
      }
  
}

  },

  /// ADD TEACHER ATTENDANCE 
  addTeacherAttendance: async data => {
    const result = await AttendanceTeacherModel
      .findOneAndUpdate({
        teacher: data.userId
      }, {
        $addToSet: {
          attendance: {
            attendance: data.attendance,
            group: data.group,
            date: data.date || new Date()
          }
        }
      }, {
        upsert: true,
        new: true
      })
      .then(result => {
        let last_key = Object.keys(result.attendance)[Object.keys(result.attendance).length - 1]
        let attendance = result.attendance[last_key]
        return { attendance }
      })
      .catch(err => {
        console.error('- Error trying to add attendance to teacher/admin', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      attendance: {
        _id: result.attendance._id,
        userId: data.userId,
        group: result.attendance.group,
        date: result.attendance.date,
        attendance: result.attendance.attendance
      }
    }
  },

  addStudentAttendance: async data => {
    let alreadyTaked = false
    let isUpdateAttendance = undefined
    const result = await AttendanceStudentModel
      .findOne({
        student: data.student
      })
      .then(result => {
        if (result && result.attendance && result.attendance.length > 0) {
          for (let i = 0; i < result.attendance.length; i++) {
            if ((moment(result.attendance[i].date).format("YYYY-MM-DD-HH:mm") == moment(data.date).format("YYYY-MM-DD-HH:mm")) && JSON.stringify(result.attendance[i].group) == JSON.stringify(data.group)) {
              if (result.attendance[i].attendance != data.attendance) {
                console.log("exiiste y rompemos")
                isUpdateAttendance = result.attendance[i]
                break
              }
              alreadyTaked = true
              break
            }
          }
          if (alreadyTaked) {
            return {
              success: true
            }
          } else {
            return {
              success: false
            }
          }
        } else {
          return {
            success: false
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to add attendance to student', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    if ('success' in result && !result.success) {
      if (isUpdateAttendance !== undefined) {
        let result = await AttendanceStudentModel.findOne({
          student: data.student
        })
        let index = result.attendance.findIndex(item => JSON.stringify(item._id) == JSON.stringify(isUpdateAttendance._id));
        let toModify = result.attendance[index]
        toModify.attendance = data.attendance
        result.attendance[index] = toModify
        result.save()
      } else {
        await AttendanceStudentModel.findOneAndUpdate({
          student: data.student
        }, {
          $addToSet: {
            attendance: {
              group: data.group,
              date: data.date || new Date(),
              attendance: data.attendance
            }
          }
        }, {
          upsert: true,
          new: true
        })
      }
      return {
        success: true,
        student: data.student
      }
    }
    return {
      success: true,
      student: data.student
    }
  },

  /**
   * REFACTOR ADD OR CREATE 
   */
  addOrUpdateTodayAttendanceStudent: async data => {
    const groupId = data.groupId;
    const studentId = data.studentId;
    const attendanceValue = data.attendance;

    //set date to cero
    const nowDate = moment().toDate()
    let nowDateCero = moment(nowDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    //in the 
    let attendanceStudent = await AttendanceStudentModel
      .findOne({
        student: studentId,
        attendance: {
          $elemMatch: {
            group: groupId,
            date: { $gte: nowDateCero }
          }
        }
      })
    if (attendanceStudent !== undefined && attendanceStudent !== null) {
      // console.log(attendanceStudent)
      const indexAttendance = attendanceStudent.attendance.findIndex(element => moment(element.date).isSameOrAfter(nowDateCero) && String(element.group) === String(groupId))
      // console.log("Indice asistencia ", indexAttendance)
      if (indexAttendance >= 0) {
        attendanceStudent.attendance[indexAttendance].attendance = attendanceValue
        await attendanceStudent.save()
      }
    } else {
      await AttendanceStudentModel.findOneAndUpdate({
        student: studentId
      }, {
        $addToSet: {
          attendance: {
            group: groupId,
            date: new Date(),
            attendance: attendanceValue
          }
        }
      }, {
        upsert: true,
        new: true
      })
    }
    return {
      success: true,
      studentId: studentId,
      attendance: attendanceValue
    }
  },

  getByStudentId: async (
    studentId,
    startDate = null,
    endDate = null,
    group = null
  ) => {
    const query = {
      student: studentId
    }

    const studentAttendance = await AttendanceStudentModel
      .findOne(query)
    if (!studentAttendance) {
      const errorExt = errMsg
        .getNotEntityFound('StudentAttendance', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    let attArray = []
    let attendance = 0

    for (let index = 0; index < studentAttendance.attendance.length; index++) {
      const element = studentAttendance.attendance[index]
      if (startDate) {
        let st = new Date(startDate)
        st.setSeconds(0)
        st.setHours(0)
        st.setMinutes(0)

        if (st < element.date) {
          if (endDate) {
            let end = new Date(endDate)
            endDate.setHours(23)
            endDate.setMinutes(59)
            endDate.setSeconds(59)

            if (end > element.date) {
              attArray.push(element)
              if (element.attendance) {
                attendance++
              }
            }
          } else {
            attArray.push(element)
            if (element.attendance) {
              attendance++
            }
          }
        }
      } else {
        attArray.push(element)
        if (element.attendance) {
          attendance++
        }
      }
    }

    if (group) {
      attArray = attArray.filter(att => String(att.group) === String(group))
    }

    const average = (attArray.length)
      ? (attendance / attArray.length) * 100
      : 0

    return {
      success: true,
      attendance: {
        _id: studentAttendance._id,
        student: studentAttendance.student,
        attendance: attArray
      },
      average
    }
  },


  getByStId: async (
    studentId
  ) => {
    const query = {
      student: studentId
    }

    const studentAttendance = await AttendanceStudentModel
      .findOne(query)
    if (!studentAttendance) {
      const errorExt = errMsg
        .getNotEntityFound('StudentAttendance', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    return {
      success: true,
      studentAttendance
    }
  },


  getByTeacherId: async (
    teacherId,
    startDate,
    endDate
  ) => {
    const teacherAttendance = await AttendanceTeacherModel
      .findOne({ teacher: teacherId })
    if (!teacherAttendance) {
      const errorExt = errMsg
        .getNotEntityFound('TeacherAttendance', teacherId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const attArray = []
    for (let index = 0; index < teacherAttendance.attendance.length; index++) {
      const element = teacherAttendance.attendance[index]

      let st = moment(startDate).set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 })
      let end = moment(endDate).set({ 'hour': 23, 'minute': 59, 'second': 59, 'millisecond': 0 })

      if (startDate) {
        if (st < element.date) {
          if (endDate) {
            if (end > element.date) {
              attArray.push(element)
            }
          } else {
            attArray.push(element)
          }
        }
      } else {
        attArray.push(element)
      }
    }

    return {
      success: true,
      attendance: {
        _id: teacherAttendance._id,
        user: teacherAttendance.teacher,
        attendance: attArray
      }
    }
  },

  getByGroupId: async (
    groupId,
    startDate = null,
    endDate = null
  ) => {
    const query = {
      group: groupId
    }
    if (startDate) {
      let st = new Date(startDate)
      st.setSeconds(0)
      st.setHours(0)
      st.setMinutes(0)

      query['date'] = {
        $gte: st
      }

      if (endDate) {
        let endDate = new Date(startDate)
        endDate.setHours(23)
        endDate.setMinutes(59)
        endDate.setSeconds(59)

        query['date'] = {
          $gte: st,
          $lte: endDate
        }
      }
    }

    let attendanceList = await AttendanceListModel
      .find(query)
      .then(attLists => {
        return attLists
      })
    if (!attendanceList) {
      const errorExt = errMsg
        .getNotEntityFound('AttendanceList', groupId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const studentListId = (attendanceList && attendanceList.length)
      ? attendanceList[0].studentList : null

    const studentList = await StudentListModel
      .findOne({
        _id: studentListId
      })

    const stInfoArr = await Promise.all(
      attendanceList.map(attList => {
        return Attendance
          .getStudentInfo(attList._id, attList.students)
      })
    )

    for (let i = 0; i < attendanceList.length; i++) {
      const attList = attendanceList[i]
      for (let j = 0; j < stInfoArr.length; j++) {
        const element = stInfoArr[j]
        if (element.attListId === attList._id) {
          attendanceList[i]['studentInfo'] = element.students
        }
      }
    }

    let students = null
    if (studentList) {
      students = await Promise.all(
        studentList.list.map(stId => {
          return StudentModel
            .findOne({ _id: stId })
            .select('name lastName secondLastName email phoneNumber')
        })
      )
    }

    return {
      success: true,
      attendanceList,
      studentList,
      students
    }
  },

  update: async (data) => {
    const id = data.id
    delete data.id

    try{
    const attendanceList = await AttendanceListModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      },
        {
          upsert: false,
          returnNewDocument: true,
          new: true
        })

    if (!attendanceList) {
      const errorExt = errMsg
        .getNotEntityFound('AttendanceList', data.id)
      return {
        success: false,
        error: errorExt
      }
    }

    const studentList = await StudentListModel
      .findOne({ _id: attendanceList.studentList })

    const group = await GroupModel
      .findOne({ _id: attendanceList.group })

    const attendance = []
    for (let i = 0; i < studentList.list.length; i++) {
      const element = String(studentList.list[i])
      if (data.students.indexOf(element) === -1) {
        attendance.push({
          date: data.date,
          student: element,
          group: group._id,
          attendance: false
        })
      } else {
        attendance.push({
          date: data.date,
          student: element,
          group: group._id,
          attendance: true
        })
      }
    }

    const result = await Promise
      .all(attendance.map(att => Attendance.updateStudentAttendance(att)))
      .catch(err => {
        console.error('- Error trying to create attendance student', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create attendance student'
          }
        }
      })
    if ('success' in result && !result.success) {
      return result // return error 500
    }

    const attendanceListResult = {}

    for (let i of Object.entries(data)) {
      attendanceListResult[i[0]] = i[1]
    }

    return {
      success: true,
      attendanceList: attendanceListResult,
      studentList
    }

  }
  catch (err) {
      const errorExt = errMsg
          .generalUpdate('Attendance')
      return {
          success: false,
          error: errorExt
      }
  
}
  },

  updateStudentAttendance: async data => {
    const studentAttendance = await AttendanceStudentModel
      .findOne({ student: data.student })

    if (!studentAttendance) {
      // Create Attendance Student
      await Attendance.addStudentAttendance(data)
      return
    }

    try {
      for (let i = 0; i < studentAttendance.attendance.length; i++) {
        const attendance = studentAttendance.attendance[i]

        let dtAtt = new Date(attendance.date)
        let dtDay = dtAtt.getUTCDate()
        let dtMonth = dtAtt.getUTCMonth()
        let dtYear = dtAtt.getUTCFullYear()

        let dataAtt = new Date(data.date)
        let dataDay = dataAtt.getUTCDate()
        let dataMonth = dataAtt.getUTCMonth()
        let dataYear = dataAtt.getUTCFullYear()

        if (
          dtDay === dataDay &&
          dtMonth === dataMonth &&
          dtYear === dataYear
        ) {
          studentAttendance.attendance[i].attendance = data.attendance
        }
      }
    } catch (error) {
      console.error('- Error trying to update student attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    await studentAttendance.save()
  },

  updateSingle: async data => {
    const studentAttendance = await AttendanceStudentModel
      .findOne({ student: data.studentId })
    if (!studentAttendance) {
      return {
        code: 404,
        success: false,
        error: errorExt.error,
        errorExt
      }
    }

    try {
      for (let i = 0; i < studentAttendance.attendance.length; i++) {
        if (studentAttendance.attendance[i]._id == data.attendanceId) {
          studentAttendance.attendance[i].date = data.date
          studentAttendance.attendance[i].attendance = data.attendance
        }
      }
    } catch (error) {
      console.error('- Error trying to update single attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    await studentAttendance.save()

    return {
      success: true,
      data
    }
  },


  deleteAttendance: async data => {
    const studentAttendance = await AttendanceStudentModel
      .findOne({ student: data.studentId })
    if (!studentAttendance) {
      return {
        code: 404,
        success: false,
        error: errorExt.error,
        errorExt
      }
    }

    try {
      let index = -1
      for (let i = 0; i < studentAttendance.attendance.length; i++) {
        if (studentAttendance.attendance[i]._id == data.attendanceId) {
          index = i
        }
      }

      if (index > -1) {
        studentAttendance.attendance.splice(index, 1)
        await studentAttendance.save()
      } else {
        return {
          success: false,
          code: 500,
          error: "No attendance identifier"
        }

      }

    } catch (error) {
      console.log(error)
      console.error('- Error trying to delete single attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    await studentAttendance.save()

    return {
      success: true,
      data
    }
  },

  //To qr first need to be a valid id of user in talk or in class 

  qrAttendance: async (id, socket = undefined) => {
    const days = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo']
    const student = await StudentModel
      .findOne({ _id: id })
      .populate({
        path: 'groups',
        populate: {
          path: "course"
        }
      })
      .populate({
        path: 'groups',
        populate: {
          path: "classRoom",
          populate: {
            path: "location"
          }
        }
      })
      .lean()
    if (!student) {
      const userTalk = await UsersTalksModel
        .findOne({ _id: id })
      if (!userTalk) {
        const errorExt = errMsg
          .getNotEntityFound('UserTalk', id)
        return {
          success: false,
          error: errorExt
        }
      }
      userTalk.attendance = true
      await userTalk.save()
      if (socket) {
        socket.to('update_table_event').emit("update_table_event", {
          event: "update_attendance",
          attendance: true,
          studentId: id
        })
      }
      return {
        success: true,
        student: userTalk,
        isTalkUser: true
      }
    }
    const groups = student.groups;
    const resultGroups = [];
    const attendanceArray = [];
    let notAttArray = [];
    let dia = 0;
    let now = moment();

    if(groups.length==0){
      return {
        success: false,
        message: "Este alumno no tiene grupos validos para pasar asistencia el día de hoy, verifica sus datos.",
        code: 406
      }
    }
    if (groups && groups.length >= 0) {
      for (let i = 0; i < groups.length; i++) {
        moment.locale('es');
        let startDate = moment(groups[i].startDate);
        startDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        let endDate = moment(groups[i].endDate);
        // this line will check if the group is on range of activated.
        if (startDate <= now && now <= endDate) {
          let dayToday = now.format('dddd');
          if(groups[i].classRoom.location._id != "5f1221fb566b9f5a6abb6fec"){
          if (groups[i].schedules) {
            for (let j = 0; j < groups[i].schedules.length; j++) {
              if (dayToday === days[groups[i].schedules[j].day]) {
                dia = groups[i].schedules[j].day

                groups[i].schedules = groups[i].schedules[j]
                groups[i].schedules.day = dayToday
                
                resultGroups.push(groups[i]);
                

              }
            }


          }
        }

        }

      }

           /**
             * At this point we need to check the groups valid for today , last code only check the startDate , endDate is on range
             * then check if today is into the schedules of the group and add to resoultGroups array 
             */
            let startHour, endHour;
            let dataAttendance = {}
            resultGroups.forEach((result) => {
              startHour = moment(result.schedules.startHour, 'hhmm').subtract(60, 'minutes');
              endHour = moment(result.schedules.endHour, 'hhmm').add(60, 'minutes');
                           
              if (now >= startHour && now <= endHour) {
                dataAttendance = {
                  course: result.course.name,
                  location: result.classRoom.location.name,
                  classroom: result.classRoom.name,
                  group: result.name,
                  startDate: result.startDate,
                  schedule: result.schedules,
                  groupId: result._id,
                  attendance: true,
                  studentId: id
                }
                attendanceArray.push(dataAttendance)
                Attendance.addOrUpdateTodayAttendanceStudent(dataAttendance)

              } else {
                dataAttendance = {
                  course: result.course.name,
                  location: result.classRoom.location.name,
                  classroom: result.classRoom.name,
                  group: result.name,
                  startDate: result.startDate,
                  schedule: result.schedules,
                  groupId: result._id,
                  attendance: false
                }
                notAttArray.push(dataAttendance)
              }
              
            })

            if(resultGroups.length == 0 ){
              return {
                success: false,
                message: "Hoy no cuenta con grupos para pasar asistencia",
                student,
                groups: []
             }
            }

            if (attendanceArray.length > 0) {
              return {
                success: true,
                message: "La asistencia se guardó correctamente en el(los) grupo(s):",
                student,
                groups: attendanceArray
              }


            } else {
              return {
                success: false,
                message: "Aun no es hora de pasar asistencia para el(la) alumno(a)",
                student,
                groups: notAttArray
              }
            }
        




            /**
             * End of itetation 
             * Compare proximity of array 
             */

            //  if (resultGroups.length > 0) {
            //    let startHour = moment(result.startHour, 'hhmm').subtract(60, 'minutes');
            //    let endHour = moment(result.endHour, 'hhmm').add(60, 'minutes');
            //    if (now >= startHour && now <= endHour) {
            //      console.log("ya es hora de tu clase")
            //      const dataAttendance = {
            //        groupId: resultGroup._id,
            //        studentId: student._id,
            //        attendance: true
            //      }
            //      await Attendance.addOrUpdateTodayAttendanceStudent(dataAttendance)
            //      return {
            //        success: true,
            //        student,
            //        group: resultGroup
            //      }
            //    } else {
            //      return {
            //        success: false,
            //        error: "No puedes pasar asistencia para este grupo, es a partir de las: " + moment(startHour).format("HH:mm A") + " y hasta las: " + moment(endHour).format("HH:mm A"),
            //        code: 406
            //      }
            //    }
            //  }
 
    
    }
  },

  getStudentInfo: async (attListId, arrStId) => {
    if (!arrStId.length) {
      return []
    }
    const students = await Promise.all(
      arrStId.map(id => {
        return StudentModel
          .findOne({ _id: id })
          .select('name lastName secondLastName email phoneNumber')
      })
    )
    return {
      attListId,
      students
    }
  },

  postSingle: async data => {

    const studentAttendance = await AttendanceStudentModel
      .findOneAndUpdate({ student: data.studentId },
        {
          $push: {
            attendance: {
              group: data.group,
              date: data.date,
              attendance: data.attendance
            }
          }
        },
        { new: true }
      )

    var attendance = studentAttendance


    try {

      if (studentAttendance == null) {
        const stAttendance = await AttendanceStudentModel.insertMany({
          student: data.studentId,
          attendance: [
            {
              group: data.group,
              date: data.date,
              attendance: data.attendance
            }
          ]

        })
        var attendance = stAttendance[0]
      }

      var nuevo = attendance.attendance.slice(-1)
      data._id = nuevo[0]._id

    } catch (error) {
      console.log(error)
      console.error('- Error trying to post single attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    return {
      success: true,
      attendance: data
    }
  },


  updateUserAtt: async data => {
    const studentAttendance = await AttendanceTeacherModel
      .findOne({ teacher: data.userId })
    if (!studentAttendance) {
      return {
        code: 404,
        success: false,
        error: errorExt.error,
        errorExt
      }
    }

    try {
      for (let i = 0; i < studentAttendance.attendance.length; i++) {
        if (studentAttendance.attendance[i]._id == data.attendanceId) {
          studentAttendance.attendance[i].date = data.date
          studentAttendance.attendance[i].attendance = data.attendance
        }
      }
    } catch (error) {
      console.error('- Error trying to update teacher/user attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    await studentAttendance.save()

    return {
      success: true,
      data
    }
  },

  deleteTeacherAtt: async data => {
    const studentAttendance = await AttendanceTeacherModel
      .findOne({ teacher: data.userId })
    if (!studentAttendance) {
      return {
        code: 404,
        success: false,
        error: errorExt.error,
        errorExt
      }
    }

    try {
      let index = -1
      for (let i = 0; i < studentAttendance.attendance.length; i++) {
        if (studentAttendance.attendance[i]._id == data.attendanceId) {
          index = i
        }
      }

      if (index > -1) {
        studentAttendance.attendance.splice(index, 1)
        await studentAttendance.save()
      } else {
        return {
          success: false,
          code: 500,
          error: "No attendance identifier"
        }

      }

    } catch (error) {
      console.log(error)
      console.error('- Error trying to delete single attendance (for)', JSON.stringify(error, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(error, null, 2)
      }
    }

    await studentAttendance.save()

    return {
      success: true,
      data
    }
  }



}






module.exports = Attendance