/**
 * models/login.js
 *
 * @description :: Describes the login functions
 * @docs        :: TODO
 */
const bcrypt = require('bcryptjs')
const rp = require('request-promise')

const Users = require('./users')
const StudentModel = require('../db/students')
const RolesModel = require('../db/roles')

const { appId, appSecret } = require('../config')

const Login = {
  exec: async (email, password = '') => {
    const result = await Users.findByEmail(email)

    if (!result.success) {
      return {
        success: false,
        code: 401,
        error: 'Invalid email or password'
      }
    }

    const user = result.user

    const hashedPass = user.hashed_password || ''

    if (bcrypt.compareSync(password, hashedPass)) {
      if (user.hashed_password) {
        delete user.hashed_password
      }

      const roles = await Promise.all(
        user.roles.map(role => RolesModel.findOne({ _id: role }))
      )

      const resources = []
      roles.forEach(role => {
        role.allows.forEach(allow => {
          let added = false
          for (let i = 0; i < resources.length; i++) {
            if (allow.resource === resources[i].resource) {
              for (let j = 0; j < allow.methods.length; j++) {
                if (
                  resources[i].methods.indexOf(allow.methods[j]) === -1
                ) {
                  resources[i].methods.push(allow.methods[j])
                  added = true
                }
              }
            }
          }
          if (!added) {
            resources.push(allow)
          }
        })
      })

      user.roles = user.roles.map(role => role._id)

      return {
        success: true,
        message: 'Logged',
        user: user.toSimple(),
        roles: roles.map(role => role._id),
        resources
      }
    } else {
      return {
        success: false,
        code: 401,
        error: 'Invalid email or password'
      }
    }
  },

  students: async (mail, password = '') => {
    var email = new RegExp(["^", mail, "$"].join(""), "i");
    const student = await StudentModel.findOne({
      email: email
    })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${email}`
      }
    }

    const hashedPass = student.hashed_password || ''

    if (bcrypt.compareSync(password, hashedPass)) {
      return {
        success: true,
        message: 'logged',
        student: student.toSimple()
      }
    } else {
      return {
        success: false,
        code: 401,
        error: 'Invalid email or password'
      }
    }
  },

  fb: async (fbId, fbAccessToken, email) => {
    const student = await StudentModel.findOne({
      email
    })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found with this email: ${email}`
      }
    }

    const st = student.toSimple()

    const url = `https://graph.facebook.com/oauth/access_token?client_id=${appId}&client_secret=${appSecret}&grant_type=fb_exchange_token&fb_exchange_token=${fbAccessToken}`

    const opts = {
      uri: url,
      method: 'GET'
    }

    const data = await rp(opts)
      .then(result => {
        return JSON.parse(result)
      })
      .catch(err => {
        console.error('- Error trying to get all credits.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in data && data.success === false) {
      return data
    }

    student.fbToken = data.access_token
    await student.save()

    return {
      success: true,
      student: st,
      fbToken: data.access_token,
      tokenType: data.token_type,
      expiresIn: data.expires_in
    }
  }
}

module.exports = Login
