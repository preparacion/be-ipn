// models/search.js
const StudentModel = require('../db/students');

const Search = {
  /**
   * esSearch - Promise wrapper for mongoosastic search
   * @param {Model} Model - Mongoose model with mongoosastic plugin
   * @param {Object} query - Elasticsearch query
   * @param {Object} options - Search options
   * @returns {Promise} Search results
   */
  esSearch: async (Model, query = {}, options = {}) => {
    return new Promise((resolve, reject) => {
      if (typeof Model !== 'function') {
        return reject(new Error('Model is not a function'));
      }
      if (typeof Model.esSearch !== 'function') {
        return reject(new Error(`Can't realize searches with this model`));
      }
      if (typeof query !== 'object') {
        return reject(new Error('The query must be an object'));
      }

      Model.esSearch(query, options, (err, results) => {
        if (err) {
          return reject(err);
        }
        return resolve(results);
      });
    });
  },

  /**
   * buildSortField - Builds sort field for elasticsearch
   * @param {string} sortField - Field to sort by
   * @param {boolean} asc - Sort direction
   * @returns {Object} - Sort configuration
   */
  buildSortField: (sortField, asc = true) => {
    const field = {};
    const dic = {
      lastName: 'lastName_lower.keyword',
      lastName_lower: 'lastName_lower.keyword',
      secondLastName: 'secondLastName_lower.keyword',
      secondLastName_lower: 'secondLastName_lower.keyword',
      fullName_lower: 'fullName_lower.keyword',
      email: 'email_lower.keyword',
      email_lower: 'email_lower.keyword',
      phoneNumber: 'phoneNumber.keyword',
      secondPhoneNumber: 'secondPhoneNumber.keyword',
      registerDate: 'registerDate',
      payments: 'balance.payments.total',
      costs: 'balance.costs.total'
    };
    
    if (sortField && dic[sortField]) {
      field[dic[sortField]] = { order: asc ? 'asc' : 'desc' };
    } else {
      field['lastName_lower.keyword'] = { order: asc ? 'asc' : 'desc' };
    }
    return field;
  },

  /**
   * search - Main search function
   * @param {Object} params - Search parameters
   * @returns {Promise} Search results
   */
  search: async ({ q = '', sort, asc, skip = 0, limit = 10 } = {}) => {
    try {
      const selectFields = [
        'name', 'lastName', 'secondLastName',
        'email', 'address', 'phoneNumber', 
        'secondPhoneNumber', 'balance', 'debt', 
        'registerDate'
      ];

      const sortField = Search.buildSortField(sort, asc);
      const queryTerm = q.toLowerCase();

      const query = {
        from: parseInt(skip),
        size: parseInt(limit),
        query: {
          multi_match: {
            query: queryTerm,
            type: 'phrase_prefix',
            fields: [
              'name_lower^3',
              'lastName_lower^3',
              'secondLastName_lower^2',
              'lastNames_lower^2',
              'fullName_lower^2',
              'firstLastName_lower^2',
              'email_lower',
              'phoneNumber',
              'secondPhoneNumber'
            ]
          }
        },
        sort: [sortField]
      };

      const options = {
        hydrate: {
          select: selectFields.join(' '),
          options: { lean: true }
        }
      };

      const results = await Search.esSearch(StudentModel, query, options);
      let filteredResults = [];

      if (results && results.hits && results.hits.hits) {
        filteredResults = results.hits.hits.map(hit => hit.doc || hit._source);
      }

      return {
        success: true,
        result: {
          hits: {
            total: results.hits.total,
            hits: filteredResults
          }
        }
      };

    } catch (err) {
      console.error('- Error trying to search students', JSON.stringify(err, null, 2));
      return {
        success: false,
        code: 500,
        error: JSON.stringify(err, null, 2)
      };
    }
  }
};

module.exports = Search;