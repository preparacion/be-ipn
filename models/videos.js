/**
 * models/videos.js
 *
 * @description :: Describes the videos functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')

const S3 = require('../lib/s3')
const transcode = require('../lib/transcode')
const ErrMsg = require('../lib/errMsg')

const PlaylistObject = require('./playlists')

const VideoModel = require('../db/videos')
const ReportModel = require('../db/reports')
const PlaylistModel = require('../db/playlists')
const QuizModel = require('../db/quiz')
const VideoCommentsModel = require('../db/videoComments')

const {
  awsCloudfromEndpoint,
  cdnCheapEndpoint,
  awsCloudfromEndpointImage
} = require('../config')

const errMsg = new ErrMsg()

const Videos = {
  get: async ({ skip, limit } = {}) => {
    const result = await VideoModel
      .find({})
      .limit(limit)
      .skip(skip)
      .then(videos => {
        return {
          success: true,
          videos
        }
      })
      .catch(err => {
        console.error('- Error trying to get all videos.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in result && !result.success) {
      // Return error
      return result
    }

    const videos = await Promise.all(result.videos.map(video => video.toPopulate()))

    return {
      success: true,
      videos
    }
  },

  getById: async id => {
    const result = await VideoModel
      .findOne({ _id: id })
      .then(video => {
        if (!video) {
          return {
            success: false,
            code: 404,
            error: `Video not found: ${id}`
          }
        }
        return {
          success: true,
          video: video
        }
      })
      .catch(err => {
        console.error('- Error trying to get a video by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in result && !result.success) {
      // Return error
      return result
    }

    const video = await result.video.toPopulate()

    return {
      success: true,
      video
    }
  },

  /* 
  * This function generate a pre-signed URL to return to server , then server upload content direct to s3
  * that's we manage the server load file problem.
  */
  createUrl: async (data) => {
    const TYPE_VIDEO = 1
    let responseUrl = await S3.genereteS3SignedUrl(data.filename, TYPE_VIDEO)
    if (!responseUrl.success) {
      return {
        success: false,
        code: 500,
        error: `Error trying to get a url`
      }
    }
    const videoId = new mongoose.Types.ObjectId()
    const video = new VideoModel({
      _id: videoId,
      title: data.title,
      description: data.description,
      duration: data.duration,
      playlist: data.playlist,
      course: data.course,
      quizParent: data.quizParent,
      transcodeId: responseUrl.keyFile,
      statusTranscode: 'WAITING_FILE',
    })
    if (video.playlist) {
      const playlist = await PlaylistModel
        .findOne({ _id: video.playlist })
      if (!playlist) {
        return {
          code: 400,
          success: false,
          error: `Playlist not found ${video.playlist}`,
          video: videoUploadResult
        }
      }
      if (!playlist.videos) {
        playlist.videos = []
      }
      playlist.videos.push({
        _id: new mongoose.Types.ObjectId(),
        order: 1,
        video: video._id
      })
      await playlist.save()
      //this will increase the number in acendent cascade 
      //for the number of items in the path and sub-paths
      PlaylistObject.getAllPlaylistId(playlist._id).then(
        async (value) => {
          let result = await PlaylistModel.updateMany(
            { _id: { $in: value } },
            { $inc: { totalItems: 1 } })
          console.log(result)
        }
      )
    }
    if (video.quizParent) {
      const quiz = await QuizModel.findOne({
        _id: video.quizParent
      })

      if (!quiz) {
        return {
          code: 400,
          success: false,
          error: `Quiz not found ${video.quizParent}`,
          video: video._id
        }
      }
      if (!quiz.videoBucket) {
        quiz.videoBucket = []
      }
      quiz.videoBucket.push(video._id)
      await quiz.save()
    }

    await video.save()
    return {
      success: true,
      url: responseUrl.url
    }
  },

  create: async (data, stream, ext = null) => {
    const videoUploadResult = await S3.uploadVideo(data, stream, ext)
    if (!videoUploadResult.success) {
      return {
        success: false,
        code: 500,
        error: `Error trying to upload a video`
      }
    }
    const transcodeResult = await transcode.initTranscode(videoUploadResult.response.Key)
    const videoId = new mongoose.Types.ObjectId()
    const statusTranscode = transcodeResult.Job.Status
    const transcodeId = transcodeResult.Job.Id
    const video = new VideoModel({
      _id: videoId,
      title: data.title,
      rate: 0,
      description: data.description,
      duration: data.duration,
      playlist: data.playlist,
      course: data.course,
      eTag: videoUploadResult.response.ETag,
      version: videoUploadResult.response.VersionId,
      bucket: videoUploadResult.response.Bucket,
      url: videoUploadResult.response.Location,
      statusTranscode: statusTranscode,
      transcodeId: transcodeId
    })

    if (video.playlist) {
      const playlist = await PlaylistModel
        .findOne({ _id: video.playlist })
      if (!playlist) {
        return {
          code: 400,
          success: false,
          error: `Playlist not found ${video.playlist}, but the video was created`,
          video: videoUploadResult
        }
      }

      if (!playlist.videos) {
        playlist.videos = []
      }

      playlist.videos.push({
        _id: new mongoose.Types.ObjectId(),
        order: 1,
        video: video._id
      })

      await playlist.save()
    }

    await video.save()

    return {
      success: true,
      video
    }
  },

  // TODO: Update Documentation
  update: async (id, data) => {
    await VideoModel.updateOne({
      _id: id
    }, {
      $set: {
        title: data.title,
        description: data.description,
        active: data.active,
      }
    }, {
      upsert: false // just in case
    })
    const video = await VideoModel.findOne({ _id: id })
    if (!video) {
      return {
        success: false,
        code: 404,
        error: `Video not found: ${data.id}`
      }
    }
    video.type = 2;
    return {
      success: true,
      video
    }
  },

  rate: async (data) => {
    const video = await VideoModel.findOne({ _id: data.id })
    if (!video) {
      return {
        success: false,
        code: 404,
        error: `Video not found: ${data.id}`
      }
    }
    const currentRate = await video.calculateRate(data)
    return {
      success: true,
      userRate: currentRate.rateUser,
      currentRate: rateTotal
    }
  },

  delete: async id => {
    const video = await VideoModel
      .findOne({ _id: id })
    if (!video) {
      return {
        success: false,
        code: 404,
        error: `Video not found: ${id}`
      }
    }

    const playlists = await PlaylistModel
      .find({ 'videos.video': id })

    await Promise.all(
      playlists.map(pl => {
        Videos.removeVideoFromPlaylist(pl, id)
      })
    )
    //This will remove the video from the processed bucked evoid lot of data in S3 bucket
    if (video.statusTranscode == "COMPLETE" || video.statusTranscode == "COMPLETE_IMAGE") {
      await S3.deleteVideoProcessed(video.transcodeId)
    }
    if (video.playlist) {
      PlaylistObject.getAllPlaylistId(video.playlist).then(
        async (value) => {
          let result = await PlaylistModel.updateMany(
            { _id: { $in: value } },
            { $inc: { totalItems: -1 } })
          console.log(result)
        }
      )
      PlaylistObject.updateDeleteItem(video.playlist, video._id).then(
        value => {
          console.log("DELETES", value)
        }
      )
    }
    await VideoModel.deleteOne({ _id: id })

    return {
      success: true,
      deleted: true
    }
  },

  report: async (id, data) => {
    const video = await VideoModel.findOne({
      _id: id
    })
    if (!video) {
      const errorExt = errMsg
        .getNotEntityFound('Video', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    data['video'] = video._id
    if (data._id) {
      delete data._id
    }

    const report = new ReportModel(data)

    await report.save()

    return {
      success: true,
      report
    }
  },

  comment: async (id, data) => {
    if (data._id) {
      delete data._id
    }
    const video = await VideoModel
      .findOne({ _id: id })
      .select('_id')
    if (!video) {
      const errorExt = errMsg
        .getNotEntityFound('Video', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const comment = new VideoCommentsModel({
      comment: data.comment,
      date: data.date,
      student: data.student,
      video: video._id,
      responses: []
    })
    await comment.save()

    return {
      success: true,
      comment
    }
  },

  //TODO
  editComment: async (id, data) => {
    const videoComment = await VideoCommentsModel
      .findOne({ _id: id })
    if (!videoComment) {
      const errorExt = errMsg
        .getNotEntityFound('VideoComment', commentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    videoComment.comment = data.comment
    await videoComment.save()
    return {
      success: true,
      videoComment
    }
  },

  //TODO
  deleteComment: async (id) => {
    await VideoCommentsMode.remove({ _id: id })
    return {
      success: true,
      deleted: true
    }
  },

  respondComment: async (commentId, data) => {
    if (data.id) {
      delete data.id
    }
    const videoComment = await VideoCommentsModel
      .findOneAndUpdate(
        { _id: commentId },
        {
          $push: { responses: data }
        },
        {
          upsert: false,
          new: true
        }
      )
    if (!videoComment) {
      const errorExt = errMsg
        .getNotEntityFound('VideoComment', commentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    return {
      success: true,
      videoComment
    }
  },

  updateStatusMedia: async (data, socket) => {
    console.log(JSON.stringify(data))
    const videoRes = await VideoModel.findOne({
      transcodeId: data.detail.userMetadata.transcodeId
    })
    if (!videoRes) {
      const errorExt = errMsg
        .getNotEntityFound('Video')
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    videoRes['statusTranscode'] = data.detail.status
    if (data.detail.status === "COMPLETE") {
      S3.deleteVideo(videoRes.transcodeId)
      let fileExtention = data.detail.userMetadata.transcodeId.split('.', 1)
      fileExtention = fileExtention + '.mp4'
      const url = awsCloudfromEndpoint + fileExtention
      videoRes['url'] = url
      videoRes['cheapUrl'] = cdnCheapEndpoint + fileExtention
      socket.to('update_material_event').emit("update_material_event", {
        event: "COMPLETE",
        video: videoRes
      })
    } else if (data.detail.status === 'PROGRESSING') {
      socket.to('update_material_event').emit("update_material_event", {
        event: "PROCESSING",
        video: videoRes
      })
      videoRes.statusTranscode = 'PROCESSING'
    }
    const video = new VideoModel(videoRes)
    await video.save()
    return {
      success: true,
      video
    }
  },

  updateStatusTranscode: async (dataEvent, socket) => {
    console.log(dataEvent)
    //Only retransmition of data without db load charge 
    if (dataEvent.status === 'UPDATE_PROGRESS') {
      socket.to('update_material_event').emit("update_material_event", {
        event: "UPDATE_PROGRESS",
        progress: dataEvent.progress,
        logs: dataEvent.logs,
        video: { transcodeId: dataEvent.id }
      })
      return {
        success: true,
      }
    }
    //other cases with a database lookIn
    const videoRes = await VideoModel.findOne({
      transcodeId: dataEvent.key
    })
    if (!videoRes) {
      console.log('erro')
      const errorExt = errMsg
        .getNotEntityFound('Video', dataEvent.key)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    videoRes.statusTranscode = dataEvent.status
    if (dataEvent.status === 'COMPLETE') {
      S3.deleteVideo(dataEvent.key);
      const url = awsCloudfromEndpoint + dataEvent.resultKey;
      videoRes.url = url
      videoRes.cheapUrl = cdnCheapEndpoint + dataEvent.resultKey;
      socket.to('update_material_event').emit("update_material_event", {
        event: "COMPLETE",
        video: videoRes
      })
    } else if (dataEvent.status === 'COMPLETE_IMAGE') {
      const url = awsCloudfromEndpointImage + 'thumbnails/' + dataEvent.resultKey;
      videoRes.urlThumbnail = url
    } else if (dataEvent.status === 'PROCESSING') {
      socket.to('update_material_event').emit("update_material_event", {
        event: "PROCESSING",
        video: videoRes
      })
    }

    const video = new VideoModel(videoRes)
    await video.save()

    return {
      success: true,
    }
  },


  /*****************************************
  ******************************************
  ************* Auxiliar Functions *********
  ******************************************
  ******************************************
  */
  removeVideoFromPlaylist: async (playlist, id) => {
    for (let i = 0; i < playlist.videos.length; i++) {
      const video = playlist.videos[i]
      if (String(id) === String(video.video)) {
        playlist.videos.splice(i, 1)
      }
    }

    await playlist.save()
  },
}
module.exports = Videos
