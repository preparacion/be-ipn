/**
 * models/comments.js
 *
 * @description :: Describes the comments functions
 * @docs        :: TODO
 */


const GroupModel = require('../db/groups')
const TalksModel = require('../db/talks')
const CommentModel = require('../db/comments')
const StudentModel = require('../db/students')
const UserTalksModel = require('../db/usersTalks')
const Users = require('../db/users')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()


const Comments = {
  get: async ({ skip, limit, group, talk, user, student, userTalks } = {}) => {
    const filter = {}
    if (group) {
      filter.group = group
    }
    if (talk) {
      filter.talk = talk
    }
    if (user) {
      filter.user = user
    }
    if (student) {
      filter.student = student
    }
    if (userTalks) {
      filter.userTalks = userTalks
    }


    try {

      const result = await CommentModel.find(filter)
        .skip(skip)
        .limit(limit)
        .catch(err => {
          console.error('- Error trying to get comments', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to get comments'
            }
          }
        })

      const cPopulated = await Promise.all(
        result.map(comment => comment.toPopulate())
      )
        .then(comments => {
          // desc sorting
          const com = comments.sort(function (a, b) {
            if (a.date < b.date) return 1
            if (a.date > b.date) return -1
            return 0
          })
          return {
            success: true,
            comments: com
          }
        })
      return cPopulated

    } catch (err) {
      const errorExt = errMsg
        .generalGet('comments')
      return {
        success: false,
        error: errorExt
      }

    }
  },


  // TODO : Update socket to suscribe only in backend to the groupId room by _id.

  create: async (data, userId, socket = null) => {
    data.user = userId
    try {
      if (data.group) {
        const group = await GroupModel
          .findOne({ _id: data.group })
        if (!group) {
          const errorExt = errMsg
            .getNotEntityFound('group', data.group)
          return {
            success: false,
            error: errorExt
          }
        }
      }
      if (data.talk) {
        const talk = await TalksModel
          .findOne({ _id: data.talk })
        if (!talk) {
          const errorExt = errMsg
            .getNotEntityFound('Talk', data.talk)
          return {
            success: false,
            error: errorExt
          }
        }
      }
      if (data.student) {
        const student = await StudentModel
          .findOne({ _id: data.student })
        if (!student) {
          const errorExt = errMsg
            .getNotEntityFound('student', data.student)
          return {
            success: false,
            error: errorExt
          }
        }
      }
      if (data.userTalks) {
        const userTalks = await UserTalksModel
          .findOne({ _id: data.userTalks })
        if (!userTalks) {
          const errorExt = errMsg
            .getNotEntityFound('userTalks', data.userTalks)
          return {
            success: false,
            error: errorExt
          }
        }
      }
      let comment = new CommentModel(data)
      await comment.save()
      comment = await comment.toPopulate();
      if (data.talk) {
        socket.to('update_table_event').emit("update_table_event", {
          event: "add_comment_talk",
          talkId: data.talk,
          studentId: data.student,
          comment: comment
        });
      }
      return {
        success: true,
        comment
      }
    } catch (err) {
      const errorExt = errMsg
        .generalCreate('comments')
      return {
        success: false,
        error: errorExt
      }
    }
  },


  getById: async ({ id }) => {
    const comment = await CommentModel
      .findOne({ _id: id })
    if (!comment) {
      return {
        success: false,
        code: 404,
        error: `Comment not found: ${id}`
      }
    }
    const cPopulated = await comment.toPopulate()
    return {
      success: true,
      comment: cPopulated
    }
  },


  update: async (data, userId) => {
    try {
      const commentId = data.id
      const user = await Users
        .findOne({ _id: userId })
        .select("name lastName")
      if (!user) {
        const errorExt = errMsg
          .getNotEntityFound('user', userId)
        return {
          success: false,
          error: errorExt
        }
      }
      // this will return the new object
      const commentEdited = await CommentModel
        .findOneAndUpdate({
          _id: commentId
        }, {
          user: user._id,
          comment: data.comment,
          date: new Date(),
          $push: {
            editions: {
              user: user._id,
              comment: data.comment,
              date: new Date(),
            }
          }
        }, {
          new: true
        })
        .populate({
          path:"user",
          select:"name lastName secondLastName"
        });
      // const cPopulated = await comment.toPopulate()
      return {
        success: true,
        comment: commentEdited
      }
    } catch (err) {
      const errorExt = errMsg
        .generalUpdate('comments')
      return {
        success: false,
        error: errorExt
      }
    }
  },


  /// *** DELETE AND ERROR EXAMPLE *** 
  delete: async ({ id }) => {

    try {
      const comment = await CommentModel.findOne({ _id: id })
      if (!comment) {
        const errorExt = errMsg
          .getNotEntityFound('comment', id)
        return {
          success: false,
          error: errorExt
        }
      }

      await comment.remove()

      return {
        success: true,
        deleted: true
      }

    } catch (err) {
      const errorExt = errMsg
        .generalDelete('comments')
      return {
        success: false,
        error: errorExt
      }

    }
  }
}



module.exports = Comments
