// N O T   U S E D


/**
 * models/studentComments.js
 *
 * @description :: Describes the studentComments functions
 * @docs        :: TODO
 */
const StudentCommentsModel = require('../db/studentComments')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()


const StudentComments = {

  getByStudentId: async (studentId) => {
    const studentComments = await StudentCommentsModel
      .findOne({ student: studentId })
      .then(stComments => {
        const comments = (stComments && stComments.comments.length)
          ? stComments.comments.sort((a, b) => b.date - a.date)
          : []
        return {
          success: true,
          comments
        }
      })

    return {
      success: true,
      comments: studentComments
    }
  },

  create: async (data) => {

    try{
    const previousComments = await StudentCommentsModel
      .findOne({ student: data.student })
    if (!previousComments) {
      const studentComment = new StudentCommentsModel({
        student: data.student,
        comments: [{
          date: data.comment.date,
          comment: data.comment.comment,
          createdBy: data.comment.createdBy
        }]
      })
      await studentComment.save()

      return {
        success: true,
        studentComment
      }
    }

    previousComments.comments.push({
      date: data.comment.date,
      comment: data.comment.comment,
      createdBy: data.comment.createdBy
    })
    await previousComments.save()

    return {
      success: true,
      previousComments
    }

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('studentComments')
    return {
      success: false,
      error: errorExt
    }
  }
  }

  

}

module.exports = StudentComments
