/**
 * models/appoiments.js
 *
 * @description :: Describes the appoiments functions
 * @docs        :: TODO
 */
const moment = require('moment')
const mongoose = require('mongoose')
const GroupsModel = require('../db/groups')
const AppoimentsModel = require('../db/appoiments')
const AppoimentsListModel = require('../db/appoimentsList')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Appoiments = {

    get: async () => {
        const arrayHour = ["8:00", "10:00", "12:00", "14:00", "16:00", "18:00"]
        let currentDate = moment(new Date()).set({
            'hour': 0,
            'minute': 0,
            'second': 0,
            'millisecond': 0
        })
        let stopDate = moment(currentDate).add(1, 'w')
        stopDate = stopDate.set({
            'hour': 0,
            'minute': 0,
            'second': 0,
            'millisecond': 0
        })
        let dateArray = []

    

            const appoimentList = await AppoimentsListModel.find({
                    date: {
                        $gte: currentDate.toISOString(),
                        $lt: stopDate.toISOString()
                    }
                }).populate({
                    path: "appoiments",
                    populate: {
                        path: 'student',
                        select: '_id name lastName secondLastName',
                    }
                })
                .populate({
                    path: "appoiments",
                    populate: {
                        path: 'courseTopics',
                    }
                })
                .populate({
                    path: 'course',
                    select: 'name'
                })
                .catch(err => {
                    console.log(err)
                    const errorExt = errMsg
                        .errorDB('find()','appoiments', 'get()')
                    return {
                        success: false,
                        error: errorExt
                    }
                })


      try {
            /* This is The date Generator base on the date now*/
            /* Check the hour available */


            while (currentDate < stopDate) {
                let current = moment(currentDate).format('DD-MM-YYYY')
                let arrayTime = []
                if (appoimentList) {
                    let listOfCurrent = appoimentList.filter((value) => {
                        let date = moment(value.date).format('DD-MM-YYYY')
                        return (date == current)
                    })
                    for (let i = 0; i < arrayHour.length; i++) {
                        let match = listOfCurrent.filter(x => moment(x.date).format("HH:mm") == moment(arrayHour[i], 'HH:mm').format('HH:mm'))
                        let date = moment(current + " " + arrayHour[i], ('DD-MM-YYYY HH:mm')).format('HH:mm')
                        if (match.length > 0) {
                            let info = []
                            let total = 0
                            match.forEach(element => {
                                let objectInfo = {
                                    courseName: element.course.name,
                                    studentsCount: element.appoiments.length
                                }
                                info.push(objectInfo)
                                total++
                            });
                            arrayTime.push({
                                value: date,
                                appoimentList: match,
                                appoimentsInfo: info,
                                totalCountStudents: total
                            })
                            //Here if exist appoimentList in this hour
                        } else {
                            arrayTime.push({
                                value: date,
                            })
                        }
                    }
                } else {
                    for (let i = 0; i < arrayHour.length; i++) {}
                }
                dateArray.push({
                    day: currentDate,
                    hours: arrayTime
                })
                //increment the day counter 
                currentDate = moment(currentDate).add(1, 'd');
            }
            return {
                success: true,
                dateArray,
            }

        } catch (err) {
            const errorExt = errMsg
                .generalGet('Appoiment')
            return {
                success: false,
                error: errorExt
            }
        }

    },


    create: async (idGroup, data, student) => {

        try{
        const dateTime = moment(data.date + ' ' + data.hour, 'DD-MM-YYYY HH:mm')
        const group = await GroupsModel.findOne({
            _id: idGroup
        })
        
        if (!group) {
            const errorExt = errMsg
              .getNotEntityFound('Group', idGroup)
            return {
              success: false,
              error: errorExt
            }
          }


 


        const appoiments = await AppoimentsModel.findOne({
            student: student._id,
            group: group._id,
            date: dateTime.toISOString()
        })
        if (appoiments) {

            {
                const errorExt = errMsg
                  .alreadyExists('Appoiment')
                return {
                  success: false,
                  error: errorExt
                }
              }

        }
        const appoimentsList = await AppoimentsListModel.findOne({
            course: group.course,
            date: dateTime.toISOString()
        })
        //IF Exist appoimentList create 
        if (appoimentsList) {
            if (appoimentList.appoiment.lenght < 11 || appoimentList.appoiment < appoimentList.maxQuota) {
                                {
                    const errorExt = errMsg
                      .overquotaError('appoimentsList')
                    return {
                      success: false,
                      error: errorExt
                    }
                  }
                
            } else {
                let appoimentId = new mongoose.Types.ObjectId()
                const appoiment = new AppoimentsModel({
                    _id: appoimentId,
                    date: dateTime.toISOString(),
                    student: student._id,
                    group: group._id,
                    parent: appoimentList._id
                })
                appoimentList.appoiment.push(appoimentId)
                appoiment.save()
                appoimentList.save()
            }
        } else {
            let appoimentListId = new mongoose.Types.ObjectId()
            let appoimentId = new mongoose.Types.ObjectId()
            const appoiment = new AppoimentsModel({
                _id: appoimentId,
                date: dateTime.toISOString(),
                student: student._id,
                group: group._id,
                parent: appoimentListId
            })
            const appoimentList = new AppoimentsListModel({
                _id: appoimentListId,
                date: dateTime.toISOString(),
                appoiments: [appoimentId],
                course: group.course
            })
            appoiment.save()
            appoimentList.save()
            return {
                success: true,
                appoiment
            }
        }
    }
    catch (err) {
        const errorExt = errMsg
            .generalCreate('Appoiment')
        return {
            success: false,
            error: errorExt
        }
    
}

    }
}
module.exports = Appoiments