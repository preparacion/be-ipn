/**
 * models/users.js
 *
 * @description :: Describes the users functions
 * @docs        :: TODO
 */
const path = require('path')
const bcrypt = require('bcryptjs')
const generator = require('generate-password')

const UserModel = require('../db/users')
const GroupsModel = require('../db/groups')
const ImageModel = require('../db/images')

// const Gcs = require('../lib/gcs')
const SendGrid = require('../lib/sendGrid')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()
// const gcs = new Gcs()

const Users = {
  findByEmail: async (email = '') => {
    const user = await UserModel
      .findOne({
        email: email
      })
      .populate('roles')
      .then(user => {
        if (user) {
          return {
            success: true,
            user
          }
        }
        return {
          success: false,
          code: 404,
          error: `User not found ${email}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a user by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return user
  },

  findByRoleId: async roleId => {
    const user = await UserModel
      .findOne({
        roleId: roleId
      })
      .then(user => {
        if (user) {
          return {
            success: true,
            user
          }
        }
        return {
          success: false,
          code: 404,
          error: `User not found ${roleId}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a user by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return user
  },

  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async (
    {
      skip,
      limit,
      rolesIn,
      noRolesIn,
      teachers
    } = {}
  ) => {


    try{
    const filter = {
      root: { $ne: true }
    }

    if (teachers) {
      filter['isProfessor'] = true
    }
    if (
      rolesIn && rolesIn.length && noRolesIn && noRolesIn.length
    ) {
      filter['role'] = {
        $in: rolesIn,
        $nin: noRolesIn
      }
    } else if (rolesIn && rolesIn.length) {
      filter['role'] = {
        $in: rolesIn
      }
    } else if (noRolesIn && noRolesIn.length) {
      filter['role'] = {
        $exists: true,
        $nin: noRolesIn
      }
    }

    const users = await UserModel
      .find(filter)
      .skip(parseInt(skip))
      .limit(parseInt(limit))
      .then(users => {
        return {
          success: true,
          users
        }
      })
      .catch(err => {
        console.error('- Error trying to get all users.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return users

  } catch (err) {
    const errorExt = errMsg
      .generalGet('users')
    return {
      success: false,
      error: errorExt
    }
  }

  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const user = await UserModel.findOne({ _id: id })
    .populate('picture')
    .then(user => {
        if (user) {
          return {
            success: true,
            user
          }
        }
        return {
          success: false,
          code: 404,
          error: `User not found ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a user by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return user
  },

  /**
   * create
   *
   * @description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {

try{

    const previousUser = await Users.findByEmail(data.email)
    if (previousUser.success) {
      const errorExt = errMsg.getByIndex('userAlreadyCreated')
    return {
      success: false,
      error: errorExt
    }
    }

    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    // create fields for searching
    if (data['name']) {
      data['name_lower'] = data['name'].toLowerCase()
    }

    if (data['email']) {
      data['email_lower'] = data['email'].toLowerCase()
    }

    if (data['lastName']) {
      data['lastName_lower'] = data['lastName'].toLowerCase()
    }

    if (data['name'] && data['lastName']) {
      data['fullName_lower'] = `${data['name']} ${data['lastName']}`
    }

    const user = new UserModel(data)
    const result = await user.save()
      .then(user => {
        return {
          success: true,
          user
        }
      })
      .catch(err => {
        console.error('- Error trying to create a user', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a user'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('users')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * update
   *
   * @description: Update a single item by id.
   * @param {string} id - Permission id.
   * @param {object} data - List of properties.
   */
  
  update: async (id, data) => {
   try{
   
    const user = await UserModel.findOne({ _id: id })

    if (!user) {
      const errorExt = errMsg
        .getNotEntityFound('User', id)
        return {
          success: false,
          error: errorExt
        }
    }


    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    const result = await UserModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        new: true
      })
      .populate('picture')
      .then(user => {
        return {
          success: true,
          user
        }
      })
      .catch(err => {
        console.error('- Error trying to update a user', JSON.stringify(err, null, 2))

        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a user'
          }
        }
      })

    return result


  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('users')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Permission id.
   */
  delete: async id => {
let idPic = ""

try{

  const user = await UserModel.findOne({ _id: id })
  .then(user => {
      if (user) {

        idPic = user.picture

      }
      return {
        success: false,
        code: 404,
        error: `User not found ${id}`
      }
    })
    .catch(err => {
      console.error('- Error trying to get a user by id', JSON.stringify(err, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(err, null, 2)
      }
    })


    const image = await ImageModel
    .deleteOne({ _id: idPic })

    const result = await UserModel.deleteOne({ _id: id })
      .then((ress) => {
        if(ress.n>0){
          return {
            success: true,
            deleted: true
          }
        }else{
          const errorExt = errMsg
          .getNotEntityFound('User', id)
        return {
          success: false,
          error: errorExt
        }

        }


      })
      .catch(err => {
        console.error('- Error trying to delete a user', JSON.stringify(err, null, 2))

        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to delete a user'
          }
        }

      })

    return result


  } catch (err) {
    const errorExt = errMsg
      .generalDelete('User')
    return {
      success: false,
      error: errorExt
    }
  }

  },

  getGroups: async (id) => {
    const groups = await GroupsModel
      .find({ teacher: id })
      .then(groups => {
        return Promise.all(
          groups.map(group => group.toPopulate())
        )
      })
      .catch(err => {
        console.error('- Error trying to get all groups.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return {
      success: true,
      groups
    }
  },

  recoverPass: async (email) => {
    const user = await UserModel.findOne({ email })
    if (!user) {
      const errorExt = errMsg
        .getNotEntityFound('User', email)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const tempPass = generator.generate({
      length: 10,
      numbers: true,
      lowercase:true,
      uppercase:false,
      symbols:false
    })

    user.hashed_password = await bcrypt.hash(tempPass, 10)
    await user.save()

    await SendGrid.sendRecoverPass(user.name, tempPass, user.email)

    return {
      success: true,
      message: 'pass recover'
    }
  }
}

module.exports = Users
