/**
 * models/courseTypes.js
 *
 * @description :: Describes the courseTypes functions
 * @docs        :: TODO
 */
const CourseModel = require('../db/courses')
const CourseTypeModel = require('../db/courseType')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const CourseType = class CourseType {
  /**
   * get
   * @description: Get all the course types
   * @returns Object with te course type list
   */
  async get () {

    try{

    const courseTypes = await CourseTypeModel
      .find({})
    return {
      success: true,
      courseTypes
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('payments')
    return {
      success: false,
      error: errorExt
    }
  }

  }

  /**
   * getById
   * @description Get a single course type by id
   * @param {string} id CourseType object id
   * @returns {object} Object with the course type
   */
  async getById (id) {
    const courseType = await CourseTypeModel
      .findOne({ _id: id })
    if (!courseType) {
      const errorExt = errMsg
        .getNotEntityFound('CourseTypes', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    return {
      success: true,
      courseType
    }
  }

  /**
   * update
   * @description Update a single course type
   * @param {string} id Course type object id
   * @param {object} data New Course type data
   */
  async update (id, data) {

    try{
    const courseType = await CourseTypeModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        upsert: false,
        returnNewDocument: true
      })
    if (!courseType) {
      const errorExt = errMsg
        .getNotEntityFound('CourseTypes', id)
        return {
          success: false,
          error: errorExt
        }
    }

    return {
      success: true,
      courseType
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('courseTypes')
    return {
      success: false,
      error: errorExt
    }

  }
  }

  /**
   * getCoursesById
   * @description Get the courses with a specific course type
   * @param {string} id Course type object id
   */
  async getCoursesById (id) {
    const courses = await CourseModel
      .find({ courseType: id })
      .populate('image')
    return {
      success: true,
      courses
    }
  }
}

module.exports = CourseType
