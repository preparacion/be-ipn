const PDFModel = require('../db/pdfs')
const StudentPDFProgressModel = require('../db/studentPDFProgress')
const S3 = require('../lib/s3')
const mongoose = require('mongoose')
const PlaylistModel = require('../db/playlists')
const PlaylistObject = require('./playlists')


const PDFs = {
    // Obtener URL firmada para subida
    getSignedUrl: async (data) => {
        let responseUrl = await S3.generatePDFSignedUrl(data.filename);
        if (!responseUrl.success) {
            return {
                success: false,
                code: 500,
                error: `Error trying to get a url`
            }
        }
        console.log("responseUrl",responseUrl);
        const pdfId = new mongoose.Types.ObjectId()
        const pdf = new PDFModel({
            _id: pdfId,
            name: data.title,
            description: data.description,
            key: responseUrl.keyFile,
            url: responseUrl.url,
            totalPages: data.totalPages,
            playlist: data.playlist,
            course: data.course,
            active: true
        })
        if (pdf.playlist) {
            const playlist = await PlaylistModel.findOne({ _id: pdf.playlist })
            if (!playlist) {
                return {
                    code: 400,
                    success: false,
                    error: `Playlist not found ${pdf.playlist}`,
                    pdf: pdfId
                }
            }
            PlaylistObject.getAllPlaylistId(playlist._id).then(
                async (value) => {
                    let result = await PlaylistModel.updateMany(
                        { _id: { $in: value } },
                        { $inc: { totalItems: 1 } }
                    )
                    console.log(result)
                }
            )
        }
        await pdf.save()
        return {
            success: true,
            url: responseUrl.url
        }
    },

    // Actualizar progreso de lectura
    updateProgress: async (pdfId, studentId, pageNumber, totalPages) => {
        try {
            let progress = await StudentPDFProgressModel.findOne({
                pdf: pdfId,
                student: studentId
            })

            if (!progress) {
                progress = new StudentPDFProgressModel({
                    pdf: pdfId,
                    student: studentId,
                    viewedPages: [pageNumber],
                    lastPage: pageNumber,
                    progress: (1/totalPages) * 100,
                    viewCount: 1,
                    lastViewedAt: new Date()
                })
            } else {
                if (!progress.viewedPages.includes(pageNumber)) {
                    progress.viewedPages.push(pageNumber)
                    progress.progress = (progress.viewedPages.length/totalPages) * 100
                }
                progress.lastPage = pageNumber
                progress.viewCount += 1
                progress.lastViewedAt = new Date()
            }

            await progress.save()
            return {
                success: true,
                progress
            }
        } catch (err) {
            console.error('Error updating PDF progress:', err)
            return {
                success: false,
                code: 500,
                error: 'Error updating progress'
            }
        }
    }
}

module.exports = PDFs