/**
 * models/quiz.js
 *
 * @description :: Describes the Quiz functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')
const ErrMsg = require('../lib/errMsg')

const PlaylistObject = require('./playlists')

const QuizModel = require('../db/quiz')
const VideosModel = require('../db/videos')
const ImageModel = require('../db/images.js')
const QuizQuestionModel = require('../db/quizQuestion')
const PlaylistModel = require('../db/playlists')


const errMsg = new ErrMsg()

const Quiz = {
    create: async (idPlaylist, body, user) => {

    try{

        const playlist = await PlaylistModel.find({
            _id: idPlaylist
        })

        if (playlist.length==0) {
            const errorExt = errMsg
            .getNotEntityFound('Playlist', idPlaylist)
            return {
                success: false,
                error: errorExt
                }
        }
        //In this point first we create the answer to push into array then asign to a quizz 
        const quizId = new mongoose.Types.ObjectId()
        let answersArray = []
        let answersArrayFull = []
        if (body.quiz) {
            for (let i = 0; i < body.quiz.length; i++) {
                let info = body.quiz[i]
                let answerId = new mongoose.Types.ObjectId()
                let data = {
                    _id: answerId,
                    parent: quizId,
                    question: info.question,
                    answerCollection: info.answerCollection,
                    active: info.active,
                }
                if (info.questionFormula && info.questionFormula.length > 0) {
                    data.questionFormula = info.questionFormula
                }
                if (info.questionImage && info.questionImage.length > 0) {
                    data.questionImage = info.questionImage
                }
                if (info.questionType) {
                    data.questionType = info.questionType
                }
                if (info.explanationVideo.length > 0) {
                    data.explanationVideo = info.explanationVideo
                }
                let question = new QuizQuestionModel(data)
                question.save()


                if (question.questionImage) {
                    question.questionImage = await ImageModel.findOne({ _id: question.questionImage })
                }
                let answers = [...question.answerCollection]
                for (let j = 0; j < answers.length; j++) {
                    if (answers[j].answerType == 2) {
                        answers[j].answerImage = await ImageModel.findOne({ _id: answers[j].answerImage })
                    }
                }
                question.answerCollection = answers

                answersArrayFull.push(question)
                answersArray.push(answerId)
            }
        }
        let data = {
            _id: quizId,
            name: body.name,
            description: body.description,
            parent: idPlaylist,
            questionCollection: answersArray,
            active: body.active,
            random: body.random,
            quizType: body.quizType,
            createdBy: user,
            registerDate: new Date(),
            quizType: body.quizType, //Define 1 for quiz 2 for exam or test
            limitTime: body.limitTime,
            type: 3, //Type 3 for quiz 
        }
        if (body.order)
            data.order = body.order

        const quiz = new QuizModel(data)
        await quiz.save()
        PlaylistObject.getAllPlaylistId(idPlaylist).then(
            async (value) => {
                let result = await PlaylistModel.updateMany(
                    { _id: { $in: value } },
                    { $inc: { totalItems: 1 } })
                //console.log(result)
            }
        )
        quiz.questionCollection = answersArrayFull
        return {
            success: true,
            quiz
        }

    } catch (err) {
        const errorExt = errMsg
          .generalCreate('quiz')
        return {
          success: false,
          error: errorExt
        }
      }
    },

    update: async (idQuizz, body, user) => {

        try{
        const quizOk = await QuizModel.findOne({
            _id: idQuizz
        })
        if (!quizOk) {
            const errorExt = errMsg
                .getNotEntityFound('Quiz', idQuizz)
                return {
                    success: false,
                    error: errorExt
                  }
        }
        let quiz = {}
        let unset = {}
        if (body.name)
            quiz.name = body.name
        if (body.description)
            quiz.description = body.description
        if (body.active !== undefined)
            quiz.active = body.active
        if (body.random !== undefined)
            quiz.random = body.random
        if (body.quizType)
            quiz.quizType = body.quizType //Define 1 for quiz 2 for exam or test
        if (body.limitTime)
            quiz.limitTime = body.limitTime
        else
            unset.limitTime = ""
        let answersArray = []
        let QuizEdit = []
        if (body.quiz) {
            //This for search for a removed elements 
            answersArray = [...quizOk.questionCollection]
            let toModifyElements = []
            let toRemoveElements = []
            let toAddElements = []
            for (let i = 0; i < body.quiz.length; i += 1) {
                if (body.quiz[i]._id) {
                    toModifyElements.push(i)
                    const question = await QuizQuestionModel
                        .findOneAndUpdate({
                            _id: body.quiz[i]._id
                        }, {
                            $set: body.quiz[i]
                        }, {
                            new: true
                        })
                        .then(quiz => {
                            return {
                                success: true,
                                quiz
                            }
                        })
                        .catch(err => {
                            console.error('- Error trying to update a question', JSON.stringify(err, null, 2))
                            return {
                                success: false,
                                error: {
                                  httpCode: 500,
                                  message: '- DB Error trying to update a question'
                                }
                              }
                        })
                    QuizEdit.push(question.quiz)
                }
                else {
                    toAddElements.push(i)
                    let info = body.quiz[i]
                    let answerId = new mongoose.Types.ObjectId()
                    let data = {
                        _id: answerId,
                        parent: quizOk._id,
                        question: info.question,
                        answerCollection: info.answerCollection,
                        active: info.active,
                    }
                    if (info.questionFormula && info.questionFormula.length > 0) {
                        data.questionFormula = info.questionFormula
                    }
                    if (info.questionImage && info.questionImage.length > 0) {
                        data.questionImage = info.questionImage
                    }
                    if (info.questionType) {
                        data.questionType = info.questionType
                    }
                    if (info.explanationVideo.length > 0) {
                        data.explanationVideo = info.explanationVideo
                    }
                    let question = new QuizQuestionModel(data)
                    question.save()
                    QuizEdit.push(question)
                    answersArray.push(answerId)
                }
            }
            // console.log("toAdd", toAddElements)
            // console.log("toModify", toModifyElements)

            for (let i = 0; i < quizOk.questionCollection.length; i += 1) {
                let index = body.quiz.findIndex(element => JSON.stringify(element._id) == JSON.stringify(quizOk.questionCollection[i]))
                if (index < 0) {
                    console.log(quizOk.questionCollection[i])
                    toRemoveElements.push(i)
                    await QuizQuestionModel.deleteOne({
                        _id: quizOk.questionCollection[i]
                    })
                    answersArray.splice(i, 1)
                }
            }
            answersArray = answersArray.sort((a, b) => a.order - b.order)
            quiz.questionCollection = answersArray
        }


        quiz.updatedBy = user
        const result = await QuizModel
            .findOneAndUpdate({
                _id: idQuizz
            }, {
                $unset: unset,
                $set: quiz
            }, {
                new: true
            })
            .then(quiz => {
                quiz.questionCollection = QuizEdit
                return {
                    success: true,
                    quiz
                }
            })
            .catch(err => {
                console.error('- Error trying to update a Quiz', JSON.stringify(err, null, 2))
                return {
                    success: false,
                    error: {
                      httpCode: 500,
                      message: '- DB Error trying to update a Quiz'
                    }
                  }
            })

        return result

    } catch (err) {
        const errorExt = errMsg
          .generalUpdate('quiz')
        return {
          success: false,
          error: errorExt
        }
      }
    },

    get: async ({ skip, limit }) => {
        try{
        const quiz = await QuizModel
            .find({})
            .populate({ path: 'questionCollection'})
            .limit(limit)
            .skip(skip)
            .then(quiz => {

               if(quiz.length>0){
                return {
                    success: true,
                    quiz
                }}


            })
            .catch(err => {
                console.error('- Error trying to get all Quiz.', JSON.stringify(err, null, 2))
                return {
                    success: false,
                    error: {
                      httpCode: 500,
                      message: '- DB Error trying to get all quiz'
                    }
                  }
            })

        if ('success' in quiz && !quiz.success) {
            // Return error
            return quiz
        }

        return {
            success: true,
            quiz
        }

    } catch (err) {
        const errorExt = errMsg
          .generalGet('quiz')
        return {
          success: false,
          error: errorExt
        }
      }
    },


    getById: async id => {
        const quiz = await QuizModel.findOne({
            _id: id
        })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'explanationVideo'
                }
            })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'questionImage'
                }
            })
        if (!quiz) {
            const errorExt = errMsg
                .getNotEntityFound('Quiz', id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        for (let i = 0; i < quiz.questionCollection.length; i++) {
            for (let j = 0; j < quiz.questionCollection[i].answerCollection.length; j++) {
                let answer = quiz.questionCollection[i].answerCollection[j]
                if (answer.answerImage) {
                    let image = await ImageModel.findOne({
                        _id: answer.answerImage
                    })
                    quiz.questionCollection[i].answerCollection[j].answerImage = image
                }
            }
        }
        const quizSort = quiz.questionCollection.sort((a, b) => a.order - b.order)
        let quizCopy = {...quiz._doc}
        quizCopy.questionCollection=quizSort
        return {
            success: true,
            quiz:quizCopy
        }
    },

    getVideosById: async id => {
        const videos = await VideosModel.find({
            quizParent: id
        })
        if (!videos) {
            const errorExt = errMsg
                .getNotEntityFound('Videos in Quiz id ', id)
            return {
                success: false,
                code: 404,
                error: errorExt.error,
                errorExt
            }
        }
        return {
            success: true,
            videos
        }

    },


    delete: async id => {

        try{
        const quiz = await QuizModel.findOne({
            _id: id
        })
        if (!quiz) {
            const errorExt = errMsg
                .getNotEntityFound('Quiz', id)
                return {
                    success: false,
                    error: errorExt
                  }
        }
        if (quiz.parent) {
            PlaylistObject.getAllPlaylistId(quiz.parent).then(
                async (value) => {
                    let result = await PlaylistModel.updateMany(
                        { _id: { $in: value } },
                        { $inc: { totalItems: -1 } })
                    console.log(result)
                }
            )
            PlaylistObject.updateDeleteItem(quiz.parent, quiz._id).then(
                value => {
                    console.log("DELETES", value)
                }
            )
        }
        await quiz.remove()
        return {
            success: true,
            deleted: true
        }
    } catch (err) {
        const errorExt = errMsg
          .generalDelete('quiz')
        return {
          success: false,
          error: errorExt
        }
      }
    },

    // This service is only to get free quizz information to the user BE CAREFUL WITH VALIDATIONS.

    getInfoFreeQuiz : async (idQuiz)=>{
        const quizAttempt = await QuizModel.findOne({
            _id: idQuiz
        })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'explanationVideo'
                }
            })
            .populate({
                path: 'questionCollection',
                populate: {
                    path: 'questionImage'
                }
            })
        if (!quizAttempt) {
            return {
                success: false,
                code: 404,
                error: `Quiz not found: ${idQuiz}`
            }
        }
        let questionCollection = []
        if (quizAttempt.random) {
            questionCollection = _.shuffle(quizAttempt.questionCollection)
        } else {
            questionCollection = quizAttempt.questionCollection.sort((a, b) => a.order - b.order)
        }
        for (let i = 0; i < quizAttempt.questionCollection.length; i++) {
            for (let j = 0; j < quizAttempt.questionCollection[i].answerCollection.length; j++) {
                let answer = quizAttempt.questionCollection[i].answerCollection[j]
                if (answer.answerImage) {
                    let image = await ImageModel.findOne({
                        _id: answer.answerImage
                    })
                    quizAttempt.questionCollection[i].answerCollection[j].answerImage = image
                }
            }
        }
        const quizAttemptNew = {
            quizParent: idQuiz,
            date: new Date(),
            status: "OPEN",
            questionCollection,
        }
        let quizAttemptcopy = quizAttemptNew
        quizAttemptcopy.quizParent = {
            _id: quizAttempt._id,
            limitTime: quizAttempt.limitTime,
            active: quizAttempt.active,
            parent: quizAttempt.parent,
            quizType: quizAttempt.quizType,
            name: quizAttempt.name
        }
        if (quizAttemptcopy.quizParent.limitTime !== undefined) {
            let start = moment(quizAttemptcopy.date)
            let end = start.add(quizAttemptcopy.quizParent.limitTime, 's')
            let diff = end.diff(moment(), 's')
            quizAttemptcopy.availableTime = diff
            quizAttemptcopy.limitTime = quizAttemptcopy.quizParent.limitTime
        }
        return {
            success: true,
            quizAttempt: quizAttemptcopy
        }
    }
}


module.exports = Quiz;