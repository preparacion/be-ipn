/**
 * models/materials.js
 *
 * @description :: Describes the materials functions
 * @docs        :: TODO
 */
const MaterialsModel = require('../db/materials')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Materials = {
  /** get all the materials */
  get: async ({ skip, limit } = {}) => {

    try{
    const materials = await MaterialsModel
      .find({})
      .skip(skip)
      .limit(limit)
    return {
      success: true,
      materials
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('materials')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /** create a new material */
  create: async (data, userId) => {

try{
    if (userId) {
      data.uploadBy = userId
    }
    const material = new MaterialsModel(data)
    await material.save()
    return {
      success: true,
      material
    }
  } catch (err) {
    const errorExt = errMsg
      .generalCreate('materials')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /** get a single material by id */
  getById: async (id) => {

  }
}

module.exports = Materials
