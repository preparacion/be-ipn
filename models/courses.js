/**
 * models/courses.js
 *
 * @description :: Describes the courses functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const moment = require('moment')

const LocationModel = require('../db/locations')
const GroupModel = require('../db/groups')
const ClassModel = require('../db/classRooms')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const PaymentModel = require('../db/payments')
const PlaylistModel = require('../db/playlists')
const StudentListModel = require('../db/studentLists')
const CourseTopicModel = require('../db/courseTopics')
const CommentsModel = require('../db/comments')

const Groups = require('./groups')
const Playlist = require('./playlists')
const StudentList = require('./studentLists')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Courses = {
  getAvailableGroups: async (Course) => {
    const groups = await GroupModel
      .find({
        course: Course._id,
        $or: [
          {
            deleted: { $exists: false }
          },
          {
            deleted: { $eq: false }
          }
        ]
      })

    const data = {
      _id: Course._id,
      name: Course.name,
      dependency: Course.dependency,
      duration: Course.duration,
      oldId: Course.oldId,
      price: Course.price,
      active: Course.active,
      creationDate: Course.creationDate,
      type: Course.type,
      groups: groups.length,
      courseLevel: Course.courseLevel || null,
      courseType: Course.courseType || null,
      stage: Course.stage
    }

    return data
  },
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({ skip, limit } = {}) => {

    try{
    const courses = await CourseModel.find()
      .populate("topicList")
      .populate('image')
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get all courses', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all courses'
          }
        }
      })

    if (courses.success) {
      const result = await Promise
        .all(courses.courses.map(course => course.getChildren()))
        .then(result => {
          return {
            success: true,
            courses: result
          }
        })
        .catch(err => {
          console.error('- Error trying to get the number of groups ', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to get the number of groups'
            }
          }
        })

      return result
    }
    return courses

  } catch (err) {
    const errorExt = errMsg
      .generalGet('courses')
    return {
      success: false,
      error: errorExt
    }

  }

  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Course id.
   */
  getById: async id => {
    const course = await CourseModel.findOne({ _id: id })
      .populate('image')
      .then(course => {
        if (!course) {
          return {
            success: false,
            code: 404,
            error: `Course not found: ${id}}`
          }
        }
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to get a course by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return course
  },

  addChildrenAttr: async (courseId) => {
    return CourseModel
      .findOneAndUpdate({
        _id: courseId
      }, {
        $set: {
          isChildren: true
        }
      }, {
        upsert: false
      })
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data - List of attributes.
   */
  create: async data => {
try{

    if (data.children && data.children.length) {
      data.isParent = true
      await Promise
        .all(data.children.map(courseId => {
          return Courses.addChildrenAttr(courseId)
        }))
    }

    const course = new CourseModel(data)
    const result = await course.save()
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to create a course', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a course'
          }
        }
      })

    return result
  } catch (err) {
    const errorExt = errMsg
      .generalCreate('courses')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {

    try{
    const course = await Courses.getById(id)

    if (!course.success) {
      const errorExt = errMsg
      .getNotEntityFound('course', id)
    return {
      success: false,
      error: errorExt
    }
    }
    let unset = {}
    if (data.image == undefined) {
      unset.image = "";
    }
    const result = await CourseModel
      .findOneAndUpdate({
        _id: id
      }, {
        $unset: unset,
        $set: data
      }, {
        new: true
      })
      .populate("topicList")
      .populate('image')
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to update a course', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a course'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('courses')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  delete: async id => {

    try{
    const course = await CourseModel
      .findOne({ _id: id })
    if (!course) {
      const errorExt = errMsg
        .getNotEntityFound('Course', id)
        return {
          success: false,
          error: errorExt
        }
    }

    const groups = await GroupModel
      .find({ course: id })
      .select('studentList')

    const studentLists = await Promise
      .all(groups.map(group => {
        return StudentListModel
          .findOne({ _id: group.studentList })
      }))
      .then(pls => pls.filter(pl => pl))
      .then(pls => {
        return pls.map(pl => pl._id)
      })

    const playlists = await PlaylistModel
      .find({ course: id })
      .select('_id')
      .then(playLists => {
        playLists.map(playlist => playlist._id)
      })

    if (playlists && playlists.length) {
      await Promise.all(playlists.map(playListId => {
        return Playlist.delete(playListId)
      }))
    }

    if (studentLists && studentLists.length) {
      await Promise.all(studentLists.map(stListId => {
        return StudentList.delete(stListId)
      }))
    }

    if (groups && groups.length) {
      await Promise.all(groups.map(groupId => {
        return Groups.delete(groupId)
      }))
    }

    await course.remove()

    return {
      success: true,
      message: 'delete'
   }

  } catch (err) {
    const errorExt = errMsg
      .generalDelete('courses')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  getByType: async type => {
    const courses = await CourseModel
      .find({
        courseType: type
      })
      .populate('image')
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get courses by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  getByLevel: async level => {
    const courses = await CourseModel
      .find({
        courseLevel: level
      })
      .populate('image')
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get courses by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  getGroups: async courseId => {
    const groups = await GroupModel
      .find({
        isBucket: { $ne: true },
        course: courseId,
        $or: [
          {
            deleted: { $exists: false }
          },
          {
            deleted: { $eq: false }
          }
        ]
      })
      .populate('image')
    const groupsPopulated = await Promise
      .all(groups.map(group => group.toPopulate()))
      .then(groups => {
        return {
          success: true,
          groups
        }
      })
      .catch(err => {
        console.error('- Error trying to get groups by course id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return groupsPopulated
  },

  getByDependency: async dependency => {
    const courses = await CourseModel
      .find({
        dependency: dependency
      })
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get courses by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return courses
  },

  // just for migration
  getByOldId: async oldId => {
    const course = await CourseModel
      .findOne({
        oldId: oldId
      })
      .then(course => {
        return {
          success: true,
          course
        }
      })
      .catch(err => {
        console.error('- Error trying to get all courses', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return course
  },

  getStudents: async (id, skip, limit) => {
    const studentListIds = await GroupModel
      .find({ course: id })
      .select('studentList')
      .then(groups => groups.map(group => group.studentList))
      .catch(err => {
        console.error('- Error trying to get all groups', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in studentListIds && !studentListIds.success) {
      return studentListIds // return error
    }

    let studentIds = await StudentListModel
      .find({ _id: { $in: studentListIds } })
      .select('list')
      .then(studentLists => {
        let ids = []
        studentLists.forEach(st => {
          for (let i = 0; i < st.list.length; i++) {
            const id = st.list[i]
            ids.push(id)
          }
        })
        return ids
      })
      .catch(err => {
        console.error('- Error trying to get all studentIds', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in studentIds && !studentIds.success) {
      return studentIds
    }

    studentIds = _.uniq(studentIds)

    studentIds = ((parseInt(skip) + parseInt(limit)) < studentIds.length) ? studentIds.slice(parseInt(skip), parseInt(limit)) : studentIds.slice(parseInt(skip))

    const st = await StudentModel
      .find({
        _id: { $in: studentIds }
      })
      .catch(err => {
        console.error('- Error trying to get all studentIds', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in st && !st.success) {
      return st
    }

    let students = await Promise.all(st.map(student => student.toBalance()))

    students = students.filter(student => {
      return student.balance.payments.total < 2000
    })

    return {
      success: true,
      students
    }
  },

  getWithChildren: async ({ skip, limit } = {}) => {
    const result = await CourseModel.find({})
      .skip(skip)
      .limit(limit)
      .then(courses => {
        return Promise.all(
          courses.map(course => course.getChildren())
        )
      })
      .then(courses => {
        return {
          success: true,
          courses
        }
      })
      .catch(err => {
        console.error('- Error trying to get all courses', JSON.stringify(err, null))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  getStudentDiscounts: async (id, studentId) => {
    const course = await CourseModel
      .findOne({ _id: id })
    if (!course) {
      const errorExt = errMsg
        .getNotEntityFound('Course', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const student = await StudentModel
      .findOne({ _id: studentId })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', studentId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const courses = await student.getCourses()
    const discounts = []

    for (let i = 0; i < course.discounts.length; i++) {
      const element = course.discounts[i]
      for (let j = 0; j < courses.length; j++) {
        if (String(courses[j]) === String(element.course)) {
          const hasDebt = await Courses.hasDebt(student._id, courses[j])
          if (!hasDebt) {
            discounts.push(element)
          }
        }
      }
    }

    return {
      success: true,
      discounts,
      course
    }
  },

  async hasDebt(studentId, courseId) {
    const payments = await PaymentModel
      .find({
        student: studentId,
        course: courseId
      })
    if (payments.length) {
      const course = await CourseModel
        .findOne({
          _id: courseId
        })
        .select('price')
      if (!course) {
        return false
      }
      let total = 0
      payments.forEach(payment => {
        total += payment.amount
      })
      return parseFloat(total) < parseFloat(course.price)
    }
    return false
  },

  report: async (id, sd, ed, wd, dd, socket = undefined) => {
    if (wd == undefined) {
      wd = 7
    }
    if (dd == undefined) {
      dd = 3
    }
    const Course = await CourseModel
      .findOne({ _id: id })
    if (!Course) {
      const errorExt = errMsg
        .getNotEntityFound('Course', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const Groups = await GroupModel
      .find({
        course: Course._id,
        $or: [
          {
            deleted: { $exists: false }
          },
          {
            deleted: { $eq: false }
          }
        ]
      })
    const newG = []
    // Reporte generado en grupos iniciados 2 dias antes
    const GroupsInfo = []
    for (const x in Groups) {
      const date = sd;
      sd = moment(sd).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      if ((Groups[x].startDate >= sd) && (Groups[x].startDate <= ed)) {
        const STL = await StudentListModel
          .find({
            _id: Groups[x].studentList
          })

        const Comments = await CommentsModel
          .find({
            student: STL[0].list
          })
        confirmed = 0
        for (const n in Comments) {
          cadena = Comments[n].comment.toLowerCase()
          if (cadena.includes("confirm") && (String(Comments[n].group) === String(Groups[x]._id))) {
            confirmed = confirmed + 1
          }
        }

        let sede = ""
        if (Groups[x].classRoom === undefined) {
          sede = "Sin sede"
        } else {

          const Class = await ClassModel
            .find({
              _id: Groups[x].classRoom
            })

          const location = await LocationModel
            .find({
              _id: Class[0].location
            })

          sede = location[0].name
        }

        let color = ""
        let diaF = moment(date, "DD-MM-YYYY").add(wd, 'days');
        let dayF = moment(date, "DD-MM-YYYY").add(dd, 'days');

        const perRegistered = (STL[0].list.length) * 100 / Groups[x].quota
        const percentage = confirmed * 100 / Groups[x].quota

        if (percentage >= 100) { // DARK GREEN
          color = "#8BC34A"
        } else if (percentage >= 90 && percentage < 100) { // LIGHT GREEN
          color = "#82E0AA"
        }
        if ((perRegistered <= 50) && (Groups[x].startDate <= diaF)) { // YELLOW
          color = "#F7DC6F"
        }
        if (Groups[x].startDate <= dayF) {
          if (percentage == 0 && perRegistered == 0) { // RED
            color = "#EC7063"
          }
        }
        if (Groups[x].startDate >= diaF) { // GRAY
          color = "#616A6B"
        }

        const data = {
          id: Groups[x]._id,
          name: Groups[x].name,
          startDate: Groups[x].startDate,
          schedules: Groups[x].schedules,
          quota: Groups[x].quota,
          registered: STL[0].list.length,
          confirmed: confirmed,
          location: sede,
          percentage_registered: perRegistered,
          percentage_confirmed: percentage,
          status_color: color
        }
        newG.push(data)
      }
    }
    return {
      success: true,
      report: newG
    }
  },





  async createTopic(id, data) {
    const course = await CourseModel
      .findOne({ _id: id })
      .populate({
        path: "schedulesModulesByDay"
      })
      .populate({
        path: "topicList"
      })
    if (!course) {
      const errorExt = errMsg
        .getNotEntityFound('Course', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    let topic = new CourseTopicModel({ name: data.name, parent: id })
    topic = await topic.save()
    let topicList = course.topicList || []
    topicList.push(topic)
    course.topicList = topicList
    course.save()
    return ({
      success: true,
      topic,
      course
    })
  },
  async updateLive(id, data) {
    const course = await CourseModel
      .findOne({ _id: id })
    if (!course) {
      const errorExt = errMsg
        .getNotEntityFound('Course', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    course["liveStreamUrl"] = data.url
    course["activeLiveStream"] = data.active
    course['liveStreamChannel'] = data.channel
    await course.save()
    return ({
      success: true,
      course
    })
  }

}

module.exports = Courses
