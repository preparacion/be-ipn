/**
 * models/register.js
 *
 * @description :: Describes the register functions
 * @docs        :: TODO
 */
const bcrypt = require('bcryptjs')

const StudentModel = require('../db/students')

const Students = require('./students')

const Register = {
  register: async data => {
    const previousStudent = await Students.findByEmail(data.email)

    if (previousStudent.success) {
      return {
        success: false,
        code: 400,
        error: `An student have been register with this email: ${data.email}`
      }
    }

    if (data.password) {
      data.hashed_password = await bcrypt.hash(data.password, 10)
      delete data.password
    }

    // create fields for searching
    if (data['name']) {
      data['name_lower'] = data['name'].toLowerCase()
    }

    if (data['email']) {
      data['email_lower'] = data['email'].toLowerCase()
    }

    if (data['lastName']) {
      data['lastName_lower'] = data['lastName'].toLowerCase()
    }

    if (data['name'] && data['lastName']) {
      data['fullName_lower'] = `${data['name']} ${data['lastName']}`
    }

    const student = new StudentModel(data)
    const studentResult = await student.save()

    return {
      success: true,
      student: studentResult
    }
  }
}

module.exports = Register
