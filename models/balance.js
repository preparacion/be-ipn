/**
 * models/balance.js
 *
 * @description :: Describes the balance functions
 * @docs        :: TODO
 */
const StudentModel = require('../db/students')
const PaymentModel = require('../db/payments')

const Balance = {
  getStudents: async ({ skip, limit } = {}) => {
    const students = await StudentModel
      .find()
      .skip(skip)
      .limit(limit)
      .catch(err => {
        console.error('- Error trying to get students ', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in students && !students.success) {
      return students
    }

    const st = await Promise.all(students.map(student => student.toBalance()))

    return {
      success: true,
      students: st
    }
  },

  getPayments: async ({ skip, limit } = {}) => {
    const payments = await PaymentModel.aggregate([
      {
        $group: {
          _id: {
            student: { '$student': '$student' }
          }
        }
      }
    ])

    return {
      success: true,
      payments
    }
  }
}

module.exports = Balance
