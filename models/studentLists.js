/**
 * models/studentLists.js
 *
 * @description :: Describes the studentLists functions
 * @docs        :: TODO
 */
const StudentListModel = require('../db/studentLists')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const StudentLists = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async () => {

    try{
    const studentLists = await StudentListModel
      .find({})
      .then(studentLists => {
        return {
          success: true,
          studentLists
        }
      })
      .catch(err => {
        console.error('- Error trying to get all studentLists.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all studentLists'
          }
        }
      })
    return studentLists


  } catch (err) {
    const errorExt = errMsg
      .generalGet('studentLists')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - StudentList id.
   */
  getById: async id => {
    const studentList = await StudentListModel
      .findOne({ _id: id })
      .then(studentList => {
        if (studentList) {
          return {
            success: true,
            studentList
          }
        }

        return {
          success: false,
          code: 404,
          error: `Student list not found ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a studentList by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return studentList
  },

  /**
   * create
   *
   * @description: Create a new Item.
   * @param {object} data - List of attributes.
   */
  create: async data => {

    try{
    const studentList = new StudentListModel(data)
    const result = await studentList.save()
      .then(studentList => {
        return {
          success: true,
          studentList
        }
      })
      .catch(err => {
        console.error('- Error trying to create a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a studentList'
          }
        }
      })

    return result



  } catch (err) {
    const errorExt = errMsg
      .generalCreate('studentLists')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * update
   *
   * @description: update an item.
   * @param {string} id - Item id
   * @param {object} data - List of attributes.
   */
  update: async (id, data) => {

    try{

    const studentList = await StudentLists.getById(id)

    if (!studentList.success) {
      const errorExt = errMsg
        .getNotEntityFound('StudentLists', id)
        return {
          success: false,
          error: errorExt
        }
    }

    const result = await StudentListModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        new: true
      })
      .then(studentList => {
        return {
          success: true,
          studentList
        }
      })
      .catch(err => {
        console.error('- Error trying to update a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a studentList'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('studentLists')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Location id.
   */
  delete: async id => {
try{

    const result = await StudentListModel.deleteOne({ _id: id })
      .then((ress) => {

        if(ress.n>0){
          return {
            success: true,
            deleted: true
          }
        }else{
          const errorExt = errMsg
          .getNotEntityFound('studentList', id)
        return {
          success: false,
          error: errorExt
        }

        }


      })
      .catch(err => {
        console.error('- Error trying to delete a studentList', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to delete a studentList'
          }
        }
      })

    return result


  } catch (err) {
    const errorExt = errMsg
      .generalDelete('studentLists')
    return {
      success: false,
      error: errorExt
    }
  }
  }
}

module.exports = StudentLists
