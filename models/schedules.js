/**
 * models/schedules.js
 *
 * @description :: Describes the schedules functions
 * @docs        :: TODO
 */
const _ = require('lodash')
const moment = require('moment')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const ScheduleModel = require('../db/schedules')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const days = ['l', 'm', 'w', 'j', 'v', 's', 'd']

const Schedules = {
  /**
   * get
   *
   * @description: Returns all the items
   */
  get: async ({ skip, limit } = {}) => {
    try{
    const schedules = await ScheduleModel
      .find({})
      .limit(limit)
      .skip(skip)
      .then(schedules => {
        return {
          success: true,
          schedules
        }
      })
      .catch(err => {
        console.error('- Error trying to get all schedules.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all schedules'
          }
        }
      })
    return schedules

  } catch (err) {
    const errorExt = errMsg
      .generalGet('schedules')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id - Schedule id.
   */
  getById: async id => {
    const schedule = await ScheduleModel
      .findOne({ _id: id })
      .then(schedule => {
        if (!schedule) {
          const errorExt = errMsg
            .getNotEntityFound('Schedule', id)
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
        return {
          success: true,
          schedule
        }
      })
      .catch(err => {
        console.error('- Error trying to get a schedule by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return schedule
  },

  /**
   * create
   *
   * @description: Create a new Schedule.
   * @param {object} data - List of attributes.
   */
  create: async data => {
    try{

    const schedule = new ScheduleModel(data)
    const result = await schedule.save()
      .then(schedule => {
        return {
          success: true,
          schedule
        }
      })
      .catch(err => {
        console.error('- Error trying to create a schedule', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a schedule'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('schedules')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  update: (id, data) => {
    try{
    return ScheduleModel.findOneAndUpdate({
      _id: id
    }, {
      $set: data
    }, {
      upsert: false,
      returnNewDocument: true
    })
      .then(schedule => {
        if (!schedule) {
          const errorExt = errMsg
            .getNotEntityFound('Schedule', id)
            return {
              success: false,
              error: errorExt
            }
        }
        return {
          success: true,
          schedule
        }
      })
      .catch(err => {
        console.error('- Error trying to update a schedule', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a schedule'
          }
        }
      })

    } catch (err) {
      const errorExt = errMsg
        .generalUpdate('schedules')
      return {
        success: false,
        error: errorExt
      }
    }
  },

  /**
   * delete
   *
   * @description: Removes a single item by id.
   * @param {string} id - Schedule id.
   */
  delete: async id => {
    try{

    
    const result = await ScheduleModel.deleteOne({ _id: id })
      .then((ress) => {

        if(ress.n>0){
          return {
            success: true,
            deleted: true
          }
        }else{
          const errorExt = errMsg
          .getNotEntityFound('schedules', id)
        return {
          success: false,
          error: errorExt
        }

        }
      })
      .catch(err => {
        console.error('- Error trying to delete a schedule', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to delete a schedule'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalDelete('schedules')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  getByOldId: async oldId => {
    const schedule = await ScheduleModel
      .findOne({
        oldId: oldId
      })
      .then(schedule => {
        if (schedule) {
          return {
            success: true,
            schedule
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Schedule not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a schedule by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return schedule
  },

  /**
   * hasDaysInRange
   * @param {date} startDate Start date from schedule proposal
   * @param {date} endDate End date from schedule proposal
   * @param {array} scheduleArray List of schedules to iterate
   * @returns {boolean} If there is a day in the list of schedules into de range
   */
  _hasDaysInRange: (startDate, endDate, scheduleArray) => {
    if (!scheduleArray.length) {
      return false
    }
    const scheduleDays = []
    scheduleArray.forEach(schedule => {
      const day = Schedules._dayOffSet(days.indexOf(schedule.day))
      if (scheduleDays.indexOf(day) === -1) {
        scheduleDays.push(day)
      }
    })

    const stDate = moment(startDate).set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 })
    const edDate = moment(endDate).set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 })

    if (edDate < stDate) {
      return false
    }

    let currentDate = stDate
    while (currentDate <= edDate) {
      if (scheduleDays.indexOf(currentDate.day()) > -1) {
        return true
      }
      currentDate = currentDate.add(1, 'd')
    }
    return false
  },

  isAvailable: async (data) => {
    if (
      !Schedules
        ._hasDaysInRange(data.startDate, data.endDate, data.scheduleArray)
    ) {
      const errorExt = errMsg
        .getNoDaysInRange()
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const groupSelectFields = [
      'name',
      'course',
      'schedule',
      'schedules',
      'startDate',
      'endDate'
    ]

    const cursor = await GroupModel
      .find({
        classRoom: data.classRoomId,
        $or: [
          { deleted: { $exists: false } },
          { deleted: { $eq: false } }
        ],
        // endDate:{
        //   $gte: new Date(),
        //   $lt: new Date(moment(data.endDate).toISOString())
        // }
      })
      .select(groupSelectFields.join(' '))
      .cursor()
    let doc = await cursor.next()
    if (doc === null) {
      return {
        success: true,
        isAvailable: true
      }
    }

    const availables = []
    const dowInSA = [] // "days of week in schedule array"
    data.scheduleArray.forEach(schedule => {
      dowInSA.push(schedule.day)
    })

    do {
      const groupEndDate = doc.endDate
        ? doc.endDate : await Schedules
          ._getGroupEndDate(doc.startDate, doc.course)

      doc.schedules.forEach(docSchedule => {
        const dsDay = days[docSchedule.day]
        const overlaps = Schedules._rangeOverlaps(
          doc.startDate, groupEndDate,
          data.startDate, data.endDate
        )
        if (
          dowInSA.indexOf(dsDay) === -1 ||
          !overlaps
        ) {
          return
        }

        const docScheduleDays = Schedules._getAllDaysInRange(
          docSchedule.day,
          doc.startDate,
          groupEndDate
        )
        const dsStartHour = Schedules._getFixedHour(docSchedule.startHour)
        const dsEndHour = Schedules._getFixedHour(docSchedule.endHour)

        data.scheduleArray.forEach(schedule => {
          const scheduleDays = Schedules._getAllDaysInRange(
            schedule.day,
            data.startDate,
            data.endDate
          )

          const scStartHour = Schedules._getFixedHour(schedule.startHour)
          const scEndHour = Schedules._getFixedHour(schedule.endHour)

          for (let i = 0; i < docScheduleDays.length; i++) {
            const dsStart = moment.utc(docScheduleDays[i])
              .hour(parseInt(dsStartHour[0]))
              .minute(parseInt(dsStartHour[1]))
            const dsEnd = moment.utc(docScheduleDays[i])
              .hour(parseInt(dsEndHour[0]))
              .minute(parseInt(dsEndHour[1]))
            for (let j = 0; j < scheduleDays.length; j++) {
              const scStart = moment.utc(scheduleDays[j])
                .hour(parseInt(scStartHour[0]))
                .minute(parseInt(scStartHour[1]))
              const scEnd = moment.utc(scheduleDays[j])
                .hour(parseInt(scEndHour[0]))
                .minute(parseInt(scEndHour[1]))

              if (
                Schedules._rangeOverlaps(
                  dsStart, dsEnd,
                  scStart, scEnd
                )
              ) {
                availables.push(doc._id)
              }
            }
          }
        })
      })

      doc = await cursor.next()
    } while (doc !== null)

    const groups = await Promise.all(
      _.uniq(availables, '_id').map(id => {
        return Schedules._fillGroup(id)
      })
    ).then(groups => groups)

    return {
      success: true,
      isAvailable: availables.length === 0,
      groups
    }
  },

  _fillGroup: async (id) => {
    const group = await GroupModel.findOne({
      _id: id
    })
      .then(group => group.toPopulate())

    return group
  },

  /**
   * _rangeOverlaps
   * @param {date} grStart First date start
   * @param {date} grEnd First date end
   * @param {date} scStart Second date start
   * @param {date} scEnd Second date end
   * @returns {boolean} true if the two dates overlaps
   */
  _rangeOverlaps: (
    grStart, grEnd, // A
    scStart, scEnd // B
  ) => {
    const aStart = moment.utc(grStart)
    const aEnd = moment.utc(grEnd)

    const bStart = moment.utc(scStart)
    const bEnd = moment.utc(scEnd)

    if (aStart <= bStart && bStart <= aEnd) {
      return true // b starts in a
    }
    if (aStart <= bEnd && bEnd <= aEnd) {
      return true // b ends in a
    }
    if (bStart < aStart && aEnd < bEnd) {
      return true // a in b
    }
    return false
  },

  /**
   * _getFixedHour
   * @example let str = '0345'
   *          Schedule.__getFixedHour(str) // ['03', '45']
   * @param {string} str Hour string from schedules array (startHour, endHour)
   * @return {array} The string hour fixed and stored in an array.
   */
  _getFixedHour: (str) => {
    let strHour = str.split('')
    if (strHour.length < 4) {
      strHour.unshift('0')
    }

    return [
      `${strHour[0]}${strHour[1]}`,
      `${strHour[2]}${strHour[3]}`
    ]
  },

  /**
   * _getAllDaysInRange
   * @param {string} day The day of the week
   * @param {date} startDate Range start date
   * @param {date} endDate Range end date
   * @returns {array} List of days (dates) in the given range
   */
  _getAllDaysInRange: (day, startDate, endDate) => {
    const dayNumber = (typeof day === 'string')
      ? Schedules._dayOffSet(days.indexOf(day)) : Schedules._dayOffSet(day)

    const result = []
    let nextDay = moment.utc(startDate).day(dayNumber)
    let sumDay = dayNumber

    while (nextDay <= endDate) {
      result.push(nextDay.format())
      nextDay = nextDay.day(sumDay + 7)
    }

    return result
  },

  /**
   * _dayOffSet
   * @param {integer} numberOfDay Number of the day in day list
   * @returns {integer} Number of the day in moment format
   */
  _dayOffSet: (numberOfDay) => {
    return (numberOfDay + 1 >= 7) ? 0 : numberOfDay + 1
  },

  /**
   * _getGroupEndDate
   * @param {date} startDate Group start date
   * @param {string} courseId Course object id
   * @returns {date} Group end date
   */
  _getGroupEndDate: async (startDate, courseId) => {
    const course = await CourseModel
      .findOne({ _id: courseId })
      .select('duration')
    const endDate = moment.utc(startDate).add(parseInt(course.duration), 'days')
    return endDate.format()
  }
}

module.exports = Schedules
