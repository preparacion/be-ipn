/**
 * models/locations.js
 *
 * @description :: Describes the locations functions
 * @docs        :: TODO
 */
const LocationModel = require('../db/locations')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Locations = {
  /**
   * get
   *
   * @description: Returns all the items.
   */
  get: async () => {

    try{
    const locations = await LocationModel
      .find({})
      .then(result => result)
      .catch(err => {
        console.error('- Error trying to get all locations.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- Error trying to get all locations'
          }
        }
      })
    return {
      success: true,
      locations
    }

  } catch (err) {
    const errorExt = errMsg
      .generalGet('locations')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const location = await LocationModel
      .findOne({ _id: id })
      .then(location => {
        if (location) {
          return {
            success: true,
            location
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Location not found: ${id}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a location by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return location
  },

  // Only used in migration process
  getByOldId: async oldId => {
    const location = await LocationModel
      .findOne({
        oldId: oldId
      })
      .then(location => {
        if (location) {
          return {
            success: true,
            location
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `Location not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a location by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return location
  },

  /**
   * create
   *
   * @description: create a new item.
   * @param {object} data Collection of attributes
   */
  create: async (data) => {

try{

    const location = new LocationModel(data)
    const result = await location
      .save()
      .then(location => {
        return location
      })
      .catch(err => {
        console.error('- Error trying to create a location', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to create a location'
          }
        }
      })

    return {
      success: true,
      location: result
    }

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('locations')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  update: async (id, data) => {

    try{
    const location = await LocationModel.findOne({ _id: id })
      .then(location => location)

    if (!location) {
      const errorExt = errMsg
      .getNotEntityFound('location', id)
    return {
      success: false,
      error: errorExt
    }
    }

    const result = await LocationModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        new: true
      })
      .then(location => {
        return {
          success: true,
          location
        }
      })
      .catch(err => {
        console.error('- Error trying to update a location', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a location'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('locations')
    return {
      success: false,
      error: errorExt
    }

  }
  },

  delete: async id => {

    {

      try{
        const result = await LocationModel.deleteOne({ _id: id })

      if (result.n == 0) {
        const errorExt = errMsg
          .getNotEntityFound('Location', id)
          return {
            success: false,
            error: errorExt
          }
      }


      return {
        success: true,
        deleted: true
      }
  
      
    } catch (err) {
      const errorExt = errMsg
        .generalDelete('stages')
      return {
        success: false,
        error: errorExt
      }
    }
    }
  }
}

module.exports = Locations
