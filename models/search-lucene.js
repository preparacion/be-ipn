// models/search.js
const StudentModel = require('../db/students');

const Search = {
  buildSortField: (sortField, asc = true) => {
    const dic = {
      lastName: 'lastName',
      lastName_lower: 'lastName_lower',
      secondLastName: 'secondLastName',
      secondLastName_lower: 'secondLastName_lower',
      fullName_lower: 'fullName_lower',
      email: 'email',
      email_lower: 'email_lower',
      phoneNumber: 'phoneNumber',
      secondPhoneNumber: 'secondPhoneNumber',
      registerDate: 'registerDate',
      payments: 'balance.payments.total',
      costs: 'balance.costs.total'
    };
    
    return sortField && dic[sortField] ? 
      { [dic[sortField]]: asc ? 1 : -1 } : 
      { 'lastName_lower': asc ? 1 : -1 };
  },

  isEmail: (query) => {
    return query.includes('@') || /^[a-zA-Z0-9._%+-]+$/.test(query);
  },

  isPhoneNumber: (query) => {
    const cleaned = query.replace(/\D/g, '');
    return cleaned.length >= 5;
  },

  detectSearchType: (query) => {
    const parts = query.trim().split(/\s+/);
    const searchTypes = [];

    // Verifica si es un email
    if (query.includes('@')) {
      searchTypes.push('fullEmail');
    } else if (Search.isEmail(query)) {
      searchTypes.push('partialEmail');
    }

    // Verifica si es un teléfono
    if (Search.isPhoneNumber(query)) {
      searchTypes.push('phone');
    }

    // Verifica los diferentes tipos de búsqueda por nombre
    if (parts.length >= 2) {
      searchTypes.push('nameParts');
      
      // Casos específicos basados en el número de palabras
      if (parts.length === 2) {
        searchTypes.push('nameAndLastName');
        searchTypes.push('lastNameAndSecondLastName');
      } else if (parts.length === 3) {
        searchTypes.push('fullName');
      }
    }

    // Si no se detectó ningún tipo específico
    if (searchTypes.length === 0) {
      searchTypes.push('singleTerm');
    }

    return searchTypes;
  },

  buildSearchQuery: (searchType, searchTerm) => {
    const terms = searchTerm.toLowerCase().trim().split(/\s+/);

    switch (searchType) {
      case 'fullEmail':
        return {
          text: {
            query: searchTerm,
            path: ["email", "email_lower"]
          }
        };

      case 'partialEmail':
        return {
          text: {
            query: searchTerm,
            path: ["email", "email_lower"],
            fuzzy: {
              maxEdits: 1,
              prefixLength: 3
            }
          }
        };

      case 'phone':
        return {
          text: {
            query: searchTerm.replace(/\D/g, ''),
            path: ["phoneNumber", "secondPhoneNumber"]
          }
        };

      case 'nameAndLastName':
        return {
          compound: {
            must: [
              {
                text: {
                  query: terms[0],
                  path: "name_lower"
                }
              },
              {
                text: {
                  query: terms[1],
                  path: "lastName_lower"
                }
              }
            ]
          }
        };

      case 'lastNameAndSecondLastName':
        return {
          compound: {
            must: [
              {
                text: {
                  query: terms[0],
                  path: "lastName_lower"
                }
              },
              {
                text: {
                  query: terms[1],
                  path: "secondLastName_lower"
                }
              }
            ]
          }
        };

      case 'fullName':
        return {
          compound: {
            must: [
              {
                text: {
                  query: terms[0],
                  path: "name_lower"
                }
              },
              {
                text: {
                  query: terms[1],
                  path: "lastName_lower"
                }
              },
              {
                text: {
                  query: terms[2],
                  path: "secondLastName_lower"
                }
              }
            ]
          }
        };

      case 'nameParts':
        return {
          compound: {
            should: [
              {
                text: {
                  query: searchTerm,
                  path: ["fullName_lower", "firstLastName_lower"],
                  score: { boost: { value: 2 } }
                }
              },
              {
                compound: {
                  must: terms.map(term => ({
                    text: {
                      query: term,
                      path: ["name_lower", "lastName_lower", "secondLastName_lower"],
                      fuzzy: {
                        maxEdits: 1,
                        prefixLength: 2
                      }
                    }
                  }))
                }
              }
            ]
          }
        };

      default:
        return {
          text: {
            query: searchTerm,
            path: ["name_lower", "lastName_lower", "secondLastName_lower", "email_lower"],
            fuzzy: {
              maxEdits: 1,
              prefixLength: 2
            }
          }
        };
    }
  },

  buildPipeline: (queryTerm, sort, asc, skip, limit) => {
    const searchTypes = Search.detectSearchType(queryTerm);
    let pipeline = [];

    if (searchTypes.includes('fullEmail')) {
      pipeline = [
        {
          $match: {
            email_lower: queryTerm.toLowerCase(),
            deleted: { $ne: true }
          }
        },
        { $limit: 1 }
      ];
    } else if (searchTypes.includes('phone')) {
      const cleanPhone = queryTerm.replace(/\D/g, '');
      pipeline = [
        {
          $match: {
            $or: [
              { phoneNumber: { $regex: `^${cleanPhone}` } },
              { secondPhoneNumber: { $regex: `^${cleanPhone}` } }
            ],
            deleted: { $ne: true }
          }
        }
      ];
    } else {
      // Construir búsquedas compuestas para cada tipo de búsqueda detectado
      const searchQueries = searchTypes.map(type => 
        Search.buildSearchQuery(type, queryTerm)
      );

      pipeline = [
        {
          $search: {
            index: "student-index",
            compound: {
              should: searchQueries,
              minimumShouldMatch: 1
            }
          }
        },
        {
          $match: {
            deleted: { $ne: true }
          }
        }
      ];
    }

    pipeline.push({
      $facet: {
        metadata: [{ $count: "total" }],
        data: [
          { 
            $project: {
              _id: 1,
              name: 1,
              lastName: 1,
              secondLastName: 1,
              email: 1,
              phoneNumber: 1,
              secondPhoneNumber: 1,
              balance: {
                payments: {
                  total: 1,
                  discounts: 1
                },
                costs: {
                  total: 1
                }
              },
              debt: 1,
              registerDate: 1,
              searchScore: { $meta: "searchScore" }
            } 
          },
          { $sort: { searchScore: -1, ...Search.buildSortField(sort, asc) } },
          { $skip: parseInt(skip) },
          { $limit: parseInt(limit) }
        ]
      }
    });

    return pipeline;
  },

  search: async ({ q = '', sort, asc = true, skip = 0, limit = 10 } = {}) => {
    try {
      const startTime = Date.now();
      const pipeline = Search.buildPipeline(q, sort, asc, skip, limit);

      const results = await StudentModel.aggregate(pipeline).exec();
      const endTime = Date.now();
      
      const totalResults = results[0].metadata[0]?.total || 0;
      const documents = results[0].data;

      return {
        success: true,
        result: {
          took: endTime - startTime,
          timed_out: false,
          _shards: {
            total: 1,
            successful: 1,
            skipped: 0,
            failed: 0
          },
          hits: {
            total: {
              value: totalResults,
              relation: "eq"
            },
            hits: documents.map(doc => ({
              _id: doc._id,
              name: doc.name,
              lastName: doc.lastName,
              secondLastName: doc.secondLastName,
              email: doc.email,
              phoneNumber: doc.phoneNumber,
              secondPhoneNumber: doc.secondPhoneNumber,
              registerDate: doc.registerDate,
              balance: doc.balance,
              debt: doc.debt,
              _score: doc.searchScore || 1
            }))
          }
        }
      };
    } catch (err) {
      console.error('Search error:', err);
      return {
        success: false,
        code: 500,
        error: err.message,
        details: JSON.stringify(err, null, 2)
      };
    }
  }
};

module.exports = Search;