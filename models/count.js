/**
 * models/students.js
 *
 * @description :: Describes the students functions
 * @docs        :: TODO
 */
const StudentCounter = require('../lib/studentCounter')

const CourseModel = require('../db/courses')

const workshops = [
  '5c965b7b5fb181b37dfb61dd',
  '5c965b8a5fb181b37dfb61de',
  '5c965b975fb181b37dfb61df',
  '5c966b7529aec7b69f78adad'
]

const Count = {
  getWorkshops: async () => {
    const result = await Promise
      .all(workshops.map(courseId => {
        return Count.getWorkshopByCourse(courseId)
      }))
      .then(results => {
        return results.map(result => {
          return {
            course: result.result.course,
            groups: result.result.groups,
            students: result.result.students
          }
        })
      })
    return {
      success: true,
      result
    }
  },

  getWorkshopByCourse: async courseId => {
    const result = await StudentCounter
      .getWorkshopByCourse(courseId)

    if ('success' in result && !result.success) {
      return result
    }
    return {
      success: true,
      result
    }
  },

  getComipemsStudents: async () => {
    const result = await StudentCounter
      .getComipemsStudents()
    return {
      success: true,
      result: {
        total: result
      }
    }
  },

  getSuperiorStudents: async () => {
    const result = await StudentCounter
      .getSuperiorStudents()
    return {
      success: true,
      result: {
        total: result
      }
    }
  },

  getCountByCourse: async id => {
    const course = await CourseModel
      .findOne({ _id: id })
      .select('_id')
    if (!course) {
      return {
        success: false,
        code: 404,
        error: `Course not found: ${id}`
      }
    }

    const result = await StudentCounter
      .getByCourse(id)
    return {
      success: true,
      result: {
        total: result
      }
    }
  },

  getDiff: async () => {
    const result = await StudentCounter
      .getDiffSimulacro()
    return {
      success: true,
      result
    }
  }
}

module.exports = Count
