/**
 * models/templates.js
 *
 * @description :: Describes the templates functions
 * @docs        :: TODO
 */
const rp = require('request-promise')

const { sendGridApi, sendGridToken } = require('../config')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Template = class Template {

  async getVersions(id, idVersion) {
    if (!id || !idVersion) {
      const errorExt = errMsg
        .getTemplateMissingParams()
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }

    const options = {
      method: 'GET',
      uri: `${sendGridApi}templates/${id}/versions/${idVersion}`,
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return {
          success: true,
          response
        }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return result
  }
}

module.exports = Template
