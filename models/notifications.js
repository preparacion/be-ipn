/**
 * models/notifications.js
 *
 * @description :: Describes the notifications functions
 * @docs        :: TODO
 */
const _ = require('lodash')

const searchHelper = require('../lib/searchEsHelper');
const moment = require('moment')

const GroupModel = require('../db/groups')
const TalksModel = require('../db/talks')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const UserTalkModel = require('../db/usersTalks')
const PaymentModel = require('../db/payments')
const StudentListModel = require('../db/studentLists')
const TalkStudentListModel = require('../db/talkStList')
const NotificationsModel = require('../db/notifications')

const { ObjectId } = require('mongodb');

const SMS = require('../lib/sms')
const SendGrid = require('../lib/sendGrid')

const Notification = class Notification {
  async send(data) {
    return {
      success: true,
      message: 'Send Notifications'
    }
  }

  async sendSocket(message, socket) {
    if (socket) {
      socket.to('students_online').emit("notification", {
        message: message.message,
        time: message.time,
        type: 1
      })
    }
    return {
      success: true,
      message: 'Send Notifications',
      data: message
    }
  }

  async getAll({
    skip,
    limit
  } = {}) {
    const result = await NotificationsModel
      .find({})
      .skip(skip)
      .limit(limit)
      .then(notifications => {
        return {
          success: true,
          notifications
        }
      })
      .catch(err => {
        console.error('- Error trying to get groups by type', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  }

  async sentToCourse(
    data,
    startGroupDate,
    endGroupDate,
    debt,
    normalize = false
  ) {
    const course = await CourseModel
      .findOne({
        _id: data.id
      })
    if (!course) {
      return {
        success: false,
        code: 404,
        error: `Course not found: ${data.id}`
      }
    }
    const groupFilter = {
      course: data.id
    }

    if (startGroupDate && endGroupDate) {
      groupFilter['startDate'] = {
        $gte: startGroupDate
      }
      groupFilter['endDate'] = {
        $lte: endGroupDate
      }
    }
    let notificationData = {}

    await GroupModel
      .find(groupFilter)
      .select('studentList')
      .then(groups => groups.map(group => group.studentList))
      .then(stIds => {
        return Promise
          .all(stIds.map(stId => {
            return StudentListModel
              .findOne({
                _id: stId
              })
              .select('list')
          }))
      })
      .then(lists => {
        const studentIds = []
        lists.forEach(stList => {
          stList.list.forEach(id => {
            studentIds.push(id)
          })
        })
        return _.uniq(studentIds)
      })
      .then(studentIds => {
        return this.sendToStudents({
          data: {
            sms: data.sms,
            email: data.email
          },
          students: studentIds,
          courseId: course._id,
          debt: debt,
          normalize: normalize
        })
      })
      .then(result => {
        console.log('--------------')
        console.log(result)
        console.log('--------------')
        notificationData = {
          sms: data.sms,
          email: data.email,
          to: result,
          date: new Date()
        }
        const notification = new NotificationsModel(notificationData)
        return notification.save()
      })

    return {
      success: true,
      message: 'sent',
      notificationData
    }
  }
  /*
   * This function will calculate if student hasDebt base on 
   * Student Id and course id .
   */
  async hasDebt(studentId, courseId) {
    const payments = await PaymentModel
      .find({
        student: studentId,
        course: courseId
      })
    if (payments.length) {
      const course = await CourseModel
        .findOne({
          _id: courseId
        })
        .select('price')
      if (!course) {
        return false
      }
      let total = 0
      payments.forEach(payment => {
        let discount = payment.discount !== undefined ? payment.discount : 0
        total += (payment.amount + discount)
      })
      return parseFloat(total) < parseFloat(course.price)
    }
    return false
  }

  async inInPaymentRange(studentId, courseId, startRange = 0, endRange = 0) {
    const payments = await PaymentModel
      .find({
        student: studentId,
        course: courseId
      })
    if (payments.length) {
      const course = await CourseModel
        .findOne({
          _id: courseId
        })
        .select('price')
      if (!course) {
        return false
      }
      let total = 0
      payments.forEach(payment => {
        let discount = payment.discount !== undefined ? payment.discount : 0
        total += (payment.amount + discount)
      })
      return parseFloat(startRange) <= parseFloat(total) <= parseFloat(endRange)
    }
    return false
  }

  async sendToGroup(
    data,
    normalize = false,
    debt
  ) {
    const group = await GroupModel
      .findOne({
        _id: data.id
      })
    if (!group) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${data.id}`
      }
    }

    let notificationData = {}
    let courseId = group.course
    await StudentListModel
      .findOne({
        _id: group.studentList
      })
      .select('list')
      .then(studentList => {
        return studentList.list
      })
      .then(ids => {
        return this.sendToStudents({
          data: {
            sms: data.sms,
            email: data.email,
          },
          students: ids,
          courseId: courseId,
          debt,
          normalize
        })
      })
      .then(result => {
        notificationData = {
          sms: data.sms,
          email: data.email,
          to: result,
          date: new Date()
        }
        const notification = new NotificationsModel(notificationData)
        return notification.save()
      })

    return {
      success: true,
      message: 'sent',
      notificationData
    }
  }


  async sendToTalkGroup(
    data,
    normalize = false
  ) {
    const talk = await TalksModel
      .findOne({
        _id: data.id
      })
    if (!talk) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${data.id}`
      }
    }
    console.log(talk)

    let notificationData = {}

    const result = await TalkStudentListModel
      .findOne({
        _id: talk.talkStudentList
      })
      .select('list')
      .then(studentList => {
        return studentList.list
      })
      .then(ids => {
        console.log(ids)
        return this.sendToStudents({
          data: {
            sms: data.sms,
            email: data.email
          },
          students: ids,
          normalize,
          isTalk: true
        })
      })
      .then(result => {
        notificationData = {
          sms: data.sms,
          email: data.email,
          to: result,
          date: new Date()
        }
        const notification = new NotificationsModel(notificationData)
        return notification.save()
      })

      console.log("RESULTSERVICE ",result)

    return {
      success: true,
      message: 'sent',
    }
  }


  /**
   * 
   * @param {Object} data 
   * @param {Boolean} normalize 
   * @returns {Object}
   */
  async sendGenericNotification(data, normalize = false) {
    const students = [];

    if (data && data.students && !data.groups) {
      const studentsQuery = await StudentModel
        .find({
          _id: {
            $in: data.students
          }
        })
        .select("name lastName email phoneNumber secondPhoneNumber")
        .lean();
      if (!studentsQuery || !(studentsQuery.length > 0)) {
        return {
          success: false,
          code: 404,
          error: `Students not found: ${data.students}`
        }
      }
      studentsQuery.forEach(st => {
        students.push(st);
      })
      if (students.length <= 0) {
        return {
          success: false,
          code: 404,
          error: `Empty Students info `
        }
      }
    } else if (data && data.groups && !data.students) {
      const groups = await GroupModel
        .find({
          _id: {
            $in: data.groups
          }
        })
        .populate({
          path: "studentList",
          select: "list",
          populate: {
            path: "list",
            select: "name lastName email phoneNumber secondPhoneNumber"
          }
        })
        .select("name studentList")
        .lean();
      if (!groups || !(groups.length > 0)) {
        return {
          success: false,
          code: 404,
          error: `Groups not found: ${data.groups}`
        }
      }

      groups.forEach(item => {
        const studentList = item.studentList && item.studentList.list ? item.studentList.list : [];

        if (studentList.length > 0) {
          studentList.forEach(student => {
            students.push(student)
          })
        }
      })
      if (students.length <= 0) {
        return {
          success: false,
          code: 404,
          error: `Empty Students on groups`
        }
      }
    } else if (data && data.students && data.groups) {
      return {
        success: false,
      }
    }
    if (data && data.sms) {
      console.log(students);
      const sms = new SMS();
      await sms.sendBulkMessage(students, data.sms.message);
    }
    if (data && data.email) {
      const emailData = {
        template: data.email.template,
        templateData: data.email.templateData
      }
      await SendGrid.sendBulkGenericMessage(students, emailData);
    }
    return {
      success: true,
    }
  }



  async sendByCourse(data) {
    const groupResult = await GroupModel.find({
      $and: [{
          course: data.id
        },
        {
          startDate: {
            $gte: new moment(data.startGroupDate, "YYYY-MM-DD"),
            $lte: new moment(data.endGroupDate, "YYYY-MM-DD")
          }
        }
      ]
    })


    if(!groupResult){
      return {
        success: false,
        code: 404,
        error: `Groups not found in course: ${data.id}`
      }
    }else{

    let lista = []

    for (let i = 0; i < groupResult.length; i++) {
      lista.push(groupResult[i]._id)
    }

    console.log(lista)

    const normalize = {}
    const body = {
      groups: lista
    }

    if (data.sms) {
      body.sms = {
        message: data.sms.message
      }
    }

    if (data.email) {
      body.email = {
        template: "d-a934c1e29ac94e4eb91061956d620114",
        templateData: {
          "subject": data.email.subject,
          "message": data.email.message
        }
      }
    }

    const result = await this.sendGenericNotification(
      body,
      normalize
    )

      if(result.success){

        return {
          success: true,
          message: "email / sms sent successfully "
        }

      }else{
        return {
          success: false,
          code: 500,
          error: `email / sms failed`
        }

      }



    }
  }




  /**
   * sendToStudent
   * @example
   *          const notification = new Notification()
   *          notification.sentToStudent({
   *            id: 'abc123',
   *            sms: {
   *              message: 'Lorem ipsum'
   *            },
   *            email: {
   *              template: 'd-aaaefbd3698d4fff94ba953142cd5c8a'.
   *              templateData: {
   *                foo: 'bar',
   *                message: 'Lorem ipsum'
   *              }
   *            }
   *          }, false)
   * @param {object} data notification data
   * @param {boolean} normalize normalize sms content
   */
  async sendToStudent(
    data,
    normalize = false
  ) {
    const student = await StudentModel
      .findOne({
        _id: data.id
      })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${data.id}`
      }
    }

    let notificationData = {}
    await this.sendToStudents({
        data: {
          sms: data.sms,
          email: data.email
        },
        students: [data.id],
        normalize
      })
      .then(result => {
        notificationData = {
          sms: data.sms,
          email: data.email,
          to: result,
          date: new Date()
        }
        const notification = new NotificationsModel(notificationData)
        return notification.save()
      })

    return {
      success: true,
      message: 'sent',
      notificationData
    }
  }

  async sendToUserTalk(
    data,
    normalize = false
  ) {
    const userTalk = await UserTalkModel
      .findOne({
        _id: data.id
      })
    if (!userTalk) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${data.id}`
      }
    }
    let notificationData = {}
    if (data.sms) {
      const sms = new SMS()
      let phoneNumber
      if (userTalk['phoneNumber'] && userTalk['phoneNumber'].length === 10) {
        phoneNumber = userTalk['phoneNumber']
      } else if (doc['secondPhoneNumber'] && userTalk['secondPhoneNumber'].length === 10) {
        phoneNumber = userTalk['secondPhoneNumber']
      }
      if (phoneNumber) {
        phoneNumber = `+521${phoneNumber}`
        if (normalize === 'true') {
          data.sms.message = normField(data.sms.message)
        }
        await sms.sendMessage(
          'prepIPN',
          phoneNumber,
          'custom message',
          data.sms.message
        )
      }
    }
    if (data.email) {
      const emailData = {
        email: userTalk.email,
        template: data.email.template,
        templateData: data.email.templateData
      }
      await SendGrid.sendNotification(emailData)
    }
    notificationData = {
      sms: data.sms,
      email: data.email,
      to: [userTalk._id],
      date: new Date()
    }
    const notification = new NotificationsModel(notificationData)
    notification.save()
    return {
      success: true,
      message: 'sent',
      to: userTalk.email
    }
  }

  async sendTo(data, normalize = false) {
    let notificationData = {}
    await this.sendToStudents({
        data: {
          sms: data.sms,
          email: data.email
        },
        students: data.to,
        normalize
      })
      .then(result => {
        notificationData = {
          sms: data.sms,
          email: data.email,
          to: result,
          date: new Date()
        }
        const notification = new NotificationsModel(notificationData)
        return notification.save()
      })

    return {
      success: true,
      message: 'sent',
      notificationData
    }
  }

  /**
   * sendToStudents
   * @example
   * const notification = new Notification()
   * notification.sendToStudents({
   *  sms: {
   *    message: 'lorem ipsum'
   *  },
   *  email: {
   *    template: 'd-aaaefbd3698d4fff94ba953142cd5c8a'
   *    templateData: {
   *      foo: 'bar',
   *      message: 'Lorem ipsum'
   *    }
   *  }
   * }, [
   *  'id1',
   *  'id2'
   * ],
   * false)
   * @param {object} data notification data
   * @param {array} students list of student ids
   * @param {boolean} normalize normalize sms message
   */
  async sendToStudents({
    data,
    students,
    courseId,
    debt,
    normalize = false,
    isTalk = false
  }) {
    const sentTo = []
    const total = students.length
    let cursor
    if (isTalk) {
      cursor = await UserTalkModel
        .find({
          _id: {
            $in: students
          }
        })
        .cursor()
    } else {
      cursor = await StudentModel
        .find({
          _id: {
            $in: students
          }
        })
        .cursor()
    }

    let doc = await cursor.next()
    let counter = 1
    while (doc != null) {
      let send = true
      if (courseId && typeof debt !== 'undefined') {
        const hasDebt = await this.hasDebt(doc._id, courseId)
        const d = debt === 'true'
        if (d !== hasDebt) {
          send = false
        }
      }
      if (data.sms && send) {
        const sms = new SMS()
        let phoneNumber
        if (doc['phoneNumber'] && doc['phoneNumber'].length === 10) {
          phoneNumber = doc['phoneNumber']
        } else if (doc['secondPhoneNumber'] && doc['secondPhoneNumber'].length === 10) {
          phoneNumber = doc['secondPhoneNumber']
        }

        if (phoneNumber) {
          phoneNumber = `+521${phoneNumber}`

          if (normalize === 'true') {
            data.sms.message = normField(data.sms.message)
          }

          sms.sendMessage(
            'prepIPN',
            phoneNumber,
            'custom message',
            data.sms.message
          )
        }
      }

      if (data.email && doc.email && send) {
        const emailData = {
          email: doc.email,
          template: data.email.template,
          templateData: data.email.templateData
        }

        SendGrid.sendNotification(emailData)
        sentTo.push(doc._id)
      }

      counter++
      // socket.to('notification-progress').emit({
      //   sentTo: {
      //     _id: doc._id,
      //     name: doc.name,
      //     lastName: doc.lastName
      //   },
      //   counter,
      //   total
      // })
      doc = await cursor.next()
    }

    return sentTo
  }

  /**
   * 
   * @param {*} param0 
   * @param startDate String in date Iso format p.e YYYY/MM/DD 
   * @param endDate String in date Iso format p.e YYYY/MM/DD 
   * @returns 
   * This will recibe default value of skip to 0 and limit to 10 
   */
  async getGroupsCourseNotification({
    idCourse,
    q,
    startDate,
    endDate,
    active,
    sort,
    asc,
    skip = 0,
    limit = 10
  }) {
    try {
      const courseObjectId = new ObjectId(idCourse);
      
      const pipeline = [
        // Stage 1: Match por course y active
        {
          $match: {
            course: courseObjectId,
            ...(active === "true" || active === "false" ? {
              active: active === "true"
            } : {})
          }
        },

        // Stage 2: Match por fechas si existen
        ...(startDate ? [{
          $match: {
            startDate: { $gte: new Date(startDate) }
          }
        }] : []),
        
        ...(endDate ? [{
          $match: {
            endDate: { $lte: new Date(endDate) }
          }
        }] : []),

        // Stage 3: Sort
        {
          $sort: {
            startDate: asc === "true" ? 1 : -1
          }
        },

        // Stage 4: Facet para resultados y conteo
        {
          $facet: {
            results: [
              { $skip: parseInt(skip) },
              { $limit: parseInt(limit) },
              {
                $project: {
                  _id: 1,
                  name: 1,
                  startDate: 1,
                  endDate: 1,
                  active: 1,
                  isSelected: { $literal: true }
                }
              }
            ],
            totalCount: [
              { $count: "count" }
            ]
          }
        }
      ];

      const results = await GroupModel.aggregate(pipeline);
      
      const formattedResults = {
        totalResults: results[0]?.totalCount[0]?.count || 0,
        page: parseInt(skip) + 1,
        pageSize: parseInt(limit),
        success: true,
        result: results[0]?.results || []
      };

      return formattedResults;

    } catch (err) {
      console.error('Error trying to fetch Groups:', err);
      return {
        success: false,
        code: 500,
        error: err.message
      };
    }
  }
} // end class

const normField = function (message) {
  if (!message || message === '') {
    return ''
  }
  return message.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}


module.exports = Notification