/**
 * models/students.js
 *
 * @description :: Describes the students functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const generator = require('generate-password')
const Utils = require('../lib/Utils')
const { frontUrl } = require('../config')

const SendGrid = require('../lib/sendGrid')
const SMS = require('../lib/sms')
const ErrMsg = require('../lib/errMsg')
const GeneratePDF = require('../lib/generatePDF')
const ImageModel = require('../db/images')


const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const CreditModel = require('../db/credits')
const StudentModel = require('../db/students')
const PaymentModel = require('../db/payments')
const PaymentInfoModel = require('../db/paymentInfo')
const StudentListModel = require('../db/studentLists')
const ClassRoomModel = require('../db/classRooms')
const LocationModel = require('../db/locations')

const Courses = require('./courses')
const Groups = require('./groups')

const errMsg = new ErrMsg()

/** if is not enabled, sms will send. */
const hotmailEnabled = true

const days = [
  'lunes', 'martes', 'miércoles',
  'jueves', 'viernes', 'sábado',
  'domingo'
]

const Students = {
  findByEmail: async (email = '') => {
    const student = await StudentModel
      .findOne({
        email: email,
        deleted: { $ne: true }
      })
      .then(student => {
        if (!student) {
          const errorExt = errMsg
            .getNotEntityFound('Student', email)
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
        return {
          success: true,
          student: student.toSimple()
        }
      })
      .catch(err => {
        console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return student
  },

  get: async ({
    skip,
    limit,
    sort,
    registerStart,
    registerEnd,
    balanceMin,
    balanceMax
  } = {}) => {

    try {

      const filter = { deleted: { $ne: true } }
      let sortDoc = { debt: -1 }

      if (registerStart) {
        filter['registerDate'] = { $gte: registerStart }
      } else if (registerEnd) {
        filter['registerDate'] = { $lte: registerEnd }
      }

      if (registerStart && registerEnd) {
        filter['registerDate'] = {
          $gte: registerStart,
          $lte: registerEnd
        }
      }

      if (balanceMin) {
        filter['balance.payments.total'] = {
          $gte: balanceMin
        }
      } else if (balanceMax) {
        filter['balance.payments.total'] = {
          $lte: balanceMax
        }
      }

      if (balanceMin && balanceMax) {
        filter['balance.payments.total'] = {
          $gte: balanceMin,
          $lte: balanceMax
        }
      }

      if (sort) {
        if (sort === 'lastName') {
          sortDoc = { lastName_lower: 1 }
        } else if (sort === 'secondLastName') {
          sortDoc = { secondLastName_lower: 1 }
        } else if (sort === 'name') {
          sortDoc = { name_lower: 1 }
        }
      } else {
        sortDoc = { debt: -1 }
      }

    const result = await StudentModel
      .find(filter)
      .skip(skip)
      .limit(limit)
      .sort(sortDoc)
      .populate('materialFolios', '-idStudent')
      .then(students => {
        return {
          success: true,
          students: students
        }
      })

      return result



    } catch (err) {
      const errorExt = errMsg
        .generalGet('students')
      return {
        success: false,
        error: errorExt
      }
    }
  },

  /**
   * getById
   *
   * @description: Return a single item by id
   * @param id {string} - item id
   */
  getById: async id => {
    const student = await StudentModel
      .findOne({
        _id: id,
        deleted: { $ne: true }
      })
      .populate({
        path: "comipemsOrder",
        select: "name startDate endDate schedules"
      })
      .populate('picture')
      .populate('materialFolios', '-idStudent')
      .then(student => {
        if (student) {
          return {
            success: true,
            student
          }
        }
        return {
          success: false,
          code: 404,
          error: `Student not found ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a single student.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (student.success) {
      student.student = await student.student.toBalance()
    }

    return student
  },


  /**
  * create
  *
  * @description: Creates a new Item
  * @param {object} data - List of attributes.
  */
  create: async data => {
    // Search by email

    try {

      if (data.email) {
        const student = await Students.findByEmail(data.email)
        if (student.success) {


          return {
            success: false,
            error: {
              httpCode: 400,
              message: `Student already registered ${data.email}`
            }
          }

        }
      }

      if (data.password) {
        data.hashed_password = await bcrypt.hash(data.password, 10)
        delete data.password
      }

      // create fields for searching
      if (data['name']) {
        data['name_lower'] = data['name'].toLowerCase()
      }

      if (data['email']) {
        data['email_lower'] = data['email'].toLowerCase()
      }

      if (data['lastName']) {
        data['lastName_lower'] = data['lastName'].toLowerCase()
      }

      if (data['name'] && data['lastName']) {
        data['fullName_lower'] = `${data['name']} ${data['lastName']}`
      }

      const student = new StudentModel(data)
      const result = await student.save()
        .then(student => {
          return {
            success: true,
            student
          }
        })
        .catch(err => {
          console.error('- Error trying to create a student', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to create a student'
            }
          }
        })

      return result

    } catch (err) {
      const errorExt = errMsg
        .generalCreate('students')
      return {
        success: false,
        error: errorExt
      }
    }

  },

/*
  * @param {*} data 
  */
 // rapid register
 register: async (data, user, isExternalRegister = false) => {
   
  const result = await Students.create(data.student)
   let student
   if (result.success) {
     student = result.student
   } else {
     return result
   }
   // At this point the student is already register , then we proceeded to register as a localUser
   const resultEnroll = await Groups.enroll(data.group.id, student._id, data, user)

   if (!resultEnroll.success) {
     return {
       ...resultEnroll
     }
   }

   const returnData = {
     success: true,
     studentId: student._id,
     paymentId: resultEnroll.paymentId,
     creditId: resultEnroll.creditId,
     paymentInfoId: resultEnroll.paymentInfoId,
   }
   if (isExternalRegister) {
     try {
       const resultPassword = await Students.passRecover(student._id, false, true); //False to sendEmail, true to returnUrl 
       if (resultPassword.success) {
         returnData.urlButton = resultPassword.urlButton
       }
     } catch (e) {
       console.log("Ocurrio un error al generar la contraseña o mandarla")
     }
   }
   return returnData;
 },




  /**
   * 
   * @param {*} data 
   */
  // rapid register
  registerLanding: async (data, user, isExternalRegister = false) => {
    const result = await Students.create(data.student)
    let student
    if (result.success) {
      student = result.student
    } else {
      return result
    }

    let resultEnroll
    let n=0
    for(j=0; j<data.groups.length; j++){

      resultEnroll = await Groups.enroll(data.groups[j], student._id, data, user)
      n = j+1
      if (!resultEnroll.success) {
        return {
          ...resultEnroll
        }
      }

    }


    // At this point the student is already register , then we proceeded to register as a localUser


    const returnData = {
      success: true,
      studentId: student._id,
      paymentId: resultEnroll.paymentId,
      paymentInfoId: resultEnroll.paymentInfoId,
      message: "El registro en su(s) "+ n+ " grupo(s) fue exitoso"
    }


    if (isExternalRegister) {
      try {
        const resultPassword = await Students.passRecover(student._id, false, true); //False to sendEmail, true to returnUrl 
        if (resultPassword.success) {
          returnData.urlButton = resultPassword.urlButton
        }
      } catch (e) {
        console.log("Ocurrio un error al generar la contraseña o mandarla")
      }
    }
    return returnData;
  },

  update: async (id, data) => {

    try {

      const student = await Students.getById(id)

      if (!student.success) {
        const errorExt = errMsg
          .getNotEntityFound('Student', id)
        return {
          success: false,
          error: errorExt
        }
      }

      // create fields for searching
      if (data['name']) {
        data['name_lower'] = data['name'].toLowerCase()
      }

      if (data['email']) {
        data['email_lower'] = data['email'].toLowerCase()
      }

      if (data['lastName']) {
        data['lastName_lower'] = data['lastName'].toLowerCase()
      }

      if (data['name'] && data['lastName']) {
        data['fullName_lower'] = `${data['name']} ${data['lastName']}`
      }

      if (data['password']) {
        data.hashed_password = await bcrypt.hash(data.password, 10)
        delete data.password
      }

      const result = await StudentModel
        .findOneAndUpdate({
          _id: id
        }, {
          $set: data
        }, {
          new: true
        })
        .populate({
          path: "comipemsOrder",
          select: "name startDate endDate schedules"
        })
        .populate('picture')
        .then(student => {
          return {
            success: true,
            student
          }
        })
        .catch(err => {
          console.error('- Error trying to update a student', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to update a student'
            }
          }
        })

      return result


    } catch (err) {
      const errorExt = errMsg
        .generalUpdate('students')
      return {
        success: false,
        error: errorExt
      }
    }
  },

  delete: async id => {


    let idPic = ""

    try{

      const st = await StudentModel.findOne({ _id: id })
      .then(st => {
          if (st) {
    
            idPic = st.picture
    
          }
          return {
            success: false,
            code: 404,
            error: `Student not found ${id}`
          }
        })
        .catch(err => {
          console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
          return {
            success: false,
            code: 500,
            error: JSON.stringify(err, null, 2)
          }
        })
    
    
        const image = await ImageModel
        .deleteOne({ _id: idPic })


      nuevo = await StudentModel.deleteOne({ _id: id })
        .then((ress) => {
          if (ress.n > 0) {
            return {
              success: true,
              deleted: true
            }
          } else {
            const errorExt = errMsg
              .getNotEntityFound('student', id)
            return {
              success: false,
              error: errorExt
            }

          }


        })
        .catch(err => {
          console.error('- Error trying to delete a student', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to delete a student'
            }
          }
        })

      // remove from studentlists
      await StudentListModel
        .updateMany({
          list: id
        }, {
          $pull: { list: id }
        })
        .catch(err => {
          console.error('- Error trying to update studentLists', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to update studentLists'
            }
          }
        })

      return nuevo

    } catch (err) {
      const errorExt = errMsg
        .generalDelete('student')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  find: async (q, skip, limit) => {
    const result = await StudentModel
      .find({
        $or: [
          { email_lower: new RegExp(q, 'i') },
          { lastName_lower: new RegExp(q, 'i') },
          { name_lower: new RegExp(q, 'i') },
          { phoneNumber: new RegExp(q, 'i') },
          { fullName_lower: new RegExp(q, 'i') }
        ]
      })
      .skip(skip)
      .limit(limit)
      .then(students => {
        return {
          success: true,
          students
        }
      })
      .catch(err => {
        console.error('- Error trying to get all students.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  /**
   * 
   * @param {*} studentId 
   * @param {*} groupId 
   * @param {*} data 
   * @param {*} force 
   */
  changeGroups: async (studentId, groupId, data, force = false) => {
    const student = await StudentModel.findOne({ _id: studentId })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${studentId}`
      }
    }

    const oldGroup = await GroupModel
      .findOne({ _id: groupId })
      .populate({ path: 'course' })
      .populate("studentList")
    if (!oldGroup) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${groupId}`
      }
    }

    const oldStudentList = oldGroup.studentList;
    if (!oldStudentList) {
      return {
        success: true,
        code: 500,
        error: `Group without studentlist`
      }
    }

    if (!data.group || !data.group.id) {
      return {
        success: false,
        code: 400,
        error: `No new group data`
      }
    }

    const newGroup = await GroupModel
      .findOne({ _id: data.group.id })
      .populate({ path: 'course' })
      .populate("studentList")
    if (!newGroup) {
      return {
        success: false,
        code: 404,
        error: `New group not found: ${data.group.id}`
      }
    }

    const groupIndex = student.groups.indexOf(groupId)
    if (groupIndex === -1) {
      return {
        success: false,
        code: 400,
        error: `Student don't enrolled in this group`
      }
    }
    /*********************************************
     *********************************************
     ********* End of main validations ************
     *********************************************
     *********************************************
     */

    // Before change payments , change student 
    const studentList = newGroup.studentList;
    if ((newGroup.quota <= studentList.list.length) && !(force === 'true')) {
      const errorExt = errMsg
        .getQuotaExceded()
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }
    studentList.list.push(student._id)
    //remove the material for last studentList and set the value to the new;
    if (oldStudentList.material && oldStudentList.material.length) {
      const index = oldStudentList.material.findIndex(item => String(item.student) === String(student._id))
      if (index >= 0) {
        studentList.material.push({
          student: student._id,
          material: oldStudentList.material[index].material
        })
        oldStudentList.material.splice(index, 1)
      } else {
        studentList.material.push({
          student: student._id,
          material: false
        })
      }
    } else {
      studentList.material.push({
        student: student._id,
        material: false
      })
    }

    student.groups.splice(groupIndex, 1)
    student.groups.push(newGroup._id)

    const studentIndex = oldStudentList.list.indexOf(student._id)
    oldStudentList.list.splice(studentIndex, 1)

    //Update the payments to the new group
    if (String(newGroup.course) !== String(oldGroup.course)) {
      await PaymentModel
        .updateMany({
          student: studentId,
          course: oldGroup.course
        }, {
          $set: {
            course: newGroup.course
          }
        })
    }

    //This will remove the students from the buckets if is parent
    if (oldGroup.course.isParent) {
      let buckets = oldGroup.buckets
      let groupsBucket = await GroupModel.find({ _id: { $in: buckets } }).populate({ path: "studentList" })
      //Empty student studentList of buckets
      for (let i = 0; i < groupsBucket.length; i++) {
        let index = groupsBucket[i].studentList ? groupsBucket[i].studentList.list.findIndex(item => String(item) === String(student._id)) : -1
        if (index >= 0) {
          let studentListPre = groupsBucket[i].studentList
          studentListPre.list.splice(index, 1)
          await studentListPre.save()
        }
      }
      student.comipemsOrder = [];
    }
    //This will Add the students from the buckets if is parent
    if (newGroup.course.isParent) {
      const phaseOrder = await Utils.shuffleArray(newGroup.course.children.length)
      let buckets = await Promise
        .all(newGroup.course.children.map((courseId, index) => {
          return Groups.enrollBucketGroup(courseId, newGroup, student._id, phaseOrder[index])
        }))
      buckets = buckets.sort((a, b) => {
        return a.phase - b.phase;
      })
      const newBuckets = []
      buckets.forEach(element => {
        newBuckets.push(element._id)
      });
      student.comipemsOrder = newBuckets;
    }

    //Update the main studentList
    await StudentListModel.updateOne({
      _id: oldGroup.studentList
    },
      {
        $pull: {
          list: student._id,
          material: { student: student._id }
        }
      }
    )

    await StudentListModel.updateOne({
      _id: newGroup.studentList
    },
      {
        $addToSet: {
          list: student._id,
          material: {
            student: student._id,
            material: false
          }
        }
      }
    )
    await student.save();
    student.saveBalance();

    return {
      success: true,
      student
    }

  },

  getSidebarInfo: async id => {
    const student = await StudentModel
      .findOne({
        _id: id
      })
      .populate({
        path: "groups",
      })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }
    const studentToBalance = await student.toBalance()
    return {
      success: true,
      student: studentToBalance,
    }
  },

  getInfo: async id => {
    let groups = []
    let courses = []

    const student = await StudentModel
      .findOne({
        _id: id
      })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    if (student.groups) {
      groups = await GroupModel.find({
        _id: { $in: student.groups }
      })

      const courseIds = groups.map(group => group.course)
      courses = await CourseModel.find({
        _id: { $in: courseIds }
      })
    }

    const studentToBalance = await student.toBalance()

    return {
      success: true,
      attendance: 98.5,
      student: studentToBalance,
      groups,
      courses
    }
  },

  /**
   * 
   * @param {*} id 
   * @param {*} groupId 
   */

  removeGroup: async (id, groupId) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    const group = await GroupModel.findOne({ _id: groupId }).populate({ path: 'course' })
      .select('studentList')
      .select('buckets')
    if (!group) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${groupId}`
      }
    }

    if (student.groups.indexOf(groupId) === -1) {
      return {
        success: false,
        code: 400,
        error: `Group not found in student's group: ${groupId}`
      }
    }

    const index = student.groups.indexOf(groupId)
    student.groups.splice(index, 1)
    await student.save()

    const studentList = await StudentListModel
      .findOne({
        _id: group.studentList
      })

    if (studentList.list.indexOf(id) !== -1) {
      studentList.list
        .splice(studentList.list.indexOf(id), 1)

      for (let i = 0; i < studentList.material.length; i++) {
        if (String(studentList.material.student) === String(id)) {
          studentList.material.splice(i, 1)
        }
      }
      await studentList.save()
    }
    if (group.course.isParent) {
      let buckets = group.buckets;
      let groupsBucket = await GroupModel.find({ _id: { $in: buckets } }).populate({ path: "studentList" });
      for (let i = 0; i < groupsBucket.length; i++) {
        let index = groupsBucket[i].studentList ? groupsBucket[i].studentList.list.findIndex(item => JSON.stringify(item) == JSON.stringify(student._id)) : -1;
        if (index >= 0) {
          let studentListPre = groupsBucket[i].studentList;
          studentListPre.list.splice(index, 1);
          await studentListPre.save();
        }
      }
    }
    return {
      success: true,
      deleted: true
    }
  },

  passStRecover: async email => {
    const student = await StudentModel
      .findOne({ email })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${email}`
      }
    }
    let requestMessage = ''
    // remove pass
    const tempPass = generator.generate({
      length: 6,
      numbers: true,
      lowercase: true,
      uppercase: false,
      symbols: false
    })
    student.hashed_password = await bcrypt.hash(tempPass, 10)
    student.hasTempPass = true

    await SendGrid.sendRecoverPass(student.name, tempPass, student.email)
    requestMessage = `Email sent: ${email}`

    await student.save()

    return {
      success: true,
      message: requestMessage
    }
  },

  passRecover: async (id, sendEmail = true, returnPass = false) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }
    // remove pass
    const tempPass = generator.generate({
      length: 10,
      numbers: true,
      lowercase: true,
      uppercase: false,
      symbols: false
    })
    student.hashed_password = await bcrypt.hash(tempPass, 10)
    student.hasTempPass = true

    await student.save();
    if (sendEmail) {
      await SendGrid.sendRecoverPass(student.name, tempPass, student.email);
    }
    const returnData = {
      success: true
    }
    if (returnData) {
      returnData.urlButton = `${frontUrl}/update-password/?email=${student.email}&provisionalPass=${tempPass}`
    }
    return returnData;
  },

  passUpdate: async (id, currentPass, newPass) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    if (bcrypt.compareSync(currentPass, student.hashed_password)) {
      student.hasTempPass = false
      student.hashed_password = await bcrypt.hash(newPass, 10)
      await student.save()
    } else {
      return {
        success: false,
        code: 400,
        error: `Wrong current password`
      }
    }

    return {
      success: true,
      student
    }
  },

  refreshBalance: async (id) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    await student.saveBalance()

    return {
      success: true,
      balance: student.balance
    }
  },

  debt: async id => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const payments = await Promise
      .all(
        student.groups.map(groupId => {
          return GroupModel.findOne({ _id: groupId })
            .select('course')
        })
      )
      .then(groups => {
        return Promise.all(
          groups
            .map(group => group.course)
        )
      })
      .then(courses => {
        return Promise.all(
          courses.map(courseId => Students
            .calcBalanceByCourse(student._id, courseId))
        )
      })
      .then(payments => payments)

    return {
      success: true,
      payments
    }
  },

  /**
   * calcBalanceByCourse
   * @param {string} studentId Student object id
   * @param {string} courseId Course object id
   * @return {object} Course basic info with student balance
   */
  calcBalanceByCourse: async (studentId, courseId) => {

    const course = await CourseModel.findOne({
      _id: courseId
    }).select('price name')


if(course){
    const balance = await PaymentModel
      .find({ student: studentId, course: courseId })
      .then(payments => {

        const price = course.price || 0
        let totalPayments = 0
        let totalDiscounts = 0

        if (payments.length) {
          payments.forEach(payment => {
            totalPayments += payment.amount
            totalDiscounts += payment.discount
          })
        }

        const balance = parseFloat(price) -
          parseFloat(totalPayments) - parseFloat(totalDiscounts)

        return {
          coursePrice: course.price,
          courseName: course.name,
          courseId: course._id,
          totalPayments,
          totalDiscounts,
          balance
        }
      })
    
    return balance
  }

  else{
    return {courseName: "Curso eliminado - No seleccionar"}
  }

  },

          // D E P R E C A T E D

          
  // updateProfile: async (id, data) => {
  //   const student = await StudentModel
  //     .findOneAndUpdate({ _id: id })
  //   if (!student) {
  //     const errorExt = errMsg
  //       .getNotEntityFound('Student', id)
  //     return {
  //       success: false,
  //       code: 404,
  //       error: errorExt.error,
  //       errorExt
  //     }
  //   }

  //   return {
  //     success: true,
  //     message: 'putme'
  //   }
  // },

  getByCourse: async (courseId) => {
    return GroupModel
      .find({ course: courseId })
      .select('studentList')
      .then(groups => groups.map(group => group.studentList))
      .then(studentLists => {
        return Promise
          .all(studentLists.map(stList => {
            return StudentListModel
              .findOne({ _id: stList })
              .select('list')
          }))
      })
      .then(stLists => {
        return stLists
          .filter(stList => stList)
          .map(stList => stList.list)
      })
      .then(lists => {
        let stIds = []
        lists.forEach(list => {
          list.forEach(stId => {
            stIds.push(stId)
          })
        })
        return _.uniq(stIds)
      })
      .then(stIds => {
        return Promise
          .all(stIds.map(stId => {
            return StudentModel
              .findOne({ _id: stId })
          }))
      })
      .then(students => {
        return Promise
          .all(students.map(student => {
            return student.toBalance()
          }))
      })
  },

  getByGroup: async (groupId) => {
    return GroupModel
      .findOne({ _id: groupId })
      .select('studentList')
      .then(group => group.studentList)
      .then(studentListId => {
        return StudentListModel
          .findOne({ _id: studentListId })
          .select('list')
      })
      .then(studentList => studentList.list)
      .then(stIds => {
        return Promise
          .all(stIds.map(stId => {
            return StudentModel
              .findOne({ _id: stId })
          }))
      })
      .then(students => {
        return Promise
          .all(students.map(student => {
            return student.toBalance()
          }))
      })
  },

  notificationFilter: async (data) => {
    let students = []

    if (data.courses) {
      const courseStudents = await Promise
        .all(data.courses.map(course => {
          return Students.getByCourse(course)
        }))
        .then(students => {
          const sts = []
          students.forEach(list => {
            list.forEach(student => {
              sts.push(student)
            })
          })
          return sts
        })
      courseStudents.forEach(student => {
        students.push(student)
      })
    }

    if (data.groups) {
      const groupStudents = await Promise
        .all(data.groups.map(group => {
          return Students.getByGroup(group)
        }))
        .then(students => {
          const sts = []
          students.forEach(list => {
            list.forEach(student => {
              sts.push(student)
            })
          })
          return sts
        })
      groupStudents.forEach(student => {
        students.push(student)
      })
    }

    return {
      success: true,
      students
    }
  },

  addFolio: async (id, objectFolio) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }
    const title = objectFolio.title;
    const folio = objectFolio.folio;
    const object = {
      title: title,
      folio: folio
    }
    student.materialFolios.unshift(object);
    await student.save();
    return {
      success: true,
      materialFolios: student.materialFolios
    }
  },

  removeFolio: async (id, idFolio) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }
    if (student.materialFolios && student.materialFolios.length > 0) {
      const index = student.materialFolios.findIndex(item => String(item._id) === String(idFolio));
      if (index >= 0) {
        student.materialFolios.splice(index, 1);
        student.save();
        return {
          success: true,
          materialFolios: student.materialFolios
        }
      }
    }
    return {
      success: true,
    }
  },
  
  updateFolio: async (id, objectFolio, idFolio) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }
    if (student.materialFolios && student.materialFolios.length > 0) {
      const index = student.materialFolios.findIndex(item => String(item._id) === String(idFolio));
      if (index >= 0) {
        student.materialFolios[index].title = objectFolio.title;
        student.materialFolios[index].folio = objectFolio.folio;
        await student.save();
        return {
          success: true,
          materialFolios: student.materialFolios
        }
      }
    }
    return {
      success: false,
    }
  }
}



function formatHour(hour) {
  let arrHour = hour.split('')
  arrHour.splice(-2, 0, ':')

  const str = arrHour.join('')
  const arrStr = str.split(':')

  return {
    string: str,
    minutes: arrStr[1],
    hours: arrStr[0]
  }
}

module.exports = Students
