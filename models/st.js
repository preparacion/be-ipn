/**
 * models/st.js
 *
 * @description :: Describes the st functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')
const moment = require('moment')
const SendGrid = require('../lib/sendGrid')

const UserModel = require('../db/users')
const AppoimentsModel = require('../db/appoiments')
const GroupModel = require('../db/groups')
const VideoModel = require('../db/videos')
const CourseModel = require('../db/courses')
const ClassRoomModel = require('../db/classRooms')
const LocationModel = require('../db/locations')
const StudentModel = require('../db/students')
const ImageModel = require('../db/images')
const StudentListModel = require('../db/studentLists')
const PaymentModel = require('../db/payments')
const PlaylistModel = require('../db/playlists')
const AttendanceStudentModel = require('../db/attendanceStudent')

const GroupModelModel = require('../models/groups')
const StudentModelModel = require('../models/students')

const Utils = require('../lib/Utils')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const days = [
  'lunes', 'martes', 'miércoles',
  'jueves', 'viernes', 'sábado',
  'domingo'
]

const St = class St {
  constructor() {
    this.state = {}
  }


  /**
   * getTotalAttendance
   * @param {string} id Student's id. Mongo Object Id.
   * @returns {object} Student total attendance
   */
  async getTotalAttendance(id) {
    const attendance = await AttendanceStudentModel
      .findOne({ student: id })
      .then(attendance => {
        if (!attendance) {
          return {}
        }
        let absences = 0
        let total = attendance.attendance.length
        attendance.attendance.forEach(att => {
          if (!att.attendance) {
            absences++
          }
        })

        return {
          absences,
          total
        }
      })
    return attendance
  }

  /**
   * getAttendance
   * @param {string} id Student's id. Mongo Object Id.
   * @returns {object} Student attendance
   */
  async getAttendance(id) {
    const attendance = await AttendanceStudentModel
      .findOne({ student: id })
      .then(attendance => {
        if (attendance) {
          return attendance.attendance
        } else {
          return {
            success: true,
            attendance: []
          }
        }
      })
    return {
      success: true,
      attendance
    }
  }

  /**
   * getProfile
   * @description Get the student profile
   * @param {string} id Student's id. Mongo Object Id
   * @returns {object} The result
   */
   async getProfile(id) {
    const student = await StudentModel
      .findOne({ _id: id })
      .populate('picture')
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    const profile = student.toProfile()

    // profile['attendance'] = await this.getTotalAttendance(id)

    return {
      success: true,
      profile
    }
  }


    /**
   * deletePicture
   * @description Delete student picture
   * @param {string} id Student's id. Mongo Object Id
   * @returns {object} The result
   */
     async deletePicture(id) {
      const student = await StudentModel
        .findOne({ _id: id })
      if (!student) {
        return {
          success: false,
          code: 404,
          error: `Student not found: ${id}`
        }
      }
  
      const image = await ImageModel
      .deleteOne({ _id: student.picture })

      return {
        success: true,
        deleted: true
      }
    }

  /**
   * getFinancial
   * @description Gets the student financial info
   * @param {string} id Student Object id
   */
  //TODO FIX MISSING COURSE 
  async getFinancial(id) {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    const payments = await PaymentModel
      .find({ student: id })
      .populate({
        path: "course",
        select: "name price"
      })

    let groupPaymentsName = []
    if (payments.length > 0) {
      const groupPayments = payments.reduce((acomulator, currentValue) => {
        if (currentValue.course !== undefined && currentValue.course !== null) {
          (acomulator[currentValue.course._id] = acomulator[currentValue.course._id] || []).push(currentValue);
        } else {
          (acomulator["0"] = acomulator["0"] || []).push(currentValue);
        }
        return acomulator;
      }, {});
      Object.keys(groupPayments).map(function (key, index) {
        const current = groupPayments[key]
        let totalPayments = 0
        if (current.length > 1) {
          totalPayments = current.reduce((a, b) => {
            if (typeof (a) == "object") {
              return (a.amount + a.discount) + (b.amount + b.discount)
            } else {
              return a + (b.amount + b.discount)
            }
          })
        } else {
          totalPayments = current[0].amount + current[0].discount
        }
        let bodyObject
        if (key !== "0") {
          const debt = (totalPayments < current[0].course.price)
          bodyObject = {
            _id: current[0].course._id,
            name: current[0].course.name,
            price: current[0].course.price,
            totalPayments: totalPayments,
            debt: debt,
            payments: current
          }
        } else {
          bodyObject = {
            name: "Otros",
            totalPayments: totalPayments,
            payments: current
          }
        }
        groupPaymentsName.push(bodyObject)
      });
    } else {
      for (let i = 0; i < student.groups.length; i++) {
        const group = await GroupModel
          .findById({ _id: student.groups[i] })
          .populate("course")
        const bodyObject = {
          _id: group.course._id,
          name: group.course.name,
          price: group.course.price,
          totalPayments: 0,
          debt: group.course.price,
          payments: []
        }
        groupPaymentsName.push(bodyObject)
      }
    }
    return {
      success: true,
      financial: {
        payments: groupPaymentsName
      }
    }
  }

  /**
   * addArrangementLesson
   * @description add or modify arrangement to the payed lesson 
   * @param {string} studentId Object group id
   */
  async addArrangementLesson(studentId, courseId) {
    const student = await StudentModel.findById(studentId)
    if (student !== undefined) {
      if (student.paymentsArranges !== undefined) {
        const index = student.paymentsArranges.findIndex(value => String(value.courseId) == String(courseId))
        if (index >= 0) {
          const paymentArrange = student.paymentsArranges[index]
          if (moment(paymentArrange.endDate).isBefore(moment(), 'day')) {
            paymentArrange.startDate = new Date()
            paymentArrange.endDate = new Date()
            await student.save()
            return {
              success: true,
              student:student._doc
            }
          }else{
            return {
              success: true,
              student
            }
          }
        } else {
          const paymentArrange = {
            courseId: courseId,
            startDate: new Date(),
            endDate: new Date(),
            typeArrange: 1,
          }
          student.paymentsArranges.push(paymentArrange)
          await student.save()
          return {
            success: true,
            student
          }
        }
      } else {
        const paymentArrange = [{
          courseId: courseId,
          startDate: new Date(),
          endDate: new Date(),
          typeArrange: 1,
        }]
        student.paymentsArranges = paymentArrange
        await student.save()
        return {
          success: true,
          student
        }
      }
    } else {
      return {
        success: false,
        error: "We can not find the student"
      }
    }
  }

  /**
   * _getCourseInfo
   * @description fill the course info
   * @param {string} groupId Object group id
   */
  async _getCourseInfo(groupId) {
    const group = await GroupModel
      .findOne({ _id: groupId })
    const course = await CourseModel
      .findOne({ _id: group.course })

    const gp = await group.toPopulate()

    return {
      name: course.name,
      evaluations: [],
      course: course,
      group: gp
    }
  }

  /**
   * _getGroupInfo
   * @description Fill the group info
   * @param {string} groupId Object group id
   */
  async _getGroupInfo(groupId) {
    let teacher

    const group = await GroupModel
      .findOne({ _id: groupId })
      .populate({
        path: "course",
      })
      .populate({
        path: "teacher",
      })

    const course = group.course
    if (group.teacher) {
      teacher = `${group.teacher.name} ${group.teacher.lastName}`
    }


    // const gp = await group.toPopulate()

    const groupInfo = {
      _id: course._id,
      name: course.name,
      poster: course.poster || null,
      duration: course.duration,
      activeLiveStream: course.activeLiveStream,
      liveStreamUrl: course.liveStreamUrl,
      price: course.price,
      group: group
      // group: gp
    }

    if (teacher) {
      groupInfo['teacher'] = teacher
    }

    return groupInfo
  }

  /**
   * getVideosByCourseId
   * @description Get the videos of a specific course
   * @param {string} playlistId Object playlist id
   * @returns {array} List of videos
   */
  async getVideosByCourseId(playlistId) {
    const playlist = await PlaylistModel
      .findOne({ _id: playlistId })
      .select('videos')
    if (!playlist) {
      const errorExt = errMsg.getNotEntityFound('Playlist', playlistId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const videos = await Promise.all(
      playlist.videos.map(video => {
        return VideoModel.findOne({ _id: video.video })
      })
    )

    return {
      success: true,
      videos
    }
  }

  /**
   * updateStudent
   * @description Update personal info from student
   * @param {string} studentToken Object student
   * @returns 
   */
  async updateStudent(studentToken, data) {
    const student = await StudentModel
      .findOne({ _id: studentToken._id })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    if (data.name)
      student.name = data.name
    if (data.lastName)
      student.lastName = data.lastName
    if (data.secondLastName)
      student.secondLastName = data.secondLastName
    if (data.email)
      student.email = data.email
    if (data.phoneNumber)
      student.phoneNumber = data.phoneNumber
    if (data.address)
      student.address = data.address
    if (data.postalCode)
      student.postalCode = data.postalCode
    if (data.birthDate)
      student.birthDate = new Date(data.birthDate)
    if(data.picture)
      student.picture = data.picture

    await student.save()
    await student.populate('picture').execPopulate()
    return {
      success: true,
      student
    }
  }


  /**
 * enrollToGroup
 * @description Enroll a student into a group
 * @param {string} idGroup Object id of group
 * @returns 
 */

  async enrollToGroup(idStudent, idGroup) {
    const student = await StudentModel
      .findOne({ _id: idStudent })
      .populate({
        path: 'groups'
      })
    if (!student) {
      const errorExt = errMsg.getNotEntityFound('Student', playlistId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const group = await GroupModel
      .findOne({ _id: idGroup })
    if (!group) {
      const errorExt = errMsg.getNotEntityFound('Group', playlistId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const course = await CourseModel
      .findOne({ _id: group.course })

    if (student.groups && student.groups.indexOf(group._id) > -1) {
      // Already enrolled to this group
      const errorExt = errMsg.getStudentAlreadyEnrolled('group', idGroup)

      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }


    // verify that a student only can enroll a course once.
    if(course._id != "64245b019180b412772f620f"){
      const alreadyEnrolled = await GroupModelModel._isAlreadyEnrolled(student._id, group._id)
    


    if (alreadyEnrolled) {
      // Already enrolled to this course
      const errorExt = errMsg.getStudentAlreadyEnrolled('course', course._id)
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }
  }

    if (course.isParent) {
      return Groups.enrollBucket(group, student, course, data, user)
    }

    const studentListId = group.studentList

    const studentList = await StudentListModel.findOne({ _id: studentListId })
      .then(studentList => studentList)

    if (group.quota <= studentList.list.length) {
      return {
        success: false,
        error: `No se puede inscribir, cupo lleno`,
        code: 400
      }
    }



    if ((student.groups)) {
      if (student.groups.findIndex(i => JSON.stringify(i.course) == JSON.stringify(group.course)) !== -1) {
        // Already enrolled to this course
        if(course._id != "64245b019180b412772f620f"){ // se agrego por requerimiento de inscribirse a diferentes grupos del mismo curso de clases de repaso
          const errorExt = errMsg
          .getStudentAlreadyEnrolled('course', idGroup)
        return {
          success: false,
          code: 400,
          error: errorExt.error,
          errorExt
        }
        }
       

      }
      student.groups.push(group._id)
    } else {
      student.groups = [group._id]
    }
  

    studentList.list.push(idStudent)
    studentList.material.push({
      student: idStudent,
      material: false
    })
    await student.save()
    await studentList.save()
    const classRoom = await ClassRoomModel
      .findOne({ _id: group.classRoom })
    const location = await LocationModel
      .findOne({ _id: classRoom.location })

    const schedulesInfo = group.schedules.map(schedule => {
      const startHour = Utils.formatHour(String(schedule.startHour))
      const endHour = Utils.formatHour(String(schedule.endHour))
      return {
        day: days[schedule.day],
        startHour: startHour.string,
        endHour: endHour.string
      }
    })
    const startDate = moment(group.startDate).format("DD-MM-YYYY")
    const sendGridData = {
      email: student.email,
      name: student.name,
      courseName: course.name,
      groupName: group.name,
      schedules: schedulesInfo,
      startDate: startDate,
      classRoomName: classRoom.name,
      locationName: location.name,
      locationDirection: `${location.street} ${location.number}, Col. ${location.district}`
    }
    await SendGrid.sendRegister(sendGridData)
    return {
      success: true,
      enrolled: true,
    }
  }

  /**
 * getCourseInfo
 * @description Get the course info for a specific course
 * @param {string} idCourses Object playlist id
 * @returns {array} List of videos
 */
  async getCourseInfo(idCourses) {
    const course = await CourseModel
      .findOne({ _id: idCourses })
    if (!course) {
      const errorExt = errMsg.getNotEntityFound('Course', playlistId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }


    return {
      success: true,
    }
  }


}

module.exports = St
