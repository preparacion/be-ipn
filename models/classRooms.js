/**
 * models/classRooms.js
 *
 * @description :: Describes the classRooms functions
 * @docs        :: TODO
 */
const ClassRoomsModel = require('../db/classRooms')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const LocationModel = require('../db/locations')
const GroupModel = require('../db/groups')
const Schedules = require('./schedules')
const moment = require('moment')
const _ = require('lodash')

const days = ['l', 'm', 'w', 'j', 'v', 's', 'd']

const ClassRooms = {
  /**
   * get
   *
   * @description: Returns all the items
   * @returns {array} - List of items
   */
  get: async () => {

    try {
      const classRooms = await ClassRoomsModel
        .find({})
        .then(classRooms => {
          return {
            success: true,
            classRooms
          }
        })
        .catch(err => {
          console.error('- Error trying to get all classRooms.', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to get all classRooms'
            }
          }
        })
      return classRooms


    } catch (err) {
      const errorExt = errMsg
        .generalGet('classRooms')
      return {
        success: false,
        error: errorExt
      }

    }

  },

  /**
   * getById
   *
   * @description: Returns a single item by id.
   * @param {string} id Item id
   */
  getById: async id => {
    const classRoom = await ClassRoomsModel
      .findOne({ _id: id })
      .then(classRoom => {
        if (classRoom) {
          return {
            success: true,
            classRoom
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `ClassRoom not found: ${id}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a classroom by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return classRoom
  },

  create: async data => {

    try {
      const classRoom = new ClassRoomsModel(data)
      const result = await classRoom.save()
        .then(classRoom => {
          return {
            success: true,
            classRoom
          }
        })
        .catch(err => {
          console.error('- Error trying to create a classroom', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to create a classroom'
            }
          }
        })

      return result

    } catch (err) {
      const errorExt = errMsg
        .generalCreate('classRooms')
      return {
        success: false,
        error: errorExt
      }

    }
  },

  update: async (id, data) => {

    try{
    const classroom = await ClassRooms.getById(id)

    if (!classroom.success) {

      const errorExt = errMsg
      .getNotEntityFound('classroom', id)
    return {
      success: false,
      error: errorExt
    }
    }

    const result = await ClassRoomsModel
      .findOneAndUpdate({
        _id: id
      }, {
        '$set': data
      }, {
        new: true
      })
      .then(classRoom => {
        return {
          success: true,
          classRoom
        }
      })
      .catch(err => {
        console.error('- Error trying to update a classroom', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to update a classroom'
          }
        }
      })

    return result

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('classRooms')
    return {
      success: false,
      error: errorExt
    }

  }

  },

  delete: async id => {

    try {

      const result = await ClassRoomsModel.deleteOne({
        _id: id
      })
      if (result.n == 0) {
        const errorExt = errMsg
          .getNotEntityFound('classroom', id)
        return {
          success: false,
          error: errorExt
        }

      } else {

        return {
          success: true,
          deleted: true
        }
      }
    } catch (err) {
      const errorExt = errMsg
        .generalDelete('classRooms')
      return {
        success: false,
        error: errorExt
      }

    }
  },

  getByOldId: async oldId => {
    const classRoom = await ClassRoomsModel
      .findOne({ oldId: oldId })
      .then(classRoom => {
        if (classRoom) {
          return {
            success: true,
            classRoom
          }
        } else {
          return {
            success: false,
            code: 404,
            error: `ClassRoom not found: ${oldId}`
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a classroom by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return classRoom
  },

  getByLocationId: async locationId => {
    const classRoom = await ClassRoomsModel
      .find({ location: locationId })
      .then(classRoom => {
        if (classRoom.length) {
          return {
            success: true,
            classRoom
          }
        } else {
          const errorExt = errMsg.getByIndex('noClassRoomInLocation')
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
      })
      .catch(err => {
        console.error('- Error trying to get a classroom by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return classRoom
  },



  //This service only return the classRoom available in the selected dates

  getByLocationFilter: async (locationId, data) => {

    const location = await LocationModel.findOne({
      _id: locationId
    })

    if (!location) {
      return {
        success: false,
        code: 404,
        error: `Location not found: ${locationId}`
      }
    }

    const classRooms = await ClassRoomsModel.find({
      location: locationId
    })

    //this will check if the day of schedules exist in range, if not return error .
    if (!Schedules._hasDaysInRange(data.startDate, data.endDate, data.schedules)) {
     
      const errorExt = errMsg
        .getNoDaysInRange()
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    const availables = []

    for (let i = 0; i < classRooms.length; i++) {
      let bodyData = {
        startDate: new Date(data.startDate),
        endDate: new Date(data.endDate),
        scheduleArray: data.schedules,
        classRoomId: classRooms[i].id
      }
      const result = await Schedules.isAvailable(bodyData)
      if (result.isAvailable) {
        availables.push(classRooms[i])

      }
    }
    return {
      success: true,
      classRooms: availables
    }
  }
}


module.exports = ClassRooms
