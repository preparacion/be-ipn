/**
 * models/sms.js
 *
 * @description :: Describes the sms functions
 * @docs        :: TODO
 */
const SmsModel = require('../db/sms')
const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const StudentListModel = require('../db/studentLists')

const SMS = require('../lib/sms')
const SendGrid = require('../lib/sendGrid')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

/* eslint-disable */
const normField = function (message) {
  if (!message || message === '') {
    return ''
  }
  return message.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}
/* eslint-disable */

const Sms = {
  get: async ({ skip, limit }) => {
    try{

      lim = parseInt(limit)
      sk = parseInt(skip)
      
    const sms = await SmsModel
      .find({})
      .limit(lim)
      .skip(sk)
      .sort({ date: -1 })
      .then(sms => {
        return {
          success: true,
          sms
        }
      })
      .catch(err => {
        console.error('- Error trying to get all sms.', JSON.stringify(err, null, 2))
        return {
          success: false,
          error: {
            httpCode: 500,
            message: '- DB Error trying to get all sms'
          }
        }
      })

    return sms

  } catch (err) {
    const errorExt = errMsg
      .generalGet('sms')
    return {
      success: false,
      error: errorExt
    }
  }
  },

  sentToCourse: async data => {
    const course = await CourseModel
      .findOne({ _id: data.id })
    if (!course) {
      return {
        success: false,
        code: 404,
        error: `Course not found: ${data.id}`
      }
    }

    const stListId = await GroupModel
      .find({ course: data.id })
      .select('_id studentList')
      .then(groups => groups.map(group => group.studentList))

    const studentsIds = []
    await Promise
      .all(stListId.map(stId => {
        return StudentListModel
          .findOne({ _id: stId })
          .select('list')
      }))
      .then(lists => {
        lists.forEach(list => {
          list.list.forEach(stId => {
            studentsIds.push(stId)
          })
        })
      })

    await Promise.all(studentsIds.map(stId => {
      return Sms.sendToStudent(stId, data.message, data.sendMail, false)
    }))

    return {
      success: true,
      message: 'Post course'
    }
  },

  sendToGroup: async data => {
    const group = await GroupModel
      .findOne({ _id: data.id })
    if (!group) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${data.id}`
      }
    }

    const studentList = await StudentListModel
      .findOne({ _id: group.studentList })
      .select('list')
      .then(studentList => studentList.list)

    await Promise.all(
      studentList
        .map(studentId => {
          return Sms.sendToStudent(studentId, data.message, data.sendMail, false)
        })
    )

    const sms = new SmsModel({
      message: data.message,
      date: new Date(),
      group: group._id
    })
    await sms.save()

    return {
      success: true,
      message: data.message
    }
  },

  sendToStudent: async (
    studentId, message, sendEmail = false, save = false
  ) => {
    const student = await StudentModel
      .findOne({ _id: studentId })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${studentId}`
      }
    }

    let phoneNumber
    const sms = new SMS()

    if (student['phoneNumber'].length === 10) {
      phoneNumber = student['phoneNumber']
    } else if (student['secondPhoneNumber'].length === 10) {
      phoneNumber = student['secondPhoneNumber']
    }

    if (phoneNumber) {
      phoneNumber = `+521${phoneNumber}`
      await sms.sendMessage(
        'prepIPN',
        phoneNumber,
        'custom message',
        message
      )
    }
    if (sendEmail) {
      const emailData = {
        to: student.email,
        email: student.email,
        name: student.name,
        lastName: student.lastName,
        message: message
      }

      await SendGrid.sendMessage(emailData)
    }

    if (save) {
      const smsToStudent = new SmsModel({
        message: message,
        date: new Date(),
        student: student._id
      })
      await smsToStudent.save()
    }
  },

  _getStudentFromGroup: groupId => {
    //
  }
}

module.exports = Sms
