/**
 * models/available.js
 *
 * @description :: Describes the available functions
 * @docs        :: TODO
 */
const mongoose = require('mongoose')
const _ = require('underscore')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const LocationModel = require('../db/locations')
const ScheduleModel = require('../db/schedules')
const ClassRoomModel = require('../db/classRooms')
const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Available = {
  fillSchedule: async data => {
    const schedule = await ScheduleModel.findOne({
      _id: data.scheduleId
    })

    delete data.scheduleId

    return _.extend(data, { schedule })
  },

  get: async ({
    skip = 0,
    limit = 10,
    startDate,
    endDate,
    schedule,
    course,
    group
  } = {}) => {


try{

    const filter = {
      active: true,
      quota: { $exists: true, $gt: 0 } // some groups doesn't have quota
    }

    if (schedule) {
      filter['schedule'] = new mongoose.Types.ObjectId(schedule)
    }

    if (course) {
      filter['course'] = new mongoose.Types.ObjectId(course)
    }

    if (group) {
      filter['_id'] = new mongoose.Types.ObjectId(group)
    }

    if (startDate) {
      let stDate = new Date(startDate)
      stDate.setHours(0)
      stDate.setMinutes(0)
      stDate.setSeconds(0)

      filter['startDate'] = { $gt: stDate }

      if (endDate) {
        let nDate = new Date(startDate)
        nDate.setHours(0)
        nDate.setMinutes(0)
        nDate.setSeconds(0)

        filter['startDate'] = {
          $gt: stDate,
          $lt: nDate
        }
      }
    }

    const availability = await GroupModel
      .aggregate([
        {
          $project: { name: 1, startDate: 1, endDate: 1, active: 1, course: 1, schedule: 1, studentList: 1, quota: 1 }
        },
        {
          $match: filter
        },
        {
          $lookup: {
            from: 'StudentLists',
            localField: 'studentList',
            foreignField: '_id',
            as: 'studentList'
          }
        },
        {
          $unwind: '$studentList'
        },
        {
          $project: {
            name: 1,
            startDate: 1,
            endDate: 1,
            quota: 1,
            schedule: 1,
            course: 1,
            students: { $size: '$studentList.list' },
            available: { $subtract: ['$quota', { $size: '$studentList.list' }] }
          }
        },
        {
          $sort: { available: -1 }
        },
        {
          $skip: skip
        },
        {
          $limit: limit
        }
      ])

    return {
      success: true,
      availability
    }

  }
  catch (err) {
      const errorExt = errMsg.generalGet('Available')
      return {
          success: false,
          error: errorExt
      }
  
}


  },

  getByCourseId: async courseId => {
    const course = await CourseModel
      .findOne({
        _id: courseId
      })
      .then(course => {
        if (!course) {
          return {
            success: false,
            code: 404,
            error: `Course not found: ${courseId}`
          }
        }
        return course
      })
      .catch(err => {
        console.error('- Error trying to get a course by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in course && !course.success) {
      // Return error
      return course
    }

    const groups = await GroupModel
      .find({
        active: true,
        course: courseId
      })
      .catch(err => {
        console.error('- Error trying to get groups course by id.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in groups && !groups.success) {
      return groups
    }

    const result = await Promise
      .all(groups.map(group => group.toAvailable()))
      .then(result => {
        const dataArray = []
        for (let i = 0; i < result.length; i++) {
          const group = result[i]
          dataArray.push({
            scheduleId: group.schedule,
            available: group.available,
            quota: group.quota
          })
        }

        return {
          success: true,
          data: dataArray
        }
      })
      .catch(err => {
        console.error('- Error trying to get groups by schedule.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if (result.success) {
      const scheduleData = await Promise
        .all(result.data.map(data => Available.fillSchedule(data)))
        .then(result => {
          return {
            success: true,
            data: result
          }
        })
        .catch(err => {
          console.error('- Error trying to fill the schedule data', JSON.stringify(err, null, 2))
          return {
            success: false,
            code: 500,
            error: JSON.stringify(err, null, 2)
          }
        })
      return scheduleData
    }

    return result
  },

  getByCourses: async () => {
    const courses = await CourseModel.find({})
      .then(courses => courses)
      .catch(err => {
        console.error('- Error trying to get all the courses', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in courses && !courses.success) {
      return courses
    }

    const availability = await Promise.all(courses.map(course => Available.getByCourseId(course._id)))
      .then(availability => {
        return {
          success: true,
          availability
        }
      })
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get courses availability', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return availability
  },

  getByLocationId: async locationId => {
    const location = await LocationModel.findOne({ _id: locationId })
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get the availability of a location by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in location && !location.success) {
      return location // return error
    }

    // Getting all classroom in this location
    const classRoomsIds = await ClassRoomModel.find({ location: locationId })
      .then(classRooms => {
        return classRooms.map(classRoom => classRoom._id)
      })
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get the location classrooms', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in classRoomsIds && !classRoomsIds.success) {
      return classRoomsIds // return errror
    }
    // Getting all the groups
    const groups = await GroupModel.find({
      classRoom: { $in: classRoomsIds }
    })
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get the location groups', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    const available = await Promise.all(groups.map(group => group.toAvailable()))
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get groups to available', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      _id: locationId,
      success: true,
      available
    }
  },

  getByLocations: async () => {
    const locations = await LocationModel.find({})
      .select('_id')
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get locations ids', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    if ('success' in locations && !locations.success) {
      return locations // return error
    }

    const available = await Promise
      .all(locations.map(location => Available.getByLocationId(location._id)))
      .catch(err => {
        console.error(err)
        console.error('- Error trying to get locations availability', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })

    return {
      success: true,
      result: available
    }
  }
}

module.exports = Available
