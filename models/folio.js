/**
 * models/folio.js
 *
 * @description :: Describes the folio functions
 * @docs        :: TODO
 */
const FolioModel = require('../db/folio')

const ErrMsg = require('../lib/errMsg')
const StudentsModel = require('../db/students')
const _ = require('lodash')
const errMsg = new ErrMsg()



const Folio = {


  create: async data => {

    const folioB = await FolioModel
      .find({
        folio: data.folio
      })

    if (folioB.length <= 0) {

      const folio = new FolioModel(data)
      const result = await folio.save()
        .then(folio => {

          return {
            success: true,
            folio
          }

        })
        .catch(err => {
          console.log(err)
          const errorExt = errMsg
              .errorDB('save()','folio', 'create()')
          return {
              success: false,
              error: errorExt
          }
        })

      const student = await StudentsModel.findOneAndUpdate({
          _id: data.idStudent
        }, {
          $push: {
            materialFolios: result.folio._id
          }
        }, {
          new: true
        })
        .then((updF) => {
          return {
            success: true,
            folio: updF
          }

        })
        .catch((err) => {
          console.log(err)
          const errorExt = errMsg
              .errorDB('findOneAndUpdate()','folio', 'create()')
          return {
              success: false,
              error: errorExt
          }
        });


      return result
    } else {
      
        const errorExt = errMsg
        .alreadyExists('Folio')
      return {
        success: false,
        error: errorExt
      }
    }


  },

  update: async (data) => {
    // si el id de folio existe

    const folioB = await FolioModel
      .find({
        _id: data.idFolio
      })


    if (folioB.length > 0) {

      // buscar si el folio a actualizar existe


      const folioF = await FolioModel
        .find({
          folio: data.folio
        })

      //   si existe denegar el update



      if (folioF.length > 0) {
        if (data.idFolio != folioF[0]._id) {
          const errorExt = errMsg
          .alreadyExists('Folio')
        return {
          success: false,
          error: errorExt
        }
        }
      }

    } else {
      const errorExt = errMsg
      .getNotEntityFound('Folio', data.idFolio)
    return {
      success: false,
      error: errorExt
    }
    }




    const folioU = await FolioModel.findOneAndUpdate({
        _id: data.idFolio
      }, {
        '$set': data
      }, {
        new: true
      })
      .then((updF) => {
        return {
          success: true,
          folio: updF

        }

      })
      .catch((err) => {
        console.log(err)
        const errorExt = errMsg
            .errorDB('findOneAndUpdate()','folio', 'update()')
        return {
            success: false,
            error: errorExt
        }
      });

    return folioU





  },

  delete: async id => {
    const result = await FolioModel.deleteOne({
        _id: id
      })
      .then((data) => {


        if (data.n > 0) {
  
          return {
            success: true,
            deleted: true
          }
        } else {
          const errorExt = errMsg
          .getNotEntityFound('Folio', id)
        return {
          success: false,
          error: errorExt
        }
        }
      })
      .catch(err => {
        console.log(err)
        const errorExt = errMsg
            .errorDB('deleteOne()','folio', 'delete()')
        return {
            success: false,
            error: errorExt
        }
      })



    return result
  },
   

}


module.exports = Folio