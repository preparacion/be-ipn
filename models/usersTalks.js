/**
 * models/usersTalks.js
 *
 * @description :: Describes the userTalks functions
 * @docs        :: TODO
 */
const _ = require('underscore')
const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const moment = require('moment')
const GeneratePDF = require('../lib/generatePDF')
const fs = require('fs')

const SendGrid = require('../lib/sendGrid')
const ErrMsg = require('../lib/errMsg')

const CommentsModel = require('../db/comments')

const TalksModel = require('../db/talks')
const UserTalksModel = require('../db/usersTalks')
const CommentModel = require('../db/comments')
const TalkStudentListModel = require('../db/talkStList')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const StudentListModel = require('../db/studentLists')
const ClassRoomModel = require('../db/classRooms')
const LocationModel = require('../db/locations')

const QuizModel = require('../db/quiz')



const errMsg = new ErrMsg()

const UsersTalks = {
  findByEmail: async (email = '') => {
    const student = await StudentModel
      .findOne({
        email: email,
        deleted: { $ne: true }
      })
      .then(student => {
        if (!student) {
          const errorExt = errMsg
            .getNotEntityFound('Student', email)
          return {
            success: false,
            code: 404,
            error: errorExt.error,
            errorExt
          }
        }
        return {
          success: true,
          student: student.toSimple()
        }
      })
      .catch(err => {
        console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return student
  },

  get: async ({
    skip,
    limit,
    sort,
    registerStart,
    registerEnd,
    balanceMin,
    balanceMax
  } = {}) => {

    try {

      const filter = { deleted: { $ne: true } }
      let sortDoc = { debt: -1 }

      if (registerStart) {
        filter['registerDate'] = { $gte: registerStart }
      } else if (registerEnd) {
        filter['registerDate'] = { $lte: registerEnd }
      }
      if (registerStart && registerEnd) {
        filter['registerDate'] = {
          $gte: registerStart,
          $lte: registerEnd
        }
      }
      if (balanceMin) {
        filter['balance.payments.total'] = {
          $gte: balanceMin
        }
      } else if (balanceMax) {
        filter['balance.payments.total'] = {
          $lte: balanceMax
        }
      }

      if (balanceMin && balanceMax) {
        filter['balance.payments.total'] = {
          $gte: balanceMin,
          $lte: balanceMax
        }
      }

      if (sort) {
        if (sort === 'lastName') {
          sortDoc = { lastName_lower: 1 }
        } else if (sort === 'secondLastName') {
          sortDoc = { secondLastName_lower: 1 }
        } else if (sort === 'name') {
          sortDoc = { name_lower: 1 }
        }
      } else {
        sortDoc = { debt: -1 }
      }

      const result = await StudentModel
        .find(filter)
        .skip(skip)
        .limit(limit)
        .sort(sortDoc)
        .then(students => {
          return {
            success: true,
            students
          }
        })
        .catch(err => {
          console.error('- Error trying to get all students.', JSON.stringify(err, null, 2))

          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to get all students'
            }
          }


        })

      if ('success' in result && result.success) {
        const sts = await Promise.all(
          result.students.map(st => st.addGroupsInfo())
        )
        return {
          success: true,
          students: sts
        }
      }
      return result

    } catch (err) {
      const errorExt = errMsg
        .generalGet('userTalks')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  /**
   * create
   *
   * @description: Creates a new Item
   * @param {object} data - List of attributes.
   */
  create: async data => {
    // Search by email

    try {

      if (data.email) {
        const student = await Students.findByEmail(data.email)
        if (student.success) {
          return {
            success: false,
            error: {
              httpCode: 500,
              message: `Student already registered ${data.email}`

            }
          }
        }
      }

      if (data.password) {
        data.hashed_password = await bcrypt.hash(data.password, 10)
        delete data.password
      }

      // create fields for searching
      if (data['name']) {
        data['name_lower'] = data['name'].toLowerCase()
      }

      if (data['email']) {
        data['email_lower'] = data['email'].toLowerCase()
      }

      if (data['lastName']) {
        data['lastName_lower'] = data['lastName'].toLowerCase()
      }

      if (data['name'] && data['lastName']) {
        data['fullName_lower'] = `${data['name']} ${data['lastName']}`
      }

      const student = new StudentModel(data)
      const result = await student.save()
        .then(student => {
          return {
            success: true,
            student
          }
        })
        .catch(err => {
          console.error('- Error trying to create a student', JSON.stringify(err, null, 2))
          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- Error trying to create a student'
            }
          }
        })

      return result

    } catch (err) {
      const errorExt = errMsg
        .generalCreate('userTalks')
      return {
        success: false,
        error: errorExt
      }
    }

  },

  find: async (q, skip, limit) => {
    const result = await StudentModel
      .find({
        $or: [
          { email_lower: new RegExp(q, 'i') },
          { lastName_lower: new RegExp(q, 'i') },
          { name_lower: new RegExp(q, 'i') },
          { phoneNumber: new RegExp(q, 'i') },
          { fullName_lower: new RegExp(q, 'i') }
        ]
      })
      .skip(skip)
      .limit(limit)
      .then(students => {
        return {
          success: true,
          students
        }
      })
      .catch(err => {
        console.error('- Error trying to get all students.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  changeGroups: async (studentId, groupId, data, force = false) => {
    const student = await StudentModel.findOne({ _id: studentId })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${studentId}`
      }
    }
    const oldGroup = await GroupModel.findOne({ _id: groupId })
    if (!oldGroup) {
      return {
        success: false,
        code: 404,
        error: `Group not found: ${groupId}`
      }
    }

    const oldStudentList = await StudentListModel.findOne({ _id: oldGroup.studentList })
    if (!oldStudentList) {
      return {
        success: true,
        code: 500,
        error: `Group without studentlist`
      }
    }

    if (!data.group || !data.group.id) {
      return {
        success: false,
        code: 400,
        error: `No new group data`
      }
    }

    const newGroup = await GroupModel.findOne({ _id: data.group.id })
    if (!newGroup) {
      return {
        success: false,
        code: 404,
        error: `New group not found: ${data.group.id}`
      }
    }

    if (String(newGroup.course) !== String(oldGroup.course)) {
      await PaymentModel
        .updateMany({
          student: studentId,
          course: oldGroup.course
        }, {
          $set: {
            course: newGroup.course
          }
        })
    }

    const groupIndex = student.groups.indexOf(groupId)
    if (groupIndex === -1) {
      return {
        success: false,
        code: 400,
        error: `Student don't enrolled in this group`
      }
    }

    // new studentList
    const studentList = await StudentListModel.findOne({ _id: newGroup.studentList })
    if ((newGroup.quota <= studentList.list.length) && !(force === 'true')) {
      const errorExt = errMsg
        .getQuotaExceded()
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }

    studentList.list.push(student._id)
    studentList.material.push({
      student: student._id,
      material: false
    })

    student.groups.splice(groupIndex, 1)
    student.groups.push(newGroup._id)

    const studentIndex = oldStudentList.list.indexOf(student._id)
    oldStudentList.list.splice(studentIndex, 1)

    if (oldStudentList.material && oldStudentList.material.length) {
      for (let i = 0; i < oldStudentList.material.length; i++) {
        if (
          String(oldStudentList.material[i].student) ===
          String(student._id)
        ) {
          oldStudentList.material.splice(i, 1)
        }
      }
    }

    await student.save()
    await student.saveBalance()
    await oldStudentList.save()
    await studentList.save()

    return {
      success: true,
      student
    }
  },

  getInfo: async id => {
    let groups = []
    let courses = []

    const student = await StudentModel
      .findOne({
        _id: id
      })
    if (!student) {
      return {
        success: false,
        code: 404,
        error: `Student not found: ${id}`
      }
    }

    if (student.groups) {
      groups = await GroupModel.find({
        _id: { $in: student.groups }
      })

      const courseIds = groups.map(group => group.course)
      courses = await CourseModel.find({
        _id: { $in: courseIds }
      })
    }

    const studentToBalance = await student.toBalance()

    return {
      success: true,
      attendance: 98.5,
      student: studentToBalance,
      groups,
      courses
    }
  },

  updateProfile: async (id, data, stream = null, ext = null) => {
    const student = await StudentModel
      .findOne({ _id: id })
    if (!student) {
      const errorExt = errMsg
        .getNotEntityFound('Student', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    return {
      success: true,
      message: 'putme'
    }
  },

  getByCourse: async (courseId) => {
    return GroupModel
      .find({ course: courseId })
      .select('studentList')
      .then(groups => groups.map(group => group.studentList))
      .then(studentLists => {
        return Promise
          .all(studentLists.map(stList => {
            return StudentListModel
              .findOne({ _id: stList })
              .select('list')
          }))
      })
      .then(stLists => {
        return stLists
          .filter(stList => stList)
          .map(stList => stList.list)
      })
      .then(lists => {
        let stIds = []
        lists.forEach(list => {
          list.forEach(stId => {
            stIds.push(stId)
          })
        })
        return _.uniq(stIds)
      })
      .then(stIds => {
        return Promise
          .all(stIds.map(stId => {
            return StudentModel
              .findOne({ _id: stId })
          }))
      })
      .then(students => {
        return Promise
          .all(students.map(student => {
            return student.toBalance()
          }))
      })
  },

  getByGroup: async (groupId) => {
    return GroupModel
      .findOne({ _id: groupId })
      .select('studentList')
      .then(group => group.studentList)
      .then(studentListId => {
        return StudentListModel
          .findOne({ _id: studentListId })
          .select('list')
      })
      .then(studentList => studentList.list)
      .then(stIds => {
        return Promise
          .all(stIds.map(stId => {
            return StudentModel
              .findOne({ _id: stId })
          }))
      })
      .then(students => {
        return Promise
          .all(students.map(student => {
            return student.toBalance()
          }))
      })
  },

  notificationFilter: async (data) => {
    let students = []
    if (data.groups) {
      const groupStudents = await Promise
        .all(data.groups.map(group => {
          return Students.getByGroup(group)
        }))
        .then(students => {
          const sts = []
          students.forEach(list => {
            list.forEach(student => {
              sts.push(student)
            })
          })
          return sts
        })
      groupStudents.forEach(student => {
        students.push(student)
      })
    }
    return {
      success: true,
      students
    }
  },

  /**
  * getById
  *
  * @description: Return a single item by id
  * @param id {string} - item id
  */


  update: async (id, data) => {

    try {

      const user = await UserTalksModel.findOne({ _id: id })
      if (!user) {

        const errorExt = errMsg
          .getNotEntityFound('UsersTalks', id)
        return {
          success: false,
          error: errorExt
        }

      }
      const result = await UserTalksModel
        .findOneAndUpdate({
          _id: id
        }, {
          $set: data
        }, {
          new: true
        })
        .lean()
        .then(userTalks => {
          return {
            success: true,
            userTalks
          }
        })
        .catch(err => {
          console.error('- Error trying to update a userTalk', JSON.stringify(err, null, 2))

          return {
            success: false,
            error: {
              httpCode: 500,
              message: '- DB Error trying to update a userTalk'
            }
          }

        })
      if (result && result.success) {
        const talksComments = await CommentModel.find({
          userTalks: id
        })
          .populate({
            path: "user",
            select: "name lastName"
          })
          .populate({
            path: "editions",
            populate: {
              path: "user",
              select: "name lastName"
            }
          })
          .sort({ userTalk: 1 })
          .lean()
        const newResult = {
          success: true,
          userTalks: {
            ...result.userTalks,
            comments: talksComments
          }
        }
        return newResult;
      } else {
        return result;
      }
    } catch (err) {
      const errorExt = errMsg
        .generalUpdate('usersTalks')
      return {
        success: false,
        error: errorExt
      }
    }


  },

  getById: async id => {
    const student = await UserTalksModel
      .findOne({
        _id: id,
        deleted: { $ne: true }
      })
      .then(student => {
        if (student) {
          return {
            success: true,
            student
          }
        }
        return {
          success: false,
          code: 404,
          error: `Student not found ${id}`
        }
      })
      .catch(err => {
        console.error('- Error trying to get a single student.', JSON.stringify(err, null, 2))
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return student
  },

  delete: async id => {
    try {
      const result = await UserTalksModel.deleteOne({ _id: id });
      
      if (result.deletedCount > 0) {
        // remove from studentlists
        await TalkStudentListModel.updateMany(
          { list: id },
          { $pull: { list: id } }
        );

        return {
          success: true,
          deleted: true
        };
      } 
      
      const errorExt = errMsg.getNotEntityFound('usersTalk', id);
      return {
        success: false,
        error: errorExt
      };

    } catch (err) {
      console.error('- Error trying to delete a usersTalk', JSON.stringify(err, null, 2));
      return {
        success: false,
        error: {
          httpCode: 500,
          message: '- DB Error trying to delete a usersTalk'
        }
      };
    }
  },
  
  talkRegister: async (groupId, data) => {
    const talk = await TalksModel.findOne({ _id: groupId })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', groupId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }
    // talk.isFeria = true
    const studentList = await TalkStudentListModel
      .findOne({ _id: talk.talkStudentList })

    const available = (talk.quota > studentList.list.length)
      ? talk.quota - studentList.list.length
      : 0

    data['name_lower'] = data.name.toLowerCase()
    data['email_lower'] = data['email'].toLowerCase()
    data['lastName_lower'] = data['lastName'].toLowerCase()

    if (data.lastName) {
      data['secondLastName_lower'] = data['secondLastName'].toLowerCase()
    }

    if (data['name'] && data['lastName']) {
      data['fullName_lower'] = `${data['name']} ${data['lastName']}`
    }

    if (data['freeTestResult']) {
      data['freeTestResult'] = data['freeTestResult']
    }

    const userTalksId = new mongoose.Types.ObjectId()
    data['_id'] = userTalksId

    let date = new Date();
    data['registerDate'] = date

    const userTalks = new UserTalksModel(data)
    await userTalks.save()
    if (available <= 0) {
      return {
        success: false,
        code: 400,
        error: `Grupo sin lugares disponibles`
      }
    }
    let list = new Set(studentList.list)
    list.add(data['_id'])
    studentList.list = [...list]
    await studentList.save()

    await userTalks.save()

    const classRoom = await ClassRoomModel
      .findOne({ _id: talk.classRoom }).lean()
    if (String(classRoom.location) !== "5f1221fb566b9f5a6abb6fec") {
      const location = await LocationModel
        .findOne({ _id: classRoom.location }).lean()
      moment.locale('es');
      let formatedDate = moment(talk.date).format("DD/MM/YYYY")
      let prov = moment(talk.schedules[0].startHour, "HHmm").format('hh:mm A')
      formatedDate = `${moment(talk.date).format('dddd')} ${formatedDate} ${prov}`
      let direction = `${location.street} ${location.number}, Col. ${location.colonia} ${location.district} ${location.city}`

      new_direction = [`${location.street} ${location.number}`, `Col. ${location.colonia}`, `${location.district} ${location.city}`, `CP ${location.postalCode}`, `${location.latitude}`, `${location.longitude}`]
      if(classRoom.location == '675a067efd2513b9bd2c5766'){
        new_direction.push('\nAdentro de COAPLAZA')
        new_direction.push('\nDeberás llegar con 30 minutos de anticipación, la plática será dentro de COAPLAZA, estamos a un costado de Citibanamex.');
      }else if(classRoom.location =='6765d1d2adae464e58a8fd72'){
        new_direction.push('')
        new_direction.push('\nDeberás llegar con 30 minutos de anticipación, recuerda que no hay estacionamiento.')
      }else{
        new_direction.push('')
        new_direction.push('\nDeberás llegar con 30 minutos de anticipación, recuerda que no hay estacionamiento. Saliendo de metro Politécnico encontrarás compañeros guías para orientarles.');
      }


      const generator = new GeneratePDF()

      let b64Data = ''
      let fichero = ''

      if(talk.filter == "Casa del terror"){
         b64Data = await generator.generateTalkTerror(userTalks, talk, new_direction)
        fichero = 'ComprobanteDeRegistro.pdf'
        }
       else if(talk.filter == "Orientación Bachillerato"){

         b64Data = await generator.generateTalkBachillerato(userTalks, talk, new_direction)
        fichero = 'ComprobanteBachillerato.pdf'

       } 
        
        
        else{
         b64Data = await generator.generateTalk(userTalks, talk, new_direction)
        fichero = 'ComprobantePlatica.pdf'
        }

  




      

      const sendGridData = {
        email: userTalks.email,
        name: userTalks.name,
        dateAndHour: formatedDate,
        locationName: location.name,
        locationDirection: direction,
        attachments: [
          {
            content: b64Data,
            filename: fichero,
            type: 'application/pdf'
          }
        ],
      }
      
     
      if(talk.isFeria){
        await SendGrid.sendRegisterToTalkTerror(sendGridData)
       }else{
        await SendGrid.sendRegisterToTalk(sendGridData)
       }




    } else {

      const generator = new GeneratePDF()

      const b64Data = await generator.generateLandingCourse(userTalks)

      let fichero = 'FichaRegistro.pdf'

      const sendGridData = {
        email: userTalks.email,
        name: userTalks.name,
        message: "¡Felicidades " + userTalks.name + "! \n \nTe registraste con éxito en alguno de nuestros cursos de Preparación IPN. Realiza tu pago por transferencia o depósito bancario",
        attachments: [
          {
            content: b64Data,
            filename: fichero,
            type: 'application/pdf'
          }
        ],
      }
      await SendGrid.sendLandingCourse(sendGridData)
    }




    return {
      success: true,
      talk,
      userTalks
    }
  },


  resendTalk: async (talkId, usersId) => {
    const talk = await TalksModel.findOne({ _id: talkId })
    if (!talk) {
      const errorExt = errMsg
        .getNotEntityFound('Talk', talkId)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }


    const classRoom = await ClassRoomModel
      .findOne({ _id: talk.classRoom })


    if (String(classRoom.location) !== "5f1221fb566b9f5a6abb6fec") {
      const location = await LocationModel
        .findOne({ _id: classRoom.location })
      moment.locale('es');
      let formatedDate = moment(talk.date).format("DD/MM/YYYY")
      let prov = moment(talk.schedules[0].startHour, "HHmm").format('hh:mm A')
      formatedDate = `${moment(talk.date).format('dddd')} ${formatedDate} ${prov}`
      let direction = `${location.street} ${location.number}, Col. ${location.colonia} ${location.district} ${location.city}`

      new_direction = [`${location.street} ${location.number}`, `Col. ${location.colonia}`, `${location.district} ${location.city}`, `CP ${location.postalCode}`, `${location.latitude}`, `${location.longitude}`]

      const generator = new GeneratePDF()

      for (let i = 0; i < usersId.usersTalk.length; i++) {
        const userTalks = await UserTalksModel.findOne({ _id: usersId.usersTalk[i] })

        const b64Data = await generator.generateTalk(userTalks, talk, new_direction)

        let fichero = 'ComprobantePlatica.pdf'

        const sendGridData = {
          email: userTalks.email,
          name: userTalks.name,
          dateAndHour: formatedDate,
          locationName: location.name,
          locationDirection: direction,
          attachments: [
            {
              content: b64Data,
              filename: fichero,
              type: 'application/pdf'
            }
          ],
        }

        await SendGrid.sendRegisterToTalk(sendGridData)
      }

    } else {

      const generator = new GeneratePDF()

      for (let i = 0; i < usersId.usersTalk.length; i++) {
        const userTalks = await UserTalksModel.findOne({ _id: usersId.usersTalk[i] })

        const b64Data = await generator.generateLandingCourse(userTalks)

        let fichero = 'FichaRegistro.pdf'

        const sendGridData = {
          email: userTalks.email,
          name: userTalks.name,
          message: "¡Felicidades " + userTalks.name + "! \n \nTe registraste con éxito en alguno de nuestros cursos de Preparación IPN. Realiza tu pago por transferencia o depósito bancario",
          attachments: [
            {
              content: b64Data,
              filename: fichero,
              type: 'application/pdf'
            }
          ],
        }
        await SendGrid.sendLandingCourse(sendGridData)
      }
    }

    return {
      success: true,
      message: "El/Los comprobante(s) fue(ron) enviado(s)."
    }
  },

  changeGroups: async (id, groupId, data, force = false, socket = undefined) => {
    const userTalks = await UserTalksModel.findOneToTalk({ _id: id })
    if (!userTalks) {
      return {
        success: false,
        code: 404,
        error: `userTalks not found: ${id}`
      }
    }
    const oldGroup = await TalksModel.findOne({ _id: groupId })
    if (!oldGroup) {
      return {
        success: false,
        code: 404,
        error: `Talk not found: ${groupId}`
      }
    }
    const oldStudentList = await TalkStudentListModel.findOne({ _id: oldGroup.talkStudentList })
    if (!oldStudentList) {
      return {
        success: true,
        code: 500,
        error: `Talk without studentlist`
      }
    }

    if (!data.talk || !data.talk.id) {
      return {
        success: false,
        code: 400,
        error: `No new Talk data`
      }
    }
    const newGroup = await TalksModel.findOne({ _id: data.talk.id })
    if (!newGroup) {
      return {
        success: false,
        code: 404,
        error: `New group not found: ${data.talk.id}`
      }
    }
    // new studentList
    const studentList = await TalkStudentListModel.findOne({ _id: newGroup.talkStudentList })
    if ((newGroup.quota <= studentList.list.length) && !(force === 'true')) {
      const errorExt = errMsg
        .getQuotaExceded()
      return {
        success: false,
        code: 400,
        error: errorExt.error,
        errorExt
      }
    }

    studentList.list.push(userTalks._id)
    const studentIndex = oldStudentList.list.indexOf(userTalks._id)
    oldStudentList.list.splice(studentIndex, 1)

    await oldStudentList.save()
    await studentList.save()

    //Change comments id to new studentlist 

    await CommentsModel.updateMany({
      talk: groupId,
      userTalks: userTalks._id
    }, {
      talk: data.talk.id
    })
    if (socket) {
      socket.to('update_table_event').emit("update_table_event", {
        event: "remove_from_talk",
        studentId: userTalks._id,
        userTalks: userTalks
      })
    }
    return {
      success: true,
      userTalks
    }
  },

  // TODO add custom error responses with new structure.

  registerFreeTest: async (data, talkId) => {
    console.log(talkId);
    const idTest = data.testId;
    const userTalk = data.userTalk;
    const quizAnswers = data.quizAnswers;
    const idTalk = talkId !== undefined ? talkId : "625e2aa3386b1312b07438d4";

    const quiz = await QuizModel.findOne({
      _id: idTest
    })
      .populate({
        path: "questionCollection",
        populate: "questionImage questionVideo explanationVideo",

      })
      .lean()
    if (!quiz) {
      return {
        success: false,
        code: 404,
        error: `The quiz you are trying to answer is not longer available`
      }
    }

    const questionCollection = quiz.questionCollection;
    const globalAnswerCollection = new Array();

    let rightAnswer = 0

    for (let i = 0; i < questionCollection.length; i++) {
      const item = questionCollection[i];
      const index = quizAnswers.findIndex(elemento => String(elemento.idQuizQuestion) == String(item._id));
      if (index >= 0) {
        const idQuestion = item._id;
        let isCorrectQuestion = false;

        const responseAnswers = quizAnswers[index].questionAnswers;
        const questionAnswers = item.answerCollection.filter(insideItem => insideItem.validAnswer).map(itemMap => itemMap.idAnswer);
        if (responseAnswers && questionAnswers && responseAnswers.length == questionAnswers.length) {
          let correctAnswerInArray = 0;
          for (let j = 0; j < responseAnswers.length; j++) {
            const userResponse = responseAnswers[j];
            const indexResponse = questionAnswers.indexOf(userResponse);
            if (indexResponse >= 0) {
              correctAnswerInArray++;
            }
          }
          if (correctAnswerInArray === responseAnswers.length) {
            rightAnswer++;
            item.isCorrect = true;
          } else {
            item.isCorrect = false;
          }
        } else {
          item.isCorrect = false;
        }
        globalAnswerCollection.push({
          answers: responseAnswers,
          answerCorrects: questionAnswers,
          question: idQuestion,
          isCorrect: isCorrectQuestion
        })
      }
    }

    const quizAttempt = new Object();
    const percentage = ((rightAnswer * 100) / questionCollection.length);
    quizAttempt.status = "CLOSE";
    quizAttempt.percentageAnswer = 100;
    quizAttempt.percentageResult = percentage;
    quizAttempt.answerCollection = globalAnswerCollection;
    quizAttempt.questionCollection = questionCollection;
    quizAttempt.totalQuestions = questionCollection.length;
    quizAttempt.rightQuestions = rightAnswer;

    userTalk.freeTestResult = percentage;
    const responseRegister = await UsersTalks.talkRegister(idTalk, userTalk);
    if (!responseRegister) {
      return {
        success: false,
        code: 404,
        error: `The Talk you want to register is no longer available, sorry.`
      }
    }

    return {
      success: true,
      userTalk: responseRegister,
      quizAttempt,
    }
  }

}

module.exports = UsersTalks
