/**
 * models/stages.js
 *
 * @description :: Describes the stages functions
 * @docs        :: TODO
 */
const StageModel = require('../db/stage')

const ErrMsg = require('../lib/errMsg')
const errMsg = new ErrMsg()

const Stage = class Stage {
  /**
   * get
   * @description Get all the stages
   * @returns Object with the list of all stages
   */
  async get () {

try{

    const stages = await StageModel
      .find({})
    return {
      success: true,
      stages
    }
  } catch (err) {
    const errorExt = errMsg
      .generalGet('stages')
    return {
      success: false,
      error: errorExt
    }
  }

  }

  /**
   * getById
   * @description Get a single stage by id
   * @param {string} id Stage object id
   * @returns {object} The stage object
   */
  async getById (id) {

    const stage = await StageModel
      .findOne({ _id: id })
    if (!stage) {
      const errorExt = errMsg
        .getNotEntityFound('Stages', id)
      return {
        success: false,
        code: 404,
        error: errorExt.error,
        errorExt
      }
    }

    return {
      success: true,
      stage
    }
  }

  /**
   * create
   * @description Create a new stage
   * @param {object} data New Stage data.
   * @returns {object} The operation result
   */
  async create (data) {
    try{
    const stage = new StageModel(data)
    await stage.save()
    return {
      success: true,
      stage
    }

  } catch (err) {
    const errorExt = errMsg
      .generalCreate('stages')
    return {
      success: false,
      error: errorExt
    }
  }
  }

  /**
   * update
   * @description Update a single stage
   * @param {string} id Stage object id
   * @param {object} data New data to update
   */
  async update (id, data) {
try{

    if (data.id) {
      delete data.id
    }

    const stage = await StageModel
      .findOneAndUpdate({
        _id: id
      }, {
        $set: data
      }, {
        newDocument: true,
        upsert: false
      })

    if (!stage) {
      const errorExt = errMsg
        .getNotEntityFound('Stages', id)
        return {
          success: false,
          error: errorExt
        }
    }

    return {
      success: true,
      stage
    }

  } catch (err) {
    const errorExt = errMsg
      .generalUpdate('stages')
    return {
      success: false,
      error: errorExt
    }
  }
  }

  /**
   * delete
   * @description Removes a single stage
   * @param {string} id Stage object id
   */
  async delete (id) {

    try{
    const stage = await StageModel
      .findOneAndDelete({ _id: id })
      
    if (!stage) {
      const errorExt = errMsg
        .getNotEntityFound('Stages', id)
        return {
          success: false,
          error: errorExt
        }
    }

    return {
      success: true,
      deleted: true
    }

    
  } catch (err) {
    const errorExt = errMsg
      .generalDelete('stages')
    return {
      success: false,
      error: errorExt
    }
  }
  }
}

module.exports = Stage
