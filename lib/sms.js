/**
 * lib/sms.js
 *
 * @description :: Sends sms
 * @docs        :: TODO
 */
'use strict'
const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid');

const {
  awsSMSAccessKeyId,
  awsSMSSecretAccessKey,
  awsSMSRegion
} = require('../config')

const prefix = '[PREPARACIONIPN]'

class SMS {
  constructor() {
    this.config = {
      accessKeyId: awsSMSAccessKeyId,
      secretAccessKey: awsSMSSecretAccessKey,
      region: awsSMSRegion
    }
    AWS.config.update(this.config)
    this.sns = new AWS.SNS()
  }

  async sendMessage(sender, phoneNumber, subject, message = 'prepIPN') {
    console.log(sender, phoneNumber, message)
    return new Promise((resolve, reject) => {
      const params = {
        Message: `${prefix} ${message}`,
        PhoneNumber: String(phoneNumber),
        Subject: subject,
        MessageStructure: 'String',
        MessageAttributes: {
          'AWS.SNS.SMS.SMSType': {
            DataType: 'String',
            StringValue: 'Promotional'
          },
          'AWS.SNS.SMS.SenderID': {
            DataType: 'String',
            StringValue: 'PIPN'
          }
        }
      }

      this.sns.publish(params, (err, data) => {
        if (err) {
          reject(err)
        } else {
          console.log(sender, phoneNumber, message)
          console.log(data)
          resolve(data)
        }
      })
    })
  }

  async sendBulkMessage(usersObject, messageToSend) {

    const numberOfStudents = usersObject.length;
    console.log("Students", numberOfStudents)
    const requestNumber = (numberOfStudents / 250) % 1 !== 0 ? Math.floor(numberOfStudents / 250) + 1 : Math.floor(numberOfStudents / 250);
    console.log("Request", requestNumber)
    for (let i = 0; i < requestNumber; i++) {
      console.log("REQUEST:", i);
      const start = 250 * i;
      const end = 250 * (i + 1);
      const students = [];
      for (let j = start; j < end && j < numberOfStudents; j++) {
        students.push(usersObject[j]);
      }
      // This will generate a new topic with a random name and return the new topic arn
      console.log("Executing sendBulkMessage");
      const TopicArn = await this.createTopic(uuidv4());
      console.log("Topic ARN:", TopicArn);
      const ArnSuscibedUserArray = await this.addUsersToTopic(TopicArn, students);
      console.log("Users added to topic");
      await this.sendMessageToTopic(TopicArn, messageToSend);
      //this will send message to the topic 
      await this.deleteUsersSubscriptions(ArnSuscibedUserArray);
      await this.deleteTopic(TopicArn);
    }
    return {
      success: true
    };
  }

  // This will return a topic Arn to suscribe the users and then send the bulk Notification
  async createTopic(nameTopic) {
    try {
      const createTopic = await this.sns.createTopic({
        Name: nameTopic,
      })
        .promise();
      return createTopic.TopicArn;
    } catch (e) {
      console.log("error", e);
    }
  }

  // This line will delete the topic 
  async deleteTopic(topicArn) {
    const deleteTopic = await this.sns.deleteTopic({
      TopicArn: topicArn
    })
      .promise();
    console.warn("Topic Deleted?", deleteTopic);
  }

  //This function add the array of phones to the topic
  async addUsersToTopic(topicArn, ArrayUsersObject) {
    const promiseMap = ArrayUsersObject.map(user => {
      const phone = '+521' + (user.phoneNumber !== undefined || user.phoneNumber !== null ? String(user.phoneNumber) : String(user.secondPhoneNumber));

      let myrge = /^(\+521)[1-9]\d{2}\d{3}\d{4}$/;
      var validation = myrge.test(phone)

      if(validation){
        const params = {
          Endpoint: phone,
          Protocol: "sms",
          TopicArn: topicArn,
          ReturnSubscriptionArn: true
        }
        return this.sns.subscribe(params).promise();
      }

    });


    return await Promise.all(promiseMap).then(results => {
      console.warn("Output of promise All", results);
      const arraySubscriptionId = [];

      results.forEach(item => {
        if (item && item.SubscriptionArn) {
          arraySubscriptionId.push(item.SubscriptionArn);
        }
      });
      return arraySubscriptionId;
    })
  }

  async deleteUsersSubscriptions(arraySubscriptionId) {
    const deletePromise = arraySubscriptionId.map(item => {
      console.log("UNSUSCRIBING TO", item);
      return this.sns.unsubscribe({SubscriptionArn:item}).promise();
    })
    return await Promise.all(deletePromise).then(response => {
      console.warn("PROMISE ALL", response);
      return response;
    })
  }

  // This Function Send the message to a topic
  async sendMessageToTopic(topicArn, message) {
    const params = {
      Message: `${prefix} ${message}`,
      TopicArn: topicArn,
      Subject: "Bulk Message",
      MessageStructure: 'String',
      MessageAttributes: {
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Promotional'
        },
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'PIPN'
        }
      }
    }
    const toReturn = await this.sns.publish(params).promise();
    console.log("sendMessageToTopic", toReturn);
    return toReturn;
  }

  //Helper to legacy function
  toString() {
    return this.config
  }


}

module.exports = SMS
