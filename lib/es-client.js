/**
 * lib/es-client.js
 *
 * @description :: create an elasticsearch client
 * @docs        :: TODO
 *              ::
 */
const elasticsearch = require('elasticsearch')

const { esHost, esPort, esLog } = require('../config')

const getElasticInstance = function () {
  const client = new elasticsearch.Client({
    host: `${esHost}:${esPort}`,
    log: ['error', 'warning']
  })

  client.ping({
    requestTimeout: 30000
  }, function (error) {
    if (error) {
      console.log('%%%%%%%%%%%%%%%%%%%')
      console.error('elasticsearch cluster is down!')
      console.log(`Host: ${esHost}`)
      console.log('%%%%%%%%%%%%%%%%%%%')
    } else {
      console.log('%%%%%%%%%%%%%%%%%%%')
      console.log('All is well')
      console.log('%%%%%%%%%%%%%%%%%%%')
    }
  })

  return client
}

module.exports = getElasticInstance
