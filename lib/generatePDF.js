/**
 * lib/generatePDF.js
 *
 * @description :: generates PDF
 * @docs        :: TODO
 */

const moment = require('moment')
const os = require('os')
const fs = require('fs')
const path = require('path')

const base64Logo = require('./base64Logo')

// const { generateQRWithLogo, generateQRWithoutLogo } = require('../lib/qrcode');
const PdfPrinter = require('pdfmake');


function base64_encode(file) {
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer.from(bitmap).toString('base64');
}


moment.locale('es');

const GeneratePDF = class {
  constructor() {
    this.base64Logo2 = base64Logo
    this.basics = {
      pageSize: 'LETTER',
      info: {
        author: 'Preparación Politécnico',
        subject: 'Other'
      },
      content: [],
      styles: {
        header: {
          fontSize: 22,
          bold: true
        },
        text: {
          fontSize: 15,
          alignment: 'justify'
        },

        bold: {
          fontSize: 15,
          bold: true
        },
        boldU: {
          fontSize: 10,
          bold: true
        },
        boldRed: {
          fontSize: 15,
          bold: true,
          color: 'red'
        },
        link: {
          fontSize: 15,
          color: 'blue'
        },
        image: {
          alignment: 'center'
        },
        table: {
          width: '100%'

        }
      },
      defaultStyle: {
        font: 'Helvetica'
      }
    }
    this.fonts = {

      Roboto: {
        normal: 'node_modules/roboto-font/fonts/Roboto/roboto-regular-webfont.ttf',
        bold: 'node_modules/roboto-font/fonts/Roboto/roboto-bold-webfont.ttf',
        italics: 'node_modules/roboto-font/fonts/Roboto/roboto-italic-webfont.ttf',
        bolditalics: 'node_modules/roboto-font/fonts/Roboto/roboto-bolditalic-webfont.ttf'
    },

      Helvetica: {
        normal: 'Helvetica',
        bold: 'Helvetica-Bold',
        italics: 'Helvetica-Oblique',
        bolditalics: 'Helvetica-BoldOblique'
      }
    }
    this.printer = new PdfPrinter(this.fonts)
    this.tmpDir = os.tmpdir()

    this.formatHour = (hour) => {
      let arrHour = hour.split('')
      arrHour.splice(-2, 0, ':')
      const str = arrHour.join('')
      const arrStr = str.split(':')
      return {
        string: str,
        minutes: arrStr[1],
        hours: arrStr[0]
      }
    }
    this.days = [
      'Lunes', 'Martes', 'Miércoles',
      'Jueves', 'Viernes', 'Sábado',
      'Domingo'
    ]
  }





  /**
   * generatePayment
   * @param {object} student Student data
   * @param {string} title Document title
   */
  async generatePayment(student, title) {

    const id = JSON.stringify(student._id)
    //let qr = await generateQRWithoutLogo(id, { errorCorrectionLevel: 'H' })
    const opts = this.basics
    opts.info['title'] = title
    opts.info['keywords'] = 'comprobante'

    opts.content.push({
      image: this.base64Logo2,
      width: 140,
      style: 'image'
    })

    opts.content.push({
      text: '\nComprobante de liquidación',
      style: 'header'
    })

    opts.content.push({
      text: '\n'
    })

    const printer = new PdfPrinter(this.fonts)

    const pdfDoc = printer.createPdfKitDocument(opts)
    pdfDoc.pipe(fs.createWriteStream('document.pdf'))
    pdfDoc.end()

    return opts
  }

  async generateTalkDepre(student, time) {
    let opts = this.basics

    opts.styles = {
      header: {
        fontSize: 18,
        margin: [10, 0, 10, 0]
      },
      text: {
        fontSize: 11,
        alignment: 'justify',
        margin: [10, -5, 10, -5],
        lineHeight: 1.5
      },

      bold: {
        fontSize: 11,
        bold: true
      },
      boldRed: {
        fontSize: 11,
        bold: true,
        margin: [10, 0, 10, 0]
      },
      link: {
        fontSize: 11,
        color: 'blue',
        margin: [10, 0, 10, 0]
      },
      image: {
        alignment: 'center'
      },
      imageQR: {
        alignment: 'right',
      },
    }
    opts.info['title'] = 'Ficha Plática'
    opts.info['keywords'] = 'Ficha'

    opts.content.push({
      layout: 'noBorders',
      table: {
        headerRows: 1,
        widths: [550, '*', '*', '*'],
        body: [
          [{ image: this.base64Logo2, width: 170, style: 'image' },
          { qr: String(student._id), fit: '100', style: 'imageQR' }],


        ]
      }
    })

    opts.content.push({
      text: '\nFicha de registro\n',
      style: 'header'
    })
    opts.content.push({
      text: [
        '\n\nHola ',
        {
          text: student.name + ' ' + student.lastName,
          style: 'bold'
        }],
      style: 'text'
    })

    opts.content.push({
      text: ['\n\nTe has registrado correctamente para asistir a nuestra plática informativa el día ', { text: time, style: 'boldRed' }, ' en Preparación Politécnico. Asiste y realiza un examen simulacro. Aprenderás técnicas para resolverlo más rápido. ', { text: "La plática durará 2:15 horas.", style: 'boldRed' }],
      style: 'text'
    })

    opts.content.push({
      text: [{ text: '\n\nSe tomarán todas las medidas sanitarias sin excepción alguna:\n', style: 'bold' }, '- Toma de temperatura\n- Aplicación de gel antibacterial\n- Sana distancia\n- Uso obligatorio de cubre bocas y de ser posible, careta'
      ],
      style: 'text'
    })

    opts.content.push({
      text: [{ text: '\n\nApunta nuestra dirección:', style: 'bold' }, '\nCalle Manuel Luis Stampa #37,\nCol. Nueva Industrial Vallejo\nGustavo A. Madero, Ciudad de México\nCP. 07700\n'],
      style: 'text'
    })

    opts.content.push({
      text: [{ text: '\nGoogle Maps: ', style: 'bold' }, { text: "https://maps.app.goo.gl/x7yUKKaAqWqFMq5A6", style: 'link', link: 'https://maps.app.goo.gl/x7yUKKaAqWqFMq5A6' }],
      style: 'text', lineHeight: 1
    })
    opts.content.push({
      text: '\n\n\nIndicaciones finales:',
      style: 'boldRed'
    })

    opts.content.push({
      text: ['\nDeberás llegar con 30 minutos de anticipación, recuerda que no hay estacionamiento. Saliendo de metro Politécnico encontrarás compañeros guías para orientarles'],
      style: 'text'
    })

    opts.content.push({
      text: '\n\n\nCualquier duda o inconveniente puedes comunicarte al (55) 8848 3185 o vía INBOX:',
      style: 'text', lineHeight: 1
    })

    opts.content.push({
      text: '\nhttps://www.facebook.com/PreparacionIPN',
      link: 'https://www.facebook.com/PreparacionIPN',
      style: 'link'
    })


    const doc = await this.getDoc(opts)
    //const b64Doc = doc.toString('base64')

    return doc
  }



  async generateTalk(userTalks, talk, direction) {
    const student = userTalks

    const id = student._id
    // const qr = await generateQRWithoutLogo(id, { errorCorrectionLevel: 'H' })
    
      moment.locale('es');
      let formatedDate = moment(talk.date).format("DD/MM/YYYY")
      let prov = moment(talk.schedules[0].startHour, "HHmm").format('hh:mm A')
      let day = `${moment(talk.date).format('dddd')}`

      let mapa = "http://www.google.com/maps/place/"+direction[4]+","+direction[5]


    let opts = {
        pageSize: 'LETTER',
        info: {
            title: 'Comprobante',
            author: 'PreparaciónIPN',
            subject: 'Platica',
            keywords: 'comprobante',
        },
        content: [],
    }
    opts.styles = {
        header: {
            fontSize: 18,
            margin: [10, 0, 10, 0]
        },
        text: {
            fontSize: 11,
            alignment: 'justify',
            margin: [10, -5, 10, -5],
            lineHeight: 1.5
        },
        bold: {
            fontSize: 11,
            bold: true
        },
        boldRed: {
            fontSize: 11,
            bold: true,
            margin: [10, 0, 10, 0]
        },
        link: {
            fontSize: 11,
            color: 'blue',
            margin: [10, 0, 10, 0]
        },
        image: {
            alignment: 'center'
        },
        imageQR: {
            alignment: 'right',
        },
    }
    opts.info['title'] = 'Ficha Plática'
    opts.info['keywords'] = 'Ficha'

    opts.content.push({
        layout: 'noBorders',
        table: {
            headerRows: 1,
            widths: [550, '*', '*', '*'],
            body: [
                [{ image: this.base64Logo2, width: 170, style: 'image' },
                { qr: String(student._id), fit: '100', style: 'imageQR' }],


            ]
        }
    })

    opts.content.push({
        text: '\nFicha de registro\n',
        style: 'header'
    })

    opts.content.push({
      text: [
          '\nFecha de la plática: ',
          {
              text: formatedDate,
              style: 'bold'
          }],
      style: 'text'
  })

    opts.content.push({
        text: [
            '\nHola ',
            {
                text: student.name + ' ' + student.lastName,
                style: 'bold'
            }],
        style: 'text'
    })

    opts.content.push({
        text: ['\nTe has registrado correctamente para asistir a nuestra plática informativa el día ', { text: day, style: 'boldRed' }, 'a las ', {text: prov, style: 'boldRed'}, ' en Preparación Politécnico. Asiste y realiza un examen simulacro, aprenderás técnicas para resolverlo más rápido. ', { text: "La plática durará 2:15 horas.", style: 'boldRed' }],
        style: 'text'
    })

    /*opts.content.push({
        text: [{ text: '', style: 'bold' }, ''
        ],
        style: 'text'
    })*/
    opts.content.push({


    })
    opts.content.push({
        text: [{ text: '\n\nApunta nuestra dirección:', style: 'bold' }],
        style: 'text'
    })
    opts.content.push({
      text: [{ text: '\n'+ direction[0] + ' ' + direction[1] + ' ' + direction[2] + ' '+ direction[3] + '\n' + direction[6]}],
      style: 'text'
  })

    opts.content.push({
        text: [{ text: '\nGoogle Maps: ', style: 'bold' }, { text: mapa, style: 'link', link: mapa }],
        style: 'text', lineHeight: 1
    })
    opts.content.push({
        text: '\n\n\nIndicaciones finales:',
        style: 'boldRed'
    })

    opts.content.push({
        text: [direction[7]],
        style: 'text'
    })

    opts.content.push({
        text: '\n\n\nCualquier duda o inconveniente puedes comunicarte al (55) 8848 3185 o vía INBOX:',
        style: 'text', lineHeight: 1
    })

    opts.content.push({
        text: '\nhttps://www.facebook.com/PreparacionIPN',
        link: 'https://www.facebook.com/PreparacionIPN',
        style: 'link'
    })

     const doc = await this.getDoc(opts)
     const b64Doc = doc.toString('base64')
     return b64Doc
}


async generateTalkTerror(userTalks, talk, direction) {
  const student = userTalks

  const id = student._id
  // const qr = await generateQRWithoutLogo(id, { errorCorrectionLevel: 'H' })
  
    moment.locale('es');
    let formatedDate = moment(talk.date).format("DD/MM/YYYY")
    let prov = moment(talk.schedules[0].startHour, "HHmm").format('hh:mm A')
    let day = `${moment(talk.date).format('dddd')}`

    let mapa = "http://www.google.com/maps/place/"+direction[4]+","+direction[5]


  let opts = {
      pageSize: 'LETTER',
      info: {
          title: 'Ficha de registro',
          author: 'Preparación politécnico',
          subject: 'Feria de carreras 2025',
          keywords: 'comprobante',
      },
      content: [],
  }
  opts.styles = {
      header: {
          fontSize: 18,
          margin: [10, 0, 10, 0]
      },
      text: {
          fontSize: 11,
          alignment: 'justify',
          margin: [10, -5, 10, -5],
          lineHeight: 1.5
      },
      bold: {
          fontSize: 11,
          bold: true
      },
      boldRed: {
          fontSize: 11,
          bold: true,
          margin: [10, 0, 10, 0]
      },
      link: {
          fontSize: 11,
          color: 'blue',
          margin: [10, 0, 10, 0]
      },
      image: {
          alignment: 'center'
      },
      imageQR: {
          alignment: 'right',
      },
  }
  opts.info['title'] = 'Ficha Plática'
  opts.info['keywords'] = 'Ficha'

  opts.content.push({
      layout: 'noBorders',
      table: {
          headerRows: 1,
          widths: [550, '*', '*', '*'],
          body: [
              [{ image: this.base64Logo2, width: 170, style: 'image' },
              { qr: String(student._id), fit: '100', style: 'imageQR' }],


          ]
      }
  })

  opts.content.push({
      text: 'Ficha de registro\n',
      style: 'header'
  })

  opts.content.push({
    text: [
        '\nFecha de asistencia:',
        {
            text: formatedDate,
            style: 'bold'
        }],
    style: 'text'
})

  opts.content.push({
      text: [
          '\nHola ',
          {
              text: student.name + ' ' + student.lastName,
              style: 'bold'
          }],
      style: 'text'
  })

  opts.content.push({
      text: ['\nTe has registrado correctamente para asistir a Feria de Carreras 2025 el ', { text: day, style: 'boldRed' }, 'a las ', {text: prov, style: 'boldRed'}, ' en Preparación Politécnico.' ],
      style: 'text'
  })


  opts.content.push({
      text: [{ text: '\nApunta nuestra dirección:', style: 'bold' }],
      style: 'text'
  })

  opts.content.push({
    text: [{ text: '\n'+ direction[0] + '\n' + direction[1] + ' ' + direction[2] + ' '+ direction[3]  }],
    style: 'text'
})

  opts.content.push({
      text: [{ text: '\nGoogle Maps: ', style: 'bold' }, { text: mapa, style: 'link', link: mapa }],
      style: 'text', lineHeight: 1
  })


  opts.content.push({
    text: [{ text: '\n\nRecomendaciones importantes:\n  ', style: 'bold' }, '- Llega con tiempo, ya que ',  { text: 'no contamos con estacionamiento', style: 'bold' },  '\n- ',  'La entrada es GRATUITA y libre para todo el público.' 
    ],
    style: 'text'
})



  opts.content.push({
      text: ['\n\n\nSi tienes alguna duda o inconveniente puedes comunicarte al 12',  { text: ' (55) 5011 8651 ', style: 'bold' },' \n o vía INBOX:'],
      style: 'text', lineHeight: 1
  })

  opts.content.push({
      text: '\n@Preparación Politécnico',
      link: 'https://www.facebook.com/PreparacionIPN',
      style: 'link'
  })
  opts.content.push({
    text: ['\n¡Te esperamos!'],
    style: 'text'
  })


   const doc = await this.getDoc(opts)
   const b64Doc = doc.toString('base64')
   return b64Doc
}



async generateTalkBachillerato(userTalks, talk, direction) {
  const student = userTalks

  const id = student._id
  // const qr = await generateQRWithoutLogo(id, { errorCorrectionLevel: 'H' })
  
    moment.locale('es');
    let formatedDate = moment(talk.date).format("DD/MM/YYYY")
    let prov = moment(talk.schedules[0].startHour, "HHmm").format('hh:mm A')
    let day = `${moment(talk.date).format('dddd')}`

    let mapa = "http://www.google.com/maps/place/"+direction[4]+","+direction[5]


  let opts = {
      pageSize: 'LETTER',
      info: {
          title: 'Ficha de registro',
          author: 'Preparación politécnico',
          subject: 'Orientación Bachillerato',
          keywords: 'comprobante',
      },
      content: [],
  }
  opts.styles = {
      header: {
          fontSize: 18,
          margin: [10, 0, 10, 0]
      },
      text: {
          fontSize: 11,
          alignment: 'justify',
          margin: [10, -5, 10, -5],
          lineHeight: 1.5
      },
      bold: {
          fontSize: 11,
          bold: true
      },
      boldRed: {
          fontSize: 11,
          bold: true,
          margin: [10, 0, 10, 0]
      },
      link: {
          fontSize: 11,
          color: 'blue',
          margin: [10, 0, 10, 0]
      },
      image: {
          alignment: 'center'
      },
      imageQR: {
          alignment: 'right',
      },
  }
  opts.info['title'] = 'Ficha Plática'
  opts.info['keywords'] = 'Ficha'

  opts.content.push({
      layout: 'noBorders',
      table: {
          headerRows: 1,
          widths: [550, '*', '*', '*'],
          body: [
              [{ image: this.base64Logo2, width: 170, style: 'image' },
              { qr: String(student._id), fit: '100', style: 'imageQR' }],


          ]
      }
  })

  opts.content.push({
      text: 'Ficha de registro\n',
      style: 'header'
  })

  opts.content.push({
    text: [
        '\nFecha de asistencia:',
        {
            text: formatedDate,
            style: 'bold'
        }],
    style: 'text'
})

  opts.content.push({
      text: [
          '\nHola ',
          {
              text: student.name + ' ' + student.lastName,
              style: 'bold'
          }],
      style: 'text'
  })

  opts.content.push({
      text: ['\nTe has registrado correctamente para asistir a Orientación Bachillerato el ', { text: day, style: 'boldRed' }, 'a las ', {text: prov, style: 'boldRed'}, ' en Preparación Politécnico.' ],
      style: 'text'
  })


  opts.content.push({
      text: [{ text: '\nApunta nuestra dirección:', style: 'bold' }],
      style: 'text'
  })

  opts.content.push({
    text: [{ text: '\n'+ direction[0] + '\n' + direction[1] + ' ' + direction[2] + ' '+ direction[3]  }],
    style: 'text'
})

  opts.content.push({
      text: [{ text: '\nGoogle Maps: ', style: 'bold' }, { text: mapa, style: 'link', link: mapa }],
      style: 'text', lineHeight: 1
  })


  opts.content.push({
    text: [{ text: '\n\nRecomendaciones importantes:\n  ', style: 'bold' }, '- Llega con tiempo, ya que ',  { text: 'no contamos con estacionamiento', style: 'bold' },  '\n- ',  'La entrada es GRATUITA y libre para todo el público.' 
    ],
    style: 'text'
})



  opts.content.push({
      text: ['\n\n\nSi tienes alguna duda o inconveniente puedes comunicarte al 12',  { text: ' (55) 5011 8651 ', style: 'bold' },' \n o vía INBOX:'],
      style: 'text', lineHeight: 1
  })

  opts.content.push({
      text: '\n@Preparación Politécnico',
      link: 'https://www.facebook.com/PreparacionIPN',
      style: 'link'
  })
  opts.content.push({
    text: ['\n¡Te esperamos!'],
    style: 'text'
  })


   const doc = await this.getDoc(opts)
   const b64Doc = doc.toString('base64')
   return b64Doc
}





async generateLandingCourse(userTalks) {
  
  var imageTicket = base64_encode('./lib/baucher.jpeg');
  imageTicket = 'data:image/jpeg;base64,'+ imageTicket

  const student = userTalks

  const id = student._id
  // const qr = await generateQRWithoutLogo(id, { errorCorrectionLevel: 'H' })
  

  let opts = {
    pageSize: 'LETTER',
    info: {
        title: 'Comprobante',
        author: 'PreparaciónIPN',
        subject: 'Other',
        keywords: 'comprobante',
    },
    content: [],
}
opts.styles = {
    header: {
        fontSize: 18,
        margin: [10, 0, 10, 0]
    },
    text: {
        fontSize: 11,
        alignment: 'justify',
        margin: [10, -5, 10, -5],
        lineHeight: 1.5
    },
    bold: {
        fontSize: 11,
        bold: true
    },
    boldRed: {
        fontSize: 11,
        bold: true,
        margin: [10, 0, 10, 0]
    },
    link: {
        fontSize: 11,
        color: 'blue',
        margin: [10, 0, 10, 0]
    },
    image: {
        alignment: 'center'
    },
    imageQR: {
        alignment: 'right',
    },
}
opts.info['title'] = 'Comprobante inscripción'
opts.info['keywords'] = 'Comprobante'

opts.content.push({
    layout: 'noBorders',
    table: {
        headerRows: 1,
        widths: [550, '', '', '*'],
        body: [
            [{ image: this.base64Logo2, width: 170, style: 'image' }],
        ]
    }
})

opts.content.push({
    text: '\nFicha de registro\n',
    style: 'header'
})
opts.content.push({
    text: [
        '\n\¡Felicidades! ',
        {
            text: student.name + ' ' + student.lastName,
            style: 'bold'
        }],
    style: 'text'
})

opts.content.push({
    text: ['\nTe registraste con éxito en alguno de nuestros cursos de Preparación Politécnico. Realiza tu pago por transferencia o depósito bancario, aquí esta nuestro número de cuenta:'],
    style: 'text'
})

opts.content.push({
    text: ['\nBBVA Bancomer'],
    style: 'text'
})


opts.content.push({
    text: ['\nNo. de cuenta: 01-9789-6115'],
    style: 'text'
})

opts.content.push({
    text: ['\nClabe: 012180001978961159'],
    style: 'text'
})

opts.content.push({
    text: '\n\n1. En caso de realizar tu pago por depósito bancario:',
    style: 'boldRed'
})
opts.content.push({
    text: ['\nEn el ticket deberás escribir tu NOMBRE y CORREO (ver imagen) . Tómale una FOTO y envíala por correo electrónico:'],
    style: 'text'
})

opts.content.push({
    text: ['\npagos.preparacion@gmail.com'],
    style: 'text'
})

opts.content.push({
    layout: 'noBorders',
    table: {
        headerRows: 1,
        widths: [400, '', '', '*'],
        body: [
            [{ image: imageTicket, width: 200, style: 'image' }],
        ]
    }
})

opts.content.push({
    text: '\n\n2. En caso de realizar transferencia bancaria:',
    style: 'boldRed'
})

opts.content.push({
    text: ['\nEn “motivo o concepto de pago” deberás colocar tu NOMBRE y APELLIDO PATERNO. Toma una captura de pantalla donde se visualice que el movimiento fue aprobado y envíala por correo eletrónico:'],
    style: 'text'
})
opts.content.push({
    text: ['\npagos.preparacion@gmail.com'],
    style: 'text'
})
opts.content.push({
    text: '\nCualquier duda o inconveniente puedes comunicarte al (55) 8848 3185 o vía INBOX:',
    style: 'text', lineHeight: 1
})

opts.content.push({
    text: '\nhttps://www.facebook.com/PreparacionIPN',
    link: 'https://www.facebook.com/PreparacionIPN',
    style: 'link'
})



   const doc = await this.getDoc(opts)
   const b64Doc = doc.toString('base64')
   return b64Doc
}



  async generateComipems(student) {
    const opts = this.basics
    opts.info['title'] = "Comprobante de Inscripción"
    opts.info['keywords'] = 'comprobante'

    opts.content.push({
      layout: 'noBorders',
      table: {
        headerRows: 1,
        widths: [100, 300, 30, '*'],
        body: [
          [{}, { image: this.base64Logo2, width: 200, style: 'image' },
          { qr: String(student._id), fit: '100', style: 'imageQR' }],
        ]
      }
    })

    opts.content.push({
      text: 'Comprobante de inscripción',
      style: 'header'
    })

    opts.content.push({
      text: '\n\n'
    })

    opts.content.push({
      text: [
        'Felicidades ',
        {
          text: student.name + ' ' + student.lastName,
          style: 'bold'
        },
        ' te has sido inscrito en un nuevo curso COMIPEMS de Preparación Politécnico.'
      ]
    })

    opts.content.push({
      text: '\n'
    })

    opts.content.push({
      text: [
        'El orden en que tomarás tus cursos es el siguiente:',
        '\n\n\n'
      ]
    })

    student.comipemsOrder.map(group => {
      const schedulesInfo = group.schedules.map(schedule => {
        const startHour = moment(this.formatHour(String(schedule.startHour)).string, 'HH:mm').format("hh:mm a")
        const endHour = moment(this.formatHour(String(schedule.endHour)).string, 'HH:mm').format("hh:mm a")
        return {
          day: this.days[schedule.day],
          startHour: startHour,
          endHour: endHour
        }
      })

      opts.content.push({
        //  layout: 'noBorders',
        table: {
          headerRows: 1,
          widths: [50, 80, 80, 80, 70, 120],
          body: [
            [
              {
                text: [
                  `Módulo: ${group.comipemsPhase}`
                ],
                style: {
                  fontSize: 10,
                  bold: true,
                  alignment: 'left',
                  fillColor: '#DCDCDC',
                }
              },

              {
                text: [
                  `${group.name}`
                ],
                style: {
                  fontSize: 11,
                  bold: true,
                  alignment: 'center',
                  fillColor: '#DCDCDC',
                }
              },
              {
                text: [
                  {
                    text: 'Profesor: ',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: `${group.teacher !== undefined ? group.teacher.name + " " + group.teacher.lastName : "Por definir"}`,
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
              {
                text: [
                  {
                    text: 'Sede: ',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: `${group.classRoom !== undefined ? group.classRoom.location.name : "Por definir"}`,
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: '\nSalón: ',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: `${group.classRoom !== undefined ? group.classRoom.name : "Por definir"}`,
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
              {
                text: [
                  {
                    text: 'Fecha de inicio: ',
                    style: {
                      fontSize: 10,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: `${group.startDate !== undefined ? moment(group.startDate).format("dddd, DD/MM/YYYY").toUpperCase() : "Por definir"}`,
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
              {
                text: [
                  {
                    text: 'Horario(s): ',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: schedulesInfo.map(horario =>
                      `\n${horario.day}: ${horario.startHour} - ${horario.endHour}`,
                    ),
                    style: {
                      fontSize: 9,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
            ],
          ]
        }
      })
      opts.content.push({
        text: [
          ' '
        ]
      })
    })
    opts.content.push({
      text: [
        'Deberás llegar con UNA HORA DE ANTICIPACIÓN a tu primer día de clase.',
        '\n\n',
        'Traer contigo este comprobante impreso en tu primer día de clases.',
        'Es importante que sigas todas las indicaciones que te de el personal.',
      ],
      style: {
        fontSize: 9,
        bold: false,
        alignment: 'justify'
      }
    })

    const doc = await this.getDoc(opts)
    const b64Doc = doc.toString('base64')
    return b64Doc
  }


  async generateExamen(data) {
    const opts = this.basics
    opts.info['title'] = "Comprobante"
    opts.info['keywords'] = 'comprobante'

    opts.content.push({
      layout: 'noBorders',
      table: {
        headerRows: 1,
        widths: [100, 300, 30, '*'],
        body: [
          [{}, { image: this.base64Logo2, width: 200, style: 'image' },
          { qr: String(data.studentData._id), fit: '100', style: 'imageQR' }],
        ]
      }
    })

    opts.content.push({
      text: 'Comprobante',
      style: 'header'
    })

    opts.content.push({
      text: '\n\n'
    })

    opts.content.push({
      text: [
        '¡ Felicidades ',
        {
          text: data.studentData.name + ' ' + data.studentData.lastName ,
          style: 'bold'
        },
        ' !\n\n'
      ],
      alignment: 'center'
    })

    opts.content.push({
      text: '\n'
    })

    opts.content.push({
      text: [
        {
          text: 'Te encuentras inscrito(a) en el Paquete'
        },
        {
          text:  ' Examen Simulacro NIVEL SUPERIOR.',
          style: 'bold'
        }
      ]
    })





    opts.content.push({
      text: '\n'
    })


    opts.content.push({
      text: [
        'Te deberás presentar de acuerdo a la siguiente información: ',
        '\n\n\n'
      ]
    })

    let cont = 0
    data.groupsArray.map(group => {
      const schedulesInfo = group.schedules.map(schedule => {
        const startHour = moment(this.formatHour(String(schedule.startHour)).string, 'HH:mm').format("hh:mm a")
        const endHour = moment(this.formatHour(String(schedule.endHour)).string, 'HH:mm').format("hh:mm a")
        return {
          day: this.days[schedule.day],
          startHour: startHour,
          endHour: endHour
        }
      })

      let fecha = moment(group.startDate)

      cont = cont+1
      opts.content.push({
        //  layout: 'noBorders',
        table: {
          headerRows: 1,
          widths: [50, 100, 65, 70, 110, 70],
          body: [
            [
              {
                text: [
                  `\nExamen \n ${cont}`
                ],
                style: {
                  fontSize: 12,
                  bold: true,
                  alignment: 'center',
                  fillColor: '#DCDCDC',
                }
              },

              {
                text: [
                  `\n${group.name}`
                ],
                style: {
                  fontSize: 11,
                  bold: true,
                  alignment: 'center',
                  fillColor: '#DCDCDC',
                }
              },
              {
                text: [
                  {
                    text: 
                      `Fecha: \n\n ${fecha.format('DD-MM-YYYY')}`,
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                  
                ],
              },
              {
                text: [
                  {
                    text: 'Sede: \n',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    //text: `${group.classRoom !== undefined ? group.classRoom.location.name : "Por definir"}`,
                    text: [
                      `${group.classRoom.location.name}\n`
                    ],
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    text: '\nSalón: \n',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    //text: `${group.classRoom !== undefined ? group.classRoom.name : "Por definir"}`,
                    text: 
                      `${group.classRoom.name}`,
                
                   
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
              {
                text: [
                  {
                    text: 'Ubicación: ',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                    //text: `${group.startDate !== undefined ? moment(group.startDate).format("dddd, DD/MM/YYYY").toUpperCase() : "Por definir"}`,
                    text: 
                      `\n${group.classRoom.location.street} ${group.classRoom.location.number}, ${group.classRoom.location.district}.`,

                    
                    style: {
                      fontSize: 10,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC'
                    }
                  }
                ],
              },
              {
                text: [
                  {
                    text: 'Horario: \n',
                    style: {
                      fontSize: 11,
                      bold: true,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  },
                  {
                     text: schedulesInfo.map(horario =>
                       `\n${horario.startHour} - ${horario.endHour}`,
                     ),
                    style: {
                      fontSize: 9,
                      bold: false,
                      alignment: 'left',
                      fillColor: '#DCDCDC',
                    }
                  }
                ],
              },
            ],
          ]
        }
      })
      opts.content.push({
        text: [
          ' '
        ]
      })
    })

    let precio = 0
    if(cont == 1){
      precio = 200
    }else if(cont == 2){
      precio = 400
    }else if(cont == 3){
      precio = 500
    }

    opts.content.push({
      text: [
        '\nDeberás llegar con UNA HORA DE ANTICIPACIÓN a tu examen para poder cubrir el pago de'
      ],
      style: {
        fontSize: 12,
        bold: false,
        alignment: 'justify'
      }
    })



    opts.content.push({
      text: 
      `${precio} pesos por el paquete adquirido.\n\nTrae contigo este comprobante impreso en tu(s) día(s) de aplicación.\n\nEs importante que sigas todas las instrucciones que te indique el personal.\n\n`,
      
      style: {
        fontSize: 12,
        bold: false,
        alignment: 'justify'
      }
    })


    opts.content.push({
      text: [
        '\n\n\n¡ Mucho Éxito !'
      ],
      style: {
        fontSize: 12,
        bold: true,
        alignment: 'center'
      }
    })

    const doc = await this.getDoc(opts)
    //const b64Doc = doc.toString('base64')
    return doc
  }




  async generateCourse(student, courseInfo) {
    
    
    const opts = this.basics
    // let qr = await generateQRWithoutLogo(String(student._id), { errorCorrectionLevel: 'H' })



    opts.info['title'] = "Comprobante de inscripción."
    opts.info['keywords'] = 'Comprobante'

    opts.content.push({
      layout: 'noBorders',
      table: {
          headerRows: 1,
          widths: [550, '*', '*', '*'],
          body: [
              [{ image: this.base64Logo2, width: 170, style: 'image' },
              { qr: String(student._id), fit: '100', alignment: 'right' }],


          ]
      }
  })

    // opts.content.push({
    //   text: '\n'
    // })

    opts.content.push({
      text: '\nComprobante de inscripción',
      style: 'header'
    })

    opts.content.push({
      text: '\n\n'
    })

    opts.content.push({
      text: [
        'Felicidades ',
        {
          text: student.name,
          style: 'bold'
        },
        ' has sido inscrito en el curso ' + courseInfo.course.name
      ]
    })

    opts.content.push({
      text: '\n\n'
    })

    opts.content.push({
      text: [
        { text: 'Nombre del grupo: ', style: 'bold' }, courseInfo.group.name,
      ]
    })

    opts.content.push({
      text: [
        '\n',
        { text: 'Fecha de inicio: ', style: 'bold' }, courseInfo.startDate
      ]
    })

if(courseInfo.schedulesInfo){

  opts.content.push({
    text: [
      '\n',
      { text: 'Horarios del grupo: ', style: 'bold' }
    ]
  })

    courseInfo.schedulesInfo.map(item => {
      opts.content.push({
        text: [
          '\n',
          `> ${item.day}: ${item.startHour} - ${item.endHour} `
        ]
      })
    }) }else{
     if(courseInfo.course._id == "617424551ba86a5c58941ccf" || courseInfo.course._id == "6174242c13b4665c416bf89b" ){

        opts.content.push({
          text: [
            '\n',
            { text: 'El horario de las transmisiones será publicado en nuestro grupo de Facebook: \n ' }
          ]
        })



      if(courseInfo.course._id == "6174242c13b4665c416bf89b"){

        opts.content.push({
          text: [{ text: '\n"PIPN COMIPEMS" ', style: 'bold' }, { text: "https://www.facebook.com/groups/pipncomipems", style: 'link', link: 'https://www.facebook.com/groups/pipncomipems' }],
          style: 'text', lineHeight: 1
        })

      } else{

        opts.content.push({
          text: [{ text: '\n"Comunidad PIPN" ', style: 'bold' }, { text: "https://www.facebook.com/groups/comunidadpipn", style: 'link', link: 'https://www.facebook.com/groups/comunidadpipn' }],
          style: 'text', lineHeight: 1
        })
      }
     }
    }



    if(courseInfo.location){

    let mapa = "http://www.google.com/maps/place/"+courseInfo.location.latitude+","+courseInfo.location.longitude

    opts.content.push({
      text: [
        '\n\n',
        { text: 'Sede: ', style: 'bold' }, courseInfo.location.name
      ]
    })


    opts.content.push({
      text: [
        '\n',
        { text: 'Salón: ', style: 'bold' }, courseInfo.classRoom.name
      ]
    })


    opts.content.push({
      text: [{ text: '\n\nApunta nuestra dirección:', style: 'bold' }],
      style: 'text'
  })

  opts.content.push({
    text: [{ text: '\n'+ courseInfo.location.street +' No. ' + courseInfo.location.number + '\n Col. ' + courseInfo.location.district + '\nGustavo A. Madero Ciudad de México' + '\nC.P. '+ courseInfo.location.postalCode }],
    lineHeight: 1.2

})

  opts.content.push({
      text: [{ text: '\nGoogle Maps:   ', style: 'bold' }, { text: 'Conocer ubicación', style: 'link', link: mapa }],
      lineHeight: 1
  })



    opts.content.push({
      text: [{ text: '\n\nDeberás llegar con ' }, { text: 'UNA HORA DE ANTICIPACIÓN', style: 'bold' }, { text: ' a tu primer día de clase.\n Traer contigo este comprobante impreso en tu primer día de clases. \nEs importante que sigas todas las indicaciones que te de el personal.\n'}],
      lineHeight: 1.35
  })

    opts.content.push({
      text: [{ text: '\nRecuerda ' }, { text: 'NO', style: 'bold' }, { text: ' ingresar en automóvil hasta la puerta de las instalaciones para evitar embotellamiento y accidentes en la comunidad. Ya que Preparación Politécnico, ' }, { text: 'NO', style: 'bold' }, { text: ' se responsabilizará del daño a su vehículo o terceros.'}],
      lineHeight: 1.25, alignment: 'center'
  })


  }else{

    opts.content.push({
      text: '\n\n\nCualquier duda o inconveniente puedes comunicarte al (55) 8848 3185 o vía INBOX:',
       lineHeight: 1
  })
  
  opts.content.push({
      text: '\nhttps://www.facebook.com/PreparacionIPN',
      link: 'https://www.facebook.com/PreparacionIPN',
      style: 'link'
  })
  

  }




    const doc = await this.getDoc(opts)
    const b64Doc = doc.toString('base64')
    return b64Doc
  }


  buildDoc(opts, cb) {
    const chunks = []
    const pdfDoc = this.printer.createPdfKitDocument(opts)

    pdfDoc.on('data', function (chunk) {
      chunks.push(chunk)
    })
    pdfDoc.on('end', function () {
      const result = Buffer.concat(chunks)
      cb(null, result)
    })

    pdfDoc.on('error', function (err) {
      cb(err)
    })

    pdfDoc.end()
  }

  getDoc(opts) {
    return new Promise((resolve, reject) => {
      this.buildDoc(opts, function (err, buffer) {
        if (err) reject(err)

        resolve(buffer)
      })
    })
  }

  // deprecrated
  printPDF(opts, docPrefix = null) {
    const pdfDoc = this.printer.createPdfKitDocument(opts)
    const docName = docPrefix ? `${docPrefix} - doc.pdf` : 'doc.pdf'
    const docPath = path.resolve(path.join(this.tmpDir, docName))

    const stream = fs.createWriteStream(docPath)
    pdfDoc.pipe(stream)
    pdfDoc.end()
    return new Promise((resolve, reject) => {
      stream.on('finish', function () {
        stream.end()
        resolve({
          docName,
          docPath
        })
      })
      stream.on('error', function (err) {
        reject(err)
      })
    })
  }

  // deprecated
  deletePDF(path) {
    return new Promise((resolve, reject) => {
      fs.unlink(path, err => {
        if (err) {
          reject(err)
        }
        resolve(path)
      })
    })
  }
}

module.exports = GeneratePDF
