/**
 * lib/errMsg.js
 *
 * @description :: Sends errMsg
 * @docs        :: TODO
 */
const _ = require('underscore')
const errMsgJson = require('./errorMessages.json')

class ErrMsg {
  constructor () {
    this.errJson = errMsgJson
  }

  /**
   * getErrors
   * @returns {array} List of errors used.
   */
  getErrors () {
    const errors = []
    Object.keys(this.errJson).forEach(key => {
      errors.push(this.errJson[key])
    })
    return errors
  }

  /**
   * getByCode
   * @description Return the error by code
   * @param {integer} code Error code
   * @returns {object|boolean} Returns false if not found
   */
  getByCode(code) {
    let err = false
    Object.keys(this.errJson).forEach(key => {
      const element = this.errJson[key]
      if (String(element.code) === String(code)) {
        err = element
      }
    })

    return err


  }

  /**
   * generalGet
   * @param {string} modelName Model  name

   */

   generalGet (modelName) {
    const err = this.errJson['getError']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ model: modelName }),
      message: {
          en: errMsgEn({ model: modelName }),
          es: errMsgEs({ model: modelName }),
  }

    }
  }

  generalCreate (modelName) {
    const err = this.errJson['createError']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ model: modelName }),
      message: {
          en: errMsgEn({ model: modelName }),
          es: errMsgEs({ model: modelName }),
  }

    }
  }


  
  generalUpdate (modelName) {
    const err = this.errJson['updateError']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ model: modelName }),
      message: {
          en: errMsgEn({ model: modelName }),
          es: errMsgEs({ model: modelName }),
  }

    }
  }


  generalDelete (modelName) {
    const err = this.errJson['deleteError']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ model: modelName }),
      message: {
          en: errMsgEn({ model: modelName }),
          es: errMsgEs({ model: modelName }),
  }

    }
  }
  

  /**
   * DataBase error in model in method
   * @param {string} operation DataBase operation
   * @param {string} model modelo
   * @param {string} method methodo del modelo
   */
   errorDB (operation, model, method) {
    const err = this.errJson['errorDB']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)


    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: err.error,
      message: {
          en: errMsgEn({ operation: operation, model: model, method: method }),
          es: errMsgEs({ operation: operation, model: model, method: method }),
      }

    }


  }


 /**
   * Entity already exists
   * @param {string} entity entity name
   */
  alreadyExists (entity) {
    const err = this.errJson['alreadyExists']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ entity: entity}),
      message: {
          en: errMsgEn({ entity: entity}),
          es: errMsgEs({ entity: entity}),
      }
    }
  }


   /**
   * Overquota in entity
   * @param {string} entity entity name
   */
    overquotaError (entity) {
      const err = this.errJson['overquotaError']
      const errMsg = _.template(err.error)
      const errMsgEs = _.template(err.message.es)
      const errMsgEn = _.template(err.message.en)
  
      return {
        code: err.code,
        model: err.model,
        httpCode: err.httpCode,
        error: err.error,
        message: {
            en: errMsgEn({ entity: entity}),
            es: errMsgEs({ entity: entity}),
        }
      }
    }



  /**
   * getNotEntityFound
   * @param {string} entityName Model / Entity name
   * @param {string} id Id not found
   */
  getNotEntityFound (entityName, id) {
    const err = this.errJson['notEntityFound']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)


    return {
      code: err.code,
      model: err.model,
      httpCode: err.httpCode,
      error: errMsg({ entity: entityName, id }),
      message: {
          en: errMsgEn({ entity: entityName, id }),
          es: errMsgEs({ entity: entityName, id }),
  }

    }


  }

  /**
   * getStudentWithoutAttendance
   * @param {string} studentId Student object id
   * @returns {object} - error description
   */
  getStudentWithoutAttendance (studentId) {
    const err = this.errJson['studentWithOutAttendance']
    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      error: errMsg({ studentId }),
      errorEs: errMsgEs({ studentId }),
      errorEn: errMsgEn({ studentId }),
      err
    }
  }

  /**
   * getStudentAlreadyEnrolled
   * @param {string} id Group id
   * @returns {object} - error description
   */
  getStudentAlreadyEnrolled (entity, id) {
    const err = this.errJson['studentAlreadyEnrolled']

    const errMsg = _.template(err.error)
    const errMsgEs = _.template(err.message.es)
    const errMsgEn = _.template(err.message.en)

    return {
      code: err.code,
      error: errMsg({ entity, id }),
      errorEs: errMsgEs({ entity, id }),
      errorEn: errMsgEn({ entity, id }),
      err
    }
  }

  getTemplateMissingParams () {
    return this.errJson['templateMissingParams']
  }

  getQuotaExceded () {
    return this.errJson['quotaExceded']
  }

  getStWrongStudent () {
    return this.errJson['notStSessionError']
  }

  getNoDaysInRange () {
    return this.errJson['noDaysInRange']
  }

  getByIndex (index) {
    return this.errJson[index]
  }

  toJson () {
    return this.errJson
  }
}

module.exports = ErrMsg
