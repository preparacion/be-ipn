
const searchHelper = {
    esSearch: async (Model, query = {}, options = {}) => {
        return new Promise((resolve, reject) => {
            if (typeof Model !== 'function') {
                return reject(new Error('Model is not a function'))
            }
            if (typeof Model.esSearch !== 'function') {
                return reject(new Error(`Can't realize searches with this model`))
            }
            if (typeof query !== 'object') {
                return reject(new Error('The query must be an object'))
            }

            Model.esSearch(query, options, (err, results) => {
                if (err) {
                    return reject(err)
                }
                return resolve(results)
            })
        })
    },
}
module.exports = searchHelper