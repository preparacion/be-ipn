const qrcode = require('qrcode');
const sharp = require('sharp');
const fs = require('fs');

//Defined the error dictionary
const ERRORS = {
    "INSUFF_PARAMS": {
        name: "InsufficientParameters Error",
        message: " is required when outputting QR code "
    },
    "ERR_CK": {
        name: "ErrorChecking Error",
        message: "Error occurred while error-checking parameters"
    },
    "INVALID_IMGFILE": {
        name: "InvalidImageFilePath Error",
        message: " is an invalid image file path for the parameter "
    }
};


async function generateQRWithLogo(embedded_data, logo_image_path, qr_options, output_type, saveas_file_name) {
    if (embedded_data && logo_image_path && output_type) {
        if (output_type == "PNG") {
            if (!saveas_file_name || (typeof saveas_file_name != 'string')) {
                throw SyntaxError(JSON.stringify({ name: ERRORS["INSUFF_PARAMS"].name, message: "saveas_file_name" + ERRORS["INSUFF_PARAMS"].message + "to PNG" }));
            } console.log("All PNG parameters");

        }
    }

    if (!output_type) {
        throw SyntaxError(JSON.stringify({ name: ERRORS["INSUFF_PARAMS"].name, message: "output_type" + ERRORS["INSUFF_PARAMS"].message }));

    } else if (!embedded_data && logo_image_path && output_type) {
        throw SyntaxError(JSON.stringify({ name: ERRORS["INSUFF_PARAMS"].name, message: "embedded_data" + ERRORS["INSUFF_PARAMS"].message + "to " + output_type }));
    } else if (!logo_image_path && embedded_data && output_type) {
        throw SyntaxError(JSON.stringify({ name: ERRORS["INSUFF_PARAMS"].name, message: "logo_image_path" + ERRORS["INSUFF_PARAMS"].message + "to " + output_type }));
    }

    if ((logo_image_path.lastIndexOf('.')) == '-1') {
        throw SyntaxError(JSON.stringify({ name: ERRORS["INVALID_IMGFILE"].name, message: logo_image_path + ERRORS["INVALID_IMGFILE"].message + "logo_image_path" }));
    }

    if ((saveas_file_name.lastIndexOf('.')) == '-1') {
        throw SyntaxError(JSON.stringify({ name: ERRORS["INVALID_IMGFILE"].name, message: saveas_file_name + ERRORS["INVALID_IMGFILE"].message + "saveas_file_name  Ensure that .png was included" }));
    }

    if (qr_options.length == 0) {
        qr_options = { errorCorrectionLevel: 'H' }
    }

    console.log("pass validation")

    // Here rewrite code using promise then 
    return generateQR(embedded_data, qr_options)
        .then(
            () => {
                //This is the first then into the data here call save AsPng and return a promise to call in the second level then.
                console.log("Enter 1 then")
                return addLogoToQRImage("qr.png", logo_image_path)
            }
        )
        .then(
            async (qrlogo_b64) => {
                console.log("Enter 2 then")
                await fs.unlink("qr.png", async function () {
                    return (qrlogo_b64);
                });
            }
        )
}


async function generateQRWithoutLogo(embedded_data, options) {
    try {
        let code = await qrcode.toDataURL(embedded_data, options)
            .then(
                (result) => {
                    return result
                }
            )
        return code;
    } catch (err) { console.error(err) }

};


async function generateQR(embedded_data, options) {
    try {
        await qrcode.toFile("qr.png", embedded_data, options)
            .then(
                (result) => {
                    console.log("OKOKOK")
                }
            )
    } catch (err) { console.error(err) }

};

async function addLogoToQRImage(qr_image_path, logo_image_path) {
    let img_resize = await sharp(qr_image_path)
        .resize({
            width: 200,
            height: 200,
            fit: "contain",
        })
        .toBuffer();
    let img_center = await sharp(logo_image_path)
    .resize({
        width: 40,
        height: 40,
        fit: "contain",
    })
    .toBuffer();
    let result = await sharp(img_resize)
        .composite([{ input: img_center, gravity: 'centre' }])
        .toBuffer()
    let base64data = Buffer.from(result, 'binary').toString('base64');
    return base64data
}


module.exports  = {
    generateQRWithLogo,
    generateQRWithoutLogo
};

