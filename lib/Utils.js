/**
 * lib/Utils.js
 *
 * @description :: TODO
 * @docs        :: None
 */
const _ = require('underscore')
const StudentModel = require('../db/students')
const CourseModel = require('../db/courses')
const GroupModel = require('../db/groups')

const Utils = {
  /**
   * updateStudentBalance
   * @example const util = new Utils()
   *  await util.updateStudentBalance('id1', [{...}, {...}])
   * @param {string} studentId Student object id
   * @param {array} payments List of payments object
   */
  async updateStudentBalance(studentId, payments) {
    const student = await StudentModel.findOne({
      _id: studentId
    })

    if (!student) {
      return false
    }

    const balance = { payments: {}, costs: {} }
    let discounts = 0.0
    let total = 0.0
    let costs = 0.0

    await GroupModel.find({
      _id: { $in: student.groups }
    })
      .select('course')
      .then(groups => {
        return Promise.all(
          groups.map(group => {
            return CourseModel.findOne({
              _id: group.course
            }).select('price')
          })
        )
      })
      .then(courses => {
        courses.forEach(course => {
          costs += course.price
        })
        return courses
      })

    payments.forEach(payment => {
      discounts += payment.discount
      total += payment.amount
    })

    balance.payments.discount = discounts
    balance.payments.total = total
    balance.costs.total = costs

    student.balance = balance
    student.debt = (
      student.balance.payments.total + student.balance.payments.total
    ) < student.balance.costs.total

    await student.save()
  },

  /**
 * shuffleArray
 * @description Creates an array of shuffled integer
 * @param {integer} length number of elements.
 */
  shuffleArray: async (length) => {
    let i = 0
    const arr = []
    for (i = 1; i <= length; i++) {
      arr.push(i)
    }
    return _.shuffle(arr)
  },


  formatHour: (hour) => {
    let arrHour = hour.split('')
    arrHour.splice(-2, 0, ':')

    const str = arrHour.join('')
    const arrStr = str.split(':')

    return {
      string: str,
      minutes: arrStr[1],
      hours: arrStr[0]
    }
  }
  
}

module.exports = Utils
