/**
 * lib/sendGrid.js
 *
 * @description :: Sends emails
 * @docs        :: TODO
 */
const rp = require('request-promise')
const sgMail = require('@sendgrid/mail');
const { sendGridUri, sendGridToken, frontUrl, frontUrlAdmin } = require('../config')
sgMail.setApiKey(sendGridToken);
const SendGrid = {
  sendRegister: async data => {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            startDate: data.startDate || '-',
            name: data.name || '-',
            courseName: data.courseName || '-',
            groupName: data.groupName || '-',
            classRoomName: data.classRoomName || '-',
            schedules: data.schedules || '-',
            locationName: data.locationName || '-',
            locationDirection: data.locationDirection || '-'
          }
        }],
        template_id: 'd-719b5558ce6748f2b60e64876d7bdfd7',
        attachments: data.attachments
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }
    // Add attach if exist
    if (data.attachments) {
      options.body.attachments = data.attachments
    }
    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },


  sendRegisterToTalk: async data => {

    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            name: data.name,
            dateAndHour: data.dateAndHour,
            locationName: data.locationName,
            locationDirection: data.locationDirection
          }
        }],
        template_id: 'd-d9cffd2a7c164d2db2210b18233d52b7'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }
    // Add attach if exist
     if (data.attachments) {
       options.body.attachments = data.attachments
     }
    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        //console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
      
    return result
  },


  sendRegisterToTalkTerror: async data => {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            name: data.name,
            dateAndHour: data.dateAndHour,
            locationName: data.locationName,
            locationDirection: data.locationDirection
          }
        }],
        template_id: 'd-a9a068c005f14fb08bb9ce80776339a6'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }
    // Add attach if exist
     if (data.attachments) {
       options.body.attachments = data.attachments
     }
    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        //console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
      console.log(result)
    return result
  },

  
  sendRecoverPass: async (name, tempPass, email, isAdmin = false) => {
    const urlRecover = `${isAdmin ? frontUrlAdmin : frontUrl}/update-password/?email=${email}&provisionalPass=${tempPass}`
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: email
          }],
          dynamic_template_data: {
            provPassword: tempPass,
            urlRecover,
            name
          }
        }],
        template_id: 'd-b76b818428c040a8be77dc3040f5b7e0'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  sendNotification: async function (data) {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Message'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: data.templateData
        }],
        template_id: data.template
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  sendComipems: async function (data) {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Message'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            name: data.name,
            startDate: data.startDate,
            schedules: data.schedules
          }
        }],
        template_id: 'd-c1fa3d780fa34d4f910b3f7ce4ca95b0',
        attachments: data.attachments
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  sendMessage: async function (data) {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Message'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            name: data.name,
            message: data.message
          }
        }],
        template_id: 'd-a934c1e29ac94e4eb91061956d620114'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },
  sendPayment: async function (data) {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            id: ` ${data.id}`,
            name: `${data.name} ${data.lastName}`,
            phone: data.phoneNumber,
            email: data.email,
            items: data.items,
            totalDiscounts: data.totalDiscounts,
            granTotal: data.granTotal
          }
        }],
        template_id: 'd-5da44f72bd6d48bdb9d8a53441c7eeae'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }

    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
    return result
  },

  sendLandingCourse: async data => {
    const options = {
      method: 'POST',
      uri: sendGridUri,
      body: {
        from: {
          email: 'no-reply@email.preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: [{
          to: [{
            email: data.email
          }],
          dynamic_template_data: {
            name: data.name,
            message: data.message
          }
        }],
        template_id: 'd-a934c1e29ac94e4eb91061956d620114'
      },
      headers: {
        Authorization: `Bearer ${sendGridToken}`
      },
      json: true
    }
    // Add attach if exist
     if (data.attachments) {
       options.body.attachments = data.attachments
     }
    const result = await rp(options)
      .then(response => {
        return { success: true }
      })
      .catch(err => {
        //console.error(err)
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2)
        }
      })
      console.log(result)
    return result
  },


  sendBulkGenericMessage: async function (arrayOfStudents, emailData) {
    const numberOfStudents = arrayOfStudents.length;
    console.log("Students", numberOfStudents)
    const requestNumber = (numberOfStudents / 1000) % 1 !== 0 ? Math.floor(numberOfStudents / 1000) + 1 : Math.floor(numberOfStudents / 1000);
    console.log("Request", requestNumber)
    for (let i = 0; i < requestNumber; i++) {
      const start = 1000 * i;
      const end = 1000 * (i + 1);
      const students = [];
      for (let j = start; j < end && j < numberOfStudents; j++) {
        students.push(arrayOfStudents[j]);
      }
      console.log("Students en " + i + " size:" + students.length);
      const personalization = [];
      students.forEach(student => {
        personalization.push({
          to: student.email,
          dynamic_template_data: {
            name: student.name,
            message: emailData.templateData.message,
            subject: emailData.templateData && emailData.templateData.subject ? emailData.templateData.subject : "Recordatorio Preparación Politécnico"
          },
        })
      });
      const msg = {
        from: {
          email: 'no-reply@preparacionipn.mx',
          name: 'Preparación Politécnico'
        },
        content: [{
          type: 'text/html',
          value: 'Invoice'
        }],
        personalizations: personalization,
        template_id: emailData.template
      };
      //Here we can make more request and catch in a Promise.all
      const toReturn = await sgMail.send(msg);
      console.log("BULK EMAIL", toReturn);
    }
  }
}

module.exports = SendGrid
