/**
 * lib/s3.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
const AWS = require('aws-sdk')

const {
    awsAccessKeyId,
    awsSecretAccessKey,
    awsBucketRegion,
} = require('../config')
AWS.config.setPromisesDependency(require('bluebird'))

AWS.config.update({
    region: awsBucketRegion,
    accessKeyId: awsAccessKeyId,
    secretAccessKey: awsSecretAccessKey
})

const MediaLive = {

    listChannels: async () => {
        const medialive = new AWS.MediaLive({ apiVersion: '2017-10-14' })
        const inputsInfoPromise = await medialive.listInputs().promise()
        if (inputsInfoPromise === undefined) {
            return {
                success: false,
                error: "Ocurrio un error al recuperar la información de los canales"
            }
        }
        let InputsChannel = []
        for (let i = 0; i < inputsInfoPromise.Inputs.length; i++) {
            const current = inputsInfoPromise.Inputs[i]
            const channels = current.AttachedChannels
            let channelsWithInfo = []
            for (j = 0; j < channels.length; j++) {
                currentChannel = channels[j]
                const channelInfo = await medialive.describeChannel({ ChannelId: currentChannel }).promise()
                const filterInfo = {
                    Id: channelInfo.Id,
                    Name: channelInfo.Name,
                    Arn: channelInfo.Arn,
                    ChannelClass: channelInfo.ChannelClass,
                    State: channelInfo.State,
                    EncoderSettings: channelInfo.EncoderSettings
                }
                channelsWithInfo.push(filterInfo)
            }
            const element = {
                Id: current.Id,
                Name: current.Name,
                Arn: current.Arn,
                State: current.State,
                Type: current.Type,
                Destinations: current.Destinations,
                AttachedChannels: channelsWithInfo

            }
            InputsChannel.push(element)
        }
        return {
            success: true,
            channels: InputsChannel
        }
    },

    getChannelInfo: async (idChannel) => {
        const medialive = new AWS.MediaLive({ apiVersion: '2017-10-14' })
        const params = {
            ChannelId: idChannel /* required */
        };
        const channelInfo = await medialive.describeChannel(params).promise()
        const filterInfo = {
            Id: channelInfo.Id,
            Name: channelInfo.Name,
            Arn: channelInfo.Arn,
            ChannelClass: channelInfo.ChannelClass,
            State: channelInfo.State,
            EncoderSettings: channelInfo.EncoderSettings
        }
        return filterInfo
    },

    startChannel: async (idChannel) => {
        const medialive = new AWS.MediaLive({ apiVersion: '2017-10-14' })
        const params = {
            ChannelId: idChannel /* required */
        };
        const resultStart = await medialive.startChannel(params).promise();
        return resultStart
    },

    stopChannel: async (idChannel) => {
        const medialive = new AWS.MediaLive({ apiVersion: '2017-10-14' })
        var params = {
            ChannelId: idChannel /* required */
        };
        const resultStop = await medialive.stopChannel(params);
    },
}


module.exports = MediaLive