/**
 * lib/paymentsProcessor.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */

const mercadopago = require('mercadopago')

const {
    mercadopagoWebhook,
    mercadopagoAt
} = require('../config');

mercadopago.configurations.setAccessToken(mercadopagoAt);

const MethodIdsPayments = [
    'visa',
    'debvisa',
    'master',
    'debmaster',
    'amex',
    'clabe',//Transferencia Interbancaria
    'ticket',//OXXO
    'atm'
]

const methodsDic = {
    visa: "Tarjeta de crédito Visa",
    debvisa: "Tarjeta de débito Visa",
    master: "Tarjeta de crédito MasterCard",
    debmaster: "Tarjeta de débito MasterCard",
    amex: "Tarjeta American Express",
    clabe: "Deposito Bancario a cuenta clabe",//Transferencia Interbancaria
    ticket: "Pago en eféctivo en tienda Oxxo",//OXXO
    atm: "Deposito en cajero automático"
}

// 

const Mercadopago = {
    /***************************************************************************************************
    ****************************************************************************************************
    ************************************ Compose Methods ***********************************************
    ****************************************************************************************************
    ****************************************************************************************************/

    //This method is for single payment only
    createSinglePayment: async (student, data) => {
        const index = MethodIdsPayments.findIndex(value => value === data.paymentMethodId)
        let payment_method_id = ""
        if (index >= 0) {
            payment_method_id = data.paymentMethodId
        } else {
            console.log("Error no valid method", index)
            return
        }
        const payment_data = {
            token: data.token,
            payment_method_id: payment_method_id,
            installments: 1, //Numero de cuotas P.E Meses sin intereses o con intereses
            binary_mode: true, //True para solo permitir , aprobado y rechazado IMPORTANT 
            transaction_amount: Number(data.transaction_amount),
            description: "Pago en una sola exhibición," + (data.description ? data.description : ""),
            notification_url: mercadopagoWebhook,
            payer: {
                email: student.email
            },
            statement_descriptor: "PreparacionIPN",
            additional_info: {
                payer: {
                    first_name: student.name,
                    last_name: student.lastName,
                    phone: {
                        area_code: '+52',
                        number: student.phoneNumber
                    }
                },
                items: [
                    {
                        id: '192211',
                        title: 'Curso de regularización',
                        quantity: 1,
                        unit_price: Number(data.transaction_amount)
                    }
                ]
            }
        };
        let resultPayment
        try {
            resultPayment = await mercadopago.payment.save(payment_data)
        } catch (err) {
            console.log(err)
            return {
                success: false,
                message: err.message,
                status: err.status
            }
        }
        const response = resultPayment.response
        const responseReturn = Mercadopago.validationResponse(response)
        if (responseReturn.success && data.rememberCard) {
            await Mercadopago.createClientAndAddCard(student.email, data.token)
        }
        return responseReturn
    },

    doPaymentToClient: async (student, data) => {
        const clientResult = await Mercadopago.findClientByEmail(student.email)
        if (clientResult !== undefined) {
            var payment_data = {
                transaction_amount: Number(data.transaction_amount),
                token: data.token,
                installments: 1,
                statement_descriptor: "PreparacionIPN",
                notification_url: mercadopagoWebhook,
                payer: {
                    type: "customer",
                    id: clientResult.id
                }
            };
            let resultPayment
            try {
                resultPayment = await mercadopago.payment.create(payment_data)
            } catch (err) {
                return {
                    success: false,
                    message: err.message,
                    status: err.status
                }
            }
            const response = resultPayment.response
            const responseReturn = Mercadopago.validationResponse(response)
            return responseReturn
        } else {
            return {
                success: false,
                message: "Ocurrio un error al intentar realizar el pago con tarjeta, no logramos encontrar tus tarjetas.",
                status: 404
            }
        }
    },

    createClientAndAddCard: async (email, tokenCard) => {
        const clientResult = await Mercadopago.findClientByEmail(email)
        if (clientResult !== undefined) {
            const responseAlreadyExist = await Mercadopago.addCardToClient(clientResult.id, tokenCard)
            const responseAlreadyExistResponse = responseAlreadyExist.response
            const card = {
                id: responseAlreadyExistResponse.id,
                expiration_month: responseAlreadyExistResponse.expiration_month,
                expiration_year: responseAlreadyExistResponse.expiration_year,
                first_six_digits: responseAlreadyExistResponse.first_six_digits,
                last_four_digits: responseAlreadyExistResponse.last_four_digits,
                payment_method_id: responseAlreadyExistResponse.payment_method.id,
                cardholder: responseAlreadyExistResponse.cardholder.name,
                date_created: responseAlreadyExistResponse.date_created,
                date_last_updated: responseAlreadyExistResponse.date_last_updated
            }
            return {
                success: true,
                card
            }
        } else {
            console.log("CLIENT NOT EXIST")
            const customer_data = { "email": email }
            console.log("customer_data", customer_data)
            const customer = await mercadopago.customers.create(customer_data)
            console.log("customer", customer)
            const responseNewCustomer = customer.response
            console.log("responseNewCustomer", responseNewCustomer)
            const card_data = {
                token: tokenCard,
                customer_id: responseNewCustomer.id
            }
            const cardResponse = await mercadopago.card.create(card_data)
            return {
                success: true,
                cardResponse
            }
        }
    },


    getCardsOfClientByEmail: async (email) => {
        const client = await Mercadopago.findClientByEmail(email)
        if (client !== undefined) {
            let cardsFiltered = []
            for (let i = 0; i < client.cards.length; i++) {
                const current = client.cards[i]
                const newCard = {
                    id: current.id,
                    cardholder: current.cardholder.name,
                    date_created: current.date_created,
                    date_last_updated: current.date_last_updated,
                    expiration_month: current.expiration_month,
                    expiration_year: current.expiration_year,
                    first_six_digits: current.first_six_digits,
                    last_four_digits: current.last_four_digits,
                    payment_method_id: current.payment_method.id,
                    length_security_code: current.security_code.lengt
                }
                cardsFiltered.push(newCard)
            }
            if (cardsFiltered.length > 0) {
                return {
                    success: true,
                    cards: cardsFiltered
                }
            } else {
                return {
                    success: true,
                    cards: [],
                    message: "No tienes tarjetas registradas o no se encontró el cliente."
                }
            }
        } else {
            return {
                success: true,
                cards: [],
                message: "No tienes tarjetas registradas o no se encontró el cliente."
            }
        }
    },
    editCartToClient: async (email, data) => {
        let dataUpdate = { ...data }
        const customer = await Mercadopago.findClientByEmail(email)
        dataUpdate.customer_id = customer.id
        let other = {
            id: customer.id,
            card_id: data.id,
            expiration_month: Number(data.cardExpirationMonth),
            expiration_year: Number(data.cardExpirationYear),
            cardholder: {
                name: data.cardholderName
            }
        }
        const response = await mercadopago.customers.cards.update(other)
        if (response !== undefined && response.response !== undefined) {
            return {
                success: true,
                message: "La tarjeta se ha actualizado correctamente.",
                card: {
                    id: response.response.id,
                    expiration_month: response.response.expiration_month,
                    expiration_year: response.response.expiration_year,
                    first_six_digits: response.response.first_six_digits,
                    last_four_digits: response.response.last_four_digits,
                    cardholder: response.response.cardholder.name,
                    date_created: response.response.date_created,
                    date_last_updated: response.response.date_last_updated,
                    payment_method_id: response.response.payment_method.id,
                }
            }
        } else {
            return {
                success: false,
                message: "Error al actualizar la tarjeta"
            }
        }
    },

    removeCardToClient: async (email, idCard) => {
        const customer = await Mercadopago.findClientByEmail(email)
        const cardResponse = await mercadopago.card.delete(customer.id, idCard)
        let bodyReturn = {}
        if (cardResponse.response) {
            bodyReturn.success = true
            bodyReturn.message = "Tarjeta borrada correctamente"
        } else {
            bodyReturn.success = false
            bodyReturn.message = "Ocurrio un error al borrar la tarjeta"
        }
        return cardResponse
    },


    /***************************************************************************************************
    ****************************************************************************************************
    ************************************ Native Methods ************************************************
    ****************************************************************************************************
    ****************************************************************************************************/



    addCardToClient: async (clientId, tokenCard) => {
        const filters = {
            id: clientId
        };
        const customer = await mercadopago.customers.search({
            qs: filters
        })
        if (customer !== undefined && customer.response.results.length > 0) {
            const card_data = {
                token: tokenCard,
                customer_id: clientId
            }
            const cardResponse = await mercadopago.card.create(card_data)
            return cardResponse
        } else {
            return false
        }
    },

    findClientByEmail: async (email) => {
        var filters = {
            email: email
        };
        const customer = await mercadopago.customers.search({
            qs: filters
        })
        return customer.response.results !== undefined ? customer.response.results[0] : {}
    },


    getCardsOfClient: async (idClient) => {
        var filters = {
            id: idClient
        };
        const customerResult = await mercadopago.customers.search({
            qs: filters
        })
        return customerResult
    },

    findPaymentDetails: async (data) => {
        try {
            const resultPayments = await mercadopago.payment.findById(data.data.id)
            return {
                success: true,
                payment: resultPayments.response
            }
        } catch (err) {
            return {
                success: false,
                message: err
            }
        }
    },

    sendRefoundPartial: async (data) => {
        const payment_refound_data = {
            id: data.id
        }
        const resultRefoundPayment = await mercadopago.payment.refundPartial(payment_refound_data)
    },

    getPaymentMethodAvailable: async () => {
        payment_methods = await mercadopago.get("/v1/payment_methods");
        return payment_methods
    },


    /***************************************************************************************************
    ****************************************************************************************************
    ***************************************** HELPERS **************************************************
    ****************************************************************************************************
    ****************************************************************************************************/
    validationResponse: (response) => {
        let toReturn = {}
        if (response !== undefined) {
            if (response.status == "approved") {
                toReturn.success = true
                toReturn.status = 1
                toReturn.idOperation = response.id
                toReturn.date_approved = response.date_approved
                toReturn.date_approved = response.date_approved
                toReturn.message = "PAGO APROBADO CORRECTAMENTE"
            } else if (response.status == "in_process") {
                toReturn.success = true
                toReturn.status = 2
                toReturn.idOperation = response.id
                switch (response.status_detail) {
                    case "pending_contingency":
                        toReturn.message = ("Estamos procesando tu pago. No te preocupes, en menos de 2 días hábiles te avisaremos por e-mail si se acreditó.")
                        break;
                    case "pending_review_manual":
                        toReturn.message = ("Estamos procesando tu pago. No te preocupes, en menos de 2 días hábiles te avisaremos por e-mail si se acreditó.")
                        break;
                    default:
                        toReturn.message = ("DEFAULT PNDG " + response.status_detail)
                }
            } else if (response.status == "rejected") {
                toReturn.success = false
                toReturn.status = 3
                toReturn.idOperation = response.id
                switch (response.status_detail) {
                    case "cc_rejected_bad_filled_card_number":
                        toReturn.message = ("Revisa el número de tarjeta.")
                        break;
                    case "cc_rejected_bad_filled_date":
                        toReturn.message = ("Revisa la fecha de vencimiento.")
                        break;
                    case "cc_rejected_bad_filled_other":
                        toReturn.message = ("Revisa los datos.")
                        break;
                    case "cc_rejected_bad_filled_security_code":
                        toReturn.message = ("Revisa el código de seguridad de la tarjeta.")
                        break;
                    case "cc_rejected_blacklist":
                        toReturn.message = ("No pudimos procesar tu pago. (BlackList)")
                        break;
                    case "cc_rejected_call_for_authorize":
                        toReturn.message = (`Debes autorizar ante ${methodsDic[response.payment_method_id]} el pago de ${response.transaction_amount}.`)
                        break;
                    case "cc_rejected_card_disabled":
                        toReturn.message = (`Llama a ${methodsDic[response.payment_method_id]} para activar tu tarjeta o usa otro medio de pago. El teléfono está al dorso de tu tarjeta.`)
                        break;
                    case "cc_rejected_card_error":
                        toReturn.message = (`No pudimos procesar tu pago.`)
                        break;
                    case "cc_rejected_duplicated_payment":
                        toReturn.message = (`Si necesitas volver a pagar usa otra tarjeta u otro medio de pago. Tu pago fue rechazado.`)
                        break;
                    case "cc_rejected_high_risk":
                        toReturn.message = (`Tu pago fue rechazado. Elige otro de los medios de pago, te recomendamos con medios en efectivo.`)
                        break;
                    case "cc_rejected_insufficient_amount":
                        toReturn.message = (`Tu ${methodsDic[response.payment_method_id]} no tiene fondos suficientes.`)
                        break;
                    case "cc_rejected_invalid_installments":
                        toReturn.message = (`${methodsDic[response.payment_method_id]} no procesa pagos en ${response.installments} cuotas.`)
                        break;
                    case "cc_rejected_max_attempts":
                        toReturn.message = (`Llegaste al límite de intentos permitidos. Elige otra tarjeta u otro medio de pago.`)
                        break;
                    case "cc_rejected_duplicated_payment":
                        toReturn.message = (`${response.payment_method_id} no procesó el pago.`)
                        break;
                    default:
                        toReturn.message = ("Operación rechazada: No se pudo procesar el pago, agrega ó intenta con otra tarjeta u otro médio de pago.")
                }
            }
        }
        return toReturn
    },

    validationCodeCardResponse: (code) => {
        let messageError = ""
        switch (code) {
            case 400:
                messageError = "BAD_REQUEST"
                break;
            case 100:
                messageError = "Las credenciales son requeridas."
                break;
            case 101:
                messageError = "El cliente ya existe."
                break;
            case 102:
                messageError = "missing customer id."
                break;
            case 103:
                messageError = "El parámetro debe ser un objeto"
                break;
            case 104:
                messageError = "La longitud del parámetro es muy grande."
                break;
            case 105:
                messageError = "El identificador de cliente es inválido."
                break;
            case 106:
                messageError = "El formato de email es inválido."
                break;
            case 107:
                messageError = "first_name inválido."
                break;
            case 108:
                messageError = "last_name inválido."
                break;
            case 109:
                messageError = "phone.area_code inválido."
                break;
            case 110:
                messageError = "phone.number inválido."
                break;
            case 111:
                messageError = "identification.type inválido."
                break;
            case 112:
                messageError = "identification.number inválido."
                break;
            case 113:
                messageError = "address.zip_code inválido."
                break;
            case 114:
                messageError = "address.street_name inválido."
                break;
            case 115:
                messageError = "date_registered format inválido."
                break;
            case 116:
                messageError = "description inválido."
                break;
            case 117:
                messageError = "metadata inválido."
                break;
            case 118:
                messageError = "El body debe ser un objeto JSON."
                break;
            case 119:
                messageError = "La tarjeta es requerida."
                break;
            case 120:
                messageError = "Tarjeta no encontrada."
                break;
            case 121:
                messageError = "La tarjeta es inválida."
                break;
            case 122:
                messageError = "card data inválida."
                break;
            case 123:
                messageError = "payment_method_id es requerido."
                break;
            case 124:
                messageError = "issuer_id es requerido."
                break;
            case 125:
                messageError = "Parámetros inválidos."
                break;
            case 126:
                messageError = "Parámetro inválido. No se puede actualizar el email."
                break;
            case 127:
                messageError = "Parámetro inválido. No se puede resolver el medio de pago de la tarjeta, revisa payment_method_id y issuer_id."
                break;
            case 128:
                messageError = "El formato de email es inválido. Usa 'test_payer_[0-9]{1,10}@testuser.com'."
                break;
            case 129:
                messageError = "El cliente llego al máximo permitido de tarjetas."
                break;
            case 140:
                messageError = "Dueño de tarjeta inválido."
                break;
            case 150:
                messageError = "Usuarios inválidos involucrados."
                break;
            case 200:
                messageError = "Formato de rango inválido (range=:date_parameter:after::date_from,before::date_to)."
                break;
            case 201:
                messageError = "El atributo del rango debe pertenecer a la entidad date."
                break;
            case 202:
                messageError = "Parámetro 'after' inválido. Debería ser date[iso_8601]."
                break;
            case 203:
                messageError = "Parámetro 'before' inválido. Debería ser date[iso_8601]."
                break;
            case 204:
                messageError = "Formato inválido de filtros."
                break;
            case 205:
                messageError = "Formato de consulta inválido."
                break;
            case 206:
                messageError = "Los atributos a ordenar deben perstenecer a la entidad 'customer'."
                break;
            case 207:
                messageError = "El orden del filtro debe ser 'asc' o 'desc'."
                break;
            case 208:
                messageError = "Formato inválido del parámetro 'sort'."
                break;
        }
        return messageError
    }
}


module.exports = Mercadopago