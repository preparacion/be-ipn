
const {
    stripePk
} = require('../config');
const stripe = require('stripe')(stripePk);

const Stripe = {
    createPayment: async (dataPayment) => {
        try {
            let intent
            if (dataPayment.payment_method_id) {
                //Amount is multiplied by 100 because is marked by stripe in cents 
                intent = await stripe.paymentIntents.create({
                    payment_method: dataPayment.payment_method_id,
                    amount: (dataPayment.amount * 100),
                    currency: 'mxn',
                    confirmation_method: 'manual',
                    confirm: true,
                    payment_method_types: ['card']
                });
            } else if (dataPayment.payment_intent_id) {
                intent = await stripe.paymentIntents.confirm(
                    dataPayment.payment_intent_id
                );
            }
            const response = Stripe.generateResponse(intent);
            return response;
        } catch (e) {
            if (e.code) {
                return Stripe.errorCodesValidation(e.code)
            } else {

                return ({
                    success: false,
                    error: e.message
                })
            }
        }
    },
    generateResponse: (intent) => {
        if (
            intent.status === 'requires_action' &&
            intent.next_action.type === 'use_stripe_sdk'
        ) {
            //Return need client action before payments 
            return {
                success: false,
                requires_action: true,
                payment_intent_client_secret: intent.client_secret
            };
        } else if (intent.status === 'succeeded') {
            // The payment didn’t need any additional actions and completed!
            // Handle post-payment fulfillment
            return {
                success: true,
                idOperation: intent.id
            };
        } else {
            return {
                success: false,
                error: 'Invalid PaymentIntent status'
            }
        }
    },


    errorCodesValidation: (errorCode) => {
        let toReturn = {}
        toReturn.success = false;
        toReturn.error = {
            code: errorCode
        }
        switch (errorCode) {
            case "authentication_required":
                toReturn.error.message = ("Esta tarjeta requiere autentificación de tu banco , por favor usa otra tarjeta.")
                break;
            case "approve_with_id":
                toReturn.error.message = ("El pago no ha sido aprobado por tu banco , intenta de nuevo, si sigue sin funcionar debes contactar al banco emisor.")
                break;
            case "call_issuer":
                toReturn.error.message = ("Necesitas contactar a tu banco para más información.")
                break;
            case "card_not_supported":
                toReturn.error.message = ("Necesitas contactar a tu banco para verificar si puedes realizar este tipo de pago.")
                break;
            case "card_velocity_exceeded":
                toReturn.error.message = ("Tú limite de credito esta excedido o no tienes saldo suficiente para realizar el pago, contacta a tu banco para más información.")
                break;
            case "currency_not_supported":
                toReturn.error.message = ("Tú tarjeta no acepta pago en pesos mexicanos (MXN), contacta a tu banco para más información de divisas.")
                break;
            case "do_not_honor":
                toReturn.error.message = ("Tú tarjeta ha sido declinada por razones desconocidas, contacta a tu banco para más información, te recomendamos intentar con otra tarjeta.")
                break;
            case "do_not_try_again":
                toReturn.error.message = ("Tú tarjeta ha sido declinada por razones desconocidas, contacta a tu banco para más información, te recomendamos intentar con otra tarjeta.")
                break;
            case "duplicate_transaction":
                toReturn.error.message = ("Pago pendiente, ya recibimos una solicitud de pago con la misma cantidad recientemente, verifica si ha llegado un comprobante de pago a tu correo electronico, en caso contrario intenta de nuevo más tarde.")
                break;
            case "expired_card":
                toReturn.error.message = ("La tarjeta con la que deseas realizar el pago ha expirado.")
                break;
            case "fraudulent":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas, contacta inmediatamente a tu banco emisor de la tarjeta")
                break;
            case "generic_decline":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;
            case "incorrect_number":
                toReturn.error.message = ("El número de la tarjeta con la que deseas pagar es incorrecto, verificalo")
                break;
            case "incorrect_cvc":
                toReturn.error.message = ("El número de seguridad de tu tarjeta (CVV/CVC) con la que deseas pagar es incorrecto, verificalo e intenta de nuevo.")
                break;
            case "incorrect_pin":
                toReturn.error.message = ("El PIN de la tarjeta con la que deseas pagar es incorrecto, verificalo")
                break;
            case "incorrect_zip":
                toReturn.error.message = ("El codigo postal es incorrecto, verificalo.")
                break;
            case "insufficient_funds":
                toReturn.error.message = ("Fondos insuficientes, por favor intenta con otra tarjeta.")
                break;
            case "invalid_account":
                toReturn.error.message = ("La tarjeta o la cuenta vinculada al pago es incorrecto, intenta con otro médio de pago.")
                break;
            case "invalid_amount":
                toReturn.error.message = ("La cantidad que deseas pagar excede el monto permitido por tu tarjeta, para más información verificalo con tu banco emisor de la tarjeta.")
                break;
            case "invalid_cvc":
                toReturn.error.message = ("El codigo de seguridad de tu tarjeta (CVV/CVC) es invalido, verificalo e intenta más tarde")
                break;
            case "invalid_expiry_month":
                toReturn.error.message = ("La fecha de expiración de la tarjeta es invalida, verificalo e intenta más tarde")
                break;
            case "invalid_expiry_year":
                toReturn.error.message = ("La fecha de expiración de la tarjeta es invalida, verificalo e intenta más tarde")
                break;
            case "invalid_number":
                toReturn.error.message = ("El número de la tarjeta con la que deseas pagar es invalido, verificalo e intenta de nuevo.")
                break;
            case "invalid_pin":
                toReturn.error.message = ("El PIN de la tarjeta con la que deseas pagar es invalido, verificalo e intenta de nuevo.")
                break;
            case "issuer_not_available":
                toReturn.error.message = ("No podemos contactar al banco para realizar la transacción, intenta con algún otro médio de pago.")
                break;
            case "lost_card":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;
            case "merchant_blacklist":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;
            case "new_account_information_available":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Verifica los datos de la tarjeta.")
                break;
            case "no_action_taken":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;
            case "not_permitted":
                toReturn.error.message = ("Pago no permitido, contacta a tu banco para más información e intenta con algún otra tarjeta")
                break;
            case "offline_pin_required":
                toReturn.error.message = ("Pin invalido , intenta de nuevo o con algún otra tarjeta.")
                break;
            case "online_or_offline_pin_required":
                toReturn.error.message = ("Pin invalido , intenta de nuevo o con algún otra tarjeta.")
                break;
            case "pickup_card":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;
            case "pin_try_exceeded":
                toReturn.error.message = ("Numero de intentos excedido, intenta con algún otra tarjeta.")
            case "processing_error":
                toReturn.error.message = ("Ocurrio un error al procesar el pago, intenta de nuevo.")
                break;
            case "reenter_transaction":
                toReturn.error.message = ("Ocurrio un error al procesar el pago, intenta de nuevo.")
                break;
            case "restricted_card":
                toReturn.error.message = ("Lamentablemente no podemos procesar la tarjeta para este pago, intenta con algún otra tarjeta")
                break;
            case "revocation_of_all_authorizations":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "revocation_of_authorization":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "security_violation":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "service_not_allowed":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "stolen_card":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "stop_payment_order":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "testmode_decline":
                toReturn.error.message = ("Estas usando una tarjeta de prueba, intenta con una tarjeta física o dígital")
                break;
            case "transaction_not_allowed":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Contacta a tu banco para más información.")
                break;
            case "try_again_later":
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas. Intenta con algún otra tarjeta o intenta de nuevo más tarde.")
                break;
            case "withdrawal_count_limit_exceeded":
                toReturn.error.message = ("Tú limite de credito esta excedido o no tienes saldo suficiente para realizar el pago, contacta a tu banco para más información.")
                break;
            case "card_declined":
                toReturn.error.message = ("Tarjeta declinada por el banco")
                break;
            default:
                toReturn.error.message = ("El pago ha sido declinado por razones desconocidas.")
                break;

        }
        return toReturn
    }
}

module.exports = Stripe