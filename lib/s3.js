/**
 * lib/s3.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
const AWS = require("aws-sdk");
const uniqid = require("uniqid");

const {
  awsAccessKeyId,
  awsSecretAccessKey,
  awsBucketName,
  awsBucketImagesName,
  awsBucketPdfsName,
  awsBucketProcessedName,
  awsBucketRegion,
  awsCloudfromEndpointImage,
} = require("../config");
AWS.config.setPromisesDependency(require("bluebird"));

AWS.config.update({
  region: awsBucketRegion,
  accessKeyId: awsAccessKeyId,
  secretAccessKey: awsSecretAccessKey,
});

const S3 = {
  uploadVideo: async (data, stream, ext = null) => {
    let key = data.title + "-" + uniqid();
    if (ext) {
      key += "." + ext;
    }

    const params = {
      Bucket: awsBucketName,
      Key: key,
      Body: stream,
    };
    const s3 = new AWS.S3({
      apiVersion: "2006-03-01",
      params,
    });

    const uploadPromise = s3.upload().promise();
    const result = await uploadPromise
      .then((result) => {
        return {
          success: true,
          response: result,
        };
      })
      .catch((err) => {
        console.error(
          "- Error trying to upload a video. (s3)",
          JSON.stringify(err, null, 2)
        );
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2),
        };
      });

    return result;
  },
  deleteVideo: async (key) => {
    const params = {
      Bucket: awsBucketName,
      Key: key,
    };
    const s3 = new AWS.S3();
    const deletePromise = await s3.deleteObject(params, function (err, data) {
      if (err) {
        console.error(
          "- Error trying to delete a video. (s3)",
          JSON.stringify(err, null, 2)
        );
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2),
        };
      } else {
        console.log(data);
        return {
          success: true,
          response: data,
        };
      }
    });
    return deletePromise;
  },

  deleteVideoProcessed: async (key) => {
    const params = {
      Bucket: awsBucketProcessedName,
      Key: key,
    };
    const s3 = new AWS.S3();
    const deletePromise = await s3.deleteObject(params, function (err, data) {
      if (err) {
        console.error(
          "- Error trying to delete a video. (s3)",
          JSON.stringify(err, null, 2)
        );
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2),
        };
      } else {
        console.log(data);
        return {
          success: true,
          response: data,
        };
      }
    });
    return deletePromise;
  },

  genereteS3SignedUrl: async (fileName, type) => {
    let fileExtention = fileName.split(".").pop();
    let key = uniqid() + "." + fileExtention;
    const params = {
      Key: key,
      Expires: 3600,
      ACL: "bucket-owner-full-control",
    };
    if (type == 1) {
      params.Bucket = awsBucketName;
    } else {
      params.Bucket = `${awsBucketImagesName}/tmp`;
    }
    const s3 = new AWS.S3({
      signatureVersion: "v4",
    });
    let url = s3.getSignedUrl("putObject", params);
    if (!url) {
      return {
        success: false,
        message: "ocurrio un error al generar una url pre firmada para s3",
      };
    }
    return {
      success: true,
      url: url,
      keyFile: key,
    };
  },

  //TYPE 1 QUIZ QUESTION
  //TYPE 2 QUIZ ANSWER
  //TYPE 3 PLAYLIST IMAGE
  //TYPE 4 COURSE IMAGE

  moveImageTemporal: async (key, type) => {
    let origin = `${awsBucketImagesName}/tmp/${key}`;
    let destination;
    if (type == 1) {
      destination = `quizQuestion/${key}`;
    } else if (type == 2) {
      destination = `quizAnswer/${key}`;
    } else if (type == 3) {
      destination = `playlistImage/${key}`;
    } else if (type == 4) {
      destination = `courseImage/${key}`;
    } else if (type == 5) {
      destination = `profilePicture/${key}`;
    }
    const params = {
      Bucket: awsBucketImagesName,
      CopySource: origin,
      Key: destination,
    };
    const s3 = new AWS.S3();
    await s3
      .copyObject(params, function (copyErr, copyData) {
        if (copyErr) {
        } else {
          console.log("Copied: ", copyData);
        }
      })
      .promise();
    const params2 = {
      Bucket: `${awsBucketImagesName}`,
      Key: `tmp/${key}`,
    };
    await s3
      .deleteObject(params2, function (err, data) {
        if (err) {
          console.error(
            "- Error trying to delete a Image. (s3)",
            JSON.stringify(err, null, 2)
          );
        } else {
          console.log("Eresed", data);
        }
      })
      .promise();
    return awsCloudfromEndpointImage + destination;
  },

  deleteImage: async (key, type) => {
    const s3 = new AWS.S3();
    let directory;
    if (type == 1) {
      directory = `quizQuestion`;
    } else if (type == 2) {
      directory = `quizAnswer`;
    } else if (type == 5) {
      directory = `profilePicture`;
    } else {
      directory = `tmp`;
    }
    const params2 = {
      Bucket: `${awsBucketImagesName}`,
      Key: `${directory}/${key}`,
    };
    await s3
      .deleteObject(params2, function (err, data) {
        if (err) {
          console.error(
            "- Error trying to delete a Image. (s3)",
            JSON.stringify(err, null, 2)
          );
        } else {
          console.log("Delete Image");
        }
      })
      .promise();
  },

  uploadImageTemp: async (data, stream, ext = null) => {
    let key = data + "-" + uniqid();
    if (ext) {
      key += "." + ext;
    }

    const params = {
      Bucket: `${awsBucketImagesName}/tmp`,
      Key: key,
      Body: stream,
    };
    const s3 = new AWS.S3({
      apiVersion: "2006-03-01",
      params,
    });

    const uploadPromise = s3.upload().promise();

    const result = await uploadPromise
      .then((result) => {
        return {
          success: true,
          response: result,
        };
      })
      .catch((err) => {
        console.error(
          "- Error trying to upload a image. (s3)",
          JSON.stringify(err, null, 2)
        );
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2),
        };
      });

    return result;
  },

  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++  PDF  +++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

  deletePDF: async (key) => {
    const params = {
      Bucket: awsBucketPdfsName,
      Key: key,
    };
    const s3 = new AWS.S3();
    const deletePromise = await s3.deleteObject(params, function (err, data) {
      if (err) {
        console.error(
          "- Error trying to delete a PDF. (s3)",
          JSON.stringify(err, null, 2)
        );
        return {
          success: false,
          code: 500,
          error: JSON.stringify(err, null, 2),
        };
      } else {
        console.log("PDF deleted:", data);
        return {
          success: true,
          response: data,
        };
      }
    });
    return deletePromise;
  },

  generatePDFSignedUrl: async (fileName) => {
    let fileExtension = fileName.split(".").pop().toLowerCase();
    // Validar que sea un PDF
    if (fileExtension !== "pdf") {
      return {
        success: false,
        error: "File must be a PDF",
      };
    }

    let key = uniqid() + ".pdf";
    const params = {
      Bucket: awsBucketPdfsName,
      Key: key,
      Expires: 3600,
      ContentType: "application/pdf",
      ACL: "bucket-owner-full-control",
    };

    const s3 = new AWS.S3({
      signatureVersion: "v4",
    });

    try {
      const url = s3.getSignedUrl("putObject", params);
      return {
        success: true,
        url: url,
        keyFile: key,
      };
    } catch (err) {
      console.error("Error generating PDF signed URL:", err);
      return {
        success: false,
        error: "Error generating signed URL for PDF upload",
      };
    }
  },

  uploadPDFThumbnail: async (thumbnail, pdfKey) => {
    const key = `thumbnails/${pdfKey.split(".")[0]}.png`;

    const params = {
      Bucket: awsBucketPdfsName,
      Key: key,
      Body: thumbnail,
      ContentType: "image/png",
    };

    const s3 = new AWS.S3({
      apiVersion: "2006-03-01",
      params,
    });

    try {
      const result = await s3.upload().promise();
      return {
        success: true,
        key: key,
        url: `${awsBucketPdfsName}/${key}`,
      };
    } catch (err) {
      console.error("Error uploading PDF thumbnail:", err);
      return {
        success: false,
        error: "Error uploading PDF thumbnail",
      };
    }
  },

  readStudentPDF: async (idPdf, idStudent) => {
    const s3 = new AWS.S3();
    try {
      const key = idPdf.split(".")[0] + "_" + idStudent + ".pdf";
      const params = {
        Bucket: `${awsBucketPdfsName}/secured_files`,
        Key: key,
      };

      const data = await s3.getObject(params).promise();
      return {
        success: true,
        response: {
          data,
          key,
          flag: 2, // Assuming the flag for successful read
        },
      };
    } catch (error) {
      console.log("erro en readStudentPDF",error);
      return {
        success: false,
        error: "PDF not found",
        httpCode: 404,
        flag: 1, // Assuming the flag for not found
      };
    }
  },

  readOriginalPDF: async (key) => {
    const s3 = new AWS.S3();
    const params = {
      Bucket: awsBucketPdfsName,
      Key: key,
    };

    try {
      const data = await s3.getObject(params).promise();
      return data.Body;
    } catch (err) {
      if (err.code === "NoSuchKey") {
        console.log("readOriginalPDF El archivo no existe",err);
        throw new Error("El archivo no existe");
      } else {
        console.log("Error al leer el archivo",err);
        throw new Error("Error al leer el archivo");
      }
    }
  },

  saveSignedPdf: async (idPdf, idStudent, pdfBytes) => {
    const s3 = new AWS.S3();
    try {
      const key = idPdf.split(".")[0] + "_" + idStudent + ".pdf";
      const params = {
        Bucket: `${awsBucketPdfsName}/secured_files`,
        Key: key,
        Body: pdfBytes,
        ContentType: "application/pdf",
      };

      const result = await s3.upload(params).promise();

      return {
        success: true,
        response: {
          url: result.Location,
          key: key,
          flag: 3, // Assuming the flag for successful save
        },
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
};

module.exports = S3;
