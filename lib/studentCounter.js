/**
 * lib/studentCounter.js
 *
 * @description :: Counts students by course
 * @docs        :: TODO
 */
const _ = require('underscore')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const StudentModel = require('../db/students')
const StudentListModel = require('../db/studentLists')

const superiorCourse = '635021ec81728d121ecf92ed'

// const simulacros = [
//   '5cc780972ad6ae065d1c9856',
//   '5cc7808c2ad6ae065d1c9855',
//   '5cc780882ad6ae065d1c9854',
//   '5cc780812ad6ae065d1c9853',
//   '5cc7807f2ad6ae065d1c9852',
//   '5cc3f3a388311c1d60b10ae5'
// ]

const simulacros = [
  '63221caeb71e4512313ab900', //fisica avanzada
  '63221ad943219511c49aad09', // calculo avanzado
  '631e704135de9e5c70414bba', // biologia 
  '631e6c3ac55b3511de1da5b9', // espaniol
  '5d3e1c342dd49004f0fee961', // mate desde cero
]

const StudentCounter = {
  getTotal: async () => {
    const deleted = await StudentModel
      .find({
        deleted: { $exists: true, $eq: true }
      })
      .countDocuments()
    const total = await StudentModel
      .find({})
      .countDocuments()
    return {
      deleted,
      total
    }
  },

  getSuperiorStudents: async () => {
    const sts = await StudentCounter.getSuperiorStudentIds()
    return sts.length
  },

  getSuperiorStudentIds: async () => {
    const list = await StudentCounter.getStudentIdsByCourse(superiorCourse)
      .then(result => {
        const lt = []
        result.forEach(id => {
          lt.push(id)
        })
        return lt
      })

    const sts = _.uniq(list)
    return sts
  },

  getComipemsStudents: async () => {
    const sts = await StudentCounter.getComipemsStIds()
    return sts.length
  },

  getComipemsStIds: async () => {
    const courseIds = await CourseModel
      .find({ dependency: 5 })
      .select('_id')
      .then(courses => {
        return courses.map(course => course._id)
      })

    const list = await Promise
      .all(courseIds.map(id => StudentCounter.getStudentIdsByCourse(id)))
      .then(result => {
        const lt = []
        result.forEach(arr => {
          arr.forEach(id => {
            lt.push(id)
          })
        })
        return lt
      })

    const sts = _.uniq(list)
    return sts
  },

  /**
   * getStudentIdsByCourse
   *
   * @description Get a list of student ids by course
   * @param {string} courseId - Course id
   * @returns {Array} A list of student ids
   */
  getStudentIdsByCourse: async courseId => {
    const stListIds = await GroupModel
      .find({ 
        course: courseId ,
        startDate: {$gte: new Date("2021-12-31")}
      })
      .select('studentList')
      .then(result => {
        return result.map(group => group.studentList)
      })

    const studentIds = await Promise
      .all(stListIds.map(id => StudentCounter.getStudentsByStList(id)))
      .then(result => {
        const stArr = []
        result.forEach(arr => {
          arr.forEach(id => {
            stArr.push(id)
          })
        })
        return stArr
      })
    return studentIds
  },

  /**
   * getSuperiorCoursesIds
   */
  getSuperiorCoursesIds: async () => {
    const courseIds = await CourseModel
      .find({ _id: superiorCourse })
      .select('_id')
      .then(courses => {
        return courses.map(course => course._id)
      })
    return courseIds
  },

  getComipemsCoursesIds: async () => {
    const courseIds = await CourseModel
      .find({ dependency: 5 })
      .select('_id')
      .then(courses => {
        return courses.map(course => course._id)
      })
    return courseIds
  },

  /**
   * getDiffSimulacro
   */
  getDiffSimulacro: async () => {
    const courseIds = await StudentCounter
      .getSuperiorCoursesIds()

    let superiorStIds = await Promise
      .all(courseIds.map(courseId => {
        return StudentCounter
          .getStudentIdsByCourse(courseId)
      }))
      .then(result => {
        const sts = []
        result.forEach(res => {
          res.forEach(id => { sts.push(String(id)) })
        })
        return sts
      })

    let simStIds = await Promise
      .all(
        simulacros.map(courseId => {
          return StudentCounter
            .getStudentIdsByCourse(courseId)
        })
      )
      .then(result => {
        const sts = []
        result.forEach(res => {
          res.forEach(id => { sts.push(String(id)) })
        })
        return sts
      })

    superiorStIds = _.uniq(superiorStIds)
      .filter(id => id && id.length !== 0 && id !== 'null')
    simStIds = _.uniq(simStIds)
      .filter(id => id && id.length !== 0 && id !== 'null')

    let diff = []
    // superiorStIds.forEach(element => {
    //   if (simStIds.indexOf(element) === -1) {
    //     diff.push(element)
    //   }
    // })
    simStIds.forEach(element => {
      if (superiorStIds.indexOf(element) === -1) {
        diff.push(element)
      }
    })
    diff = _.uniq(diff)

    // const diff = _.uniq(_.difference(superiorStIds, simStIds))

    console.log('-----------')
    console.log(`alumnos de superior: ${superiorStIds.length}`)
    console.log(`alumnos que han tomado el examen simulacro: ${simStIds.length}`)
    console.log(`la diferencia: ${diff.length}`)
    console.log(superiorStIds[0])
    console.log(simStIds[0])
    console.log('-----------')

    // const diffSt = await Promise
    //   .all(diff.map(id => {
    //     return StudentModel.findOne({ _id: id })
    //       .select(studentFields.join(' '))
    //   }))

    // return diffSt
    return diff
  },

  /**
   * getStudentsByStList
   * @description Get a list of student ids by student list
   * @param {string} stId StudentList id
   * @returns {string} List of students ids
   */
  getStudentsByStList: async stId => {
    const stList = await StudentListModel
      .findOne({ _id: stId })
      .select('list')
      .then(result => result.list)

    return stList
  },

  /**
   * getGroupStudents
   *
   * @description Get a list of student ids by group.
   *              Search by a student list and gets its students
   * @param {string} groupId - Group id
   * @returns {Array} A list of student ids
   */
  getGroupStudentIds: async groupId => {
    const stListId = await GroupModel
      .findOne({ _id: groupId })
      .select('studentList')
      .then(result => result.studentList)
    const stList = await StudentListModel
      .findOne({ _id: stListId })
      .select('list')
      .then(result => result.list)
    return stList
  },

  getWorkshopByCourse: async courseId => {
    const totalByCourse = {}
    const course = await CourseModel
      .findOne({ _id: courseId })
      .select('name')

    const stList = await GroupModel
      .find({
        course: courseId
      })
      .select('_id studentList')
      .then(groups => {
        return groups.map(group => {
          return group.studentList
        })
      })

    totalByCourse['course'] = course.name
    totalByCourse['groups'] = stList.length
    totalByCourse['students'] = await Promise
      .all(stList.map(stId => StudentCounter.getTotalByStudentList(stId)))
      .then(results => {
        let total = 0
        results.forEach(element => {
          total += parseInt(element.total)
        })
        return total
      })
    return totalByCourse
  },

  getTotalByStudentList: async studentListId => {
    const totalBySL = await StudentListModel
      .findOne({ _id: studentListId })
      .select('list')
      .then(studentList => {
        return studentList.list.length
      })
    return {
      total: totalBySL
    }
  },

  /**
   * getByCourse
   * @param {string} id - Course Object id
   * @returns {integer} - Number of unique student by course
   */
  getByCourse: async id => {
    const stListIds = await GroupModel
      .find({ course: id })
      .select('studentList')
      .then(groups => {
        return groups.map(group => group.studentList)
      })

    const total = await Promise
      .all(stListIds.map(stId => StudentCounter.getTotalByStudentList(stId)))
      .then(results => {
        let total = 0
        results.forEach(result => {
          total += result.total
        })

        return total
      })

    return total
  }
}

module.exports = StudentCounter
