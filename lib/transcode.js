const {
    awsAccessKeyId,
    awsSecretAccessKey,
    awsBucketName,
    awsBucketRegion,
    awsMediaConverterPoint
} = require('../config')
var AWS = require('aws-sdk');
// Set up aws api
AWS.config.update({
    region: awsBucketRegion,
    accessKeyId: awsAccessKeyId,
    secretAccessKey: awsSecretAccessKey
});

AWS.config.mediaconvert = { endpoint: awsMediaConverterPoint };


const transcode = {
    initTranscode: async (fileIn) => {
        // snippet-start:[mediaconvert.JavaScript.jobs.createJob_define]
        var params = {
            "UserMetadata": {},
            "Role": "arn:aws:iam::043962249814:role/Mediaconverter-rol",
            "Settings": {
                "OutputGroups": [
                    {
                        "Name": "File Group",
                        "Outputs": [
                            {
                                "ContainerSettings": {
                                    "Container": "MP4",
                                    "Mp4Settings": {
                                        "CslgAtom": "INCLUDE",
                                        "FreeSpaceBox": "EXCLUDE",
                                        "MoovPlacement": "PROGRESSIVE_DOWNLOAD"
                                    }
                                },
                                "VideoDescription": {
                                    "Width": 1280,
                                    "ScalingBehavior": "DEFAULT",
                                    "Height": 720,
                                    "VideoPreprocessors": {
                                        "Deinterlacer": {
                                            "Algorithm": "INTERPOLATE",
                                            "Mode": "DEINTERLACE",
                                            "Control": "NORMAL"
                                        }
                                    },
                                    "TimecodeInsertion": "DISABLED",
                                    "AntiAlias": "ENABLED",
                                    "Sharpness": 50,
                                    "CodecSettings": {
                                        "Codec": "H_264",
                                        "H264Settings": {
                                            "InterlaceMode": "PROGRESSIVE",
                                            "ParNumerator": 1,
                                            "NumberReferenceFrames": 3,
                                            "Syntax": "DEFAULT",
                                            "Softness": 0,
                                            "FramerateDenominator": 1,
                                            "GopClosedCadence": 1,
                                            "HrdBufferInitialFillPercentage": 90,
                                            "GopSize": 2,
                                            "Slices": 1,
                                            "GopBReference": "ENABLED",
                                            "HrdBufferSize": 9000000,
                                            "MaxBitrate": 2139000,
                                            "SlowPal": "DISABLED",
                                            "ParDenominator": 1,
                                            "SpatialAdaptiveQuantization": "ENABLED",
                                            "TemporalAdaptiveQuantization": "ENABLED",
                                            "FlickerAdaptiveQuantization": "ENABLED",
                                            "EntropyEncoding": "CABAC",
                                            "Bitrate": 2139000,
                                            "FramerateControl": "SPECIFIED",
                                            "RateControlMode": "VBR",
                                            "CodecProfile": "HIGH",
                                            "Telecine": "NONE",
                                            "FramerateNumerator": 30,
                                            "MinIInterval": 0,
                                            "AdaptiveQuantization": "HIGH",
                                            "CodecLevel": "LEVEL_4",
                                            "FieldEncoding": "PAFF",
                                            "SceneChangeDetect": "ENABLED",
                                            "QualityTuningLevel": "SINGLE_PASS_HQ",
                                            "FramerateConversionAlgorithm": "DUPLICATE_DROP",
                                            "UnregisteredSeiTimecode": "DISABLED",
                                            "GopSizeUnits": "SECONDS",
                                            "ParControl": "SPECIFIED",
                                            "NumberBFramesBetweenReferenceFrames": 3,
                                            "RepeatPps": "DISABLED"
                                        }
                                    },
                                    "AfdSignaling": "NONE",
                                    "DropFrameTimecode": "ENABLED",
                                    "RespondToAfd": "NONE",
                                    "ColorMetadata": "INSERT"
                                },
                                "AudioDescriptions": [
                                    {
                                        "AudioTypeControl": "FOLLOW_INPUT",
                                        "AudioSourceName": "Audio Selector 1",
                                        "CodecSettings": {
                                            "Codec": "AAC",
                                            "AacSettings": {
                                                "AudioDescriptionBroadcasterMix": "NORMAL",
                                                "Bitrate": 128000,
                                                "RateControlMode": "CBR",
                                                "CodecProfile": "LC",
                                                "CodingMode": "CODING_MODE_2_0",
                                                "RawFormat": "NONE",
                                                "SampleRate": 44100,
                                                "Specification": "MPEG4"
                                            }
                                        },
                                        "LanguageCodeControl": "FOLLOW_INPUT",
                                        "AudioType": 0
                                    }
                                ]
                            }
                        ],
                        "OutputGroupSettings": {
                            "Type": "FILE_GROUP_SETTINGS",
                            "FileGroupSettings": {
                                "Destination": "s3://ipn-videos-processed/"
                            }
                        }
                    }
                ],
                "AdAvailOffset": 0,
                "Inputs": [
                    {
                        "AudioSelectors": {
                            "Audio Selector 1": {
                                "Offset": 0,
                                "DefaultSelection": "DEFAULT",
                                "ProgramSelection": 1
                            }
                        },
                        "VideoSelector": {
                            "ColorSpace": "FOLLOW",
                        },
                        "FilterEnable": "AUTO",
                        "PsiControl": "USE_PSI",
                        "FilterStrength": 0,
                        "DeblockFilter": "DISABLED",
                        "DenoiseFilter": "DISABLED",
                        "TimecodeSource": "EMBEDDED",
                        "FileInput": "s3://" + awsBucketName + "/" + fileIn
                    }
                ]
            }
        };
        const mediaconvert = new AWS.MediaConvert();
        var endpointPromise = mediaconvert.createJob(params).promise();
        endpointPromise.then(
            function (data) {
                return data
            },
            function (err) {
                return err
            }
        );
        const result = await endpointPromise;
        return result;
    }
}
module.exports = transcode