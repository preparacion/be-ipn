/**
 * lib/s3.js
 *
 * @description :: Loads config values
 * @docs        :: TODO
 */
let Vimeo = require('vimeo').Vimeo;
const {
    vimeoClientId,
    vimeoSecret,
    vimeoAccessToken
} = require('../config')
let client = new Vimeo("{client_id}", "{client_secret}", "{access_token}");

AWS.config.update({
    region: awsBucketRegion,
    accessKeyId: awsAccessKeyId,
    secretAccessKey: awsSecretAccessKey
})

const S3 = {
    listVideos: async () => {
        client.request({
            method: 'GET',
            path: '//videos'
        }, function (error, body, status_code, headers) {
            if (error) {
                console.log(error);
            }

            console.log(body);
        })
    },
    searchVideo: async () => {

    },
}

module.exports = S3
