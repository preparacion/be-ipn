/groups:
  get:
    summary: List all the groups
    description: Get all the groups
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - skip:
        name: skip
        in: query
        type: integer
        example: 20
        description: Pagination. The number of result to skip
      - limit:
        name: limit
        in: query
        type: integer
        example: 9
        default: 0
        description: Pagination. The number of results to show
      - schedule:
        name: schedule
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group schedule
      - course:
        name: course
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group course
      - teacher:
        name: teacher
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group teacher
      - location:
        name: location
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Location
      - stRngStart:
        name: stRngStart
        in: query
        type: date
        example: 2019-01-26
        description: Group startDate Range
      - stRngEnd:
        name: stRngEnd
        in: query
        type: date
        example: 2019-01-26
        description: Group startDate Range
      - endRngStart:
        name: endRngStart
        in: query
        type: date
        example: 2019-01-26
        description: Group endDate Range
      - endRngEnd:
        name: endRngEnd
        in: query
        type: date
        example: 2019-01-26
        description: Group endDate Range
    responses:
      400:
        $ref: '#/responses/badRequest'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            groups:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/GroupInfo'
  post:
    summary: Create
    description: Creates a new group
    produces:
      - application/json
    consumes:
      - application/json
    tags:
      - Groups
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Schedule object schema
            properties:
              name:
                type: string
                description: Group's name
                example: ABC-123
              startDate:
                type: date
                description: Group's start date.
                example: 2019-11-02
              endDate:
                type: date
                description: Group's end date.
                example: 2020-11-02
              active:
                type: boolean
                description: The group is active?
                default: false
                example: true
              activeLanding:
                type: boolean
                description: The group is active in landing Page?
                default: false
                example: true
              quota:
                type: integer
                description: Group's quota
                example: 42
              comment:
                type: string
                description: Any comment
                example: This is a special group
              course:
                type: objectId
                description: Course associated. (Required).
                example: 5c02cf14eefac11c7a6d0006
              teacher:
                type: objectId
                description: Group's teacher
                example: 5c02cf14eefac11c7a6d0005
              schedule:
                type: objectId
                description: Deprecated. Use schedules array instead.
                example: 5c02cf14eefac11c7a6d0005
              classRoom:
                type: objectId
                description: classRoom associated. (Required).
                example: 5c02cf14eefac11c7a6d0006
              location:
                type: objectId
                description: Location associated. (Required).
                example: 5c02cf14eefac11c7a6d0006
              studentList:
                type: objectId
                description: StudentList associated. (Required).
                example: 5c02cf14eefac11c7a6d0006
              schedules:
                type: array
                description: List of schedules
                items:
                  type: object
                  properties:
                    day:
                      type: integer
                      description: Day of the week. Is the index in this array ['l', 'm', 'w', 'j', 'v', 's', 'd']
                      example: 2
                    startHour:
                      type: string
                      description: Four letter start hour. 24 hours format
                      example: 0345
                    endHour:
                      type: string
                      description: Four letter end hour. 24 hours format
                      example: 2330
    responses:
      400:
        $ref: '#/responses/badRequest'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            group:
              $ref: '#/definitions/schemas/Group'
/groups/:id:
  get:
    summary: Get by Group Id.
    description: Get all the Group info by group Id.
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            group:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/GroupInfo'
  put:
    summary: Update
    description: Update a single group by id
    produces:
      - application/json
    consumes:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Schedule object schema
            properties:
              name:
                type: string
                description: Group's name
                example: ABC-123
              startDate:
                type: date
                description: Group's start date.
                example: 2019-11-02
              active:
                type: boolean
                description: The group is active?
                default: false
                example: true
              quota:
                type: integer
                description: Group's quota
                example: 42
              comment:
                type: string
                description: Any comment
                example: This is a special group
              course:
                type: objectId
                description: Course associated.
                example: 5c02cf14eefac11c7a6d0006
              teacher:
                type: objectId
                description: Group's teacher
                example: 5c02cf14eefac11c7a6d0005
              schedule:
                type: objectId
                description: Deprecated. Use schedules array instead.
                example: 5c02cf14eefac11c7a6d0005
              classRoom:
                type: objectId
                description: classRoom associated.
                example: 5c02cf14eefac11c7a6d0006
              location:
                type: objectId
                description: Location associated.
                example: 5c02cf14eefac11c7a6d0006
              studentList:
                type: objectId
                description: StudentList associated.
                example: 5c02cf14eefac11c7a6d0006
              schedules:
                type: array
                description: List of schedules
                items:
                  type: object
                  properties:
                    day:
                      type: integer
                      description: Day of the week. Is the index in this array ['l', 'm', 'w', 'j', 'v', 's', 'd']
                      example: 2
                    startHour:
                      type: string
                      description: Four letter start hour. 24 hours format
                      example: 0345
                    endHour:
                      type: string
                      description: Four letter end hour. 24 hours format
                      example: 2330
    responses:
      400:
        $ref: '#/responses/badRequest'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            group:
              $ref: '#/definitions/schemas/Group'
  delete:
    summary: Delete group
    description: Removes a specific group by id
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            message:
              type: string
              description: action message
              example: "deleted"
/groups/:id/active/toggle:
  put:
    summary: Status group
    description: Active/Inactive group
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            active:
              type: boolean
              description: Active or inactive status group
/groups/:id/enroll/:idStudent:
  put:
    summary: Enroll student
    description: Enroll a student to a group
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - id:
        name: id
        in: path
        type: objectId
        example: 5ea354a1cda284543232c338
        description: Group Identifier
      - idStudent:
        name: idStudent
        in: path
        type: objectId
        example: 5dafc6e5f5288f11b61b4ac7
        description: Student Identifier
    requestBody:
      content:
        application/json:
          schema:
            $ref: '#/definitions/schemas/PaymentEnroll'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            enrolled:
              type: boolean
              description: if the student was enrolled
              example: true
            creditId:
              type: objectId
              description: Credit identifier
              example: 5ede88c5cc3f9821346ee323
            paymentId:
              type: objectId
              description: payment identifier
              example: 5ede88c5cc3f9821346ee321
/groups/:id/material:
  put:
    summary: Update material
    description: Update material status by student
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - id:
        name: id
        in: path
        type: objectId
        example: 5ea354a1cda284543232c338
        description: Group Identifier
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: material object request
            properties:
              material:
                $ref: '#/definitions/schemas/MaterialGroup'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            material:
              type: array
              description: material list
              example: []
            studentList:
              $ref: '#/definitions/schemas/StudentList'
/groups/:id/students:
  get:
    summary: Get students
    description: Get the enrolled students to a specific group
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            students:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/Student'
            studentList:
              $ref: '#/definitions/schemas/StudentList'
/groups/:id/students/lite:
  get:
    summary: (lite) Get students
    description: Get the enrolled students to a specific group
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            students:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/StudentLT'
            studentList:
              $ref: '#/definitions/schemas/StudentListLT'
/groups/:id/students/download/:name:
  get:
    summary: Download student list
    description: Generates a csv with the students rolled in a group
    produces:
      - application/octet-stream
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'      
      - :name:
        name: name
        in: path
        type: string
        example: uno
        description: .csv file name
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: 'OK'
        examples:
          text/csv; charset=utf-8:
            Apellido Paterno, Apellido Materno, Nombre, Salón
/groups/:id/receipt/:studentId:
  post:
    summary: Student receipt
    description: Send a group receipt to a student
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
      - studentId:
        name: studentId
        in: path
        type: objectId
        example: 5dafc6e5f5288f11b61b4ac7
        description: student Identifier
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            status:
              type: message
              description: receipt status
              example: "sended"

/groups/available/:id:
  get:
    summary: Get availability
    description: Get group availability by group Id
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            result:
              $ref: '#/definitions/schemas/GroupInfo'
/groups/classroom/:id:
  get:
    summary: Groups by classRoom
    description: Get the groups by classroom id
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - $ref: '#/parameters/id'
      - skip:
        name: skip
        in: query
        type: integer
        example: 20
        description: Pagination. The number of result to skip
      - limit:
        name: limit
        in: query
        type: integer
        example: 9
        default: 0
        description: Pagination. The number of results to show
      - course:
        name: course
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group course
      - startDate:
        name: startDate
        in: query
        type: date
        example: 2019-12-26
      - endDate:
        name: endDate
        in: query
        type: date
        example: 2019-12-26
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            groups:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/Group'
/groups/sp/lite:
  get:
    summary: (lite) List all the groups
    description: Get all the groups with less propeties
    produces:
      - application/json
    tags:
      - Groups
    parameters:
      - skip:
        name: skip
        in: query
        type: integer
        example: 20
        description: Pagination. The number of result to skip
      - limit:
        name: limit
        in: query
        type: integer
        example: 9
        default: 0
        description: Pagination. The number of results to show
      - schedule:
        name: schedule
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group schedule
      - course:
        name: course
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group course
      - teacher:
        name: teacher
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Group teacher
      - location:
        name: location
        in: query
        type: objectId
        example: 5c02cf14eefac11c7a6d0006
        description: Location
      - stRngStart:
        name: stRngStart
        in: query
        type: date
        example: 2019-01-26
        description: Group startDate Range
      - stRngEnd:
        name: stRngEnd
        in: query
        type: date
        example: 2019-01-26
        description: Group startDate Range
      - endRngStart:
        name: endRngStart
        in: query
        type: date
        example: 2019-01-26
        description: Group endDate Range
      - endRngEnd:
        name: endRngEnd
        in: query
        type: date
        example: 2019-01-26
        description: Group endDate Range
    responses:
      400:
        $ref: '#/responses/badRequest'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            groups:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/GroupInfoLT'
