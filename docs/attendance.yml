/attendance/group/:id:
  get:
    summary: By group
    description: Get the group attendance in a specific date
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
      - startDate:
        name: startDate
        in: query
        description: Range. start date
        type: date
        example: 2019-01-01
      - endDate:
        name: endDate
        in: query
        description: Range. end date
        type: date
        example: 2019-01-01
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The response result
              example: true
            attendanceList:
              $ref: '#/definitions/schemas/AttendanceList'
            studentList:
              $ref: '#/definitions/schemas/StudentList'
            students:
              type: 
              description: List of all students
              items:
                type: object
                description: Student properties
                properties:
                  _id: 
                    type: string
                    description: student identifier
                    example: 5dd032fdf0ca913482230646
                  name:
                    type: string
                    description: student name
                    example: "Daniela"
                  lastName:
                    type: string
                    description: student lastname
                    example: "Soto"
                  email:
                    type: string
                    description: student email
                    example: "dansoto030502@gmail.com"
                  secondLastName:
                    type: string
                    description: student second lastname
                    example: "Silva"
                  phoneNumber:
                    type: string
                    description: student phone number
                    example: "5568685882"

/attendance/student/:id:
  get:
    summary: By student
    description: Get the student attendance average
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
      - startDate:
        name: startDate
        in: query
        description: Range. start date
        type: date
        example: 2019-01-01
      - endDate:
        name: endDate
        in: query
        description: Range. end date
        type: date
        example: 2019-01-01
      - group:
        name: group
        in: query
        description: Filter by group
        type: ObjectId
        example: 5c5eaa245f94d252c9bc422c
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The response result
              example: true
            attendance:
              type: object
              properties:
                _id:
                  type: string
                  description: attendance identifier
                  example: "5e3580d6f286c5b0f6b506e4"
                student:
                  type: string
                  description: student identifier
                  example: "5dafc6e5f5288f11b61b4ac7"
                attendance:
                  type: array
                  description: List of attendance
                  items:
                    type: object
                    properties:
                      date:
                        type: date
                        description: registered date
                        example: "2020-02-06T00:00:00.000Z"
                      _id:
                        type: string
                        description:
                        example: "5e3580d5d7c876575a3547a0"
                      group:
                        type: string
                        description: group identifier
                        example: "5dc6619cddb6e2178da83710"
                      attendance:
                        type: boolean
                        description: attendance could be true or false
                        example: true
            average:
              type: float
              description: total attendance averaged in true
              example: 33.33333333333333

/attendance/teacher/:id:
  get:
    summary: By teacher
    description: Get the teacher attendance
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
      - startDate:
        name: startDate
        in: query
        description: Range. start date
        type: date
        example: 2019-01-01
      - endDate:
        name: endDate
        in: query
        description: Range. end date
        type: date
        example: 2019-01-01
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The response result
              example: true
            attendance:
              $ref: '#/definitions/schemas/Attendance'

/attendance:
  post:
    summary: Register
    description: Register the attendance in a group
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Attendance
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Attendance object
            properties:
              students:
                type: array
                description: List of students objectIds (required)
                items:
                  type: objectId
                  description: List of students ids (required)
                  example: 5c02cf14eefac11c7a6d0006
              studentList:
                type: objectId
                description: Student list (required)
                example: 5c02cf14eefac11c7a6d0005
              date:
                type: date
                description: today.
                example: 2018-12-26
              teacher:
                type: objectId
                description: Teacher id (user)
                example: 5c02cf14eefac11c7a6d0005
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          $ref: '#/definitions/schemas/AttendanceResponse'

/attendance/:id:
  put:
    summary: Update
    description: Update the attendance in a group
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Attendance object
            properties:
              students:
                type: array
                description: List of students objectIds (required)
                items:
                  type: objectId
                  description: List of students ids (required)
                  example: 5c02cf14eefac11c7a6d0006
              studentList:
                type: objectId
                description: Student list (required)
                example: 5c02cf14eefac11c7a6d0005
              date:
                type: date
                description: today.
                example: 2018-12-26
              teacher:
                type: objectId
                description: Teacher id (user)
                example: 5c02cf14eefac11c7a6d0005
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          $ref: '#/definitions/schemas/AttendanceResponse'
/attendance/single/:studentId:
  put:
    summary: Update attendance
    description: Update a single attendance by date
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Attendance object
            properties:
              date:
                type: date
                description: today.
                example: 2018-12-26
              attendance:
                type: boolean
                description: attendance
                example: true
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          $ref: '#/definitions/schemas/AttendanceResponse'
/attendance/qr/:id:
  put:
    summary: Update by QR
    description: Update a single attendance by QR CODE
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          description: Attendance object
          properties:
            success:
              type: boolean
              description: OK
              example: true
            student:
              $ref: '#/definitions/schemas/Student'
            group:
              $ref: '#/definitions/schemas/AttendanceGroup'
/attendance/moduleQr/:id:
  put:
    summary: Update Module QR
    description: Update Comipems Module by QR CODE
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Attendance
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: Attendance object
            properties:
              module:
                type: integer
                description: Number of module assigned
                example: 1
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          description: Attendance object
          properties:
            success:
              type: boolean
              description: OK
              example: true
            student:
              $ref: '#/definitions/schemas/Student'
            group:
              $ref: '#/definitions/schemas/AttendanceGroup'

                
                
                
