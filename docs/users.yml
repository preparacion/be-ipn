/users:
  post:
    summary: New user
    description: Create a new user
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    requestBody:
      content:
        application/json:
          schema:
            $ref: '#/definitions/schemas/UserReq'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          $ref: '#/definitions/schemas/User'
  get:
    summary: Get users
    description: Get all the users
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - skip:
        name: skip
        in: query
        type: integer
        example: 20
        description: Pagination. The number of result to skip
      - limit:
        name: limit
        in: query
        type: integer
        example: 9
        default: 0
        description: Pagination. The number of results to show
      - teachers:
        name: teachers
        in: query
        type: boolean
        example: true
        description: Shows only the teachers
      - rolesIn:
        name: rolesIn
        in: query
        type: string
        example: [5d07dfa3cf2183401b20325a, 5d07dfa3cf2183401b20325c]
        description: This service will show only the users with roles in this list.
      - noRolesIn:
        name: noRolesIn
        in: query
        type: string
        example: [5d07dfa3cf2183401b20325a, 5d07dfa3cf2183401b20325c]
        description: This service won't show the users with roles in this list.
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            users:
              type: array
              description: List of User
              items:
                $ref: '#/definitions/schemas/User'

/users/:id:
  get:
    summary: Get user by Id
    description: Get all user information
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            user:
              $ref: '#/definitions/schemas/User'
  put:
    summary: Update
    description: Update a single user by id
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            $ref: '#/definitions/schemas/UserReq'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            users:
              $ref: '#/definitions/schemas/User'
  delete:
    summary: Delete
    description: Removes a single user by id
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The response result
              example: true
            deleted:
              type: boolean
              description: Is Deleted
              example: true
/users/:id/role:
  put:
    summary: Update role
    description: Update a user role by id
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    requestBody:
      content:
        application/json:
          schema:
            type: object
            description: role properties
            properties:
              roles:
                type: array
                description: role identifiers list
                example: ["5d07dfa3cf2183401b20325a"]
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
              example: true
            users:
              $ref: '#/definitions/schemas/User'
/users/:id/groups:
  get:
    summary: Get groups
    description: Get the groups that have this user as teacher
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request
            groups:
              type: array
              description: List of groups
              items:
                $ref: '#/definitions/schemas/GroupInfo'
/users/:id/pass/recover:
  get:
    summary: Recover password
    description: Recover user password request
    consumes:
      - application/json
    produces:
      - application/json
    tags:
      - Users
    parameters:
      - $ref: '#/parameters/id'
    responses:
      400:
        $ref: '#/responses/badRequest'
      404:
        $ref: '#/responses/notFound'
      200:
        description: Ok
        schema:
          type: object
          properties:
            success:
              type: boolean
              description: The result of the request