/**
 * router/api/quizzAttempt.js
 *
 * @description :: Describes the quizz Attempt api 
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Appoiments = require('../../models/appoiments')

// validation schemas
const {
    createAppoiment
  } = require('../../schemas/appoiments')

router.prefix('/api/appoiments')

router.get('/', async (ctx, next) => {
    if (ctx.state && ctx.state.user) {
        const result = await Appoiments.get()
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

router.post('/:idGroup', async (ctx, next) => {
    const idGroup = ctx.params.idGroup
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const result = await Appoiments.create(idGroup, body, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


module.exports = router