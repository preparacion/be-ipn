/**
 * router/api/payments.js
 *
 * @description :: Describes the payments api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Payments = require('../../models/payments')

const {
  paymentSchema,
  paymentIdSchema,
  paymentUpdateSchema,
  createSchema,
  paginationSchema
} = require('../../schemas/payment')


router.prefix('/api/payments')

// GET ALL PAYMENTS
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Payments.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `Get all the payments`
      ctx.state.data = {}

      ctx.body = {
        success: true,
        payments: result.payments
      }
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// CREATE A PAYMENT
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}
  const user = ctx.state.user

  const validationResult = Joi.validate(body, createSchema)
  if (validationResult.error === null) {
    const result = await Payments.create(body, user)
    if (result.success) {
      ctx.state.action = `create a payment`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Get a single item
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Payments.getById(id)

  if (result.success) {
    ctx.state.action = `get a payment - by id ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      payment: result.payment
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// UPDATE A PAYMENT BY ID
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(_.extend(body, { id }), paymentUpdateSchema)

  if (validationResult.error === null) {
    const result = await Payments.update(id, body)

    if (result.success) {
      ctx.state.action = `update a payment`
      ctx.state.data = { body, id }

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// DELETE A PAYMENT BY ITS ID
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, paymentIdSchema)

  if (validationResult.error === null) {
    const result = await Payments.delete(id)

    if (result.success) {
      ctx.state.action = `delete a payment`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET PAYMENTS BY STUDENT ID
router.get('/student/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Payments.getByStudentId(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


//TODO: REQUIRE DOCUMENTATION
// GET PAYMENTS BY STUDENT ID GROUPED BY COURSE
/**
 * 
 */
router.get('/student/:id/byCourse', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Payments.getByStudentIdGroupedByCourse(id)
  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// SEND PAYMENT TICKET
router.put('/send/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, paymentIdSchema)

  if (validationResult.error === null) {
    const result = await Payments.send(id)

    if (result.success) {
      ctx.state.action = `send a payment`
      ctx.state.data = { id }
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



module.exports = router
