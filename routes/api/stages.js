/**
 * router/api/stages.js
 *
 * @description :: Describes the stages api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Stage = require('../../models/stages')
const stageObj = new Stage()

const {
  stageIdSchema,
  stageSchema,
  updateStageSchema
} = require('../../schemas/stage')

router.prefix('/api/stages')

//CREATE STAGE
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, stageSchema)
  if (validationResult.error === null) {
    const result = await stageObj.create(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

//GET STAGE
router.get('/', async (ctx, next) => {
  const result = await stageObj.get()
  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
    ctx.body = {
    success: false,
    error: result.error
  }
})

// GET ONLY STAGE BY ITS ID
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, stageIdSchema)
  if (validationResult.error === null) {
    const result = await stageObj.getById(id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// UPDATE STAGE
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const data = _.extend(body, { id })

  const validationResult = Joi.validate(data, updateStageSchema)
  if (validationResult.error === null) {
    const result = await stageObj.update(
      validationResult.value.id,
      validationResult.value
    )

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// DELETE STAGE
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, stageIdSchema)
  if (validationResult.error === null) {
    const result = await stageObj.delete(id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// AN EXAMPLE
router.put('/:id/foo/:bar/lorem', async (ctx, next) => {
  ctx.body = {
    message: 'This is a test route. Please ignore it.'
  }
  return false
})

module.exports = router
