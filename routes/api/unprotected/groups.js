/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })
const Joi = require('joi')

// const MercadoPago = require('../../../lib/paymentsProcessor')

const Stripe = require('../../../lib/stripe')
const Students = require('../../../models/students')

const GroupsModel = require('../../../db/groups')
const Groups = require('../../../models/groups')
router.prefix('/api/unpr/groups')


// validation schemas
const {
  registerSchema,
  registerStudentSchema,
  idsSchemaForExa
} = require('../../../schemas/unprotected/groups')


// Create a student
router.post('/register', async (ctx, next) => {
  const body = ctx.request.body || {}

  // logged user
  const user = ctx.state.user

  const validationResult = Joi.validate(body, registerStudentSchema)

  if (validationResult.error === null) {
    if (ctx.state && ctx.state.user) {
      validationResult.value = _.extend(
        validationResult.value, { registeredBy: ctx.state.user._id }
      )
    }

    const result = await Students.registerLanding(validationResult.value, user)

    if (result.success) {
      ctx.state.action = `register a student`
      ctx.state.data = validationResult.value
      ctx.status = 200
      ctx.body = result
      return result
    }
else{
  ctx.status = 500
  ctx.body = {
    success: false,
    error: result.error
  }
}
   
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



//This will return the groups by course id from show in landing page
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Groups.getGroupsForLanding(id)
  if (result.success) {
    ctx.body = result
    return false
  }
  ctx.status = result.code
  ctx.body = result
  return false
})


//This will return the groups by course id from show in landing page
router.get('/byId/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Groups.getGroupsByIdForLanding(id)
  if (result.success) {
    ctx.body = result
    return false
  }
  ctx.status = result.code
  ctx.body = result
  return false
})


router.post('/register/:idGroup', async (ctx, next) => {
  const idGroup = ctx.params.idGroup
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, registerSchema)
  if (validationResult.error === null) {
    const isRegister = await Students.findByEmail(body.studentData.email)
    if (isRegister.success) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: "Ya te encuentras registrado a nuestro sistema , registrate a tu curso desde tu panel de alumnos.preparacionpolitecnico.mx"
      }
    } else {
      const groupInfo = await GroupsModel
        .findById({
          _id: idGroup
        })
        .populate({
          path: "course",
          select: "price name"
        })
        .select("name course price")
        .lean()
      body.amount = groupInfo.course.price;
      const resultPayment = await Stripe.createPayment(body)
      console.log(resultPayment)
      if (resultPayment.success) {
        const bodyPayment = {
          amount: body.amount,
          concept: "Pago con tarjeta",
          course: groupInfo.course._id,
          paymentType: "card",
          onlinePaymentId: resultPayment.idOperation,
          status: resultPayment.status
        }
        let request = {}
        request.student = { ...body.studentData }
        request.group = {}
        request.group.id = idGroup
        request.payment = bodyPayment
        const resultRegister = await Students.register(request, undefined, true)
        if (resultRegister.success) {
          ctx.body = resultRegister
          return false
        }
      } else {
        ctx.status = 400
        ctx.body = resultPayment;
      }
    }
    return false
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


router.get('/Politecnico/ingenieria', async (ctx, next)=>{
  
  const ids = {
    /*examen1: '660453c8077bcb5eefd8b9fd',
    examen2: '660453e28bddb83a4861d180',
    examen3: '660453fc42b5475ed1c895d9'*/
    examen1: '66c28197b621485ef461f7e3',
    examen2: '66c2903322aa923a672501b1',
    examen3: '66c0c9bf6cba053a16a2bfde'
  }
   
  const validationResult = Joi.validate(ids, idsSchemaForExa)
  
  if(validationResult.error===null){
    const result = await Groups.getCoursesIngMedSoc(ids)
    if(result.success){
      ctx.status = 200
      ctx.body = result
    }else{
      ctx.status = 404
      ctx.body = result
    }
  }else{
    ctx.status = 400
    ctx.body ={
      success: false,
      error: validationResult.error || {}
    }
  }
})

router.get('/Politecnico/medicina', async (ctx, next)=>{
  const ids = {
    /*examen1: '6604541d4f58755f2632f631',
    examen2: '6604542c10abc23a277117d0',
    examen3: '660454ddabb8545ee3437ac1'*/
    examen1: '66c286bd254f8a3a303d1fd9',
    examen2: '66c2941d6013ed5f1b9eacf4',
    examen3: '66c0ce9c4c1fcd5f1a8ae3aa'
}
  
  const validationResult = Joi.validate(ids, idsSchemaForExa)
  
  if(validationResult.error===null){
      const result = await Groups.getCoursesIngMedSoc(ids)
      if(result.success){
        ctx.status = 200
        ctx.body = result
      }else{
        ctx.status = 404
        ctx.body = result
      }
  }else{
    ctx.status = 400
    ctx.body ={
      success: false,
      error: validationResult.error || {}
    }
  }

})

router.get('/Politecnico/sociales', async (ctx, next)=>{
  const ids={
    /*examen1: '660455365772f039f1b64da6',
    examen2: '660455b2745fb25ed4f6fe1e',
    examen3: '660455cc546b3c3a47b445cc'*/
    examen1: '66c28bc68116985ef11d52fe',
    examen2: '66c296cfbeeb355efc916ea8',
    examen3: '66c0ce3fcc9ebe5f14b24299'

  }

  const validationResult = Joi.validate(ids, idsSchemaForExa)
  
  if(validationResult.error===null){
    const result = await Groups.getCoursesIngMedSoc(ids)
    if(result.success){
      ctx.status = 200
      ctx.body = result
    }else{
      ctx.status = 404
      ctx.body = result
    }
  }else{
    ctx.status = 400
    ctx.body ={
      success: false,
      error: validationResult.error || {}
    }
  }
  
})


router.get('/Exam/Unam', async (ctx, next)=>{
  const ids={
    examen1: '60275ac951b1475b449d3175',
    examen2: '60275ae0ab5f3c5b2a7b7bee',
    examen3: '60275af204e0525b2738b2f7'
  }

  const validationResult = Joi.validate(ids, idsSchemaForExa)
  
  if(validationResult.error===null){
    const result = await Groups.getCoursesIngMedSoc(ids)
    if(result.success){
      ctx.status = 200
      ctx.body = result
    }else{
      ctx.status = 404
      ctx.body = result
    }
  }else{
    ctx.status = 400
    ctx.body ={
      success: false,
      error: validationResult.error || {}
    }
  }
})

router.get('/Exam/Comipems', async (ctx, next)=>{
  const ids={
    examen1: '60a1a1d789a29f5b3a22acad',
    examen2: '60a17460c904ee5b5077cc99',
    examen3: '60a1a25e5804b45b4d5de060'
  }

  const validationResult = Joi.validate(ids, idsSchemaForExa)
  
  if(validationResult.error===null){
    const result = await Groups.getCoursesIngMedSoc(ids)
    if(result.success){
      ctx.status = 200
      ctx.body = result
    }else{
      ctx.status = 404
      ctx.body = result
    }
  }else{
    ctx.status = 400
    ctx.body ={
      success: false,
      error: validationResult.error || {}
    }
  }
})


module.exports = router
