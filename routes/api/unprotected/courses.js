/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })
const Joi = require('joi')


const Course = require('../../../models/courses')
router.prefix('/api/unpr/courses')


//This will return the courses by level id from show in landing page
router.get('/type/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Course.getByType(id)
  if (result.success) {
    ctx.body = result
    return false
  }
  ctx.status = result.code
  ctx.body = result
  return false
})






module.exports = router
