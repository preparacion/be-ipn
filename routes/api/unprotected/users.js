/**
 * router/api/users.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Users = require('../../../models/users')

router.prefix('/api/unpr/users')

router.put('/pass/recover', async (ctx, next) => {
  const { email } = ctx.request.body

  const result = await Users.recoverPass(email)

  if (result.success) {
    ctx.body = result
    return
  }

  ctx.status = result.code
  ctx.body = result
})

module.exports = router
