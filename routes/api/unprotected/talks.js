/**
 * router/api/unprotected/talks.js
 *
 * @description :: Describes the talks api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Talks = require('../../../models//talks')
const UsersTalks = require ('../../../models/usersTalks')
router.prefix('/api/unpr/talks')

// validation schemas
const {
  registerSchema
} = require('../../../schemas/unprotected/talks')




/** get all the talks */
router.get('/feria', async (ctx, next) => {
  const result = await Talks.getFeria()

  if (result.success) {
    ctx.body = result
    return false
  }

  ctx.status = result.code
  ctx.body = result
  return false
})



router.get('/groups', async (ctx, next) => {
  const { courseLevel } = ctx.query || {}
  const result = await Talks.getTalkGroups(courseLevel)

  if (result.success) {
    ctx.body = result
    return false
  }

  ctx.status = result.code
  ctx.body = result
  return false
})

router.post('/register/:idGroup', async (ctx, next) => {
  const idGroup = ctx.params.idGroup
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, registerSchema)
  if (validationResult.error === null) {
    const result = await UsersTalks.talkRegister(idGroup, validationResult.value)
    if (result.success) {
      ctx.body = result
      return false
    }
    ctx.status = result.code
    ctx.body = result
    return false
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
