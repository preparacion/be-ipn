/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })
const Joi = require('joi')

const Students = require('../../../models/students')

const GroupsModel = require('../../../db/groups')
const Groups = require('../../../models/groups')
router.prefix('/api/unpr/exams')


// validation schemas
const {
    registerSchema
  } = require('../../../schemas/unprotected/exams')




router.post('/register/', async (ctx, next) => {
    const body = ctx.request.body || {}
    const validationResult = Joi.validate(body, registerSchema)
    if (validationResult.error === null) {
        const isRegister = await Students.findByEmail(body.studentData.email)
        if (isRegister.success) {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: "Ya te encuentras registrado a nuestro sistema para registrarte inicia sesión en tu plataforma de alumnos con este mismo correo y ve a la sección de inscribir."
            }
        } else {
            let idGroups = body.groupsArray;
            const groupInfo = await GroupsModel
                .findById({
                    _id: idGroups[0]
                })
                .populate({
                    path: "course",
                    select: "price name"
                })
                .select("name course price")
                .lean()
            body.amount = 0
            const bodyPayment = {
                amount: body.amount,
                concept: "Registro sin pago",
                course: groupInfo.course._id,
                paymentType: "CASH",
            }
            let request = {}
            request.student = { ...body.studentData }
            request.group = {}
            request.group.id = groupInfo._id
            request.payment = bodyPayment
            const resultRegister = await Students.register(request, undefined, true)
            if (idGroups.length > 1) {
                for (let i = 1; i < idGroups.length; i++) {
                    const result = await Groups.enroll(idGroups[i], resultRegister.studentId, {}, undefined)
                }
            }
            ctx.body = resultRegister
            return false
        }
        return false
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})



module.exports = router
