/**
 * router/api/unprotected/talks.js
 *
 * @description :: Describes the talks api routes
 * @docs        :: TODO
 */
// const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })
const Videos = require('../../../models/videos')
const PaymentsModel = require('../../../models/payments')
const Payments = require('../../../models/payments')

router.prefix('/api/unpr/webhook')

/************************************************
*************************************************
*****************AWS WEBHOOKS *******************
*************************************************
*************************************************
*/
router.post('/updateMediaStatus', async (ctx, next) => {
  const body = ctx.request.body || {}
  const jsonBody = JSON.parse(body)
  console.log(jsonBody)
  const jsonMessage = JSON.parse(jsonBody.Message)
  const socket = ctx.socket_io
  const result = await Videos.updateStatusMedia(jsonMessage, socket)
  ctx.body = result
  return false
})

router.post('/updateTranscoderStatus', async (ctx, next) => {
  const body = ctx.request.body || {}
  const jsonBody = JSON.parse(body);
  const socket = ctx.socket_io
  // console.log(jsonBody)
  const dataEvent = JSON.parse(jsonBody.Message);
  const result = await Videos.updateStatusTranscode(dataEvent, socket)
  ctx.body = result
  return false;
})


/************************************************
*************************************************
***************** Mercadopago *******************
*************************************************
*************************************************
*/
router.post('/mercadopago/updatePaymentStatus', async (ctx, next) => {
  const body = ctx.request.body || {}
  const socket = ctx.socket_io
  PaymentsModel.webhookPayment("mercadopago", body)
  ctx.body = {
    success: true
  }
  return false;
})


module.exports = router