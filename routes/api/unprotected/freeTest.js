/**
 * router/api/freeTest.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Students = require('../../../models/students');
const UserTalks = require('../../../models/usersTalks');

const Quiz = require('../../../models/quiz');



router.prefix('/api/unpr/freeTest');


const registerFreeTestSchema = Joi.object().keys({
    testId: Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
        .required(),
    userTalk: Joi.object().keys({
        name: Joi.string().min(1).max(250).required(),
        lastName: Joi.string().min(1).max(250).required(),
        secondLastName: Joi.string(),
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        phoneNumber: Joi.string().required(),
        secondPhoneNumber: Joi.string(),
    }),
    quizAnswers: Joi.array().items([
        Joi.object().keys({
            idQuizQuestion: Joi.string()
                .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required(),
            questionAnswers: Joi.array().items([
                Joi.string().required()
            ])
        })
    ])
})

const objectIdSchema = Joi.object().keys({
    _id: Joi.string()
        .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i)
        .required()
});

router.get('/getFreeExam/:idExam', async (ctx, next) => {
    const id = ctx.params.idExam
    const validationResult = Joi.validate({ _id: id }, objectIdSchema);
    if (validationResult.error === null) {
        const resultFreeTest = await Quiz.getInfoFreeQuiz(validationResult.value._id)
        if (resultFreeTest.success) {
            ctx.body = resultFreeTest
            return
        }
        ctx.status = resultFreeTest.code
        ctx.body = resultFreeTest
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }

})

// TODO: Add error message new functions
/**
 * This method will register the user to a talk and add some information about their result in a free exam.
 */

router.post('/register', async (ctx, next) => {
    const objUrlParams = new URLSearchParams(ctx.request.querystring);
    const idTalk = objUrlParams.get("idTalk")
    const body = ctx.request.body
    const validationQueryResult = Joi.validate({ _id: idTalk }, objectIdSchema);
    const validationResult = Joi.validate(body, registerFreeTestSchema);
    if (idTalk && validationQueryResult.error === null || !idTalk) {
        if (validationResult.error === null) {
            const resultRegister = await UserTalks.registerFreeTest(validationResult.value, validationQueryResult.error === null ? validationQueryResult.value._id : undefined);
            if (resultRegister.success) {
                ctx.body = resultRegister
                return
            }
            ctx.status = resultRegister.code
            ctx.body = resultRegister
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationQueryResult.error.details || 'Validation error'
        }
    }
})


router.post('/student', async (ctx, next) => {
    const body = ctx.request.body
    let request = {}
    request.student = { ...body }
    request.group = {}
    request.group.id = groupId
    request.freeLessonGroups = [groupId]
    const resultRegister = await Students.register(request)
    if (resultRegister.success) {
        ctx.body = resultRegister
        return
    }
    ctx.status = resultRegister.code
    ctx.body = resultRegister
})




module.exports = router
