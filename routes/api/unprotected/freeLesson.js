/**
 * router/api/users.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Students = require('../../../models/students')

router.prefix('/api/unpr/freeLesson')

router.post('/student', async (ctx, next) => {
    const body = ctx.request.body
    const groupId = body.groupId
    let request = {}
    request.student = { ...body }
    request.group = {}
    request.group.id = groupId
    request.freeLessonGroups = [groupId]
    const resultRegister = await Students.register(request);
    if (resultRegister.success) {
        ctx.body = resultRegister
        return
    }
    console.log(resultRegister)
    ctx.status = resultRegister.error.httpCode
    ctx.body = resultRegister.error
})


module.exports = router
