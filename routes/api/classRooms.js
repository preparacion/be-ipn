/**
 * router/api/classRooms.js
 *
 * @description :: Describes the classRooms api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const ClassRooms = require('../../models/classRooms')

router.prefix('/api/classrooms')

const {
  classRoomSchema,
  updateClassRoomSchema } = require('../../schemas/classRooms')



// Create a new classroom
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, classRoomSchema)

  if (validationResult.error === null) {
    const result = await ClassRooms.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a classroom`
      ctx.state.data = {}

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Get Classrooms
router.get('/', async (ctx, next) => {
  const result = await ClassRooms.get()

  if (result.success) {
    ctx.state.action = `Get all the classrooms`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      classRooms: result.classRooms
    }
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await ClassRooms.getById(id)

  if (result.success) {
    ctx.state.action = `get a classrooms ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Put parameters in a classroom
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateClassRoomSchema)

  if (validationResult.error === null) {
    const result = await ClassRooms.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update a classroom ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

//Delete classroom by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await ClassRooms.delete(id)

  if (result.success) {
    ctx.state.action = `delete a classroom ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get classrooms by location identifier
router.get('/location/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await ClassRooms.getByLocationId(id)

  if (result.success) {
    ctx.state.action = `get classrooms by location id ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = result
})


//Get if classroom filtered by location is available
router.post('/location/:id/available', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const result = await ClassRooms.getByLocationFilter(id,body)
  if (result.success) {
    ctx.state.action = `get classrooms by location id ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }
  ctx.status = result.code
  ctx.body = result
})




module.exports = router
