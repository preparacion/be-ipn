/**
 * router/api/logs.js
 *
 * @description :: Describes the logs api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const Logs = require('../../models/logs')

router.prefix('/api/logs')

//  Get all logs
router.get('/', async (ctx, next) => {
  const result = await Logs.get()

  if (result.success) {
    ctx.state.action = `Get all logs`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      logs: result.logs
    }
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get a log properties by Id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Logs.getById(id)

  if (result.success) {
    ctx.state.action = `Get log ${id}`
    ctx.state.data = { id }
    ctx.body = {
      success: true,
      log: result.log
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// NOT IMPLEMENTED -- Get logs by User
router.get('/user/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Logs.getByUserId(id)

  if (result.success) {
    ctx.state.action = `get logs by user ${id}`
    ctx.state.data = { id }
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
