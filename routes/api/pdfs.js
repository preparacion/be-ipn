const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })
const PDFs = require('../../models/pdfs')

router.prefix('/api/pdfs')

const pdfSchema = Joi.object().keys({
    filename: Joi.string().required(),
    name: Joi.string(),
    description: Joi.string(),
    playlist: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
    title: Joi.string().required(),
    totalPages: Joi.number().required(),
    course: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
})

const progressSchema = Joi.object().keys({
    pageNumber: Joi.number().required(),
    totalPages: Joi.number().required()
})

// Crear URL firmada para subida
router.post('/signed-url', async (ctx, next) => {
    const body = ctx.request.body || {}
    const validationResult = Joi.validate(body, pdfSchema)

    if (validationResult.error === null) {
        const result = await PDFs.getSignedUrl(validationResult.value)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: 'Validation error'
        }
    }
})

// Actualizar progreso
router.post('/:id/progress', async (ctx, next) => {
    if (ctx.state && ctx.state.student) {
        const body = ctx.request.body || {}
        const id = ctx.params.id
        const validationResult = Joi.validate(body, progressSchema)

        if (validationResult.error === null) {
            const result = await PDFs.updateProgress(
                id,
                ctx.state.student._id,
                validationResult.value.pageNumber,
                validationResult.value.totalPages
            )

            if (result.success) {
                ctx.body = result
                return next()
            }

            ctx.status = result.code
            ctx.body = {
                success: false,
                error: result.error
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated'
        }
    }
})

module.exports = router