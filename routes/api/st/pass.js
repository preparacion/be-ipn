/**
 * router/api/st/pass.js
 *
 * @description :: Describes the pass api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Students = require('../../../models/students')

router.prefix('/api/st/pass')

// validation schemas
const {
  emailSchema
} = require('../../../schemas/st/pass')


// RECOVER STUDENT PASSWORD
router.put('/recover', async (ctx, next) => {
  const body = ctx.request.body

  const validationResult = Joi.validate(body, emailSchema)
  if (validationResult.error === null) {
    const result = await Students.passStRecover(body.email)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
