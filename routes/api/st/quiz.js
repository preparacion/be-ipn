/**
 * router/api/quizzAttempt.js
 *
 * @description :: Describes the quizz Attempt api 
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })
const QuizModel = require('../../../models/st/quiz')

router.prefix('/api/st/quiz')


router.get('/:idQuiz', async (ctx, next) => {
    const idAttempt = ctx.params.idQuiz
    if (ctx.state && ctx.state.student) {
        const result = await QuizModel.getById(idAttempt, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


module.exports = router