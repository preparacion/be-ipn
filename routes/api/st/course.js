

/**
 * 
 * @description :: Describes the course for student 
 * @docs        :: TODO
 * 
 */

const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Course = require('../../../models/st/course')
const CourseLevel = require('../../../models/courseLevels')


router.prefix('/api/st/course/')

// validation schemas
const {
  courseIdSchema
} = require('../../../schemas/st/course')


// GET THE STUDENT COURSES INFO
router.get('/', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const mainResult = await Course.getCourses(ctx.state.student._id)
    if (mainResult.success) {
      ctx.body = mainResult
      return next()
    }
    ctx.status = mainResult.code
    ctx.body = {
      success: false,
      error: mainResult.error
    }
    return false
  }
  else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

//Get playlist by course id 
router.get('/:id/playlists', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const id = ctx.params.id
    const validationResult = Joi
      .validate({ id }, courseIdSchema)
    if (validationResult.error === null) {
      const result = await Course.getPlayListsByCourse(validationResult.value.id, ctx.state.student)
      if (result.success) {
        ctx.state.action = `get a single video ${validationResult.value.id}`
        ctx.state.data = {}

        ctx.body = result
        return next()
      }

      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error
      }
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: validationResult.error.details || 'Validation error'
      }
    }

  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

// GET all course levels
router.get('/getCourseLevels', async (ctx, next) => {
  const result = await CourseLevel.get()
  if (result.success) {
    ctx.body = result
    return
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

//Get groups by course
router.get('/:idCourses', async (ctx, next) => {
  const courseId = ctx.params.idCourses
  const validationResult = Joi
    .validate({ id: courseId }, courseIdSchema)
  if (validationResult.error === null) {
    if (
      ctx.state &&
      ctx.state.student &&
      String(ctx.state.student._id)
    ) {
      const courseResult = await Course.getGroupsByCourse(courseId)
      if (courseResult.success) {
        ctx.body = courseResult
        return next()
      }
      ctx.status = courseResult.code
      ctx.body = {
        success: false,
        error: courseResult.error,
        errorExt: courseResult.errorExt
      }
      return false
    } else {
      const errExt = errMsg.getStWrongStudent()
      ctx.status = 401
      ctx.body = {
        success: false,
        error: errExt.error || '',
        errExt
      }
      return false
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET course level of a course by course id
router.get('/levels/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, courseIdSchema)
  if (validationResult.error === null) {
    const result = await Course
      .getCoursesByLevelId(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router