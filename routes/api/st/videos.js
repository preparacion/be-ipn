

/**
 * 
 * @description :: Describes the videos for student 
 * @docs        :: TODO
 * 
 */

const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Videos = require('../../../models/st/videos')
const {
    videoIdSchema,
    rateVideoSchema,
    commentSchema,
    updateCommentSchema,
    deleteCommentSchema
} = require('../../../schemas/video')

router.prefix('/api/st/videos')


// shows only de video info
router.get('/:id', async (ctx, next) => {
    if (ctx.state && ctx.state.student) {
        const id = ctx.params.id
        const validationResult = Joi.validate({ id }, videoIdSchema)

        if (validationResult.error === null) {
            const result = await Videos.getById(validationResult.value.id, ctx.state.student)

            if (result.success) {
                ctx.state.action = `get a single video ${validationResult.value.id}`
                ctx.state.data = {}

                ctx.body = result
                return next()
            }

            ctx.status = result.code
            ctx.body = {
                success: false,
                error: result.error
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// RATE VIDEO
router.post('/:id/rate', async (ctx, next) => {
    const id = ctx.params.id
    const body = ctx.request.body || {}

    const validationResult = Joi.validate({
        student: body.student,
        rate: body.rate,
        id
    }, rateVideoSchema)

    if (validationResult.error === null) {
        const result = await Videos.rate(validationResult.value)
        if (result.success) {
            ctx.status = 201
            ctx.body = result
            return next()
        }

        ctx.status = result.code
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

router.post('/:id/watchVideo', async (ctx, next) => {
    if (ctx.state && ctx.state.student) {
        const id = ctx.params.id
        const validationResult = Joi.validate({ id }, videoIdSchema)
        if (validationResult.error === null) {
            const result = await Videos.watchVideo(id,ctx.state.student)
            if (result.success) {
                ctx.status = 201
                ctx.body = result
                return next()
            }
            ctx.status = result.code
            ctx.body = {
                success: false,
                error: result.error
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// Comment a video
router.post('/:id/comment', async (ctx, next) => {
    const id = ctx.params.id
    const body = ctx.request.body
    const validationResult = Joi.validate(_.extend(body, { id }), commentSchema)
    if (validationResult.error === null) {
        const result = await Videos.comment(id, validationResult.value)
        if (result.success) {
            ctx.status = 201
            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

// respond a comment 
router.post('/comment/:commentId/respond', async (ctx, next) => {
    const commentId = ctx.params.commentId
    const body = ctx.request.body

    const validationResult = Joi.validate(_.extend(body, { id: commentId }), commentSchema)
    if (validationResult.error === null) {
        const result = await Videos.respondComment(commentId, validationResult.value)
        if (result.success) {
            ctx.status = 201
            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})





// Update a video
router.put('/:id/comment', async (ctx, next) => {
    const id = ctx.params.id
    const body = ctx.request.body
    const validationResult = Joi.validate(_.extend(body, { id }), updateCommentSchema)
    if (validationResult.error === null) {
        const result = await Videos.editComment(id, validationResult.value)
        if (result.success) {
            ctx.status = 201
            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})



// Delete a video
router.delete('/:id/comment/:idComment', async (ctx, next) => {
    const id = ctx.params.id
    const idComment = ctx.params.idComment
    const validationResult = Joi.validate(_.extend({commentId: idComment}, { id }), deleteCommentSchema)
    if (validationResult.error === null) {
        const result = await Videos.deleteComment(validationResult.value)
        if (result.success) {
            ctx.status = 201
            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})


module.exports = router
