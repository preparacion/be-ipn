/**
 * router/api/quizzAttempt.js
 *
 * @description :: Describes the quizz Attempt api 
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Appoiments = require('../../../models/st/appoiments')

// validation schemas
const {
    createAppoiment
  } = require('../../../schemas/st/appoiments')

router.prefix('/api/st/appoiments')

// GET APPOIMENTS BY ID ROUTE
router.get('/:idGroup', async (ctx, next) => {
    const idGroup = ctx.params.idGroup
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const result = await Appoiments.get(idGroup, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// CREATE APPOIMENT BY ID GROUP
router.post('/:idGroup', async (ctx, next) => {
    const idGroup = ctx.params.idGroup
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const result = await Appoiments.create(idGroup, body, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.status=result.code
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


module.exports = router