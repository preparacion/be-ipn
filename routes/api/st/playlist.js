

/**
 * 
 * @description :: Describes the playlist for student 
 * @docs        :: TODO
 * 
 */



 const Joi = require('joi')
 const _ = require('lodash')
 const router = require('koa-router')({ sensitive: true })

 const Playlist = require('../../../models/st/playlist')
 
 router.prefix('/api/st/playlists')
 
 const playlistIdSchema = Joi.object().keys({
     id: Joi.string()
         .regex(/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i).required()
 })
 
 
 router.get('/:id', async (ctx, next) => {
     if (ctx.state && ctx.state.student) {

        let student = ctx.state.student
        student.authorization = ctx.header.authorization

         const id = ctx.params.id
         const validationResult = Joi.validate({ id }, playlistIdSchema)
 
         if (validationResult.error === null) {
             const result = await Playlist.getById(validationResult.value.id, student)
 
             if (result.success) {
                 ctx.state.action = `get a playlist ${validationResult.value.id}`
                 ctx.state.data = {}
                 ctx.body = result
                 return next()
             }
 
             ctx.status = result.code
             ctx.body = {
                 success: false,
                 error: result.error
             }
         } else {
             ctx.status = 400
             ctx.body = {
                 success: false,
                 error: validationResult.error.details || 'Validation error'
             }
         }
     } else {
         ctx.status = 401
         ctx.body = {
             success: false,
             error: 'No authenticated',
             message: 'No authenticated'
         }
     }
 })
 
 router.get('/course/:id', async (ctx, next) => {
     if (ctx.state && ctx.state.student) {
         const id = ctx.params.id
         const validationResult = Joi.validate({ id }, playlistIdSchema)
         if (validationResult.error === null) {
             const result = await Playlist.getByCourseId(validationResult.value.id, ctx.state.student)
 
             if (result.success) {
                 ctx.body = result
                 return next()
             }
 
             ctx.status = result.code
             ctx.body = {
                 success: false,
                 error: result.error
             }
         } else {
             ctx.status = 400
             ctx.body = {
                 success: false,
                 error: validationResult.error.details || 'Validation error'
             }
         }
     } else {
         ctx.status = 401
         ctx.body = {
             success: false,
             error: 'No authenticated',
             message: 'No authenticated'
         }
     }
 })
 
 module.exports = router