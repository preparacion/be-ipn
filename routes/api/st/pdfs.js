const Router = require('koa-router');
const SecurePDFService = require('../../../models/protectedPdf');
const { pdfSchema } = require('../../../schemas/st/playlist');

const router = new Router();
router.prefix('/api/st/pdfs');

router.get('/:idPdf', async (ctx) => {
  try {
    const { idPdf } = ctx.params;
    const userId = ctx.state.student._id;
    const validation = await pdfSchema.validate({ idPdf, user: userId });
    if (validation.error) {
      ctx.status = 400;
      ctx.body = { 
        success: false,
        error: validation.error.details[0].message 
      };
      return;
    }

    const result = await SecurePDFService.getById(idPdf, userId);
    if (result.success) {
      ctx.body = result;
    } else {
      ctx.status = result.code || 500;
      ctx.body = { 
        success: false, 
        error: result.error 
      };
    }
  } catch (error) {
    ctx.status = 500;
    ctx.body = { 
      success: false, 
      error: 'Internal server error' 
    };
  }
});

module.exports = router;