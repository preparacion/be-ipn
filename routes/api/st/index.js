/**
 * router/api/st/index.js
 *
 * @description :: Describes the 'st' api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const St = require('../../../models/st')
const Images = require('../../../models/images')

const Course = require('../../../models/st/course')

const Groups = require('../../../models/groups')

// const PaymentsModel = require('../../../models/st/payments')
//const Stripe = require('../../../lib/stripe')

const GroupsModel = require('../../../db/groups')

const ErrMsg = require('../../../lib/errMsg')

const stObj = new St()
const errMsg = new ErrMsg()

router.prefix('/api/st/')

const {
  stIdSchema,
  updateStSchema, 
  imageSchema
  
} = require('../../../schemas/st')

router.get('/', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const profileResult = await stObj.getProfile(ctx.state.student._id, stObj)
    if (profileResult.success) {
      ctx.body = profileResult
      return next()
    }

    ctx.status = profileResult.code
    ctx.body = {
      success: false,
      error: profileResult.error
    }
    return false
  }
  else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})


router.put('/', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const body = ctx.request.body || {}
    const validationResult = Joi.validate(body, updateStSchema)
    if (validationResult.error === null) {
      const result = await stObj.updateStudent(ctx.state.student, validationResult.value)
      if (result.success) {
        ctx.body = result
        return next()
      }
      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error,
        errorExt: result.errorExt
      }
      return false
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: validationResult.error.details || 'Validation error'
      }
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

router.delete('/picture', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const profileResult = await stObj.deletePicture(ctx.state.student._id, stObj)
    if (profileResult.success) {
      ctx.body = profileResult
      return next()
    }

    ctx.status = profileResult.code
    ctx.body = {
      success: false,
      error: profileResult.error
    }
    return false
  }
  else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})


router.get('/schoolData', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const schoolDataResult = await Course.getSchoolData(ctx.state.student._id)
    if (schoolDataResult.success) {
      ctx.body = schoolDataResult
      return next()
    }
    ctx.status = schoolDataResult.code
    ctx.body = {
      success: false,
      error: schoolDataResult.error
    }
    return false
  }
  else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

// GET  STUDENT ATTENDANCE DATA
router.get('/:id/attendance', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi
    .validate({ id }, stIdSchema)
  if (validationResult.error === null) {
    if (
      ctx.state &&
      ctx.state.student &&
      String(ctx.state.student._id) === String(id)
    ) {
      const attendanceResult = await stObj.getAttendance(id)

      if (attendanceResult.success) {
        ctx.body = attendanceResult
        return next()
      }

      ctx.status = attendanceResult.code
      ctx.body = {
        success: false,
        error: attendanceResult.error
      }
      return false
    } else {
      const errExt = errMsg.getStWrongStudent()
      ctx.status = 401
      ctx.body = {
        success: false,
        error: errExt.error || '',
        errExt
      }
      return false
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

router.get('/courses/:courseId', async (ctx, next) => {
  if (ctx.state && ctx.state.student) {
    const courseId = ctx.params.courseId
    const validationResult = Joi
      .validate({ id: courseId }, stIdSchema)

    if (validationResult.error === null) {
      const playlistResult = await Course.getCourseinfo(courseId, ctx.state.student._id)
      if (playlistResult.success) {
        ctx.body = playlistResult
        return next()
      }
      ctx.status = playlistResult.code
      ctx.body = {
        success: false,
        error: playlistResult.error,
        errorExt: playlistResult.errorExt
      }
      return false
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: validationResult.error.details || 'Validation error'
      }
    }
  } else {
    const errExt = errMsg.getStWrongStudent()
    ctx.status = 400
    ctx.body = {
      success: false,
      error: errExt.error || '',
      errExt
    }
  }
})

router.get('/:id/videos/:playListId', async (ctx, next) => {
  const id = ctx.params.id
  const playListId = ctx.params.playListId

  const validationResult = Joi
    .validate({ id: playListId }, stIdSchema)
  if (validationResult.error === null) {
    if (
      ctx.state &&
      ctx.state.student &&
      String(ctx.state.student._id) === String(id)
    ) {
      const playlistResult = await stObj.getVideosByCourseId(playListId)

      if (playlistResult.success) {
        ctx.body = playlistResult
        return next()
      }

      ctx.status = playlistResult.code
      ctx.body = {
        success: false,
        error: playlistResult.error,
        errorExt: playlistResult.errorExt
      }
      return false
    } else {
      const errExt = errMsg.getStWrongStudent()
      ctx.status = 401
      ctx.body = {
        success: false,
        error: errExt.error || '',
        errExt
      }
      return false
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// enroll me with a group with payment
router.post('/enrollPayment/:idGroup', async (ctx, next) => {
  const id = ctx.params.idGroup
  const body = ctx.request.body || {}
  if (ctx.state && ctx.state.student) {
    const EnrolledVerificatiton = await Groups._isAlreadyEnrolled(ctx.state.student._id, id)
    if (!EnrolledVerificatiton) {
      const groupInfo = await GroupsModel
        .findById({
          _id: id
        })
        .populate({
          path: "course",
          select: "price name"
        })
        .select("name course price")
        .lean()
      body.amount = groupInfo.course.price;
      const resultPayment = await Stripe.createPayment(body)
      if (resultPayment.success) {
        const bodyPayment = {
          amount: body.amount,
          concept: "Pago con tarjeta",
          course: groupInfo.course._id,
          paymentType: "card",
          onlinePaymentId: resultPayment.idOperation,
          status: resultPayment.status
        }
        const resultRegister = await Groups.enroll(id, ctx.state.student._id, bodyPayment, null)
        if (resultRegister.success) {
          ctx.body = resultRegister
        } else {
          ctx.body = {
            success: false,
            error: {
              message: "Ocurrio un error al realizar tu registro, Si tienes alguna duda comunicate inmediatamente con nosotros vía redes sociales o por nuestros medios de contacto"
            }
          }
        }
      } else {
        ctx.status = 400
        ctx.body = resultPayment;
      }
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: {
          message: "Ya te encuentras inscrito a este grupo, si tienes alguna duda contactanos a nuestras redes sociales para brindarte soporte."
        }
      }
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
  return false
})

// enroll me with a group
router.post('/enroll/:idGroup', async (ctx, next) => {
  const idGroup = ctx.params.idGroup
  if (ctx.state && ctx.state.student) {
    const validationResult = Joi
      .validate({ id: idGroup }, stIdSchema)
    if (validationResult.error === null) {
      const result = await stObj.enrollToGroup(ctx.state.student, validationResult.value.id)
      if (result.success) {
        ctx.body = result
        return next()
      }
      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error,
        errorExt: result.errorExt
      }
      return false
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: validationResult.error.details || 'Validation error'
      }
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

/*********************************************
**********************************************
*************** Upload a image  **************
**********************************************
*********************************************/
router.post('/image', async (ctx, next) => {
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, imageSchema)
  if (validationResult.error === null) {
    const result = await Images.createUrl(validationResult.value)
    if (result.success) {
      ctx.state.action = `createUrlToUpload`
      ctx.state.data = {}
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


module.exports = router
