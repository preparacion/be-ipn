/**
 * router/api/st/index.js
 *
 * @description :: Describes the 'st' api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const CourseLevel = require('../../../models/courseLevels')
const Playlists = require('../../../models/playlists')

router.prefix('/api/st/list')

// validation schemas
const {
  idSchema
} = require('../../../schemas/st/list')


// GET COURSE LEVELS
router.get('/levels', async (ctx, next) => {
  const result = await CourseLevel.get()
  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// GET COURSES BY LEVEL ID
router.get('/levels/:id/courses', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await CourseLevel
      .getCoursesById(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET PLAYLIST BY ITS ID
router.get('/playlists/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)

  if (validationResult.error === null) {
    const result = await Playlists.getById(validationResult.value.id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
