/**
 * router/api/quizzAttempt.js
 *
 * @description :: Describes the quizz Attempt api 
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const PaymentsModel = require('../../../models/st/payments');
const St = require('../../../models/st')
const Course = require('../../../models/st/course')
const stObj = new St()

// validation schemas
const {
    courseIdSchema,
    paymentSchema,
    paymentSchemaCards,
    schemaCards
  } = require('../../../schemas/st/payments')


router.prefix('/api/st/payments')

router.get('/', async (ctx, next) => {
    if (ctx.state && ctx.state.student) {
        const financialResult = await stObj.getFinancial(ctx.state.student._id)
        if (financialResult.success) {
            ctx.body = financialResult
            return next()
        }
        ctx.status = financialResult.code
        ctx.body = {
            success: false,
            error: financialResult.error,
            errorExt: financialResult.errorExt || {}
        }
        return false
    }
    else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
    return "ok"
})

router.post('/', async (ctx, next) => {
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const validationResult = Joi.validate(body, paymentSchema)
        if (validationResult.error === null) {
            const result = await PaymentsModel.createPayment(ctx.state.student, body)
            if (result.success) {
                ctx.body = result
                return next()
            } else {
                ctx.status = 402
                ctx.body = result
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

//Do a payment with a card created 
router.post('/pay', async (ctx, next) => {
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const validationResult = Joi.validate(body, paymentSchemaCards)
        if (validationResult.error === null) {
            const result = await PaymentsModel.doPaymetWithCard(ctx.state.student, body)
            if (result.success) {
                ctx.body = result
                return next()
            } else {
                ctx.status = 402
                ctx.body = result
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


//Do a payment with a card created 
router.post('/payLesson', async (ctx, next) => {
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const resultPayment = await PaymentsModel.doPaymentGeneral(ctx.state.student, body)
        if (resultPayment.success) {
            const resultAddStudentArrangement = await stObj.addArrangementLesson(ctx.state.student, body.courseId)
            ctx.body = resultPayment
            return next()
        } else {
            ctx.status = 402
            ctx.body = result
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


//register a new card to the client
router.post('/cards', async (ctx, next) => {
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const validationResult = Joi.validate(body, schemaCards)
        if (validationResult.error === null) {
            const result = await PaymentsModel.addCardsToUser(ctx.state.student, body)
            if (result.success) {
                ctx.body = result
                return next()
            } else {
                ctx.status = 402
                ctx.body = result
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

//register get all cards of client
router.get('/cards', async (ctx, next) => {
    if (ctx.state && ctx.state.student) {
        const result = await PaymentsModel.getCardsOfUser(ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        } else {
            ctx.status = 402
            ctx.body = result
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

//register a new card to the client
router.put('/cards', async (ctx, next) => {
    const body = ctx.request.body || {}
    if (ctx.state && ctx.state.student) {
        const result = await PaymentsModel.editCardToUser(ctx.state.student, body)
        if (result.success) {
            ctx.body = result
            return next()
        } else {
            ctx.status = 402
            ctx.body = result
        }

    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


router.delete('/cards/:id', async (ctx, next) => {
    const idCard = ctx.params.id
    if (ctx.state && ctx.state.student) {
        const result = await PaymentsModel.removeCardToUser(ctx.state.student, idCard)
        if (result.success) {
            ctx.body = result
            return next()
        } else {
            ctx.status = 402
            ctx.body = result
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

router.get('/debtByCourse/:idCourses', async (ctx, next) => {
    const courseId = ctx.params.idCourses
    const validationResult = Joi
        .validate({ id: courseId }, courseIdSchema)
    if (validationResult.error === null) {
        if (
            ctx.state &&
            ctx.state.student &&
            String(ctx.state.student._id)
        ) {
            const courseResult = await Course.hasDebts(courseId, ctx.state.student)
            console.log(courseResult)
            if (courseResult.success) {
                ctx.body = courseResult
                return next()
            }
            ctx.status = courseResult.code
            ctx.body = {
                success: false,
                error: courseResult.error,
                errorExt: courseResult.errorExt
            }
            return false
        } else {
            const errExt = errMsg.getStWrongStudent()
            ctx.status = 401
            ctx.body = {
                success: false,
                error: errExt.error || '',
                errExt
            }
            return false
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})
module.exports = router