/**
 * router/api/quizzAttempt.js
 *
 * @description :: Describes the quizz Attempt api 
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const QuizAttempt = require('../../../models/st/quizAttempt')

const {
    quizAttemptAnswerSchema,
    attemptIdSchema
} = require('../../../schemas/quizAttempt')


router.prefix('/api/st/quizAttempt')

// Create quizz attempt
router.post('/:idQuiz', async (ctx, next) => {
    const idQuiz = ctx.params.idQuiz
    if (ctx.state && ctx.state.student) {
        const result = await QuizAttempt.create(idQuiz, ctx.state.student)
        console.log(result)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// POST ANSWERS BY ATTEMPT
router.post('/:idAttempt/answer', async (ctx, next) => {
    const idAttempt = ctx.params.idAttempt
    const body = ctx.request.body
    const validationResult = Joi.validate(body, quizAttemptAnswerSchema)
    if (ctx.state && ctx.state.student) {
        const result = await QuizAttempt.answerQuestion(idAttempt, validationResult.value, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// finish attempt
router.put('/:idAttempt/finish', async (ctx, next) => {
    const idAttempt = ctx.params.idAttempt
    // const body = ctx.request.body
    // const validationResult = Joi.validate(body, quizAttemptAnswerSchema)
    if (ctx.state && ctx.state.student) {
        const result = await QuizAttempt.finishQuizAttempt(idAttempt, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// Get quizz attempts by quiz ID
router.get('/quiz/:idQuiz', async (ctx, next) => {
    const idQuiz = ctx.params.idQuiz
    if (ctx.state && ctx.state.student) {
        const result = await QuizAttempt.getByQuizId(idQuiz, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// Get quizz attempt info
router.get('/:idAttempt', async (ctx, next) => {
    const idAttempt = ctx.params.idAttempt
    if (ctx.state && ctx.state.student) {
        const result = await QuizAttempt.getById(idAttempt, ctx.state.student)
        if (result.success) {
            ctx.body = result
            return next()
        }
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})







module.exports = router

