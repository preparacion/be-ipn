/**
 * router/api/st/login.js
 *
 * @description :: Describes the login api routes
 * @docs        :: TODO
 */
const jwt = require('jsonwebtoken')
const router = require('koa-router')({ sensitive: true })

const { jwtKey } = require('../../../config')

const Login = require('../../../models/login')

router.prefix('/api/st/login')

// STUDENT LOGIN
router.post('/', async (ctx, next) => {
  const { email, password } = ctx.request.body || {}

  const result = await Login.students(email, password)

  if (result.success) {
    const student = result.student
    const token = jwt.sign({
      iat: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 365),
      student: JSON.stringify(student)
    }, jwtKey)
    ctx.body = {
      success: true,
      message: 'Logged',
      student,
      token
    }
    return
  }
  ctx.status = 401
  ctx.body = result
})

// STUDENT LOGIN VIA FACEBOOK
router.post('/fb', async (ctx, next) => {
  const { fbAccessToken, fbId, email } = ctx.request.body || {}

  const result = await Login.fb(fbId, fbAccessToken, email)

  if (result.success) {
    const student = result.student
    const token = jwt.sign({
      iat: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 365),
      student: JSON.stringify(student)
    }, jwtKey)
    ctx.body = {
      success: true,
      message: 'Logged',
      fbToken: result.fbToken,
      expiresIn: result.expiresIn,
      student,
      token
    }
    return
  }
  ctx.status = 401
  ctx.body = result
})

module.exports = router
