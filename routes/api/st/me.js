// /**
//  * router/api/st/me.js
//  *
//  * @description :: Describes the 'me' api routes
//  * @docs        :: TODO
//  */
// const fs = require('fs')
// const Joi = require('joi')
// const router = require('koa-router')({ sensitive: true })

// router.prefix('/api/st/me')

// const Students = require('../../../models/students')

// // validation schemas
// const {
//   updateSchema
// } = require('../../../schemas/st/me')


// // GET ME INFO
// router.get('/', async (ctx, next) => {
//   if (ctx.state && ctx.state.student) {
//     ctx.body = {
//       success: true,
//       student: ctx.state.student
//     }
//     return next()
//   } else {
//     ctx.status = 401
//     ctx.body = {
//       success: false,
//       error: 'No authenticated',
//       message: 'No authenticated'
//     }
//   }
// })

// // UPDATE ME
// router.put('/', async (ctx, next) => {
//   if (ctx.state && ctx.state.student) {
//     const id = ctx.state.student._id
//     const body = ctx.request.body || {}

//     const validationResult = Joi.validate(body, updateSchema)
//     if (validationResult.error === null) {
//       // let stream
//       // let ext
//       // const image = ctx.request.files.image
//       // if (image) {
//       //   const nameArray = image.name.split('.')
//       //   ext = nameArray[1] || null
//       //   stream = fs.readFileSync(image.path)
//       // }

//       const result = await Students.updateProfile(
//         //id, validationResult.value, stream, ext
//         id, validationResult.value
//       )
//       if (result.success) {
//         ctx.body = result
//         return next()
//       }

//       ctx.status = result.code
//       ctx.body = result
//     } else {
//       ctx.status = 400
//       ctx.body = {
//         success: false,
//         error: validationResult.error.details || 'Validation error'
//       }
//     }
//   } else {
//     ctx.status = 401
//     ctx.body = {
//       success: false,
//       error: 'No authenticated',
//       message: 'No authenticated'
//     }
//   }
// })

// module.exports = router
