/**
 * router/api/count.js
 *
 * @description :: Describes the count api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const Count = require('../../models/count')

router.prefix('/api/count')

//  GET all workshops count
router.get('/workshops', async (ctx, next) => {
  const result = await Count.getWorkshops()

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get a specific workshop count
router.get('/workshop/course/:courseId', async (ctx, next) => {
  const courseId = ctx.params.courseId
  const result = await Count.getWorkshopByCourse(courseId)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get COMIPEMS count
router.get('/comipems', async (ctx, next) => {
  const result = await Count.getComipemsStudents()

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get superior count
router.get('/superior', async (ctx, next) => {
  const result = await Count.getSuperiorStudents()

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// GET a general course count
router.get('/course/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Count.getCountByCourse(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// THIS FUNCTION BREAKS THE SYSTEM
// get Simulacro count
router.get('/diff/simulacro', async (ctx, next) => {
  const result = await Count.getDiff()

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
