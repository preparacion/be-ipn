/**
 * router/api/users.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

const Users = require('../../models/users')

router.prefix('/api/users')

const splitRoles = (param) => {
  let str = param.split('')
  str.shift()
  str.splice(-1, 1)
  return str.join('').split(',').map(item => {
    return item.replace(/\s/g, '')
  })
}


// user validation schema
const {
  teachers,
  userSchema,
  updateUserSchema,
  idSchema,
  userFilterSchema,
  rolesUpdateSchema
} = require('../../schemas/users')

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, userSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }

    const result = await Users.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `Create an user`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get all items
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, userFilterSchema)
  if (validationResult.error === null) {
    if (query.rolesIn) {
      const rolesIn = splitRoles(query.rolesIn)

      if (!rolesIn.length) {
        ctx.status = 400
        ctx.body = {
          success: false,
          error: `Wrong string on 'rolesIn' param: ${query.rolesIn}`
        }
      }

      query.rolesIn = rolesIn
    }

    if (query.noRolesIn) {
      const noRolesIn = splitRoles(query.noRolesIn)

      if (!noRolesIn.length) {
        ctx.status = 400
        ctx.body = {
          success: false,
          error: `Wrong string on 'noRolesIn' param: ${query.noRolesIn}`
        }
      }

      query.noRolesIn = noRolesIn
    }

    const result = await Users.get(query)

    if (result.success) {
      ctx.body = {
        success: true,
        users: result.users
      }
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.getById(id)

  if (result.success) {
    ctx.body = {
      success: true,
      user: result.user
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// update an user info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateUserSchema)

  if (validationResult.error === null) {
    const result = await Users.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update an user ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// delete a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.delete(id)

  if (result.success) {
    ctx.state.action = `Delete an user ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

// update role in an user
router.put('/:id/role', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, rolesUpdateSchema)

  if (validationResult.error === null) {
    const result = await Users.updateRole(id, body.role)

    if (result.success) {
      ctx.state.action = `Update an user's role ${id}`
      ctx.state.data = body.role

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get the user's groups
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Users.getGroups(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// User password recover
router.put('/:id/pass/recover', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Users.recoverPass(validationResult.value.id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

//   ***** Users.findByPhone is not a function *****
// Get user phone number
router.get('/phone/:phoneNumber', async (ctx, next) => {
  const phoneNumber = ctx.params.phoneNumber

  const result = await Users.findByPhone(phoneNumber)

  if (result.success) {
    ctx.state.action = `Find user by phone ${phoneNumber}`
    ctx.state.data = { phoneNumber }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


module.exports = router
