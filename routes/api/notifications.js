/**
 * router/api/notification.js
 *
 * @description :: Describes the notification api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Notifications = require('../../models/notifications')
const Notification = new Notifications()

router.prefix('/api/notifications')

const {
  notificationSchema,
  genericNotificationSchema,
  byCourseSchema,
  sendToSchema,
  paginationSchema,
  searchFilter
} = require('../../schemas/notifications')


// Get Notifications
router.get('/', async (ctx, next) => {
  const query = ctx.query

  const validationResult = Joi.validate(query, paginationSchema)
  if (validationResult.error === null) {
    const result = await Notification.getAll(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Create a notification
router.post('/', async (ctx, next) => {
  const { normalize } = ctx.query || {}
  const body = ctx.request.body

  const validationResult = Joi.validate(body, sendToSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendTo(
      body,
      normalize
    )

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// New service of creating a notification 

router.post('/genericNotification', async (ctx, next) => {
  const { normalize } = ctx.query || {}
  const body = ctx.request.body
  const validationResult = Joi.validate(body, genericNotificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendGenericNotification(
      body,
      normalize
    )
    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Create a notification by studet¿nt
router.post('/student/:id', async (ctx, next) => {
  const id = ctx.params.id
  const { normalize } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, notificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendToStudent(
      body,
      normalize
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Create a notification by User talk
router.post('/userTalk/:id', async (ctx, next) => {
  const id = ctx.params.id
  const { normalize } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id })
  const validationResult = Joi.validate(body, notificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendToUserTalk(
      body,
      normalize
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Create a notification by a Group
router.post('/group/:id', async (ctx, next) => {
  const id = ctx.params.id
  const { normalize, debt } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, notificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendToGroup(
      body,
      normalize,
      debt
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Create a notification by a Talk Group
router.post('/talk/:id', async (ctx, next) => {
  const id = ctx.params.id
  const { normalize } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, notificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendToTalkGroup(
      body,
      normalize
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


//    D E P R E C A T E D  ////
// Create a notification by a Course
router.post('/course/:id', async (ctx, next) => {
  const id = ctx.params.id
  const {
    startGroupDate,
    endGroupDate,
    debt,
    normalize
  } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, notificationSchema)
  if (validationResult.error === null) {
    const result = await Notification.sentToCourse(
      body,
      startGroupDate,
      endGroupDate,
      debt,
      normalize
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Create a notification by a Course
router.post('/byCourse/:id', async (ctx, next) => {
  const id = ctx.params.id
  const {
    startGroupDate,
    endGroupDate
  } = ctx.query || {}
  const body = _.extend(ctx.request.body, { id, startGroupDate, endGroupDate })
  const validationResult = Joi.validate(body, byCourseSchema)
  if (validationResult.error === null) {
    const result = await Notification.sendByCourse(
      body
    )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})




// Create a notification to a client
router.post('/socketStudent', async (ctx, next) => {
  const socket = ctx.socket_io
  const body = ctx.request.body || {}
  if (ctx.state && ctx.state.user) {
    const result = await Notification.sendSocket(body, socket)
    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

// Create a notification to a client
router.get('/groupsByCourse/:idCourse', async (ctx, next) => {
  const socket = ctx.socket_io
  const query = ctx.query
  const idCourse = ctx.params.idCourse
  const validationResult = Joi.validate(query, searchFilter)
  if (validationResult.error === null) {
    if (ctx.state && ctx.state.user) {
      const result = await Notification.getGroupsCourseNotification({
        idCourse,
        ...validationResult.value
      })
      if (result.success) {
        ctx.status = 201
        ctx.body = result
        return next()
      }
      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error
      }
    } else {
      ctx.status = 401
      ctx.body = {
        success: false,
        error: 'No authenticated',
        message: 'No authenticated'
      }
    }
  }
})

module.exports = router
