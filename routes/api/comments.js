/**
 * router/api/comments.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

router.prefix('/api/comments')

const Comments = require('../../models/comments')

const {
  commentIdSchema,
  commentSchema,
  commentFilterSchema,
  commentUpdateSchema } = require('../../schemas/comment')

  // Get comments by group, talk, user, student, userTalk ... it depends on the filter applied
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, commentFilterSchema)

  if (validationResult.error === null) {
    const result = await Comments.get(validationResult.value)
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a comment by id comment
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, commentIdSchema)

  if (validationResult.error === null) {
    const result = await Comments.getById(validationResult.value)
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Update a comment by comment id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const user = ctx.state.user
  const body = ctx.request.body

  const data = _.extend({ id }, body)

  const validationResult = Joi.validate(data, commentUpdateSchema)

  if (validationResult.error === null) {
    const result = await Comments.update(validationResult.value, user._id)

    ctx.state.action = `update a comment ${id}`
    ctx.state.data = { id }

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// delete a comment by comment id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, commentIdSchema)

  if (validationResult.error === null) {
    const result = await Comments.delete(validationResult.value)

    ctx.state.action = `Delete a comment ${id}`

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// create a comment
router.post('/', async (ctx, next) => {
  const body = ctx.request.body
  const user = ctx.state.user
  const socket = ctx.socket_io

  const validationResult = Joi.validate(body, commentSchema)

  if (validationResult.error === null) {
    const result = await Comments.create(validationResult.value, user._id, socket)

    ctx.state.action = `Create a comment`
    ctx.state.data = validationResult.value

    ctx.status = 201
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
