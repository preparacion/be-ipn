/**
 * router/api/usersTalks.js
 *
 * @description :: Describes the users api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })
const UsersTalks = require('../../models/usersTalks')

router.prefix('/api/usersTalks')

// validation schemas
const {
  updateUserTalksSchema,
  resendUserTalkSchema
} = require('../../schemas/usersTalks')


// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await UsersTalks.getById(id)
  if (result.success) {
    ctx.state.action = `Get UserTalks by id ${id}`
    ctx.state.data = { id }
    ctx.body = {
      success: true,
      student: result.student
    }
    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// update an student info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, updateUserTalksSchema)
  if (validationResult.error === null) {
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }
    const result = await UsersTalks.update(id, validationResult.value)
    if (result.success) {
      ctx.state.action = `Update an UserTalks ${id}`
      ctx.state.data = validationResult.value
      ctx.body = result
      return next()
    }
    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Delete an user Talk
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const result = await UsersTalks.delete(id)
  if (result.success) {
    ctx.state.action = `Delete an UserTalks ${id}`
    ctx.body = result
    return result
  }
  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
}),

// Change user talk from talk id
  router.put('/:id/talks/:talkId', async (ctx, next) => {
    const id = ctx.params.id
    const talkId = ctx.params.talkId
    const body = ctx.request.body || {}
    const { force } = ctx.query
    const socket = ctx.socket_io

    const result = await UsersTalks.changeGroups(id, talkId, body, force, socket)
    if (result.success) {
      ctx.state.action = `Change a userTalks from talkId ${id}, ${talkId}`
      ctx.state.data = { id, body, talkId }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  })


  /** resend pdf to usersTalks */
router.post('/resend/:idTalk', async (ctx, next) => {
  const idTalk = ctx.params.idTalk
  const body = ctx.request.body

  const validationResult = Joi.validate(_.extend({ idTalk }, body), resendUserTalkSchema)
  if (validationResult.error === null) {
    const result = await UsersTalks.resendTalk(idTalk, body)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



module.exports = router