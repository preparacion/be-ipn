/**
 * router/api/materials.js
 *
 * @description :: Describes the materials api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const LiveChannels = require('../../models/liveChannels')

router.prefix('/api/livechannels')

// validation schemas
const {
    paginationSchema,
    idSchema,
    createChannelSchema
  } = require('../../schemas/liveChannels')

//  Get all the materials
router.get('/', async (ctx, next) => {
    const query = ctx.query
    const validationResult = Joi.validate(query, paginationSchema)
    if (validationResult.error === null) {
        const result = await LiveChannels.get(validationResult.value)

        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

//Important note , if we move the router function behind :id koa will take first priority to :id instead awsInfo
router.get('/awsInfo', async (ctx, next) => {
    const query = ctx.query
    const validationResult = Joi.validate(query, paginationSchema)
    if (validationResult.error === null) {
        const result = await LiveChannels.getAwsInfo(validationResult.value)

        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

router.get('/:id', async (ctx, next) => {
    const id = ctx.params.id
    const validationResult = Joi.validate({ id: id }, idSchema);

    if (validationResult.error === null) {
        const result = await LiveChannels.get(validationResult.value)
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

// Create a material
router.post('/', async (ctx, next) => {
    const body = ctx.request.body

    const validationResult = Joi.validate(body, createChannelSchema)
    if (validationResult.error === null) {
        let user
        if (ctx.state && ctx.state.user) {
            user = ctx.state.user._id
        }
        const result = await LiveChannels.create(validationResult.value, user)
        ctx.body = result
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

router.put('/:id', async (ctx, next) => {
    const id = ctx.params.id
    const body = ctx.request.body
    const validationIdResult = Joi.validate({ id: id }, idSchema);
    //First validate the id of the request
    if (validationIdResult.error === null) {
        const validationResult = Joi.validate(body, createChannelSchema)
        //Then validate the body of request
        if (validationResult.error === null) {
            let user
            if (ctx.state && ctx.state.user) {
                user = ctx.state.user._id
            }
            const result = await LiveChannels.create(validationResult.value, user)
            ctx.body = result
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationIdResult.error.details || 'Validation error'
        }
    }
})


module.exports = router
