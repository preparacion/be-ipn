/**
 * router/api/me.js
 *
 * @description :: Describes the 'me' api routes
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Users = require('../../models/users')

// validation schemas
const {
  updateSchema
} = require('../../schemas/me')

router.prefix('/api/me')

// Get all info about Me
router.get('/', async (ctx, next) => {
  if (ctx.state && ctx.state.user) {
    ctx.body = {
      success: true,
      user: ctx.state.user
    }
    return next()
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

// Update Me Info
// ******* Image function isn´t available ******
router.put('/', async (ctx, next) => {
  if (ctx.state && ctx.state.user) {
    const id = ctx.state.user._id
    const body = ctx.request.body || {}

    const validationResult = Joi.validate(body, updateSchema)
    if (validationResult.error === null) {
/*    let stream
      let ext
      const image = ctx.request.files.image 

      if (image) {
        const nameArray = image.name.split('.')
        ext = nameArray[1] || null
        stream = fs.readFileSync(image.path)
      }

      const result = await Users.update(
        id, validationResult.value,
        image,
        stream,
        ext
      ) */
      const result = await Users.update(
        id, validationResult.value)


      if (result.success) {
        ctx.body = result
        return next()
      }

      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error
      }
    } else {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: validationResult.error.details || 'Validation error'
      }
    }
  } else {
    ctx.status = 401
    ctx.body = {
      success: false,
      error: 'No authenticated',
      message: 'No authenticated'
    }
  }
})

module.exports = router
