/**
 * router/api/docs.js
 *
 * @description :: Describes the doc api route
 * @docs        :: TODO
 */
const fs = require('fs')
const path = require('path')
const swaggerJSDoc = require('swagger-jsdoc')
const router = require('koa-router')({ sensitive: true })

router.prefix('/docs')

const readFileThunk = function (src) {
  return new Promise(function (resolve, reject) {
    fs.readFile(src, { 'encoding': 'utf8' }, function (err, data) {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

// -- setup up swagger-jsdoc --
const swaggerDefinition = {
  servers: [
    {
      url: 'https://dev.api.preparacionipn.mx/api/',
      description: 'Dev server'
    },
    {
      url: 'https://dev.api.preparacionipn.mx/api',
      description: 'Production server'
    }
  ],
  info: {
    title: 'Preparación Politécnico',
    version: '1.9.2',
    description: 'Backend api routes',
    contact: {
      name: 'API support',
      email: 'support@bluedotmx.com'
    }
  },
  host: 'localhost:3100',
  basePath: '/api'
}

const options = {
  apis: [
    path.resolve(__dirname, '../../docs/appoiments.yml'),
    path.resolve(__dirname, '../../docs/attendance.yml'),
    path.resolve(__dirname, '../../docs/available.yml'),
    path.resolve(__dirname, '../../docs/calendar.yml'),
    path.resolve(__dirname, '../../docs/classrooms.yml'),
    path.resolve(__dirname, '../../docs/comipems.yml'),
    path.resolve(__dirname, '../../docs/comments.yml'),
    path.resolve(__dirname, '../../docs/count.yml'),
    path.resolve(__dirname, '../../docs/courses.yml'),
    path.resolve(__dirname, '../../docs/courselevels.yml'),
    path.resolve(__dirname, '../../docs/coursetypes.yml'),
    path.resolve(__dirname, '../../docs/credits.yml'),
    path.resolve(__dirname, '../../docs/errors.yml'),
    path.resolve(__dirname, '../../docs/external.yml'),
    path.resolve(__dirname, '../../docs/folio.yml'),
    path.resolve(__dirname, '../../docs/groups.yml'),
    path.resolve(__dirname, '../../docs/home.yml'),
    path.resolve(__dirname, '../../docs/images.yml'),
    path.resolve(__dirname, '../../docs/locations.yml'),
    path.resolve(__dirname, '../../docs/login.yml'),
    path.resolve(__dirname, '../../docs/logs.yml'),
    path.resolve(__dirname, '../../docs/materials.yml'),
    path.resolve(__dirname, '../../docs/me.yml'),
    path.resolve(__dirname, '../../docs/notifications.yml'),
    path.resolve(__dirname, '../../docs/paymentInfo.yml'),
    path.resolve(__dirname, '../../docs/payments.yml'),
    path.resolve(__dirname, '../../docs/components.yml'),
    path.resolve(__dirname, '../../docs/playlists.yml'),
    path.resolve(__dirname, '../../docs/quiz.yml'),
    path.resolve(__dirname, '../../docs/register.yml'),
    path.resolve(__dirname, '../../docs/reports.yml'),
    path.resolve(__dirname, '../../docs/roles.yml'),
    path.resolve(__dirname, '../../docs/schedules.yml'),
    path.resolve(__dirname, '../../docs/search.yml'),
    path.resolve(__dirname, '../../docs/sms.yml'),
    path.resolve(__dirname, '../../docs/stages.yml'),
    path.resolve(__dirname, '../../docs/studentComments.yml'),
    path.resolve(__dirname, '../../docs/studentLists.yml'),
    path.resolve(__dirname, '../../docs/students.yml'),
    path.resolve(__dirname, '../../docs/talks.yml'),
    path.resolve(__dirname, '../../docs/templates.yml'),
    path.resolve(__dirname, '../../docs/users.yml'),
    path.resolve(__dirname, '../../docs/usersTalks.yml'),
    path.resolve(__dirname, '../../docs/videos.yml'),
    path.resolve(__dirname, '../../docs/unpr.yml'),
    path.resolve(__dirname, '../../docs/st.yml')
  ],
  swaggerDefinition
}

const swaggerSpec = swaggerJSDoc(options)

router.get('/', async (ctx, next) => {
  ctx.body = await readFileThunk(path.join(__dirname, '../../docs/redoc.html'))
})

router.get('/download/swagger.json', async (ctx, next) => {
  ctx.set('Content-Type', 'application/json')
  ctx.body = swaggerSpec
})

module.exports = router
