/**
 * router/api/images.js
 *
 * @description :: Describes the videos api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Images = require('../../models/images')

// validation schemas
const { paginationSchema } = require('../../schemas/pagination')

router.prefix('/api/images')


const imageSchema = Joi.object().keys({
  filename: Joi.string().required(),
})


/*********************************************
**********************************************
*************** Upload a image  **************
**********************************************
*********************************************/
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, imageSchema)
  if (validationResult.error === null) {
    const result = await Images.createUrl(validationResult.value)
    if (result.success) {
      ctx.state.action = `createUrlToUpload`
      ctx.state.data = {}
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


/*********************************************
**********************************************
************* Get All images *****************
**********************************************
*********************************************/

router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Images.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `Get all the videos`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

/*********************************************
**********************************************
************* Get images  by id  *************
**********************************************
*********************************************/

router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, videoIdSchema)

  if (validationResult.error === null) {
    const result = await Images.getById(validationResult.value.id)

    if (result.success) {
      ctx.state.action = `get a single video ${validationResult.value.id}`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


/*********************************************
**********************************************
*************** Delete a image  **************
**********************************************
*********************************************/

router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, videoIdSchema)

  if (validationResult.error === null) {
    const result = await Images.delete(validationResult.value.id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
