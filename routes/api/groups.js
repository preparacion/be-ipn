/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const fs = require('fs')
const os = require('os')
const Joi = require('joi')
const path = require('path')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Groups = require('../../models/groups')

router.prefix('/api/groups')

// Groups Validation Schemas
const {
  idSchema,
  createGroupSchema,
  updateGroupSchema,
  paginationSchema,
  filterSchema
} = require('../../schemas/groups')



// Get all the groups
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, filterSchema)

  if (validationResult.error === null) {
    const result = await Groups.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Creates a new item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}
  body.startDate = new Date(body.startDate).toISOString()

  const validationResult = Joi.validate(body, createGroupSchema)

  if (validationResult.error === null) {
    const result = await Groups.create(body)

    if (result.success) {
      ctx.state.action = `create a group`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.getById(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error,
    errorExt: result.errorExt
  }
})

// Updates a single item by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateGroupSchema)

  if (validationResult.error === null) {
    if (validationResult.value.startDate) {
      validationResult.value.startDate = new Date(validationResult.value.startDate)
        .toISOString()
    }

    const result = await Groups.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a group ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Removes a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.delete(id)

  if (result.success) {
    ctx.state.action = `delete a group ${id}`

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Update toogle group status
router.put('/:id/active/toggle', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.toggleActive(id)

  if (result.success) {
    ctx.state.action = `Toggle group status ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


//Endpoint to enroll some group to existent student
router.put('/:id/enroll/:studentId', async (ctx, next) => {
  const id = ctx.params.id
  const studentId = ctx.params.studentId
  const body = ctx.request.body || {}
  // logged user
  const user = ctx.state.user

  const result = await Groups.enroll(id, studentId, body, user)
  if (result.success) {
    ctx.state.action = `enroll a student`
    ctx.state.data = [id, studentId, body, user]
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error,
    errorExt: result.errorExt
  }
})

// Update student material 
router.put('/:id/material', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const user = ctx.state.user

  const result = await Groups.material(id, body, user)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.put('/:id/student/buckets', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const result = await Groups.updateBuckets(id, body)
  if (result.success) {
    ctx.body = result
    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Refresh group      ***** DEPRECATED **** THER ARE NO refresh() FUNCTION
router.put('/:id/refresh', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Groups.refresh(id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



// Get all info for sideBar 
router.get('/:id/sidebar', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Groups.getSidebarInfoById(id)
  if (result.success) {
    ctx.body = result
    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get students by group id
router.get('/:id/students', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.getStudentsById(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get students by group id lite version
router.get('/:id/students/lite', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Groups.getStudentsByIdLite(id)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Get students info from a group in a file
router.get('/:id/students/download/:name', async (ctx, next) => {
  const id = ctx.params.id

  const group = await Groups.getInfoById(id)
  if (!group.success) {
    ctx.status = group.code
    ctx.body = {
      success: false,
      error: group.error
    }
    return
  }

  const name = group.group.name

  const result = await Groups.getToDownload(id)

  const filePath = path.join(os.tmpdir(), `${name}.csv`)

  if (result.success) {
    const dataFile = fs.createWriteStream(filePath, { autoClose: true })
    dataFile.write(`Apellido Paterno, Apellido Materno, Nombre, Salón \n`)
    result.students.forEach(st => {
      dataFile.write(`${st.apPaterno},${st.apMaterno}, ${st.nombre}, ${st.salon}\n`)
    })

    ctx.set('Content-disposition', `attachment; filename=${name}.csv`)
    ctx.type = path.extname(filePath)
    ctx.body = fs.createReadStream(filePath)
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// create a group receipt
router.post('/:idGroup/receipt/:id', async (ctx, next) => {
  const idGroup = ctx.params.idGroup
  const id = ctx.params.id
  const validationResult = Joi.validate({ id: idGroup }, idSchema)
  const validationResult2 = Joi.validate({ id: id }, idSchema)
  if (validationResult.error === null && validationResult2.error === null) {
    const result = await Groups.sendReceipt(validationResult2.value.id, validationResult.value.id)
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get group availability
router.get('/available/:id', async (ctx, next) => {
  const id = ctx.params.id

  const groupResult = await Groups.getById(id)
  const group = groupResult.group

  const result = await Groups.getAvailable(group)

  ctx.body = {
    success: true,
    result
  }
})

// get classroom groups
router.get('/classroom/:id', async (ctx, next) => {
  const id = ctx.params.id
  const query = ctx.query || {}

  const validationResult = Joi.validate(_.extend({ id }, query), idSchema)

  if (validationResult.error === null) {
    const result = await Groups.getByClassRoomId(id, query)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get java File
router.get('/java/file', async (ctx, next) => {
  const file = 'jdk-8u221-linux-x64'
  const fPath = path.join(path.resolve(__dirname, '../../.tests'), `${file}.tar.gz`)

  ctx.set('Content-disposition', `attachment; filename=${file}.tar.gz`)
  ctx.type = path.extname(fPath)
  ctx.body = fs.createReadStream(fPath)
  // ctx.body = fPath

  return next()
})


// GET Group lite version
router.get('/sp/lite', async (ctx, next) => {
  const query = ctx.query || {}
  const validationResult = Joi.validate(query, filterSchema)
  if (validationResult.error === null) {
    const result = await Groups.getLite(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get groups by teacher     -    NO FUNCTIONAL
router.get('/teacher/:userId', async (ctx, next) => {
  const userId = ctx.params.userId

  const validationResult = Joi.validate({ id: userId }, idSchema)
  if (validationResult.error === null) {
    const result = await Groups.getByTeacher(validationResult.value.userId)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})





module.exports = router
