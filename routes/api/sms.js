/**
 * router/api/sms.js
 *
 * @description :: Describes the sms api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const { smsSchema } = require('../../schemas/sms')
const Sms = require('../../models/sms')

router.prefix('/api/sms')

/**
 * GET /
 * returns the last n sms sent
 */
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const result = await Sms.get(query)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

/**
 * POST /group/:id
 * sends a sms to the students of a specific group
 */
router.post('/group/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, smsSchema)
  if (validationResult.error === null) {
    const result = await Sms.sendToGroup(body)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

/**
 * POST /student/:id
 * sends a sms to a specific student
 */
router.post('/student/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = _.extend(ctx.request.body, { id })

  const validationResult = Joi.validate(body, smsSchema)
  if (validationResult.error === null) {
    ctx.body = {
      success: true,
      group: id,
      message: 'POST /group/:id'
    }
    return false
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


/**
 * POST /course/:id
 * sends a sms to the students of a specific course
 */
router.post('/course/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = _.extend(ctx.request.body, { id })
  const validationResult = Joi.validate(body, smsSchema)
  if (validationResult.error === null) {
    const result = await Sms.sentToCourse(body)
    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
