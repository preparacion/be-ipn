/**
 * router/api/errors.js
 *
 * @description :: Describes the errors api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const ErrMsg = require('../../lib/errMsg')
const errMsg = new ErrMsg()

router.prefix('/api/errors')

// GET ALL ERRORS
router.get('/', async (ctx, next) => {
  ctx.body = {
    success: true,
    errors: errMsg.getErrors()
  }
  return false
})

module.exports = router
