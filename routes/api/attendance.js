/**
 * router/api/groups.js
 *
 * @description :: Describes the groups api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

router.prefix('/api/attendance')

const Attendance = require('../../models/attendance')

// validation schemas
const {
  attendanceSchema,
  attendanceQuery,
  attendanceUpdateSchema,
  attendancePostSingle,
  attendanceTeacher,
  attendanceUpdateSingle,
  attendanceUpdateUsers,
  attendanceDelete,
  attendanceTeacherDelete,
  attendanceIdSchema
} = require('../../schemas/attendance')

// get attendance by group
router.get('/group/:id', async (ctx, next) => {
  const id = ctx.params.id
  const query = ctx.query || {}

  const validationResult = Joi.validate({
    id,
    startDate: query.startDate,
    endDate: query.endDate
  }, attendanceQuery)

  if (validationResult.error === null) {
    const result = await Attendance
      .getByGroupId(
        validationResult.value.id,
        validationResult.value.startDate,
        validationResult.value.endDate
      )

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get attendance by student 
router.get('/student/:id', async (ctx, next) => {
  const id = ctx.params.id
  const query = ctx.query || {}

  const validationResult = Joi.validate({
    id,
    startDate: query.startDate,
    endDate: query.endDate,
    group: query.group
  }, attendanceQuery)

  if (validationResult.error === null) {
    const result = await Attendance
      .getByStudentId(
        validationResult.value.id,
        validationResult.value.startDate,
        validationResult.value.endDate,
        validationResult.value.group
      )

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



// post attendance
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, attendanceSchema)

  if (validationResult.error === null) {
    const result = await Attendance.create(validationResult.value)

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    //ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


//Add or update attendace by studentId 
router.put('/student/today', async (ctx, next) => {
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, attendanceUpdateSingle)
  if (validationResult.error === null) {
    const result = await Attendance.addOrUpdateTodayAttendanceStudent(validationResult.value)
    if (result.success) {
      ctx.status = 200
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// put attendance by idAttendance
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const data = _.extend(body, { id })

  const validationResult = Joi.validate(data, attendanceUpdateSchema)

  if (validationResult.error === null) {
    const result = await Attendance.update(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// put single attendance by student
router.put('/single/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const body = ctx.request.body || {}
  const data = _.extend(body, { studentId })

  const validationResult = Joi.validate(data, attendanceUpdateSingle)

  if (validationResult.error === null) {
    const result = await Attendance.updateSingle(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// put attendance QR by student 
router.put('/qr/:studentId', async (ctx, next) => {
  const id = ctx.params.studentId
  const socket = ctx.socket_io
  const validationResult = Joi.validate({ id }, attendanceIdSchema)
  if (validationResult.error === null) {
    const result = await Attendance.qrAttendance(validationResult.value.id, socket)
    if (result.false && result.code) {
      ctx.status = result.code
      ctx.body = {
        success: false,
        error: result.error
      }
    }else{
      ctx.body = result
      return next()
    }


  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// put attendance by module QR by Student
router.put('/moduleQr/:studentId', async (ctx, next) => {
  const id = ctx.params.studentId
  const validationResult = Joi.validate({ id }, attendanceIdSchema)
  const body = ctx.request.body || {}
  if (validationResult.error === null) {
    const result = await Attendance.setComipemsBucket(validationResult.value.id, body)
    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// post single attendance by student
router.post('/st/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const body = ctx.request.body || {}
  const data = _.extend(body, { studentId })

  const validationResult = Joi.validate(data, attendancePostSingle)

  if (validationResult.error === null) {
    const result = await Attendance.postSingle(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Removes a single attendance by id in student attendance
router.delete('/single/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const query = ctx.query || {}
  const data = _.extend(query, { studentId })

  const validationResult = Joi.validate(data, attendanceDelete)

  if (validationResult.error === null) {
    const result = await Attendance.deleteAttendance(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

//////// ******  T E A C H E R S   *******

// post single attendance by user
router.post('/users/:userId', async (ctx, next) => {
  const userId = ctx.params.userId
  const body = ctx.request.body || {}
  const data = _.extend(body, { userId })

  const validationResult = Joi.validate(data, attendanceTeacher)

  if (validationResult.error === null) {
    const result = await Attendance.addTeacherAttendance(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})





// get attendance by teacher
router.get('/user/:id', async (ctx, next) => {
  const id = ctx.params.id
  const query = ctx.query || {}

  const validationResult = Joi.validate({
    id,
    startDate: query.startDate,
    endDate: query.endDate
  }, attendanceQuery)

  if (validationResult.error === null) {
    const result = await Attendance
      .getByTeacherId(
        validationResult.value.id,
        validationResult.value.startDate,
        validationResult.value.endDate
      )

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// put single attendance by teacher
router.put('/user/:userId', async (ctx, next) => {
  const userId = ctx.params.userId
  const body = ctx.request.body || {}
  const data = _.extend(body, { userId })

  const validationResult = Joi.validate(data, attendanceUpdateUsers)

  if (validationResult.error === null) {
    const result = await Attendance.updateUserAtt(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Removes a single attendance by id in teacher attendance
router.delete('/user/:userId', async (ctx, next) => {
  const userId = ctx.params.userId
  const query = ctx.query || {}
  const data = _.extend(query, { userId })

  const validationResult = Joi.validate(data, attendanceTeacherDelete)

  if (validationResult.error === null) {
    const result = await Attendance.deleteTeacherAtt(data)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


module.exports = router
