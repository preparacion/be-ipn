/**
 *  router/api/home.js
 *
 * @description :: Describes the home api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

router.get('/', async (ctx) => {
  ctx.body = {
    success: true,
    message: 'prep ipn be, ok'
  }
})

module.exports = router
