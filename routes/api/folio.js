/**
 * router/api/folio.js
 *
 * @description :: Describes the folio api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })
const _ = require('underscore')


const Folio = require('../../models/folio')

router.prefix('/api/folio')

const {
  FolioSchema,
  updateFolioSchema
 } = require('../../schemas/folio')



// Create a new folio
router.post('/:idStudent', async (ctx, next) => {

  const idStudent = ctx.params.idStudent
  const body = _.extend(ctx.request.body, { idStudent })
  const validationResult = Joi.validate(body, FolioSchema)
  if (validationResult.error === null) {
    const result = await Folio.create(validationResult.value)
    if (result.success) {
      ctx.state.action = `create a Folio`
      ctx.state.data = {}
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



// Put parameters in a folio
router.put('/:idFolio', async (ctx, next) => {
  const idFolio = ctx.params.idFolio
  const body = _.extend(ctx.request.body, { idFolio })
  const validationResult = Joi.validate(body, updateFolioSchema)

  if (validationResult.error === null) {
    const result = await Folio.update(validationResult.value)

    if (result.success) {
      ctx.state.action = `Update a folio ${idFolio}`
      ctx.state.data = validationResult.value
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


//Delete folio by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Folio.delete(id)

  if (result.success) {
    ctx.state.action = `delete a folio ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})




module.exports = router
