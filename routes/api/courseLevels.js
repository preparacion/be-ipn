/**
 * router/api/courseLevels.js
 *
 * @description :: Describes the courseLevels api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const CourseLevel = require('../../models/courseLevels')

router.prefix('/api/course/levels')

const { clId, clUpdateSchema } = require('../../schemas/courseLevels')


// GET all course levels
router.get('/', async (ctx, next) => {
  const result = await CourseLevel.get()
  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get course level by level id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, clId)
  if (validationResult.error === null) {
    const result = await CourseLevel.getById(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// UPDATE a single course level by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const data = _.extend(body, { id })
  const validationResult = Joi.validate(
    data,
    clUpdateSchema
  )
  if (validationResult.error === null) {
    const result = await CourseLevel.update(validationResult.value.id, data)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// GET course level of a course by course id
router.get('/:id/courses', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, clId)
  if (validationResult.error === null) {
    const result = await CourseLevel
      .getCoursesById(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
