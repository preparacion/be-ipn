/**
 * router/api/calendar.js
 *
 * @description :: Describes the calendar api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Calendar = require('../../models/calendar')

router.prefix('/api/calendar')

// validation schemas
const {
  calendarSchema
} = require('../../schemas/calendar')

// Get calendar info
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}


  const validationResult = Joi.validate(query, calendarSchema)
  if (validationResult.error === null) {
    const result = await Calendar.get(query.student, query.group)

    if (result.success) {
      ctx.body = result
      return false
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
