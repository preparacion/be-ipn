// N O T     U S E D

/**
 * router/api/studentComments.js
 *
 * @description :: Describes the Student Comments api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const StudentComments = require('../../models/studentComments')

router.prefix('/api/studentComments')

// validation schemas
const {
  idSchema,
  createStudentCommentSchema
} = require('../../schemas/studentComments')

// CREATE STUDENT COMMENT
router.post('/', async (ctx, next) => {
  const body = ctx.request.body
  const validationResult = Joi.validate(body, createStudentCommentSchema)
  if (validationResult.error === null) {
    const result = await StudentComments.create(validationResult.value)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET STUDENT COMMENTS
router.get('/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const validationResult = Joi.validate({ id: studentId }, idSchema)
  if (validationResult.error === null) {
    const result = await StudentComments.getByStudentId(validationResult.value.id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
    ctx.body = {
      success: true,
      message: 'get student comments'
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



module.exports = router
