/**
 * router/api/quizz.js
 *
 * @description :: Describes the quizz api 
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Videos = require('../../models/videos')
const Quiz = require('../../models/quiz')

const {
    quizSchema,
    quizSchemaUpdate,
    quizIdSchema,
    paginationSchema
} = require('../../schemas/quiz')


router.prefix('/api/quiz')


// GET QUIZ
router.get('/', async (ctx, next) => {
    const query = ctx.query || {}
    const validationResult = Joi.validate(query, paginationSchema)

    if (validationResult.error === null) {
        const result = await Quiz.get(validationResult.value)
        if (result.success) {
            ctx.state.action = `Get all the Quiz`
            ctx.state.data = {}

            ctx.body = result
            return next()
        }

        ctx.status = result.error.httpCode
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})


// CREATE A QUIZ
router.post('/:idPlaylist', async (ctx, next) => {
    if (ctx.state && ctx.state.user) {
        const id = ctx.params.idPlaylist
        const body = ctx.request.body
        const validationResult = Joi.validate({ id }, quizIdSchema)
        const validationResult2 = Joi.validate(body, quizSchema)
        if (validationResult.error === null && validationResult2.error === null) {
            const result = await Quiz.create(validationResult.value.id, validationResult2.value, ctx.state.user)
            if (result.success) {
                ctx.state.action = `Create Quiz`
                ctx.state.data = {}
                ctx.body = result
                return next()
            }
            ctx.status = result.error.httpCode
            ctx.body = {
                success: false,
                error: result.error
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult2.error || 'Validation error'
            }

        }

    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

// UPDATE QUIZ
router.put('/:idQuiz', async (ctx, next) => {
    if (ctx.state && ctx.state.user) {
        const id = ctx.params.idQuiz
        const body = ctx.request.body
        const validationResult = Joi.validate({ id }, quizIdSchema)
        const validationResult2 = Joi.validate(body, quizSchemaUpdate)
        if (validationResult.error === null && validationResult2.error === null) {
            const result = await Quiz.update(validationResult.value.id, validationResult2.value, ctx.state.user)
            if (result.success) {
                ctx.state.action = `Update Quiz`
                ctx.state.data = {}
                ctx.body = result
                return next()
            }
            ctx.status = result.error.httpCode
            ctx.body = {
                success: false,
                error: result.error
            }
        } else {
            ctx.status = 400
            ctx.body = {
                success: false,
                error: validationResult2.error.details || 'Validation error'
            }
        }
    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})


// GEY BY ID
router.get('/:idQuiz', async (ctx, next) => {
    const id = ctx.params.idQuiz
    const validationResult = Joi.validate({ id }, quizIdSchema)
    if (validationResult.error === null) {
        const result = await Quiz.getById(validationResult.value.id)
        if (result.success) {
            ctx.state.action = `get a single quiz ${validationResult.value.id}`
            ctx.state.data = {}

            ctx.body = result
            return next()
        }

        ctx.status = result.code
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})


// DELETE QUIZ
router.delete('/:idQuiz', async (ctx, next) => {
    const id = ctx.params.idQuiz
    const validationResult = Joi.validate({ id }, quizIdSchema)
    if (validationResult.error === null) {
        const result = await Quiz.delete(validationResult.value.id)

        if (result.success) {
            ctx.body = result
            return next()
        }

        ctx.status = result.error.httpCode
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})

// GET QUIZ BY VIDEO ID
router.get('/videos/:id', async (ctx, next) => {
    const id = ctx.params.id
    const validationResult = Joi.validate({ id }, quizIdSchema)
    if (validationResult.error === null) {
        const result = await Quiz.getVideosById(validationResult.value.id)
        if (result.success) {
            ctx.state.action = `get a single quiz ${validationResult.value.id}`
            ctx.state.data = {}

            ctx.body = result
            return next()
        }
        ctx.status = result.code
        ctx.body = {
            success: false,
            error: result.error
        }
    } else {
        ctx.status = 400
        ctx.body = {
            success: false,
            error: validationResult.error.details || 'Validation error'
        }
    }
})




module.exports = router
