/**
 * router/api/roles.js
 *
 * @description :: Describes the roles api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Roles = require('../../models/roles')

router.prefix('/api/roles')

// validation schemas
const {
  createRoleSchema,
  updateRoleSchema,
  idSchema,
  paginationSchema
} = require('../../schemas/roles')

// CREATE A ROL
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createRoleSchema)

  if (validationResult.error === null) {
    const result = await Roles.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a new Role`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET ALL ROLES
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)
  if (validationResult.error === null) {
    const result = await Roles.get(validationResult.value)
    if (result.success) {
      ctx.body = {
        success: true,
        roles: result.roles
      }
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET A ROL BY ID
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Roles.getById(id)

    if (result.success) {
      ctx.body = {
        success: true,
        role: result.role
      }
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

//UPDATE A ROL
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(_.extend({ id }, body), updateRoleSchema)

  if (validationResult.error === null) {
    const result = await Roles.update(id, body)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// DELETE A ROL
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)

  if (validationResult.error === null) {
    const result = await Roles.delete(id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET USERS BY ROL ID
router.get('/:id/users', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    ctx.body = {
      success: true,
      message: 'users'
    }
    const result = await Roles.getUsers(id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
