/**
 * router/api/materials.js
 *
 * @description :: Describes the materials api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Materials = require('../../models/materials')

router.prefix('/api/materials')

// validation schemas
const {
  paginationSchema,
  createMaterialSchema
} = require('../../schemas/materials')


//  Get all the materials
router.get('/', async (ctx, next) => {
  const query = ctx.query
  const validationResult = Joi.validate(query, paginationSchema)
  if (validationResult.error === null) {
    const result = await Materials.get(validationResult.value)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Create a material
router.post('/', async (ctx, next) => {
  const body = ctx.request.body

  const validationResult = Joi.validate(body, createMaterialSchema)
  if (validationResult.error === null) {
    let user
    if (ctx.state && ctx.state.user) {
      user = ctx.state.user._id
    }
    const result = await Materials.create(validationResult.value, user)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a material by its
router.get('/:id', async (ctx, next) => {
  const query = ctx.query
  const validationResult = Joi.validate(query, paginationSchema)
  if (validationResult.error === null) {
    const result = await Materials.get(validationResult.value)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
