/**
 * router/api/courses.js
 *
 * @description :: Describes the courses api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Courses = require('../../models/courses')
router.prefix('/api/courses')

// courses validation schemas
const {
  createCourseSchema,
  datesQuery,
  updateCourseSchema,
  paginationSchema,
  discountsSchema
} = require('../../schemas/courses')

// GET all courses by pagination schema
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Courses.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Create a Course
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createCourseSchema)

  if (validationResult.error === null) {
    const result = await Courses.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a course`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.getById(id)

  if (result.success) {
    ctx.state.action = `get a course ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Update course info by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateCourseSchema)

  if (validationResult.error === null) {
    const result = await Courses.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a course: ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Delete course by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.delete(id)

  if (result.success) {
    ctx.state.action = `delete a course ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// get all groups associated to a course
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Courses.getGroups(id)

  if (result.success) {
    ctx.state.action = `get courses by type ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// GET all students by Course Id
router.get('/:id/students', async (ctx, next) => {
  const id = ctx.params.id
  const skip = ctx.query.skip || 0
  const limit = ctx.query.limit || 10

  const result = await Courses.getStudents(id, skip, limit)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Update Course Livestream
router.put('/:id/updateLive', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const validationResult = Joi.validate({ id }, discountsSchema)
  if (validationResult.error === null) {
    const result = await Courses.updateLive(id, body)
    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
  ctx.body = {
    success: true,
    message: 'topic created'
  }
})


// Create topics
router.post('/:id/topics', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const validationResult = Joi.validate({ id }, discountsSchema)
  if (validationResult.error === null) {
    const result = await Courses.createTopic(id, body)
    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
  ctx.body = {
    success: true,
    message: 'topic created'
  }
})


// Get if a student have discounts by course id
router.get('/:id/discounts/:studentId', async (ctx, next) => {
  const id = ctx.params.id
  const studentId = ctx.params.studentId

  const validationResult = Joi.validate({ id, studentId }, discountsSchema)
  if (validationResult.error === null) {
    const result = await Courses.getStudentDiscounts(id, studentId)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
  ctx.body = {
    success: true,
    message: 'discounts'
  }
})


// Get Course wiith children
router.get('/with/children', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Courses.getWithChildren(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Get all courses from a specific level Id
router.get('/level/:level', async (ctx, next) => {
  const level = ctx.params.level

  const result = await Courses.getByLevel(level)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get all courses from a specific type Id
router.get('/type/:type', async (ctx, next) => {
  const type = ctx.params.type

  const result = await Courses.getByType(type)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// Get by dependency
router.get('/dependency/:dependency', async (ctx, next) => {
  const dependency = ctx.params.dependency

  const result = await Courses.getByDependency(dependency)

  if (result.success) {
    ctx.state.action = `get courses by dependency ${dependency}`
    ctx.state.data = { dependency }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// get 
router.get('/report/:id', async (ctx, next) => {
  const id = ctx.params.id
  const query = ctx.request.query || {}

  const validationResult = Joi.validate({
    id,
    startDate: query.startDate,
    endDate: query.endDate
  }, datesQuery)


  if (validationResult.error === null) {
    const result = await Courses
      .report(
        validationResult.value.id,
        validationResult.value.startDate,
        validationResult.value.endDate,
        query.wd,
        query.dd

      )

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


module.exports = router
