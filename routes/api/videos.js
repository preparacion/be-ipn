/**
 * router/api/videos.js
 *
 * @description :: Describes the videos api routes
 * @docs        :: TODO
 */
const fs = require('fs')
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Videos = require('../../models/videos')

// validation schemas
const { paginationSchema } = require('../../schemas/pagination')
const {
  videoSchema,
  videoIdSchema,
  rateVideoSchema,
  reportSchema,
  commentSchema,
  watchVideoSchema
} = require('../../schemas/video')



router.prefix('/api/videos')



// upload video
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, videoSchema)
  if (validationResult.error === null) {
    const result = await Videos.createUrl(validationResult.value)
    if (result.success) {
      ctx.state.action = `createUrlToUpload`
      ctx.state.data = {}
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
  // const body = ctx.request.body || {}
  // const validationResult = Joi.validate(body, videoSchema)
  // if (validationResult.error === null) {
  //   const video = ctx.request.files.video
  //   if (!video) {
  //     ctx.status = 400
  //     ctx.body = {
  //       success: false,
  //       error: `No video`
  //     }
  //     return false
  //   }
  //   const nameArray = video.name.split('.')
  //   const ext = nameArray[1] || null
  //   const stream = fs.readFileSync(video.path)
  //   const result = await Videos.create(validationResult.value, stream, ext)
  //   if (result.success) {
  //     ctx.state.action = `upload video`
  //     ctx.state.data = {}
  //     ctx.body = result
  //     return next()
  //   }

  //   ctx.status = result.code
  //   ctx.body = {
  //     success: false,
  //     error: result.error
  //   }
  // } else {
  //   ctx.status = 400
  //   ctx.body = {
  //     success: false,
  //     error: validationResult.error.details || 'Validation error'
  //   }
  // }

})

// get all videos
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Videos.get(validationResult.value)

    if (result.success) {
      ctx.state.action = `Get all the videos`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// Edit the video info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const validationResult = Joi.validate({ id }, videoIdSchema)
  const validationResult1 = Joi.validate({
    title: body.title,
    description: body.description,
    active: body.active,
  }, videoSchema)
  if (validationResult.error === null && validationResult1.error === null) {
    const result = await Videos.update(validationResult.value.id, validationResult1.value)
    if (result.success) {
      ctx.status = 200
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// delete video info
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, videoIdSchema)

  if (validationResult.error === null) {
    const result = await Videos.delete(validationResult.value.id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// shows only de video info
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, videoIdSchema)

  if (validationResult.error === null) {
    const result = await Videos.getById(validationResult.value.id)

    if (result.success) {
      ctx.state.action = `get a single video ${validationResult.value.id}`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// report video
router.post('/report/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body

  const validationResult = Joi.validate(_.extend({ id }, body), reportSchema)

  if (validationResult.error === null) {
    const result = await Videos.report(validationResult.value.id, validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// rate video
router.post('/:id/rate', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate({
    student: body.student,
    rate: body.rate,
    id
  }, rateVideoSchema)

  if (validationResult.error === null) {
    const result = await Videos.rate(validationResult.value)
    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }

  ctx.body = {
    success: true,
    message: 'ok',
    id,
    body
  }
})

// Comment a video
router.post('/:id/comment', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body

  const validationResult = Joi.validate(_.extend(body, { id }), commentSchema)
  if (validationResult.error === null) {
    const result = await Videos.comment(id, validationResult.value)

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// comment video in a comment respond
router.post('/comment/:commentId/respond', async (ctx, next) => {
  const commentId = ctx.params.commentId
  const body = ctx.request.body

  const validationResult = Joi.validate(_.extend(body, { id: commentId }), commentSchema)
  if (validationResult.error === null) {
    const result = await Videos.respondComment(commentId, validationResult.value)

    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



// update viewed by
router.post('/:id/viewed/by/:studentId', async (ctx, next) => {
  const id = ctx.params.id
  const studentId = ctx.params.studentId

  const validationResult = Joi.validate({ id, studentId }, watchVideoSchema)
  if (validationResult.error === null) {
    const result = await Videos.watchVideo(id, studentId)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



module.exports = router
