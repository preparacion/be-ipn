/**
 * router/api/courseTypes.js
 *
 * @description :: Describes the courseTypes api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const CourseType = require('../../models/courseTypes')
const courseType = new CourseType()

router.prefix('/api/course/types')

const { ctId, ctUpdateSchema } = require('../../schemas/courseTypes')

// GET ALL COURSES TYPES
router.get('/', async (ctx, next) => {
  const result = await courseType.get()
  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// GET COURSE TYPE BY ID
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, ctId)
  if (validationResult.error === null) {
    const result = await courseType.getById(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// UPDATE A COURSE TYPE BY ID
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}
  const data = _.extend(body, { id })
  const validationResult = Joi.validate(
    data,
    ctUpdateSchema
  )
  if (validationResult.error === null) {
    const result = await courseType.update(validationResult.value.id, data)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET COURSES BY COURSE TYPE ID
router.get('/:id/courses', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, ctId)
  if (validationResult.error === null) {
    const result = await courseType.getCoursesById(validationResult.value.id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
