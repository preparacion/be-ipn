/**
 * router/api/register.js
 *
 * @description :: Describes the register api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')

const router = require('koa-router')({ sensitive: true })

const Register = require('../../models/register')

const { registerSchema } = require('../../schemas/register')

router.prefix('/api/register')

// REGISTER STUDENT POST
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, registerSchema)

  if (validationResult.error === null) {
    if (ctx.state && ctx.state.user) {
      validationResult.value = _.extend(
        validationResult.value, { registeredBy: ctx.state.user._id }
      )
    }

    const result = await Register.register(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a new register`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return false
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


module.exports = router
