/**
 * router/api/talks.js
 *
 * @description :: Describes the talks api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('lodash')
const router = require('koa-router')({ sensitive: true })

const Talks = require('../../models/talks')

router.prefix('/api/talks')

// Talks validation Schemas
const {
  paginationSchema,
  idSchema,
  createTalkSchema,
  updateTalkSchema,
  callTalkSchema,
  statusTalkSchema,
  attendanceTalkSchema
} = require('../../schemas/talks')

/** create a talk */
router.post('/', async (ctx, next) => {
  const body = ctx.request.body
  if(!body.filter) {
    body.filter = ''
  }
  const validationResult = Joi.validate(body, createTalkSchema)
  if (validationResult.error === null) {
    const result = await Talks.create(validationResult.value)
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})





/** get all the talks */
router.get('/', async (ctx, next) => {
  const query = ctx.query
  const validationResult = Joi.validate(query, paginationSchema)
  if (validationResult.error === null) {
    const result = await Talks.get(validationResult.value)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

/** get a single talk by id */
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Talks.getById(id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get all info for sideBar 
router.get('/:id/sidebar', async (ctx, next) => {
  const id = ctx.params.id
  const result = await Talks.getSidebarInfoById(id)
  if (result.success) {
    ctx.body = result
    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

/** updates a single talk by id */
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body

  const validationResult = Joi.validate(_.extend({ id }, body), updateTalkSchema)
  if (validationResult.error === null) {
    const result = await Talks.update(id, body)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


/** deletes a single talk by id */
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Talks.delete(id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

/** toggle active status of a single talk by id */
router.put('/:id/toggle', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Talks.toggle(id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

/** get a single talk by id */
router.get('/:id/students', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Talks.getStudents(id)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// call student by its id
router.put('/call/student/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const body = ctx.request.body
  const user = ctx.state.user
  const socket = ctx.socket_io
  const validationResult = Joi.validate(_.extend({ studentId }, body), callTalkSchema)
  if (validationResult.error === null) {
    const result = await Talks.callStudent(studentId, user, socket)
    if (result.success) {
      ctx.body = result
      return
    }
    ctx.status = result.code
    ctx.body = result
    ctx.body = {
      success: true,
      message: 'get student comments'
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Update student status
router.put('/status/student/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const body = ctx.request.body
  const user = ctx.state.user
  const socket = ctx.socket_io

  const validationResult = Joi.validate(_.extend({ studentId }, body), statusTalkSchema)
  if (validationResult.error === null) {
    const result = await Talks.statusStudent(studentId, body.status, user, socket)
    if (result.success) {
      ctx.body = result
      return
    }
    ctx.status = result.code
    ctx.body = result
    ctx.body = {
      success: true,
      message: 'get student comments'
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// update attendace 
router.put('/attendance/student/:studentId', async (ctx, next) => {
  const studentId = ctx.params.studentId
  const body = ctx.request.body
  const user = ctx.state.user
  const socket = ctx.socket_io

  const validationResult = Joi.validate(_.extend({ studentId }, body), attendanceTalkSchema)
  if (validationResult.error === null) {
    const result = await Talks.attendanceStudent(studentId, body.attendance, socket)
    if (result.success) {
      ctx.body = result
      return
    }
    ctx.status = result.code
    ctx.body = result
    ctx.body = {
      success: true,
      message: 'Update userTalks attendance'
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get talk reports
router.get('/reports/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Talks.reports(id)
    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})




module.exports = router
