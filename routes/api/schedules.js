/**
 * router/api/schedules.js
 *
 * @description :: Describes the schedules api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Schedules = require('../../models/schedules')

router.prefix('/api/schedules')

// validation schemas
const {
  paginationSchema,
  idSchema,
  creationSchema,
  updateSchema,
  isAvailableSchema
} = require('../../schemas/schedules')

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, creationSchema)
  if (validationResult.error === null) {
    const result = await Schedules.create(body)
    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GETT ALL ITEMS
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, paginationSchema)

  if (validationResult.error === null) {
    const result = await Schedules.get(validationResult.value)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)

  if (validationResult.error === null) {
    const result = await Schedules.getById(id)

    if (result.success) {
      ctx.body = {
        success: true,
        location: result.schedule
      }
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error,
      errorExt: result.errorExt
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// UODATE AN ITEM
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body

  const validationResult = Joi.validate(
    _.extend({ id }, body), updateSchema
  )
  if (validationResult.error === null) {
    const result = await Schedules.update(id, body)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Delete an item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Schedules.delete(id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Check if an new schedule is available
router.post('/is/available', async (ctx, next) => {
  const body = ctx.request.body || {}
  const validationResult = Joi.validate(body, isAvailableSchema)
  if (validationResult.error === null) {
    const result = await Schedules.isAvailable(validationResult.value)
    if (result.success) {
      ctx.body = result
      return false
    }
    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
