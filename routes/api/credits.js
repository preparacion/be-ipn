/**
 * router/api/credits.js
 *
 * @description :: Describes the credits api routes
 * @docs        :: http://app_host:app_port/docs#tag/credits
 */
const Joi = require('joi')

const router = require('koa-router')({ sensitive: true })

// validation schemas
const {
  creditSchema
} = require('../../schemas/credits')

const Credits = require('../../models/credits')

router.prefix('/api/credits')

// Get all items with credit
router.get('/', async (ctx, next) => {
  const result = await Credits.get()

  if (result.success) {
    ctx.state.action = `Get all the credits`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      credits: result.credits
    }
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})


// CREATE A CREDIT ITEM
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, creditSchema)

  if (validationResult.error === null) {
    const result = await Credits.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `create a student`
      ctx.state.data = {}

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Credits.getById(id)

  if (result.success) {
    ctx.state.action = `get a credit - catalog ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      credit: result.credit
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// GET CREDITS BY STUDENT ID
router.get('/student/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Credits.getByStudentId(id)

  if (result.success) {
    ctx.state.action = `get credits by student id - ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
