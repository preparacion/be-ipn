/**
 * router/api/paymentsProcessor.js
 *
 * @description :: Describes the credit card payment integration
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')
const router = require('koa-router')({ sensitive: true })

const Payments = require('../../models/payments')


router.prefix('/api/paymentsProcessor')


router.post('/', (ctx, next) => {
    if (ctx.state && ctx.state.user) {

    } else {
        ctx.status = 401
        ctx.body = {
            success: false,
            error: 'No authenticated',
            message: 'No authenticated'
        }
    }
})

module.exports = router