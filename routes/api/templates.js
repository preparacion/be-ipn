/**
 * router/api/templates.js
 *
 * @description :: Describes the templates api routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const Template = require('../../models/templates')
const template = new Template()

router.prefix('/api/templates')

// GET TEMPLATES VERSION
router.get('/:id/versions/:idVersion', async (ctx, next) => {
  const id = ctx.params.id
  const idVersion = ctx.params.idVersion

  const result = await template.getVersions(id, idVersion)

  if (result.success) {
    ctx.body = result
    return
  }

  ctx.status = result.code
  ctx.body = result
})

module.exports = router
