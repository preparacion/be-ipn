/**
 * router/api/students.js
 *
 * @description :: Describes the students api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const _ = require('underscore')

const router = require('koa-router')({ sensitive: true })

const Students = require('../../models/students')

router.prefix('/api/students')

// Validation Schemas

const {
  studentSchema,
  updateStudentSchema,
  studentIdSchema,
  deleteGroupSchema,
  studentFilterSchema,
  idSchema,
  notificationsFilter,
  registerStudentSchema
} = require('../../schemas/student')





// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, studentSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }

    if (ctx.state && ctx.state.user) {
      validationResult.value = _.extend(
        validationResult.value, { registeredBy: ctx.state.user._id }
      )
    }

    const result = await Students.create(validationResult.value)

    if (result.success) {
      ctx.state.action = `Create a student`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get all items
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, studentFilterSchema)

  if (validationResult.error === null) {
    const result = await Students.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.getById(id)

  if (result.success) {
    ctx.state.action = `Get student by id ${id}`
    ctx.state.data = { id }

    ctx.body = {
      success: true,
      student: result.student
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// update an student info
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateStudentSchema)

  if (validationResult.error === null) {
    // fixing dynamo date type
    if (validationResult.value.birthDate) {
      validationResult.value.birthDate = new Date(validationResult.value.birthDate)
        .toISOString()
    }

    const result = await Students.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `Update an student ${id}`
      ctx.state.data = validationResult.value

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// delete a single item by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.delete(id)

  if (result.success) {
    ctx.state.action = `Delete an student ${id}`

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create a student
router.post('/register', async (ctx, next) => {
  const body = ctx.request.body || {}

  // logged user
  const user = ctx.state.user

  const validationResult = Joi.validate(body, registerStudentSchema)

  if (validationResult.error === null) {
    if (ctx.state && ctx.state.user) {
      validationResult.value = _.extend(
        validationResult.value, { registeredBy: ctx.state.user._id }
      )
    }

    const result = await Students.register(validationResult.value, user)
    if (result.success) {
      ctx.state.action = `register a student`
      ctx.state.data = validationResult.value

      ctx.status = 201
      ctx.body = result

      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


//   ******* getGroups is not a FUNCTION  ******
// Get the student's groups
router.get('/:id/groups', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Students.getGroups(id)

  if (result.success) {
    ctx.state.action = `Get the student's groups ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Change student from group
router.put('/:id/groups/:groupId', async (ctx, next) => {
  const id = ctx.params.id
  const groupId = ctx.params.groupId
  const body = ctx.request.body || {}
  const { force } = ctx.query

  const result = await Students.changeGroups(id, groupId, body, force)
  if (result.success) {
    ctx.state.action = `Change a student from group ${id}, ${groupId}`
    ctx.state.data = { id, body, groupId }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error,
    errorExt: result.errorExt
  }
})

// Remove group from student
router.delete('/:id/group/:groupId', async (ctx, next) => {
  const id = ctx.params.id
  const groupId = ctx.params.groupId

  const validationResult = Joi.validate(
    { id, groupId },
    deleteGroupSchema
  )

  if (validationResult.error === null) {
    const result = await Students.removeGroup(id, groupId)

    if (result.success) {
      ctx.state.action = `remove group from student`
      ctx.state.data = { id, groupId }

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Find student by email
router.get('/email/:email', async (ctx, next) => {
  const email = ctx.params.email

  const result = await Students.findByEmail(email)

  if (result.success) {
    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error,
    errorExt: result.errorExt
  }
})



router.get('sidebarIndo/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, studentIdSchema)
  if (validationResult.error === null) {
    const result = await Students.getSidebarInfo(validationResult.value.id)
    if (result.success) {
      ctx.state.action = `get student info${validationResult.value.id}`
      ctx.state.data = {}
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get student information
router.get('/:id/info', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, studentIdSchema)

  if (validationResult.error === null) {
    const result = await Students.getInfo(validationResult.value.id)

    if (result.success) {
      ctx.state.action = `get student info${validationResult.value.id}`
      ctx.state.data = {}

      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Student recover password
router.put('/:id/pass/recover', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Students.passRecover(id)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Student update password
router.put('/:id/pass/update', async (ctx, next) => {
  const id = ctx.params.id
  const {
    currentPass,
    newPass
  } = ctx.request.body

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Students.passUpdate(id, currentPass, newPass)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Refresh student balance
router.put('/:id/balance/refresh', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Students.refreshBalance(id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// student notification filter
router.post('/notifications/filter', async (ctx, next) => {
  const body = ctx.request.body
  const validationResult = Joi.validate(body, notificationsFilter)
  if (validationResult.error === null) {
    const result = await Students.notificationFilter(validationResult.value)

    if (result.success) {
      ctx.body = result
      return
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// get student debt
router.get('/:id/debt', async (ctx, next) => {
  const id = ctx.params.id

  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Students.debt(id)
    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Search student
router.get('/search/term', async (ctx, next) => {
  const q = ctx.query.q
  const limit = ctx.query.limit
  const skip = ctx.query.skip

  const result = await Students.find(q, skip, limit)

  if (result.success) {
    ctx.state.action = `Search student ${q}`
    ctx.state.data = { q }

    ctx.body = result
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

router.put('/:id/folio/:idFolio', async (ctx, next) => {
  const id = ctx.params.id
  const idFolio = ctx.params.idFolio
  const body = ctx.request.body
  const validationResult = Joi.validate({ id }, studentIdSchema)
  if (validationResult.error === null) {
    const result = await Students.updateFolio(validationResult.value.id, body, idFolio);
    if (result.success) {
      ctx.body = result
      return
    }
  }
})
router.delete('/:id/folio/:idFolio', async (ctx, next) => {
  const id = ctx.params.id
  const idFolio = ctx.params.idFolio
  const validationResult = Joi.validate({ id }, studentIdSchema)
  if (validationResult.error === null) {
    const result = await Students.removeFolio(validationResult.value.id, idFolio);
    if (result.success) {
      ctx.body = result
      return
    }
  }
})

router.post('/:id/folio', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body
  const validationResult = Joi.validate({ id }, studentIdSchema)
  if (validationResult.error === null) {
    const result = await Students.addFolio(validationResult.value.id, body);
    if (result.success) {
      ctx.body = result
      return
    }
  }
})


module.exports = router
