/**
 * router/api/available.js
 *
 * @description :: Describes the available api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Available = require('../../models/available')

const { filterSchema } = require('../../schemas/available')

router.prefix('/api/available')

// GET  available
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}

  const validationResult = Joi.validate(query, filterSchema)

  if (validationResult.error === null) {
    const result = await Available.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// GET available courses
router.get('/courses', async (ctx, next) => {
  const result = await Available.getByCourses()

  if (result.success) {
    ctx.state.action = `get availability by courses`
    ctx.state.data = {}

    ctx.body = result

    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// GET availability by course identifier
router.get('/courses/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Available.getByCourseId(id)

  if (result.success) {
    ctx.state.action = `get availability by course ${id}`
    ctx.state.data = { id }

    ctx.body = result

    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// GET availability by location
router.get('/location', async (ctx, next) => {
  const result = await Available.getByLocations()

  if (result.success) {
    ctx.state.action = `get availability by locations `
    ctx.state.data = {}

    ctx.body = result

    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// GET availability by location identifier
router.get('/location/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Available.getByLocationId(id)

  if (result.success) {
    ctx.state.action = `get availability by location ${id}`
    ctx.state.data = { id }

    ctx.body = result

    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})


// GET availability by schedules
// No existe la funcion getBySchedules
router.get('/schedules', async (ctx, next) => {
  const result = await Available.getBySchedules()

  if (result.success) {
    ctx.state.action = `get availability by schedules`
    ctx.state.data = {}

    ctx.body = result

    return next()
  }
  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router