/**
 * router/api/reports.js
 *
 * @description :: Describes the reports api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Reports = require('../../models/reports')

router.prefix('/api/reports')

// validation schemas
const {
  idSchema,
  createReportSchema,
  filterSchema
} = require('../../schemas/reports')

// GET REPORTS
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}
  const validationResult = Joi.validate(query, filterSchema)
  if (validationResult.error === null) {
    const result = await Reports.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// CREATE A REPORT
router.post('/', async (ctx, next) => {
  const body = ctx.request.body
  const validationResult = Joi.validate(body, createReportSchema)
  if (validationResult.error === null) {
    const result = await Reports.create(validationResult.value)
    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return
    }
    ctx.status = result.error.httpCode
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET REPORT BY ID
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  let user = null
  if (ctx.state && ctx.state.user) {
    user = ctx.state.user._id
  }

  if (validationResult.error === null) {
    const result = await Reports.getById(validationResult.value.id, user)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// UPDATE A REPORT
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const validationResult = Joi.validate({ id }, idSchema)
  if (validationResult.error === null) {
    const result = await Reports.update(validationResult.value.id)

    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// GET REPORT BY COURSE
router.get('/course/:courseId', async (ctx, next) => {
  const courseId = ctx.params.courseId
  const validationResult = Joi.validate({ id: courseId }, idSchema)
  if (validationResult.error === null) {
    const result = await Reports.getByCourse(validationResult.value)
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


// CREATE A REPORT COMMENT
router.post('/comment/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body
  const validationResult = Joi.validate(body, createReportSchema)
  if (validationResult.error === null) {
    const result = await Reports.comment(id, body)
    if (result.success) {
      ctx.status = 201
      ctx.body = result
      return
    }
    ctx.status = result.code
    ctx.body = result
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})


module.exports = router
