/**
 * router/api/videoComments.js
 *
 * @description :: Describes the Video Comments made byh students api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const VideoComments = require('../../models/videoComments')

router.prefix('/api/videoComments')

// validation schemas
const {
  idSchema,
  getVideoComments
} = require('../../schemas/videoComments')


// GET STUDENT COMMENTS
// Get all items
router.get('/', async (ctx, next) => {
  const query = ctx.query || {}
  const validationResult = Joi.validate(query, getVideoComments)

  if (validationResult.error === null) {
    const result = await VideoComments.get(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get by video ID
// Get all items
router.get('/comments/:videoId', async (ctx, next) => {
  console.log("Hola")
  const query = ctx.query.videoId

  const validationResult = Joi.validate({id: query}, idSchema)

  if (validationResult.error === null) {
    console.log(validationResult.value)
    const result = await VideoComments.getByVideo(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})



//Delete videoComment by id
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await VideoComments.delete(id)

  if (result.success) {
    ctx.state.action = `delete a videocomment ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
