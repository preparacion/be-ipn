/**
 * router/api/search.js
 *
 * @description :: Describes the search api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Search = require('../../models/search')
const SearchLucene = require('../../models/search-lucene')

router.prefix('/api/search')

// validation schemas
const {
  searchFilter
} = require('../../schemas/search')

// GET ALL SEARCH RESULT
router.get('/', async (ctx, next) => {
  const query = ctx.query

  const validationResult = Joi.validate(query, searchFilter)
  if (validationResult.error === null) {
    // const result = await Search.search(validationResult.value)
    const result = await SearchLucene.search(validationResult.value)

    if (result.success) {
      ctx.body = result
      return next()
    }
    ctx.status = result.code
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

module.exports = router
