/**
 * router/api/locations.js
 *
 * @description :: Describes the locations api routes
 * @docs        :: TODO
 */
const Joi = require('joi')
const router = require('koa-router')({ sensitive: true })

const Locations = require('../../models/locations')
router.prefix('/api/locations')

// validation schemas
const {
  createLocationSchema,
  updateLocationSchema
} = require('../../schemas/locations')


// Get all locations
router.get('/', async (ctx, next) => {
  console.log(ctx.state.roles)
  const result = await Locations.get()
  if (result.success) {
    ctx.state.action = `get all locations`
    ctx.state.data = {}

    ctx.body = {
      success: true,
      locations: result.locations
    }
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Create an item
router.post('/', async (ctx, next) => {
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, createLocationSchema)

  if (validationResult.error === null) {
    const result = await Locations.create(body)

    if (result.success) {
      ctx.state.action = `create a location`
      ctx.state.data = body

      ctx.status = 201
      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Get a single item by id
router.get('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Locations.getById(id)

  if (result.success) {
    ctx.body = {
      success: true,
      location: result.location
    }
    return next()
  }

  ctx.status = result.code
  ctx.body = {
    success: false,
    error: result.error
  }
})

// Updates a single item by id
router.put('/:id', async (ctx, next) => {
  const id = ctx.params.id
  const body = ctx.request.body || {}

  const validationResult = Joi.validate(body, updateLocationSchema)

  if (validationResult.error === null) {
    const result = await Locations.update(id, validationResult.value)

    if (result.success) {
      ctx.state.action = `update a location ${id}`
      ctx.state.data = { id }

      ctx.body = result
      return next()
    }

    ctx.status = result.error.httpCode
    ctx.body = {
      success: false,
      error: result.error
    }
  } else {
    ctx.status = 400
    ctx.body = {
      success: false,
      error: validationResult.error.details || 'Validation error'
    }
  }
})

// Delete a single item
router.delete('/:id', async (ctx, next) => {
  const id = ctx.params.id

  const result = await Locations.delete(id)

  if (result.success) {
    ctx.state.action = `delete a location ${id}`
    ctx.state.data = { id }

    ctx.body = result
    return next()
  }

  ctx.status = result.error.httpCode
  ctx.body = {
    success: false,
    error: result.error
  }
})

module.exports = router
