/**
 * router/api/playlists.js
 *
 * @description :: Describes the playlists api routes
 * @docs        :: TODO
 */
 const Joi = require('joi')
 const router = require('koa-router')({ sensitive: true })
 
 const Playlist = require('../../models/playlists')
 router.prefix('/api/playlists')
 
 // Validation Schemas
 const { playlistSchema, playlistIdSchema, updatePlaylistSchema } = require('../../schemas/playlists')
 const { paginationSchema } = require('../../schemas/pagination')
 
 // GET ALL PLAYLISTS
 router.get('/', async (ctx, next) => {
   const query = ctx.query || {}
   const validationResult = Joi.validate(query, paginationSchema)
 
   if (validationResult.error === null) {
     const result = await Playlist.get(validationResult.value)
 
     if (result.success) {
       ctx.state.action = `Get all the playlists`
       ctx.state.data = {}
 
       ctx.body = result
       return next()
     }
 
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // CREATE A PLAYLIST
 router.post('/', async (ctx, next) => {
   const body = ctx.request.body || {}
 
   const validationResult = Joi.validate(body, playlistSchema)
 
   if (validationResult.error === null) {
     const result = await Playlist.create(validationResult.value)
 
     if (result.success) {
       ctx.state.action = `Create a playlist`
       ctx.state.data = validationResult.value
 
       ctx.status = 201
       ctx.body = result
       return next()
     }
 
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // GET A SINGLE PLAYLIST
 router.get('/:id', async (ctx, next) => {
   const id = ctx.params.id
   const validationResult = Joi.validate({ id }, playlistIdSchema)
 
   if (validationResult.error === null) {
     const result = await Playlist.getById(validationResult.value.id)
 
     if (result.success) {
       ctx.state.action = `get a playlist ${validationResult.value.id}`
       ctx.state.data = {}
 
       ctx.body = result
       return next()
     }
 
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // UPDATE A PLAYLIST
 router.put('/:id', async (ctx, next) => {
   const id = ctx.params.id
   const body = ctx.request.body || {}
   const validationResult = Joi.validate({ id }, playlistIdSchema)
   const validationResult1 = Joi.validate(body, playlistSchema)
   if (validationResult.error === null && validationResult1.error === null) {
     const result = await Playlist.update(validationResult.value.id, validationResult1.value)
     if (result && result.success) {
       ctx.state.action = `Update a playlist`
       ctx.state.data = validationResult.value
       ctx.status = 200
       ctx.body = result
       return next()
     }
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error || 'Validation error'
     }
   }
 })
 
 // DELETE A PLAYLIST
 router.delete('/:id', async (ctx, next) => {
   const id = ctx.params.id
   const validationResult = Joi.validate({ id }, playlistIdSchema)
 
   if (validationResult.error === null) {
     const result = await Playlist.delete(validationResult.value.id)
 
     if (result.success) {
       ctx.body = result
       return next()
     }
 
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // GET PLAYLIST FROM A COURSE
 router.get('/course/:id', async (ctx, next) => {
   const id = ctx.params.id
   const validationResult = Joi.validate({ id }, playlistIdSchema)
 
   if (validationResult.error === null) {
     const result = await Playlist.getByCourseId(validationResult.value.id)
 
     if (result.success) {
       ctx.body = result
       return next()
     }
 
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // UPDATE PLAYLIST ORDER
 router.put('/updateOrder/:id', async (ctx, next) => {
   const id = ctx.params.id
   const body = ctx.request.body || {}
   const validationResult = Joi.validate({ id }, playlistIdSchema)
   const validationResult1 = Joi.validate(body, updatePlaylistSchema)
   if (validationResult.error === null) {
     const result = await Playlist.updateOrder(validationResult.value.id, validationResult1.value)
     if (result.success) {
       ctx.body = result
       return next()
     }
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 //UPDATE ORDER IN COURSE 
 router.put('/updateOrder/course/:id', async (ctx, next) => {
   const id = ctx.params.id
   const body = ctx.request.body || {}
   const validationResult = Joi.validate({ id }, playlistIdSchema)
   const validationResult1 = Joi.validate(body, updatePlaylistSchema)
   if (validationResult.error === null) {
     const result = await Playlist.updateOrderCourse(validationResult.value.id, validationResult1.value)
     if (result.success) {
       ctx.body = result
       return next()
     }
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 // CHANGE MATERIAL ROUTE
 router.put('/changeMaterial/:idOrigin/:idDestination', async (ctx, next) => {
   const originId = ctx.params.idOrigin
   const destinationId = ctx.params.idDestination
   const body = ctx.request.body || {}
   const validationResult0 = Joi.validate({ id:originId }, playlistIdSchema)
   const validationResult = Joi.validate({ id:destinationId }, playlistIdSchema)
   if (validationResult.error === null) {
     const result = await Playlist.changeMaterialList(validationResult0.value.id, validationResult.value.id, body)
     if (result.success) {
       ctx.body = result
       return next()
     }
     ctx.status = result.code
     ctx.body = {
       success: false,
       error: result.error
     }
   } else {
     ctx.status = 400
     ctx.body = {
       success: false,
       error: validationResult.error.details || 'Validation error'
     }
   }
 })
 
 module.exports = router
 
 