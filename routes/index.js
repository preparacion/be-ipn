/**
 * routes/index.js
 *
 * @description :: Defines routes
 * @docs        :: TODO
 */
const router = require('koa-router')({ sensitive: true })

const isAuth = require('../middleware/isAuth')
const whoami = require('../middleware/whoami')
const hasPermission = require('../middleware/hasPermissions')

// Home routes
const homeApi = require('./api/home')
router.use('', homeApi.routes(), homeApi.allowedMethods())
// Docs routes
const docs = require('./api/docs')
router.use('', docs.routes(), docs.allowedMethods())
// Login route
const loginApi = require('./api/login')
router.use('', loginApi.routes(), loginApi.allowedMethods())
// Register route
const registerApi = require('./api/register')
router.use('', registerApi.routes(), registerApi.allowedMethods())

// St/ Pass recover
const stPassRecoverApi = require('./api/st/pass')
router.use('', stPassRecoverApi.routes(), stPassRecoverApi.allowedMethods())
// St/ Login route
const stLoginApi = require('./api/st/login')
router.use('', stLoginApi.routes(), stLoginApi.allowedMethods())


// List Groups (Aug, 2019)
const unpCourseApi = require('./api/unprotected/courses')
router.use('', unpCourseApi.routes(), unpCourseApi.allowedMethods())
// List Groups (Aug, 2019)
const unpGroupsApi = require('./api/unprotected/groups')
router.use('', unpGroupsApi.routes(), unpGroupsApi.allowedMethods())
// List Talks (Aug, 2019)
const talksApi = require('./api/unprotected/talks')
router.use('', talksApi.routes(), talksApi.allowedMethods())
// Users
const unpUsersApi = require('./api/unprotected/users')
router.use('', unpUsersApi.routes(), unpUsersApi.allowedMethods())
// FREE TEST
const unpFreeTestApi = require('./api/unprotected/freeTest')
router.use('', unpFreeTestApi.routes(), unpFreeTestApi.allowedMethods())
// PRESENTIAL EXAMS ROUTE
const unpExamsApi = require('./api/unprotected/exams')
router.use('', unpExamsApi.routes(), unpExamsApi.allowedMethods())
// FREE LESSON
const unpFreeLessonApi = require('./api/unprotected/freeLesson')
router.use('', unpFreeLessonApi.routes(), unpFreeLessonApi.allowedMethods())
// Webhooks
const unpWebhookApi = require('./api/unprotected/webhook')
router.use('', unpWebhookApi.routes(), unpWebhookApi.allowedMethods())

// ---- Protected routes ----
//PDFs
const pdfsAPI = require('./api/st/pdfs')
router.use(isAuth, pdfsAPI.routes(), pdfsAPI.allowedMethods())
// Credits routes
const creditsApi = require('./api/credits')
router.use(isAuth, creditsApi.routes(), creditsApi.allowedMethods())
router.use(whoami(hasPermission))
// PaymentInfo routes
const paymentInfoApi = require('./api/paymentInfo')
router.use('', paymentInfoApi.routes(), paymentInfoApi.allowedMethods())
// Payments routes
const paymentsApi = require('./api/payments')
router.use('', paymentsApi.routes(), paymentsApi.allowedMethods())
// Classrooms routes
const classRoomsApi = require('./api/classRooms')
router.use('', classRoomsApi.routes(), classRoomsApi.allowedMethods())
// Folio routes
const FolioApi = require('./api/folio')
router.use('', FolioApi.routes(), FolioApi.allowedMethods())
// Courses routes
const coursesApi = require('./api/courses')
router.use('', coursesApi.routes(), coursesApi.allowedMethods())
// Groups routes
const groupsApi = require('./api/groups')
router.use('', groupsApi.routes(), groupsApi.allowedMethods())
// Locations routes
const locationsApi = require('./api/locations')
router.use('', locationsApi.routes(), locationsApi.allowedMethods())
// Schedules routes
const schedulesApi = require('./api/schedules')
router.use('', schedulesApi.routes(), schedulesApi.allowedMethods())
// StudentList routes
const studentListsApi = require('./api/studentLists')
router.use('', studentListsApi.routes(), studentListsApi.allowedMethods())
// Logs routes
const logsApi = require('./api/logs')
router.use('', logsApi.routes(), logsApi.allowedMethods())
// me route
const meApi = require('./api/me')
router.use('', meApi.routes(), meApi.allowedMethods())
// Roles routes
const rolesApi = require('./api/roles')
router.use('', rolesApi.routes(), rolesApi.allowedMethods())
// Students routes
const studentsApi = require('./api/students')
router.use('', studentsApi.routes(), studentsApi.allowedMethods())
// Users routes
const usersApi = require('./api/users')
router.use('', usersApi.routes(), usersApi.allowedMethods())
// UsersTalks routes
const usersTalksApi = require('./api/usersTalks')
router.use('', usersTalksApi.routes(), usersTalksApi.allowedMethods())
// Available routes
const availableApi = require('./api/available')
router.use('', availableApi.routes(), availableApi.allowedMethods())
// Video routes
const videoApi = require('./api/videos')
router.use('', videoApi.routes(), videoApi.allowedMethods())
// Images routes
const imageApi = require('./api/images')
router.use('', imageApi.routes(), imageApi.allowedMethods())
// Quiz routes
const quizApi = require('./api/quiz')
router.use('', quizApi.routes(), quizApi.allowedMethods())
// Video routes
const pdfsApi = require('./api/pdfs')
router.use('', pdfsApi.routes(), pdfsApi.allowedMethods())

// Playlist routes
const playlistApi = require('./api/playlist')
router.use('', playlistApi.routes(), playlistApi.allowedMethods())
// Balance
const balanceApi = require('./api/balance')
router.use('', balanceApi.routes(), balanceApi.allowedMethods())
// Attendance
const attendanceApi = require('./api/attendance')
router.use('', attendanceApi.routes(), attendanceApi.allowedMethods())
// Search
const searchApi = require('./api/search')
router.use('', searchApi.routes(), searchApi.allowedMethods())
// Comments
const commentsApi = require('./api/comments')
router.use('', commentsApi.routes(), commentsApi.allowedMethods())
// Calendar
const calendarApi = require('./api/calendar')
router.use('', calendarApi.routes(), calendarApi.allowedMethods())
// Counter
const counterApi = require('./api/count')
router.use('', counterApi.routes(), counterApi.allowedMethods())
// SMS
const smsApi = require('./api/sms')
router.use('', smsApi.routes(), smsApi.allowedMethods())
// Notifications
const notificationApi = require('./api/notifications')
router.use('', notificationApi.routes(), notificationApi.allowedMethods())
// CourseLevels
const clApi = require('./api/courseLevels')
router.use('', clApi.routes(), clApi.allowedMethods())
// CourseTypes
const ctApi = require('./api/courseTypes')
router.use('', ctApi.routes(), ctApi.allowedMethods())
// Stages
const stagesApi = require('./api/stages')
router.use('', stagesApi.routes(), stagesApi.allowedMethods())
// Templates
const templatesApi = require('./api/templates')
router.use('', templatesApi.routes(), templatesApi.allowedMethods())
// Errors
const errorApi = require('./api/errors')
router.use('', errorApi.routes(), errorApi.allowedMethods())
// Reports
const reportsApi = require('./api/reports')
router.use('', reportsApi.routes(), reportsApi.allowedMethods())
// Materials
const materialsApi = require('./api/materials')
router.use('', materialsApi.routes(), materialsApi.allowedMethods())
// Talks
const talksRoutesApi = require('./api/talks')
router.use('', talksRoutesApi.routes(), talksRoutesApi.allowedMethods())
// Student Comments
const stCommentsApi = require('./api/studentComments')
router.use('', stCommentsApi.routes(), stCommentsApi.allowedMethods())
// Appoiments routes
const appoimentsApi = require('./api/appoiments')
router.use('', appoimentsApi.routes(), appoimentsApi.allowedMethods())
// LiveChannels routes
const liveChannelsApi = require('./api/livechannels')
router.use('', liveChannelsApi.routes(), liveChannelsApi.allowedMethods())
// VideoComments routes
const videoCommentsApi = require('./api/videoComments')
router.use('', videoCommentsApi.routes(), videoCommentsApi.allowedMethods())

// St/
const stApi = require('./api/st/index')
router.use('', stApi.routes(), stApi.allowedMethods())
// // St/ me
// const stMeApi = require('./api/st/me')
// router.use('', stMeApi.routes(), stMeApi.allowedMethods())
// St/ list
const stListApi = require('./api/st/list')
router.use('', stListApi.routes(), stListApi.allowedMethods())
// Quiz ST routes
const quizStApi = require('./api/st/quiz')
router.use('', quizStApi.routes(), quizStApi.allowedMethods())
// QuizAttempt routes
const quizAttemptApi = require('./api/st/quizAttempt')
router.use('', quizAttemptApi.routes(), quizAttemptApi.allowedMethods())
// Course routes
const courseStudent = require('./api/st/course')
router.use('', courseStudent.routes(), courseStudent.allowedMethods())
// Playlist routes
const playlistStudent = require('./api/st/playlist')
router.use('', playlistStudent.routes(), playlistStudent.allowedMethods())
// Videos routes
const videosStudent = require('./api/st/videos')
router.use('', videosStudent.routes(), videosStudent.allowedMethods())
// Appoiments routes Student 
const appoimentsStApi = require('./api/st/appoiments')
router.use('', appoimentsStApi.routes(), appoimentsStApi.allowedMethods())
//PAYMENTS 
const paymentsStApi = require('./api/st/payments')
router.use('', paymentsStApi.routes(), paymentsStApi.allowedMethods())



module.exports = router
