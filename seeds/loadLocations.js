/**
 * loadLocations.js
 */
const mongoose = require('mongoose')
const fs = require('fs')
const util = require('util')

const Locations = require('../models/locations')

const readFile = util.promisify(fs.readFile)

const file = './old_data/sedes.csv'
const dataFile = fs.createWriteStream('./data/sedes.csv', { autoClose: true })

const getLocationsLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const location = line.split(',')
  let locationData = {
    _id: new mongoose.Types.ObjectId(),
    oldId: location[0],
    name: location[1],
    street: location[2],
    number: location[3],
    colonia: location[4],
    postalcode: location[5],
    district: location[6],
    city: location[7],
    opening: location[8].replace(':', ''),
    closing: location[9].replace(':', ''),
    latitude: location[10],
    longitude: location[11]
  }

  for (const i in locationData) {
    if (locationData[i] === '') {
      delete locationData[i]
    }
  }

  return locationData
}

const saveLocation = async function (line) {
  const locationData = lineToJson(line)
  const result = await Locations.create(locationData)
  const id = result.location._id

  dataFile.write(id + ',' + line + '\n')
}

const init = async function () {
  const data = await getLocationsLines(file)
  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveLocation(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
