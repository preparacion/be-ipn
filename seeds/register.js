/**
 * seeds/regsiter.js
 *
 * @description :: Creates example node seeds/registerd.js
 * @docs        :: TODO
 */

const Users = require('../models/users')
const Roles = require('../models/roles')

const crateUsersAdmin = async function () {
  const result = await Roles.findByName('SuperAdmin')
  const role = result.role

  const userData = {
    name: 'admin1',
    lastmane: 'admin1',
    email: 'admin1@gmail.com',
    password: '123456',
    roleId: role._id
  }

  await Users.create(userData)
}

const crateUsersAdmin2 = async function () {
  const result = await Roles.findByName('SuperAdmin')
  const role = result.role

  const userData = {
    name: 'admin2',
    lastmane: 'admin2',
    email: 'admin2@gmail.com',
    password: '123456',
    roleId: role._id
  }

  await Users.create(userData)
}

const crateUsersAdmin3 = async function () {
  const result = await Roles.findByName('SuperAdmin')
  const role = result.role

  const userData = {
    name: 'admin3',
    lastmane: 'admin3',
    email: 'admin3@gmail.com',
    password: '123456',
    roleId: role._id
  }
  await Users.create(userData)
}

const crateUsersAdmin4 = async function () {
  const result = await Roles.findByName('SuperAdmin')
  const role = result.role

  const userData = {
    name: 'admin4',
    lastmane: 'admin4',
    email: 'admin4@gmail.com',
    password: '123456',
    roleId: role._id
  }

  await Users.create(userData)
}

const createSeeds = async function () {
  await crateUsersAdmin()
  await crateUsersAdmin2()
  await crateUsersAdmin3()
  await crateUsersAdmin4()
  // const users = await Users.get()
  // console.log(users)
}

Promise.resolve(createSeeds())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
