/**
 * loadClassRooms.js
 */
const mongoose = require('mongoose')
const fs = require('fs')
const util = require('util')
const _ = require('underscore')

const LocationModel = require('../db/locations')
const ClassRoomModel = require('../db/classRooms')

const ClassRooms = require('../models/classRooms')

const readFile = util.promisify(fs.readFile)

const file = './old_data/salones.csv'
const dataFile = fs.createWriteStream('./data/salones.csv', { autoClose: true })

const getClassroomsLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const classRoom = line.split(',')
  const classRoomData = {
    oldId: classRoom[0],
    name: classRoom[1],
    capacity: classRoom[2],
    oldLocationId: classRoom[3]
  }

  for (const i in classRoomData) {
    if (classRoomData[i] === '') {
      delete classRoomData[i]
    }
  }

  return classRoomData
}

const saveClassRoom = async function (line) {
  let classRoomData = lineToJson(line)

  const location = await LocationModel.findOne({ oldId: classRoomData.oldLocationId })
  
  const id = new mongoose.Types.ObjectId()
  classRoomData = _.extend(classRoomData, { location: location._id, _id: id })

  const classRoom = new ClassRoomModel(classRoomData)
  await classRoom.save()

  dataFile.write(id + ',' +
    classRoomData.oldId + ',' +
    classRoomData.name + ',' +
    classRoomData.capacity + ',' +
    location._id + ',' +
    '\n')
}

const init = async function () {
  const data = await getClassroomsLines(file)

  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveClassRoom(line)))
    .then(() => {
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
