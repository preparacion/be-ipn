/**
 * loadLocations.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')

const Users = require('../db/users')
const Roles = require('../db/roles')

const readFile = util.promisify(fs.readFile)

const file = './old_data/admin.csv'
const dataFile = fs.createWriteStream('./data/admin.csv', { autoClose: true })

const getAdminLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const user = line.split(',')
  let userData = {
    _id: new mongoose.Types.ObjectId(),
    oldId: user[0],
    name: user[1],
    lastName: user[2],
    email: user[3],
    password: user[4],
    roleName: user[5]
  }

  for (const i in userData) {
    if (userData[i] === '') {
      delete userData[i]
    }
  }

  return userData
}

const saveUser = async line => {
  let userData = lineToJson(line)

  const role = await Roles.findOne({
    name: userData.roleName
  })
    .then(role => role)

  if (!role) {
    console.log(userData)
    process.exit(1)
  }

  userData = _.extend(userData, { role: role._id })
  delete userData['roleName']

  if (userData.password) {
    userData.hashed_password = await bcrypt.hash(userData.password, 10)
    delete userData['password']
  }

  const user = new Users(userData)
  await user.save()

  dataFile.write(userData._id + ',' + line + '\n')
}

const init = async function () {
  const data = await getAdminLines(file)

  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveUser(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
