/**
 * loadSt.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const Promise = require('bluebird')

const UserModel = require('../db/users')
const GroupModel = require('../db/groups')
const StudentListModel = require('../db/studentLists')

const Students = require('../models/students')
const StudentList = require('../models/studentLists')

const readFile = util.promisify(fs.readFile)

const file = './old_data/dataStudent.csv'
const dataFile = fs.createWriteStream('./data/dataStudent.csv', { autoClose: true })

let specialGroups = {
  '1001': '',
  '2002': '',
  '2003': '',
  '3003': ''
}

const lineToJson = function (line) {
  const student = line.split(',')
  const paymentType = (student[11] && student[11].toLowerCase() === 'efectivo') ? 'cash' : 'card'
  const dateArray = student[13].substring(0, 10).split('/')
  const date = new Date(dateArray[2] + '-' + dateArray[0] + '-' + dateArray[1])
  const studentData = {
    student: {
      lastName: student[0],
      secondLastName: student[1],
      name: student[2],
      email: student[3],
      phoneNumber: student[4],
      secondPhoneNumber: student[5],
      registerDate: date,
      userOldId: student[12]
    },
    payment: {
      amount: student[10],
      discount: student[9],
      paymentType
    },
    group: {
      groupOldId: student[8]
    }
  }

  for (const i in studentData) {
    if (studentData[i] === '' || studentData[i] === 0 || studentData[i] === '0') {
      delete studentData[i]
    }
  }

  return studentData
}

const studentLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

// THIS CODE CAN BE IMPROVED
const saveStudent = async function (line) {
  const data = lineToJson(line)

  const group = await GroupModel.findOne({ oldId: data.group.groupOldId })

  if (!group) {
    console.log(data)
  }
  const groupId = group._id

  const studentList = await StudentListModel.findOne({ _id: group.studentList })

  if (data.student.userOldId && (data.student.userOldId === 'Trajeta' || data.student.userOldId === 'Efectivo' || data.student.userOldId === 'Tarjeta' || data.student.userOldId === 'Terminal')) {
    console.log(data.student)
    console.log(data.student.userOldId)
    // const user = await UserModel.findOne({ oldId: data.student.userOldId })
    // const userId = user._id
  }

  // console.log(data, group, user, studentList.list)
}

const init = async function () {
  const data = await studentLines(file)

  const lines = data.lines.map(line => saveStudent(line))
  await Promise.mapSeries(lines, line => { return line })


//   const groupsResult = await Groups.get()

//   const allGroups = groupsResult.groups

//   allGroups
//     .filter(group => group.special)
//     .forEach(group => {
//       specialGroups[group.oldId] = {
//         groupId: group._id,
//         studentListId: group.studentListId
//       }
//     })

//   // save header
//   dataFile.write('id,' + data.header + '\n')

//   await Promise.all(data.lines.map(line => saveStudent(line)))
//     .then(() => {})
//     .catch(err => {
//       console.error(err)
//     })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })