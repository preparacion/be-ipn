/**
 * seeds/loadDevCourse.js
 *
 * @description :: Creates example
 * @docs        :: TODO
 */
const _ = require('underscore')
const bcrypt = require('bcryptjs')

const GroupModel = require('../db/groups')
const PhaseModel = require('../db/phase')
const CourseModel = require('../db/courses')
const ScheduleModel = require('../db/schedules')
const ClassRoomModel = require('../db/classRooms')
const StudentListModel = require('../db/studentLists')

const Group = require('../models/groups')
const Student = require('../models/students')

const studentsData = [
  {
    'deleted': false,
    'hasTempPass': false,
    'registerDate': new Date(),
    'name': 'Aldo',
    'lastName': 'test',
    'email': 'aldo_islasg@hotmail.com',
    'secondLastName': 'test',
    'phoneNumber': '5542848859',
    'secondPhoneNumber': '5542848859',
    'password': '123456'
  },
  {
    'deleted': false,
    'hasTempPass': false,
    'registerDate': new Date(),
    'name': 'Moy',
    'lastName': 'test',
    'email': 'moises@bluedotsoft.xyz',
    'secondLastName': 'test',
    'phoneNumber': '5582610114',
    'secondPhoneNumber': '5582610114',
    'password': '123456'
  },
  {
    'deleted': false,
    'hasTempPass': false,
    'registerDate': new Date(),
    'name': 'Hiram',
    'lastName': 'test',
    'email': 'hiram.eps@gmail.com',
    'secondLastName': 'test',
    'phoneNumber': '5523296731',
    'secondPhoneNumber': '5523296731',
    'password': '123456'
  },
  {
    'deleted': false,
    'hasTempPass': false,
    'registerDate': new Date(),
    'name': 'Adan',
    'lastName': 'test',
    'email': 'Adan@plincos.com',
    'secondLastName': 'test',
    'phoneNumber': '5525131379',
    'secondPhoneNumber': '5525131379',
    'password': '123456'
  }
]
const courseData = {
  'creationDate': new Date(),
  'name': 'CursoPruebas_2',
  'duration': '30',
  'active': true,
  'price': 1000,
  'type': 3
}
const studentListData = {
  list: [],
  name: 'TEST_STLIST',
  material: []
}
const createSeeds = async () => {
  // delete previous data
  await deleteData()
  // create Course
  const course = new CourseModel(courseData)
  await course.save()
  const studentList = new StudentListModel(
    studentListData
  )
  await studentList.save()
  const rndData = await getRandomData()
  _.extend(rndData, {
    etapa: 1,
    name: 'TEST_GROUP',
    startDate: new Date(),
    active: true,
    quota: 45
  })
  const group = new GroupModel(rndData)
  await group.save()
  console.log('-----------')
  console.log(rndData)
  console.log('-----------')
}

const enroll = async (groupId, studentId, data, user) => {
  await Group.enroll(groupId, studentId, {}, null)
}

const getRandomData = async () => {
  const data = {}
  const schedule = await ScheduleModel
    .aggregate([
      { $sample: { size: 1 } }
    ])
  const classRoom = await ClassRoomModel
    .aggregate([
      { $sample: { size: 1 } }
    ])
  const phase = await PhaseModel
    .aggregate([
      { $sample: { size: 1 } }
    ])

  data.schedule = schedule[0]._id
  data.classRoom = classRoom[0]._id
  data.phase = phase[0]._id
  return data
}

const deleteData = async () => {
  const course = await CourseModel
    .findOne({ name: 'CursoPruebas_2' })
  if (course) {
    await course.remove()
  }
  const stList = await StudentListModel
    .findOne({ name: 'TEST_STLIST' })
  if (stList) {
    await stList.remove()
  }
  const group = await GroupModel
    .findOne({ name: 'TEST_STLIST' })
  if (group) {
    await group.remove()
  }
}

if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
  Promise.resolve(createSeeds())
    .then(() => {
      process.exit(0)
    })
    .catch(err => {
      console.log(err)
      process.exit(1)
    })
} else {
  console.log('Este script solo debe ejecutarse en dev...')
  console.log('Intentar con NODE_ENV=development node script.js')
  process.exit(1)
}
