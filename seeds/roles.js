/**
 * seeds/roles.js
 *
 * @description :: Creates example node seeds/roles.js
 * @docs        :: TODO
 */
const Roles = require('../models/roles')
const Permissions = require('../models/permissions')

const PermissionsModel = require('../db/permissions')
const RolesModel = require('../db/roles')

const superAdminPermissions = [
  'Profesores',
  'Horarios',
  'Administrativos',
  'Alumnos'
]

const teacherPermissions = [
  'Horarios'
]

const adminPermissions = [
  'Profesores',
  'Alumnos'
]

const createRolAdminstrativo = async function () {
  const permissions = await Promise.all(superAdminPermissions.map(permission => {
    return PermissionsModel.findOne({ name: permission })
  }))

  const superUserData = {
    name: 'SuperAdmin',
    permissions: permissions.map(permission => permission._id)
  }

  const role = new RolesModel(superUserData)
  await role.save()
}

const createRolProfesor = async function () {
  const permissions = await Promise.all(teacherPermissions.map(permission => {
    return PermissionsModel.findOne({ name: permission })
  }))

  const profesorData = {
    name: 'Profesor',
    permissions: permissions.map(permission => permissions._id)
  }

  const role = new RolesModel(profesorData)
  await role.save()
}

const createRolAdministrativo = async function () {
  const permissions = await Promise.all(adminPermissions.map(permission => {
    return PermissionsModel.findOne({ name: permission })
  }))

  const adminData = {
    name: 'Administrativo',
    permissions: permissions.map(permission => permission._id)
  }

  const role = new RolesModel(adminData)
  await role.save()
}

const createRolAlumno = async function () {
  const alumnoData = {
    name: 'Alumnos',
    permissions: []
  }
  const result = await Roles.create(alumnoData)
  return result
}

const createSeedsBasics = async function () {
  await createRolAdminstrativo()
  await createRolProfesor()
  await createRolAlumno()
  await createRolAdministrativo()
}

Promise.resolve(createSeedsBasics())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
