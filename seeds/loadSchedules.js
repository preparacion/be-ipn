/**
 * loadSchedules.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const mongoose = require('mongoose')

const readFile = util.promisify(fs.readFile)

const ScheduleModel = require('../db/schedules')

const Schedules = require('../models/schedules')

const file = './old_data/horarios.csv'
const dataFile = fs.createWriteStream('./data/horarios.csv', { autoClose: true })

const getSchedulesLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const schedule = line.split(',')

  let scheduleData = {
    oldId: schedule[0],
    name: schedule[1],
    day: schedule[2],
    startHour: schedule[3].replace(':', ''),
    endHour: schedule[4].replace(':', '')
  }

  for (const i in scheduleData) {
    if (scheduleData[i] === '') {
      delete scheduleData[i]
    }
  }

  return scheduleData
}

const saveSchedule = async function (line) {
  let scheduleData = lineToJson(line)
  const id = new mongoose.Types.ObjectId()

  scheduleData = _.extend(scheduleData, { _id: id })

  const schedule = new ScheduleModel(scheduleData)
  await schedule.save()

  dataFile.write(id + ',' +
   line  +
    '\n'
  )
}

const init = async function () {
  const data = await getSchedulesLines(file)
  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveSchedule(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })