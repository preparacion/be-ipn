/**
 * loadSt.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const Promise = require('bluebird')
const mongoose = require('mongoose')

const UserModel = require('../db/users')
const GroupModel = require('../db/groups')
const StudentModel = require('../db/students')
const PaymentModel = require('../db/payments')
const StudentListModel = require('../db/studentLists')

const readFile = util.promisify(fs.readFile)

const fileName = 'data-students.csv'
// const fileName = 'data-students-example.csv'

const file = `./old_data/${fileName}`
const dataFile = fs.createWriteStream(`./data/${fileName}`, { autoClose: true })

let userOne = null

const convertDate = (dateToConvert) => {
  const dateArray = dateToConvert.split(' ')

  if (dateArray.length !== 2) {
    return new Date()
  }
  const dateString = dateArray[0]
  const hourString = dateArray[1]

  const dt = dateString.split('/')

  const date = new Date(`${dt[1]}/${dt[0]}/${dt[2]} ${hourString}`)

  if (date.getTime() !== date.getTime()) {
    return new Date()
  }

  return date
}

const lineToJson = function (line) {
  const student = line.split(',')
  // getting payment type
  student[11] = student[11].toLowerCase()
  const paymentType = (student[11] && student[11] === 'efectivo') ? 'cash' : 'card'
  // converting date
  let date = convertDate(student[13])

  if (date.getTime() !== date.getTime()) {
    const dateConverted = convertDate(student[13])
    date = convertDate(date)
  }

  const studentData = {
    student: {
      _id: new mongoose.Types.ObjectId(),
      lastName: student[0],
      secondLastName: student[1],
      name: student[2],
      email: student[3],
      phoneNumber: student[4],
      secondPhoneNumber: student[5],
      userOldId: student[12] || 1,
      date: date,
      groups: []
    },
    payment: {
      _id: new mongoose.Types.ObjectId(),
      discount: student[9],
      amount: student[10] || 0,
      paymentType: paymentType,
      date
    },
    group: {
      courseOldId: student[7],
      groupOldId: student[8]
    }
  }

  for (const i in studentData) {
    if (studentData[i] === '' ||
      studentData[i] === 0 ||
      studentData[i] === '0' ||
      studentData[i] === '-') {
      delete studentData[i]
    }
  }

  return studentData
}

const listLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

// 1380 sin datos.
const saveStudent = async function (line) {
  const data = lineToJson(line)

  const group = await GroupModel.findOne({
    oldId: data.group.groupOldId
  }).then(group => group)
  const groupId = group._id

  const studentId = data.student._id
  data.student.groups.push(groupId)

  const studentList = await StudentListModel.findOne({
    _id: group.studentList
  }).then(studentList => studentList)

  let user = await UserModel.findOne({
    oldId: data.student.userOldId
  }).then(user => user)

  if (data.payment.amount === 0) {
    console.log(data.student.email)
    if (!data.student.email || data.student.email === '') {
      console.log(data.student)
    }
  }

  if (!user) {
    user = userOne
    console.log(data.student)
    console.log(userOne)
  }

  const userId = user._id

  data.student['registeredBy'] = userId
  data.student['registerDate'] = data.student.date

  if (data.student['name']) {
    data.student['name_lower'] = data.student['name'].toLowerCase()
  }

  if (data.student['lastName']) {
    data.student['lastName_lower'] = data.student['lastName'].toLowerCase()
  }

  if (data.student['email']) {
    data.student['email_lower'] = data.student['email'].toLowerCase()
  }

  if (data['name'] && data['lastName']) {
    data['fullName_lower'] = `${data['name']} ${data['lastName']}`
  }

  const student = new StudentModel(data.student)
  await student.save()

  studentList.list.push(studentId)
  await studentList.save()

  data.payment['student'] = data.student._id
  data.payment['user'] = userId

  const payment = new PaymentModel(data.payment)
  await payment.save()
}

const init = async function () {
  const data = await listLines(file)

  // save header
  dataFile.write('id,' + data.header + '\n')

  userOne = await UserModel.findOne({ oldId: 1 }).then(user => user)

  const lines = data.lines.map(line => saveStudent(line))
  await Promise.mapSeries(lines, line => { return line })
    .then(() => {})
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
