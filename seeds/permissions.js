/**
 * seeds/permissions.js
 *
 * @description :: Creates example node seeds/permissions.js
 * @docs        :: TODO
 */
const Permissions = require('../models/permissions')
const PermissionModel = require('../db/permissions')

const createPermissionAdminstrativo = async function () {
  const adminsData = {
    name: 'Administrativos',
    description: 'Puede agregar Administrativos',
    typePermission: 1
  }

  const permission = new PermissionModel(adminsData)
  await permission.save()
}

const createPermissionTeachers = async function () {
  const teachersData = {
    name: 'Profesores',
    description: 'Puede agregar Profesores',
    typePermission: 2
  }
  const permission = new PermissionModel(teachersData)
  await permission.save()
}

const createPermissionAlumnos = async function () {
  const alumnoData = {
    name: 'Alumnos',
    description: 'Puede agregar Alumnos',
    typePermission: 3
  }
  const permission = new PermissionModel(alumnoData)
  await permission.save()
}

const createSchedule = async function () {
  const cheduleData = {
    name: 'Horarios',
    description: 'Puede agregar Horarios',
    typePermission: 4
  }
  const permission = new PermissionModel(cheduleData)
  await permission.save()
}

const createSeedsBasics = async function () {
  await createPermissionAdminstrativo()
  await createPermissionTeachers()
  await createPermissionAlumnos()
  await createSchedule()
}

Promise.resolve(createSeedsBasics())
  .then(() => {
    process.exit(0)
  })
  .catch(err => {
    console.log(err)
    process.exit(1)
  })
