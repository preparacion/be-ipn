/**
 * loadCourses.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const mongoose = require('mongoose')

const CourseModel = require('../db/courses')

const readFile = util.promisify(fs.readFile)

const file = './old_data/cursos.csv'
const dataFile = fs.createWriteStream('./data/cursos.csv', { autoClose: true })

const getCoursesLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const course = line.split(',')
  let date = course[3].split('/')
  date = new Date(date[2] + '-' + date[1] + '-' + date[0]).toISOString()
  let courseData = {
    oldId: course[0],
    name: course[1],
    duration: course[2],
    creationDate: date,
    active: course[4],
    price: course[5],
    type: course[6],
    tipo: course[7],
    dependency: course[8]
  }

  for (const i in courseData) {
    if (courseData[i] === '') {
      delete courseData[i]
    }
  }

  return courseData
}

const saveCourse = async function (line) {
  let courseData = lineToJson(line)
  const id = new mongoose.Types.ObjectId()

  courseData = _.extend(courseData, { _id: id })

  const course = new CourseModel(courseData)
  await course.save()

  dataFile.write(id + ',' +
   line +
    '\n'
  )
}

const init = async function () {
  const data = await getCoursesLines(file)
  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveCourse(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })