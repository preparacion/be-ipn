const _ = require('lodash')
const StudentModel = require('../../db/students')
const GroupModel = require('../../db/groups')
const StudentListModel = require('../../db/studentLists')

function shuffleArray(length) {
    let i = 0
    const arr = []
    for (i = 1; i <= length; i++) {
        arr.push(i)
    }
    return _.shuffle(arr)
}

const enrollBucketGroup = async (courseId, studentId, phase, buckets) => {
    let OnCourse = []
    let OnCourse2
    for (let i = 0; i < buckets.length; i++) {
        let group = await GroupModel
            .findOne({
                _id: buckets[i]
            })
        if (JSON.stringify(group.course) === JSON.stringify(courseId)) {
            OnCourse.push(group)
        }
    }
    console.log(OnCourse)
    for (let i = 0; i < OnCourse.length; i++) {
        if (OnCourse[i].comipemsPhase == phase) {
            OnCourse2 = OnCourse[i]
        }
    }
    console.log(OnCourse2)
    const bucketGroup = await GroupModel
        .findOne({
            _id: OnCourse2._id
        })
    const studentList = await StudentListModel
        .findOne({ _id: bucketGroup.studentList })

    studentList.list.push(studentId)
    await studentList.save()
    console.log(studentList)
    return {
        _id: bucketGroup._id,
        name: bucketGroup.name,
        phase
    }
}
const registerToBucket = async (student, course, group) => {
    let studentId, paymentId, creditId, paymentInfoId
    if (!course.children || !course.children.length) {
        return {
            code: 400,
            error: 'No children courses',
            success: false
        }
    }
    const phaseOrder = shuffleArray(course.children.length)
    const buckets = await Promise
        .all(course.children.map((courseId, index) => {
            return enrollBucketGroup(courseId, student._id, phaseOrder[index], group.buckets)
        }))
    student.comipemsGroups = buckets
    if (student.groups) {
        student.groups.push(group._id)
    } else {
        student.groups = [group._id]
    }
    await student.save()
    return {
        success: true,
        studentId,
        paymentId,
        creditId,
        paymentInfoId,
        buckets
    }
}

const init = async function () {
    let idGroup = "5dc65c42ddb6e2178da834bb"
    let group = await GroupModel
        .findOne({
            _id: idGroup
        })
        .populate({
            path: 'studentList',
        })
        .populate({
            path: 'course',
        })
    let buckets = group.buckets
    //Empty all studentList of buckets
    for (let i = 0; i <= buckets.length; i++) {
        let group = await GroupModel
            .findOne({
                _id: buckets[i]
            })
        let studentList = await StudentListModel
            .findOne({
                _id: group.studentList
            })
        studentList.list = []
        await studentList.save()
    }
    // End of empty
    // let course = group.course
    // for (let i = 0; i < group.studentList.list.length; i++) {
    //     let student = await StudentModel
    //         .findOne({
    //             _id: group.studentList.list[i]
    //         })
    //     let index = student.groups.indexOf(idGroup)
    //     console.log("index", index)
    //     if (index > -1) {
    //         student.groups.splice(index, 1)
    //     }
    //     await registerToBucket(student, course, group)
    // }
}



Promise.resolve(init())
    .then(result => {
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })