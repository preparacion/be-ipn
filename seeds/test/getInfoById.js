const StudentModel = require('../../db/students')
const fs = require('fs').promises;
const init = async function () {
    let ids = ["5cef4d468158503e0094172f", "5d3e2cb92dd49004f0feee13", "5d3e2e9d2dd49004f0feef4f", "5d3e2ff02dd49004f0fef09b", "5d3e300b2dd49004f0fef0bf", "5d3e33cd2dd49004f0fef37f", "5d3e37a62dd49004f0fef5a3", "5d3e40192dd49004f0fef9ee", "5d3f61be2dd49004f0ff05a8", "5d4060dd2dd49004f0ff103d", "5d40674e2dd49004f0ff119a", "5d4089d52dd49004f0ff1ee2", "5d4225f62dd49004f0ff35b0", "5d45bcdb2dd49004f0ff5d20", "5d45d3a32dd49004f0ff66d7", "5d4726b92dd49004f0ff95a6", "5d4726e72dd49004f0ff95b4", "5d477bf52dd49004f0ffaa9a", "5d4b26172dd49004f0ffc267", "5d4b29a12dd49004f0ffc402", "5d4b2ca92dd49004f0ffc5a7", "5d4b2dbe2dd49004f0ffc651", "5d4b2e232dd49004f0ffc6a3", "5d4b45822dd49004f0ffc874", "5d4b63512dd49004f0ffc99b", "5d4f0b8d4d893e0395f859ae", "5d4f56da4d893e0395f866e3", "5d505e874d893e0395f873f6", "5d505f634d893e0395f87463", "5d5061f94d893e0395f875b6", "5d506c174d893e0395f87936", "5d5070734d893e0395f87ad2", "5d5346d54d893e0395f89e28", "5d534b314d893e0395f8a0fe", "5d5442ba4d893e0395f8af61", "5d5449794d893e0395f8b24b", "5d544c704d893e0395f8b356", "5d544dc74d893e0395f8b424", "5d5450f54d893e0395f8b594", "5d54a9024d893e0395f8bb7b", "5d54a9d44d893e0395f8bbd8", "5d54ad7a4d893e0395f8bd5a", "5d54b30e4d893e0395f8bee8", "5d58321dec873a1b5bbd0977", "5d58453cec873a1b5bbd1010", "5d584882ec873a1b5bbd1153", "5d584983ec873a1b5bbd125c", "5d584c33ec873a1b5bbd1405", "5d584ea6ec873a1b5bbd15b3", "5d584ee3ec873a1b5bbd160c", "5d584fb5ec873a1b5bbd16a5", "5d598907dc6b861ab276c46e", "5d59890fdc6b861ab276c47a", "5d598913dc6b861ab276c485", "5d5989abdc6b861ab276c4b5", "5d599664dc6b861ab276c9a5", "5d599eeddc6b861ab276cd6a", "5d59a0afdc6b861ab276ceac", "5d5d7a37fb36ac2af301c692", "5d5d80ecfb36ac2af301c9f6", "5d5d9d3efb36ac2af301d16e", "5d5dbbccfb36ac2af301d649", "5d5de6c7fb36ac2af301e181", "5d5ef33479d29773b5467075", "5d615da279d29773b546ce57", "5d6189c079d29773b546f10f", "5d62928f79d29773b54721d7", "5d671d86f3967925428f3e84", "5d6adbe2f3967925428f7f1b", "5d6adcd9f3967925428f7fd8", "5d6be87ff3967925428f9294", "5d6bf44ef3967925428f983d", "5d6bfc03f3967925428f9d94", "5d6c2972f3967925428fa52a", "5d6c2c49f3967925428fa6a9", "5d6c315cf3967925428fa985", "5d6c320df3967925428faa43", "5d6c4939f3967925428fad97", "5d70627c0785d00f304cc733", "5d7067560785d00f304cc86e", "5d73dcba0785d00f304cf4c9", "5d73f0a20785d00f304cfc52", "5d741aee0785d00f304d06d5", "5d7529010785d00f304d17f0", "5d752eb90785d00f304d1abe", "5d765c0c0785d00f304d4811", "5d7e983ac7b9c1439dde4b58", "5d826681d4a8c20c20e4e6ee", "5d826ef1d4a8c20c20e4eb84", "5d8270d4d4a8c20c20e4ebef", "5d82ccecd4a8c20c20e500d3", "5d852577d4a8c20c20e55f42", "5d864e70d4a8c20c20e5d447", "5d869720d4a8c20c20e63587", "5d8698aad4a8c20c20e63743", "5d879fc0d4a8c20c20e65781", "5d87a149d4a8c20c20e65836", "5d87a1bed4a8c20c20e65884", "5d87dc65d4a8c20c20e67304", "5d88155dd4a8c20c20e6845c", "5d88cc2dd4a8c20c20e6a095", "5d8a118fd4a8c20c20e6cece", "5d8bb4c0d4a8c20c20e6e368", "5d8bfe0cd4a8c20c20e6e738", "5d8f88a7d4a8c20c20e73a5c", "5d8f88d9d4a8c20c20e73a70", "5d8f8d95d4a8c20c20e7408b", "5d8f904fd4a8c20c20e7444b", "5d8f9ba0d4a8c20c20e74ea8", "5d8fa7d0d4a8c20c20e75260", "5d8fc99ed4a8c20c20e75e77", "5d91181bd4a8c20c20e78a80", "5d9119a3d4a8c20c20e78b8a", "5d912cded4a8c20c20e7931f", "5d9131b2d4a8c20c20e793bf", "5d915ac1d4a8c20c20e7982e", "5d94e877d4a8c20c20e7c292", "5d953bb6d4a8c20c20e7c9f6", "5d953c52d4a8c20c20e7ca0f", "5d95412fd4a8c20c20e7ccef", "5d95424fd4a8c20c20e7cd8b", "5d98fcb4d4a8c20c20e81aae", "5d9904bfd4a8c20c20e820a4", "5d9905dbd4a8c20c20e821dc", "5d990d4dd4a8c20c20e82663", "5d990dbfd4a8c20c20e82672", "5d9a0782d4a8c20c20e82c79", "5d9a144ad4a8c20c20e82e9c", "5d9a1c12d4a8c20c20e83408", "5d9a1c19d4a8c20c20e83413", "5d9de0e1d4a8c20c20e888ea", "5d9e186dd4a8c20c20e89548", "5d9e2ce5d4a8c20c20e89c2e", "5d9e4942d4a8c20c20e89fab", "5d9e8279d4a8c20c20e8ac16", "5da079b3d4a8c20c20e8d1a6", "5da39496d4a8c20c20e9f9a5", "5da3954fd4a8c20c20e9fa77", "5da39933d4a8c20c20e9fe3b", "5da3b941d4a8c20c20ea05c9", "5da4e74fd4a8c20c20ea234e", "5da75a19f5288f11b61a4ea8", "5dac94e9f5288f11b61aeeca", "5db08abbf5288f11b61b4e29", "5db09606f5288f11b61b5407", "5db0960cf5288f11b61b5416", "5db0f52cf5288f11b61b6035", "5db0f52ef5288f11b61b6041", "5db5dea0f5288f11b61c02fc", "5db638b2f5288f11b61c28b4", "5dc311386116be09190c08e6", "5dc8952bb2691841ef9bd4a7", "5dc9eebfb2691841ef9c1153", "5dcacc2f3507623e5a47e692", "5dcad4083507623e5a47ecf2", "5dcae7653507623e5a47fada", "5dcae87a3507623e5a47fbae", "5dcaf3ad3507623e5a48045d", "5dcd6db45fc8e353db282065", "5dcd74165fc8e353db282527", "5dcd7b4e5fc8e353db282843", "5dcd928b5fc8e353db283384", "5dcd9ed75fc8e353db283ba9", "5dcda1e15fc8e353db283f79", "5dcda71e3002120d7aa88dea", "5dcda8733002120d7aa88f11"]
    let results = []
    for (let i = 0; i < ids.length; i++) {
        let student = await StudentModel
            .findOne({
                _id: ids[i],
                deleted: { $ne: true }
            })
            .then(student => {
                if (!student) {
                    const errorExt = errMsg
                        .getNotEntityFound('Student', email)
                    return {
                        success: false,
                        code: 404,
                        error: errorExt.error,
                        errorExt
                    }
                }
                return {
                    student: student
                }
            })
            .catch(err => {
                console.error('- Error trying to get a student by id', JSON.stringify(err, null, 2))
                return {
                    success: false,
                    code: 500,
                    error: JSON.stringify(err, null, 2)
                }
            })
        results.push(student)
    }
    await fs.writeFile("output.json", JSON.stringify(results), 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }
        console.log("JSON file has been saved.");
    });
}



Promise.resolve(init())
    .then(result => {
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })