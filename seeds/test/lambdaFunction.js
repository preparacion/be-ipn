const init = async function () {
    let trS="Guessed Channel Layout for Input Stream #0.1 : stereo\nInput #0, mov,mp4,m4a,3gp,3g2,mj2, from 'https://ipn-videos.s3-accelerate.amazonaws.com/2yqcb951pfuk4s3jxds.MOV':\n  Metadata:\n    major_brand     : qt  \n    minor_version   : 537331968\n    compatible_brands: qt  niko\n    creation_time   : 2019-01-22T15:44:08.000000Z\n  Duration: 00:00:35.54, start: 0.000000, bitrate: 19418 kb/s\n"
    if (trS.indexOf("Input #")>=0 || trS.indexOf("Input Stream #")>=0) {
        let inicio = trS.indexOf("Duration: ")+ 10
        let fin = trS.indexOf(", start")
        let duration = trS.substring(inicio,fin)
        console.log("ok",duration)
        return duration	
    }else{
        return undefined
    }
}


Promise.resolve(init())
    .then(result => {
        // console.log(result)
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })