const _ = require('lodash')
const TalksModel = require('../../db/talks')
const UsersTalksModel = require('../../db/talkStList')
const init = async function () {
  let talks = await TalksModel
    .find({})
    .catch(err => {
      console.error('- Error trying to get a talk', JSON.stringify(err, null, 2))
      return {
        success: false,
        code: 500,
        error: JSON.stringify(err, null, 2)
      }
    })

  let i = 0
  for (i; i < talks.length; i++) {
    let list = await UsersTalksModel.findOne({
      _id: talks[i].talkStudentList
    })
    console.log(list.list.length)
    let set = new Set(list.list)
    let listClean = [...set]
    console.log("clean",listClean.length)
  }
  console.log(talks.length)
}



Promise.resolve(init())
  .then(result => {
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })