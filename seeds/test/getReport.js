
const moment = require('moment')
const StudentModel = require('../../db/students')
const GroupsModel = require('../../db/groups')
const fs = require('fs').promises;


const init = async () => {

    const groupsSuperior = await GroupsModel
        .find({
            course: "5c02310a5d6f712db49d3103"
        })
    const groupsComipems = await GroupsModel
        .find({
            course: "5dc65abeddb6e2178da83448"
        })
    const result = groupsSuperior.concat(groupsComipems)

    const groupsMate = await GroupsModel
        .find({
            course: "5d6be3c2f3967925428f91c4"
        })
    console.log("Grupos de superior", groupsSuperior.length)
    console.log("Grupos de comipems", groupsComipems.length)
    console.log("Grupos de mate", groupsMate.length)
    const students = await StudentModel
        .find({
            registerDate: { $gte: new moment("2019-02-01", "YYYY-MM-DD"), $lte: new moment("2020-01-17", "YYYY-MM-DD") },
            groups: { $in: groupsMate, $nin: result }
        })

    console.log("StudentLenght:", students.length)

    await fs.writeFile("output.json", JSON.stringify(students), 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }
        console.log("JSON file has been saved.");
    });
}



Promise.resolve(init())
    .then(result => {
        process.exit(0)
    })
    .catch(err => {
        console.error(err)
        process.exit(1)
    })