/**
 * loadCourses.js
 */
const fs = require('fs')
const util = require('util')
const _ = require('underscore')
const mongoose = require('mongoose')

const GroupModel = require('../db/groups')
const CourseModel = require('../db/courses')
const ScheduleModel = require('../db/schedules')
const ClassRoomModel = require('../db/classRooms')
const StudentListModel = require('../db/studentLists')

const readFile = util.promisify(fs.readFile)

const file = './old_data/grupos.csv'
const dataFile = fs.createWriteStream('./data/grupos.csv', { autoClose: true })

const getGroupsLines = async function (path) {
  const dataFile = await readFile(path, 'utf-8')
  let lines = dataFile.split(/\n/)
  let header = lines.shift().replace(/\r/, '').split(',')
  header[0] = 'old_id'

  return {
    lines: lines.map(line => line.replace(/\r/, '')),
    header
  }
}

const lineToJson = function (line) {
  const course = line.split(',')
  const dateArray = course[2].split('-')
  const day = dateArray[0]
  const year = `20${dateArray[2]}`
  const month = (dateArray[1] === 'ene') ? '0'
    : (dateArray[1] === 'oct') ? '9'
      : (dateArray[1] === 'nov') ? '10' : ''

  let groupData = {
    oldId: course[0],
    name: course[1],
    startDate: new Date(year, month, day),
    active: course[3],
    quota: course[4],
    oldCourseId: course[5],
    oldScheduleId: course[8],
    oldClassRoomId: course[10]
  }

  for (const i in groupData) {
    if (groupData[i] === '') {
      delete groupData[i]
    }
  }

  return groupData
}

const saveGroup = async function (line) {
  let groupData = lineToJson(line)

  const course = await CourseModel.findOne({ oldId: groupData.oldCourseId })
  groupData = _.extend(groupData, { course: course._id })

  const schedule = await ScheduleModel.findOne({ oldId: groupData.oldScheduleId })
  groupData = _.extend(groupData, { schedule: schedule._id })

  const classRoom = await ClassRoomModel.findOne({ oldId: groupData.oldClassRoomId })
  groupData = _.extend(groupData, { classRoom: classRoom._id })

  let studentListData = {
    name: groupData.name,
    list: []
  }
  const studentListId = new mongoose.Types.ObjectId()
  studentListData = _.extend(studentListData, { _id: studentListId })
  const studentList = new StudentListModel(studentListData)
  await studentList.save()

  const id = new mongoose.Types.ObjectId()
  groupData = _.extend(groupData, { studentList: studentListId, _id: id })

  const group = new GroupModel(groupData)
  await group.save()

  dataFile.write(id + ',' +
   line +
    '\n'
  )
}

const init = async function () {
  const data = await getGroupsLines(file)
  // save header
  dataFile.write('id,' + data.header + '\n')

  await Promise.all(data.lines.map(line => saveGroup(line)))
    .then(() => {
      // Promise.resolve(Locations.get())
      //   .then(result => {
      //     console.log(result)
      //   })
    })
    .catch(err => {
      console.error(err)
    })
}

Promise.resolve(init())
  .then(result => {
    // console.log(result)
    process.exit(0)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })