/**
 * server.js
 *
 * @description :: Loads server configuration
 */
const Koa = require('koa')
const cors = require('koa-cors')
const bodyParser = require('koa-body')
const SocketModel = require('./db/socketConnections')
const MAX_BODY = (25000 * 1024 * 1024)
const routes = require('./routes')
const app = new Koa()
const server = require('http').createServer(app.callback())
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});
const redisAdapter = require('socket.io-redis');
// Register log of activities.
const logs = require('./middleware/createLogs')
app.use(cors())

app.use(bodyParser({
  multipart: true,
  json: true,
  formidable: {
    maxFieldsSize: (100 * 1024 * 1024),
    maxFileSize: MAX_BODY
  }
}))


//Sockets Config and connections
io.adapter(
  redisAdapter({
    host: "socket-io.7vx8af.ng.0001.use1.cache.amazonaws.com",
    port: "6379",
  })
);
io.of('/').adapter.on('error', function (error) { console.log("Socket error Redis", error.code) });
io.on('connection', (socket) => {
  socket.ip = socket.handshake.headers['x-forwarded-for'] || "";
  console.log(`  >>>> Conection ${socket.id} ${socket.ip}`)
  SocketModel.create({
    idSocket: socket.id,
    ip: socket.ip
  })
  socket.on('join', function (room) {
    socket.join(room);
    console.log(room)
  });

  //On disconnect socket
  socket.on('disconnect', async () => {
    console.log(`  >>>> disconnect ${socket.id}`);
    await SocketModel.deleteOne({
      idSocket: socket.id,
    });
  });
})

//this will attach the socket_io to context to use in the modules
app.context.socket_io = io

// Attach socket Routes to app server
app.use(routes.routes(), routes.allowedMethods())

app.use(logs)


module.exports = server
